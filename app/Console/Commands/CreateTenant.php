<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Illuminate\Support\Facades\Artisan;
use Hyn\Tenancy\Environment;
use App\Models\System\Customer;
use App\Models\System\CustomersHostname;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use App\Models\Customer\User;
use App\Models\Customer\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Exception;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateTenant extends Command
{
    
    protected $signature = 'tenant:create {nit} {name_company} {hostname} {prefijo} {email} {next_billing_at} {ends_at} {licence} {free_licence} ';

    protected $description = 'Creates a tenant with the provided username and email address e.g. php artisan tenant:create example example@example.com';

    public function handle()
    {
        

        $command = new ConsoleOutput();
        
        
        $nit = $this->argument('nit');
        $name_company = $this->argument('name_company');
        $hostname = $this->argument('hostname');
        $prefix = $this->argument('prefijo');
        $email = $this->argument('email');
        $next_billing_at = $this->argument('next_billing_at');
        $ends_at = $this->argument('ends_at');
        
        $licence = $this->argument('licence');
        $free_licence = $this->argument('free_licence');

        // configs
        $baseurl = config('app.url_base');
        $fqdn = "{$hostname}.{$baseurl}";
        $uuid = (string) Str::uuid(16);
        $uuid = str_replace('-', null, $uuid);

        // asignar las variables globales
        // config(['tenancy.username' => $username]);
        config(['tenancy.fqdn' => $fqdn]);
        config(['tenancy.uuid' => $uuid]);
        
        config(['tenancy.name_company' => $name_company]);
        config(['tenancy.nit' => $nit]);
        
        config(['tenancy.email' => $email]);
        
        config(['tenancy.prefijo' => $prefix]); //para pasar el prefijo

        if ($this->tenantExists($hostname)) {
            $this->error("A tenant with subdomain '{$hostname}' already exists.");
            return;
        }
        
        // variable para realizar la transaccion de la base de datos y evitar que si no se cumple un registro todo se cancele
        $error = null;
        
        
        try {
             
            $tenant = $this->registerTenant($nit, $name_company, $hostname, $prefix, $email , $next_billing_at , $ends_at , $licence, $free_licence, $fqdn, $uuid);
            
            if(!is_object($tenant)){
                throw new Exception($tenant);
            }

            app(Environment::class)->tenant($tenant->website);         

            // we'll create a random secure password for our to-be admin        
            $this->addAdmin($nit, $name_company);            

            
            $success = true;
        } catch (\Exception $e) {
            
            $this->deleteDB($hostname, $uuid);

            $success = false;
            $error = $e->getMessage().' - Line: '.$e->getLine().' - File: '.$e->getFile();
//            $this->error($error);

        }

        // si hubo un error en la base de datos retornar un mensaje avisando
        if ($success) {
            //$this->info("Tenant '{$username}' is created and is now accessible at {$hostname->fqdn}");
            
//            $this->info("Cuenta '{$hostname}' con base de datos '{$uuid}'");
            $command->writeln("Cuenta '".$hostname."' con base de datos '".$uuid."'");
            return true;
        }else{
            // dd($error);
            $command->writeln("Error: ".$error);
            return $error;
        }

    }

    // verificar si existe el cliente
    private function tenantExists($hostname)
    {
        return Customer::where('subdomain', $hostname)->exists();
    }


    // crear el cliente
    private function registerTenant($nit, $name_company, $hostname, $prefix, $email , $next_billing_at , $ends_at , $licence, $free_licence, $fqdn, $uuid)
    {
        try{
            DB::beginTransaction();
                // create a customer
                $customer = new Customer;
                $customer->nit = $nit;
                $customer->name_company = $name_company;
                $customer->subdomain = $hostname;
                $customer->prefix = $prefix;


                $customer->email = $email;

                $customer->next_billing_at = $next_billing_at;
                $customer->ends_at = $ends_at;

                $customer->licencias = $licence;
                $customer->licencias_gratis = $free_licence;


                // $customer->email = $email;
                $customer->save();        
                
                // create database
                $website = new Website;
                

                $website->uuid = $uuid;

                
                app(WebsiteRepository::class)->create($website);
                
                
                
                // create hostname
                $hostname = new Hostname;
                $hostname->fqdn = $fqdn;
                app(HostnameRepository::class)->attach($hostname, $website);
                
                // asignar el host al cliente
                CustomersHostname::create([
                    'hostname_id' => $hostname->id,
                    'customer_id' => $customer->id
                ]);

                return $hostname;
                DB::commit();
        }catch(Exception $e){
            // DB::rollback();
            return $e->getMessage();
        }
        

    }
    
    private function addAdmin($nit, $name_company)
    {

        // crear usuario de soporte
        $support = User::create([
            'cedula' => '901086265',
            'nombre' => 'Soporte',
            'cargo' => 'Admin software',
            'correo' => env('SUPPORT_ACCOUNT_EMAIL'),
            'susuario' => env('SUPPORT_ACCOUNT_USERNAME'),
            'slogin' => env('SUPPORT_ACCOUNT_PASSWORD'),            
            'perfil' => 'S',           
            'rol' => '1',           
            'coordinador' => 'S',           
            'comercial' => 'S',
            'adtivo' => 'N',
            'pricing' => 'N',
            'whatsapp' => 'N',
            'password' => Hash::make(env('SUPPORT_ACCOUNT_PASSWORD'))
        ]);

        // asignar role de admin
        // $support->attachRole('admin');

//         // crear usuario admin
//         $admin = User::create([
//             'cedula' => $nit,
//             'nombre' => $first_name,
//             'cargo' => $position,
//             'correo' => $email,
//             'susuario' => $username,
//             'slogin' => $password,             
//             'perfil' => 'S',        
//             'rol' => '1',           
//             'coordinador' => 'S',           
//             'comercial' => 'S',
//             'password' => Hash::make($password)
// //            'password' => $password
//         ]);

        // asignar role
        // $admin->attachRole('admin');

        $output = Artisan::output();

        return $support;
    }

     // eliminar base de datos
     private function deleteDB($hostname, $uuid)
     {
       
         $host = config('database.connections.system.host');
         $customer = Customer::where('subdomain', $hostname)->first();
         
         // verificar si existe el cliente
         if($customer){
             
             $customer_host = CustomersHostname::where('customer_id', $customer->id)->first();
 
             if($customer_host){
                 $hostname_data = Hostname::whereId($customer_host->hostname_id)->first();
                 $customer_host->delete();
                 $hostname_data->forceDelete();
             }
 
             // 
             $customer->forceDelete();
 
         }
 
         DB::connection('system')->table('websites')->where('uuid', $uuid)->delete();
        DB::connection('system')->getPdo()->exec("DROP USER IF EXISTS `{$uuid}`@`{$host}`");
        DB::connection('system')->getPdo()->exec("DROP DATABASE IF EXISTS `{$uuid}`");
     }
    
}