<?php

namespace App\Console\Commands;

use App\Traits\TenantQueryTrait;
use App\Mail\DailyReminder;
use App\Mail\NotifyException;
use App\Models\System\Customer;
use App\Models\Customer\TmDo;
use App\Models\Customer\User;
use App\Models\Customer\SgcCampana;
use App\Models\Customer\SgcTarifario;
use App\Models\Customer\SgcTarifarioProv;
use App\Models\Customer\SgcCotizaTarifario;
use App\Models\Customer\SgcCotizaciones;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use \Exception;
use Vonage\Voice\NCCO\Action\Notify;

class CronConectaCarga extends Command
{

    use TenantQueryTrait; // Importa el trait

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:cronconecta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update shipments activities alerts';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $tenantName;

    protected $operativeUsers = [];

    protected $commercialUsers = [];

    protected $pricingUsers = [];

    protected $administrativeUsers = [];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

        $tenant = app(\Hyn\Tenancy\Environment::class)->tenant();
        $tenantName = $tenant->hostnames[0]->fqdn ?? "Sin nombre";
        $this->tenantName = $tenantName;

        $result = $this->getActiveTenants([$tenant->id] ?: null);
        if(!$result){
            return Command::info("El tenant no esta activo");
        }
        
        [$this->operativeUsers, $this->commercialUsers, $this->pricingUsers, $this->administrativeUsers] = $this->getUsers();
        // $this->getUsers();
        
        // dd($this->pricingUsers);
            
            
            $this->info('--Tenant: '. $this->tenantName);
            
            $this->cotizaciones();

            $this->shipments();
            
            $this->campanas();
            
            $this->tarifarios();

             $this->expiredDocuments();
                
            
            return Command::info("--Tenancy ($this->tenantName) updated");
        } catch (\Throwable $th) {
            Mail::to('spedroza@conectacarga.co')->send(new NotifyException( $this->tenantName . " - " . $th->getMessage()));
            return Command::info("Error: " . $th->getMessage());
        }


        // $command = new ConsoleOutput();
        // $customers = Customer::with('hostnames')->get();

        // foreach($customers as $customer){
        //     $command->writeln($customer->name_company);
        //     $command->writeln($customer->hostname->fqdn);
        //     $this->info('The command was successful! '. $customer->name_company);
        // }


        // $this->info('The command was successful!');
        // echo "hola";
        return 0;
    }

    public function shipments(){
        
        // foreach($this->operativeUsers as $user){
        //     $shipments = TmDo::where('ESTADO', 'ABIERTA')->where('coordinador', $user->cedula )->get();

        //     Mail::to($user->correo)->send(new DailyReminder($user,$shipments,"Embarques", $this->tenantName ));
        //     $this->info('Embarques mail to: '. $user->correo );
        // }

        $fecha_limite = Carbon::now()->subDays(180)->toDateString();

            // $shipments = TmDo::all();
            $shipments = TmDo::where('ESTADO', 'ABIERTA')
                                ->whereRaw("STR_TO_DATE(fecha_ingreso, '%d/%m/%Y') >= ?", [$fecha_limite])->get();

            
            // Mail::to($user->correo)->send(new DailyReminder($user));
            // $this->info('mail to: '. $user->correo );


            foreach ($shipments as $shipment) {

                $result[$shipment->DO] = $this->DMLO_IMPOCALC($shipment, "AUTOM", "www.conectacargo.co");
               
            }

            

            Command::info("Alertas tenancy ($this->tenantName) finalizadas");
    }

    public function cotizaciones(){

        // foreach($this->commercialUsers as $user){
        //     $quotes = SgcCotizaciones::where('ESTADO', 'ENPROCESO')->where('ASESOR_ID', $user->cedula)->get();
        //     Mail::to($user->correo)->send(new DailyReminder($user,$quotes,"Cotizaciones", $this->tenantName ));
        //     $this->info('Cotizaciones mail to: '. $user->correo );
        // }


        // Command::info("Alertas tenancy ($this->tenantName) finalizadas");
}

    public function DMLO_IMPOCALC($shipment,$Usuario_Act, $Maquina_Act) 
    {
        try {
            
            $arreglo = [];
        
            config(['database.default' => 'tenant']);
                
            $tipo_op = $shipment->TIPO;
            $SubTipo_Act = $shipment->SUBTIPO_OPER;
            $tabla = $tipo_op == "IMPO" ? "tm_detalle_impo" : "tm_detalle_expo";
    
            $sql = "SELECT i.DO, i.CONTENEDOR,   d.estado  , i.id_actividad,  i.INFORMACION, i.FECHA_ESTIMADA, 
              IFNULL( i.ESTADO_ACTIV,'N') as ESTADO_ACTIV,
              IFNULL( i.OBLIGATORIA,'N')  as OBLIGATORIA ,
              ADDDATE(str_to_date(i.FECHA_ESTIMADA,'%d/%m/%Y') , INTERVAL -2 DAY)  as fecha_amarilla,
              ADDDATE(str_to_date(i.FECHA_ESTIMADA,'%d/%m/%Y') , INTERVAL -1 DAY)  as fecha_naranja,
              ADDDATE(str_to_date(i.FECHA_ESTIMADA,'%d/%m/%Y') , INTERVAL 0 DAY)  as fecha_roja,
              ADDDATE(str_to_date(i.FECHA_ESTIMADA,'%d/%m/%Y') , INTERVAL + 1  DAY)  as fecha_morada,
              ADDDATE(str_to_date(i.FECHA_ESTIMADA,'%d/%m/%Y') , INTERVAL  -3 DAY)  as fecha_verde
            FROM
              $tabla i,
              tm_do d
            WHERE
              i.do             = d.do
            -- AND ifnull(i.contenedor,'')   = ifnull(d.contenedor,'')
            AND d.TIPO         = '$tipo_op'
            AND d.estado       = 'ABIERTA' -- PARA CALCULAR CERRADAS COMENTAR ESTA LINEA
            AND d.subtipo_oper = '$SubTipo_Act'
            AND d.DO = '$shipment->DO' 
            AND length(i.FECHA_ESTIMADA)>0
            AND i.FECHA_ESTIMADA <> ''
            AND i.FECHA_ESTIMADA IS NOT NULL
             ORDER BY i.do, i.contenedor,i.SECUENCIA;";
    
            //  die($sql);
            $arreglo = DB::select($sql);

            if (empty($arreglo)) {
                throw new Exception('No hay embarques');
            }
    
    
            $this->info('-- DO: '. $shipment->DO);
            foreach ($arreglo as $row) {
                $row = json_decode(json_encode ( $row ) , true);
                $hoy = Carbon::now();
    
                $bInfo = "N";
                $sColor = "0 VERDE";
                if ($row['INFORMACION'] == "N/A") {
                    $bInfo = "N";
                } elseif ($row['INFORMACION'] == "PTE" || $row['INFORMACION'] == "") {
                    if ($row['FECHA_ESTIMADA'] == "" || $row['FECHA_ESTIMADA'] == "PTE" || $row['FECHA_ESTIMADA'] == "N/A" || $row['FECHA_ESTIMADA'] == NULL) {
                        $bInfo = "N";
                    } else {
                        $bInfo = "S";
                        //                $fecha_estimada = Carbon::createFromFormat('d/m/Y', date("d/m/Y"));
    
                        // $fecha_estimada = Carbon::createFromFormat('d/m/Y', $row['FECHA_ESTIMADA']);
    
                        $sFechaV = Carbon::createFromFormat('Y-m-d', $row['fecha_verde']);
                        $sFechaA = Carbon::createFromFormat('Y-m-d', $row['fecha_amarilla']);
                        $sFechaN = Carbon::createFromFormat('Y-m-d', $row['fecha_naranja']);
                        $sFechaR = Carbon::createFromFormat('Y-m-d', $row['fecha_roja']);
                        $sFechaM = Carbon::createFromFormat('Y-m-d', $row['fecha_morada']);
                    }
                }
    
                if ($bInfo == "S") {
    
                    if ($sFechaM->diffInHours($hoy, false) >= 0) {
                        $sColor = "4 MORADO";
    
                    } elseif ($sFechaR->diffInHours($hoy, false) >= 0) {
                        $sColor = "3 ROJA";
    
                    } elseif ($sFechaN->diffInHours($hoy, false) >= 0) {
                        $sColor = "2 NARANJA";
    
                    } elseif ($sFechaA->diffInHours($hoy, false) >= 0) {
                        $sColor = "1 AMARILLA";
    
                    } elseif ($sFechaV->diffInHours($hoy, false) <= 0) {
                        $sColor = "0 VERDE";
    
                    }
                }
    
    
                $sql = "UPDATE $tabla SET   
                  COLOR_ALR     = '" . $sColor . "',
                  FECHA_ALR     = '" . date("d/m/Y") . "',
                  MAQUINA       = '$Maquina_Act',
                  USUARIO       = '$Usuario_Act'
                WHERE DO = '$shipment->DO'
                AND /* ACTIVIDAD_ID */ id_actividad = '" . $row['id_actividad'] . "';";
    
                $result = DB::statement($sql);
                
                $this->info('---- Actividad: '. $shipment->DO  . " - ". $row['id_actividad'] . " actualizado con color ". $sColor);
    
                $sql = "SELECT MAX(COLOR_ALR) as max FROM $tabla WHERE DO='" . $shipment->DO . "'";
                $max_color = "0 VERDE";

                $result = collect(DB::select($sql))->first();
            
                if (!$result) {
                    throw new Exception('Error en la consulta: '. $sql);
                }

                $max_color = $result->max;
    
    
                $sql = "UPDATE tm_do SET
                  COLOR_ALERTA   =  '$max_color'
                  WHERE DO = '" . $shipment->DO . "';";
    
                $result = DB::statement($sql);

            }
            $this->info('-- DO: '. $shipment->DO . " actualizado con color ". $max_color);
            
            return array(true, "OK");
        } catch (Exception $e) {
            return array(false, $e->getMessage());
        }
    }

    public function campanas(){
        Command::info("Actualizando campañas  ($this->tenantName)");
        SgcCampana::whereRaw("FECHA_FIN != '' AND STR_TO_DATE(FECHA_FIN, '%d/%m/%Y') < NOW()")
                ->update(['ESTADO' => 'CERRADA']);
        
    }

    public function tarifarios(){
        Command::info("Actualizando tarifario venta  ($this->tenantName)");
        SgcTarifario::whereRaw("vigencia != '' AND STR_TO_DATE(vigencia, '%d/%m/%Y') < NOW() and STR_TO_DATE(vigencia, '%d/%m/%Y') IS NOT NULL")
        ->update(['ESTADO' => '0']);
        
        Command::info("Actualizando tarifario compra  ($this->tenantName)");
        SgcTarifarioProv::whereRaw("vigencia != '' AND STR_TO_DATE(vigencia, '%d/%m/%Y') < NOW() and STR_TO_DATE(vigencia, '%d/%m/%Y') IS NOT NULL")
            ->update(['ESTADO' => '0']);
        
        Command::info("Actualizando tarifario en cotizaciones  ($this->tenantName)");
        SgcCotizaTarifario::whereRaw("vigencia != '' AND STR_TO_DATE(vigencia, '%d/%m/%Y') < NOW() and STR_TO_DATE(vigencia, '%d/%m/%Y') IS NOT NULL")
            ->update(['ESTADO' => '0']);

    }

    public function expiredDocuments(){
        config(['database.default' => 'tenant']);
        Command::info("Actualizando documentacion legal ($this->tenantName)");
            $cedulas = DB::table('sgc_clientes as a')
            ->join('sgc_clientes_doclegal as b', 'a.CEDULA', '=', 'b.cedula')
            ->whereRaw("STR_TO_DATE(b.valor, '%d/%m/%Y') < DATE_SUB(NOW(), INTERVAL 1 YEAR)")
            ->pluck('a.CEDULA');
        
            // Actualizar los registros en `sgc_clientes`
            $updatedRows = DB::table('sgc_clientes')
                ->whereIn('CEDULA', $cedulas)
                ->update(['ESTADO_INFO' => 'DOCVENCIDA']);
                Command::info("Se actualizo un total de " . $updatedRows . " registros en ($this->tenantName)");   
    }

   /* function getUsers(){
        $this->operativeUsers = User::where('coordinador', 'S')->where('perfil', 'S')->get();
        $this->commercialUsers = User::where('comercial', 'S')->where('perfil', 'S')->get();
        $this->pricingUsers = User::where('pricing', 'S')->where('perfil', 'S')->get();
        $this->administrativeUsers = User::where('coordinador', 'S')->where('perfil', 'S')->get();
    } */
}
