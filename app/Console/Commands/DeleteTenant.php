<?php

namespace App\Console\Commands;

use Hyn\Tenancy\Contracts\Repositories\CustomerRepository;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Environment;
use App\Models\System\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class DeleteTenant extends Command
{
    
    protected $signature = 'tenant:delete {username} {force}';
    
    protected $description = 'Deletes a tenant of the provided name. Only available on the local environment e.g. php artisan tenant:delete boise';
    
    public function handle()
    {
        // because this is a destructive command, we'll only allow to run this command
        // if you are on the local environment
        /*if (!app()->isLocal()) {
            $this->error('This command is only avilable on the local environment.');
            return;
        }*/
        $username = $this->argument('username');
        $force = $this->argument('force');
        $this->deleteTenant($username, $force);

        return 0;
    }
    
    private function deleteTenant($username, $force)
    {
        if ($customer = Customer::where('username', $username)->firstOrFail()) {
            
            $hostname = $customer->hostname;
            $website = $customer->website;
            app(HostnameRepository::class)->delete($hostname, $force);
            app(WebsiteRepository::class)->delete($website, $force);
            
            // si se borra de la base de datos
            if($force){
                $customer->forceDelete();
            }else{
                $customer->delete();
            }

            $this->info("Tenant {$username} successfully deleted.");
        }
    }
    
}