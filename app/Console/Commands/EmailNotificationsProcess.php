<?php

namespace App\Console\Commands;

use App\Traits\TenantQueryTrait;
use Illuminate\Console\Command;
use App\Mail\NotifyException;
use App\Mail\DailyReminder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Customer\SgcCotizaciones;
use App\Models\Customer\TmDo;
use \Exception;

class EmailNotificationsProcess extends Command
{
    use TenantQueryTrait; // Importa el trait
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:email-notifications-process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $tenantName;

    protected $operativeUsers = [];

    protected $commercialUsers = [];

    protected $pricingUsers = [];

    protected $administrativeUsers = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            $tenant = app(\Hyn\Tenancy\Environment::class)->tenant();
            $tenantName = $tenant->hostnames[0]->fqdn ?? "Sin nombre";
            $this->tenantName = $tenantName;
    
            $result = $this->getActiveTenants([$tenant->id] ?: null);
            if(!$result){
                return Command::info("El tenant no esta activo");
            }

            [$this->operativeUsers, $this->commercialUsers, $this->pricingUsers, $this->administrativeUsers] = $this->getUsers();

            $this->info('--Tenant: '. $this->tenantName);


            foreach($this->commercialUsers as $user){
                $quotes = SgcCotizaciones::where('ESTADO', 'ENPROCESO')->where('ASESOR_ID', $user->cedula)->get();
                Mail::to($user->correo)->send(new DailyReminder($user,$quotes,"Cotizaciones", $this->tenantName ));
                $this->info('Cotizaciones mail to: '. $user->correo );
            }

            foreach($this->operativeUsers as $user){
                $shipments = TmDo::where('ESTADO', 'ABIERTA')->where('coordinador', $user->cedula )->get();
    
                Mail::to($user->correo)->send(new DailyReminder($user,$shipments,"Embarques", $this->tenantName ));
                $this->info('Embarques mail to: '. $user->correo );
            }


        }catch (\Throwable $th) {
            Mail::to('spedroza@conectacarga.co')->send(new NotifyException( $this->tenantName . " - " . $th->getMessage()));
            return Command::info("Error: " . $th->getMessage());
        }

        return 0;
    }
}
