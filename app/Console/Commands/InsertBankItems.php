<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InsertBankItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:bank-items {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate incomes to bank items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {
            $validTypes = ['income', 'outcome','purchase'];
            $type = $this->argument('type') ?? 'income';
            $tenant = app(\Hyn\Tenancy\Environment::class)->tenant()->uuid;
            
            if (!in_array($type, $validTypes)) {
                return Command::info("Only types are allowed: (" . implode(',', $validTypes) . ")");
            }

            if(in_array($type, ['income','outcome'])){
                $this->insertItems($tenant,$type);
            }elseif($type == "purchase"){
                $this->insertPurchaseItems($tenant,$type);
            }


           


        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return 1;
        }


        return 0;
    }

    public function insertItems($tenant,$type){


        $itemableTable = $type == "income" ? "tm_banco_ingresos" : "tm_banco_egresos";
        $billableTable = $type == "income" ? "tm_facturas" : "tm_compras";
        // $items = Item::where('itemable_type', $itemableType)->get();
    
        $excludedIds = DB::table($tenant.'.tm_banco_items')
        ->where('itemable_type', $type)
        ->pluck('itemable_id');
    
    
        $result = DB::table("{$tenant}.{$itemableTable}")->whereNotIn('id', $excludedIds)->get();
    
        // Dividir IDs en bloques pequeños
        $chunkedFacturaIds = $result->pluck('factura_id')->chunk(1000);
    
    
        // ENCONTRAR LA FACTURA RELACIONADA
        $relatedInvoices = collect();
        foreach ($chunkedFacturaIds as $chunk) {
            $relatedInvoices = $relatedInvoices->merge(
                DB::table($tenant.'.'.$billableTable)->whereIn('id', $chunk)->get()
            );
        }
    
        $insertData = $result->map(function ($item) use ($type,$relatedInvoices) {
            $relatedInvoice = $relatedInvoices->firstWhere('id', $item->factura_id);
    
            return [
                'do' => $relatedInvoice->do ?? "",
                'itemable_id' => $item->id,
                'itemable_type' => $type,
                'billable_id' => $relatedInvoice->id ?? null,
                'billable_type' => $type == 'income' ? 'invoice' : 'purchase',
                'concepto' => null,
                'subtotal_venta' => $item->valor,
                'monedaventa' => $item->moneda,
                'trmventa' => 1,
                'tasa_impuesto' => null,
                'venta_total' => $item->valor,
                'venta_total_mn' => $item->valor,
                'impuesto' => null,
                'impuesto_mn' => null,
                'subtotal_venta_mn' => $item->valor,
                'notas' => $item->notas,
                'creado_por' => 'SOPORTE',
                'fecha_creacion' => now(),
                'modificado_por' => 'SOPORTE',
                'fecha_modificacion' => now(),
                'maquina' => 'SOPORTE'
            ];
        })->toArray();
    
        $chunkedInsertData = array_chunk($insertData, 1000);
    
        foreach ($chunkedInsertData as $chunk) {
            DB::table($tenant.'.tm_banco_items')->insert($chunk);
            // DB::table($tenant.'.tm_banco_items')->insert($insertData);
            Command::info("Se insertaron un total de " . count($chunk) . " items.");
        }
    
        return Command::info($tenant . "Migrado correctamente");
    
    }


    public function insertPurchaseItems($tenant,$type){

        // $items = Item::where('itemable_type', $itemableType)->get();
    
        $excludedIds = DB::table($tenant.'.tm_compras_detalle')
        ->pluck('factura_id');
    
    
        $result = DB::table("{$tenant}.tm_compras")->whereNotIn('id', $excludedIds)->get();
    
        // Dividir IDs en bloques pequeños
        $chunkedFacturaIds = $result->pluck('id')->chunk(1000);
    
    
        // ENCONTRAR LA FACTURA RELACIONADA
        $relatedInvoices = collect();
        foreach ($chunkedFacturaIds as $chunk) {
            $relatedInvoices = $relatedInvoices->merge(
                DB::table($tenant.'.tm_compras')->whereIn('id', $chunk)->get()
            );
        }
    
        $insertData = $result->map(function ($item) use ($type,$relatedInvoices) {
            $relatedInvoice = $relatedInvoices->firstWhere('id', $item->id);
    
            /*return [
                'do' => $relatedInvoice->do ?? "",
                'itemable_id' => $item->id,
                'itemable_type' => $type,
                'billable_id' => $relatedInvoice->id ?? null,
                'billable_type' => $type == 'income' ? 'invoice' : 'purchase',
                'concepto' => null,
                'subtotal_venta' => $item->valor,
                'monedaventa' => $item->moneda,
                'trmventa' => 1,
                'tasa_impuesto' => null,
                'venta_total' => $item->valor,
                'venta_total_mn' => $item->valor,
                'impuesto' => null,
                'impuesto_mn' => null,
                'subtotal_venta_mn' => $item->valor,
                'notas' => $item->notas,
                'creado_por' => 'SOPORTE',
                'fecha_creacion' => now(),
                'modificado_por' => 'SOPORTE',
                'fecha_modificacion' => now(),
                'maquina' => 'SOPORTE'
            ]; */

            // $trm = $relatedInvoice->monedaventa == $item->monedaventa ? 1 : $item->trmventa;
            // $subtotal_mn = $item->subtotal_venta * $trm;

            return [
                'factura_id' => $item->id,
                'concepto' => 0,
                'id_grupo' => 0,
                'variacion' => 0,
                'proveedor_id' => $item->proveedor_id,
                'unidad' => 1,
                'tarifa_compra' => $item->subtotal_venta,
                'monedacompra' => $item->monedaventa,
                'trmcompra' => $item->trmventa,
                'retencion' => 0,
                'valor_retencion' => 0,
                'valor_retencion_mn' => 0,
                'subtotal_compra' => $item->subtotal_venta,
                'subtotal_compra_mn' => $item->subtotal_venta_mn,
                'tipo_descuento' => 'V',
                'descuento' => '0',
                'valor_descuento' => '0',
                'valor_descuento_mn' => '0',
                'tasa_impuesto' => $item->tasa_impuesto,
                'impuesto' => $item->impuesto,
                'impuesto_mn' => $item->impuesto_mn,
                'compra_total' => $item->venta_total,
                'compra_total_mn' => $item->venta_total_mn,
                'notas' => $item->concepto,
                'usuario' => $item->creado_por,
                'maquina' => '::1',
                'fecha_mod' => now(),
            ];


        })->toArray();
    
        $chunkedInsertData = array_chunk($insertData, 1000);
    
        foreach ($chunkedInsertData as $chunk) {
            DB::table($tenant.'.tm_compras_detalle')->insert($chunk);
            // DB::table($tenant.'.tm_banco_items')->insert($insertData);
            Command::info("Se insertaron un total de " . count($chunk) . " items.");
        }
    
        return Command::info($tenant . "Migrado correctamente");
    
    }
    

}
