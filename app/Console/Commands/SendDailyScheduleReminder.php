<?php

namespace App\Console\Commands;

use App\Models\Customer\TmUsersSchedule;
use App\Models\Customer\User as TmUser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ScheduleReminder;
use Carbon\Carbon;
use SevenSpan\WhatsApp\WhatsApp;

class SendDailyScheduleReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:send-daily-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily schedule reminder emails to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->jorney();
        
    }

    public function jorney(){
        $dayOfWeek = Carbon::now()->dayOfWeek; // Obtener el día actual en inglés
        $currentTime = Carbon::now('America/Bogota');
        $currentHour = $currentTime->format('H');
        $schedules = TmUsersSchedule::all()->where('dia', $dayOfWeek);
        $tenant = app(\Hyn\Tenancy\Environment::class)->tenant()->uuid;

        
       
        foreach ($schedules as $schedule) {
            $horaInicio = explode(":",$schedule->hora_entrada);
            if(isset($horaInicio[0]) && ($horaInicio[0] != $currentHour)){
                continue;
            }
            // Asumiendo que tienes una tabla de usuarios con email
            $user = TmUser::withTrashed()->where('cedula', $schedule->user_id)->where('perfil','S')->first();

            if ($user) {
                Mail::to($user->correo)->send(new ScheduleReminder($user, $schedule));
                $this->info('mail to: '. $user->correo );
                
                $result = DB::table("{$tenant}.configuraciones")->where('configuracion', 'WHATSAPP_API')->first();
                
                if($user->telefono && isset($result->valor) && $result->valor == true){
                    
                    $accessToken = optional(DB::table("{$tenant}.configuraciones")->where('configuracion', 'WHATSAPP_TOKEN')->first())->valor;
                    $phoneNumberId = optional(DB::table("{$tenant}.configuraciones")->where('configuracion', 'WHATSAPP_PHONE_ID')->first())->valor;
                    $template = DB::table("{$tenant}.sgc_wp_templates")->where('id_process', 'entry_registration')->first();
                    $contacto = DB::table("{$tenant}.sgc_clientes_contactos")->where('celular', $user->telefono)->first();
                    $contactoId = optional($contacto)->id;

                    
                    $whatsappObject = new WhatsApp();
                    $templateName = $template->name ?? ""; // Template name of your template (Required).
                    $languageCode = $template->language ?? ""; // Template language code of your template (Required).
                    $templateBody = $template->components ?? "{}"; // Template language code of your template (Required).
                    $templateBody = json_decode($templateBody, true);
                    if($templateName != "" && $languageCode != "")
                        $responseMessage = $whatsappObject->sendTemplateMessage( $user->telefono,  $templateName,  $languageCode,$accessToken,$phoneNumberId);

                        if(isset($responseMessage['messages'][0]['id'])){

                            $mensaje = $templateBody[1]['text'];

                            $data = [
                                'id_contacto' => $contactoId,
                                'wamid' => $responseMessage['messages'][0]['id'],
                                'status' => 'created',
                                'wp_number' => $user->telefono,
                                'message' => $mensaje,
                                'body' => $mensaje,
                                'timestamp' => time(),
                                'created_at' => now(),
                                'created_by' => '', // Asigna el valor apropiado aquí
                                'id_template' => $template->id_wp,
                            ];
                            
                            DB::table("$tenant.sgc_wp_message")->insert($data);


                        //     $sql = "INSERT INTO $tenant.`sgc_wp_message` (`id_contacto` ,`wamid`, status ,`wp_number`, message , `body` ,`timestamp`,created_at, created_by,id_template) 
                        //             VALUES ('$contactoId','" . $responseMessage['messages'][0]['id'] . "', 'created' , '$user->telefono', '$mensaje','" . $mensaje . "' , '" . time() . "', '" . time() . "', '' ,'" . $template->id_wp . "');";
                        //     DB::statement($sql);
                        // 
                        }   
                        sleep(1);
                }

            }
            // return true;
        }

        $this->info('Daily schedule reminder emails sent successfully.'. $tenant);    
    }

    // public function 
}
