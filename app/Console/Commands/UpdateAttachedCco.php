<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\Customer\TmCuadroContable;

class UpdateAttachedCco extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:updateattachedcco';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update attached column in TmCuadroContable';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
        $tenant = app(\Hyn\Tenancy\Environment::class)->tenant();
        $tenantName = $tenant->hostnames[0]->fqdn ?? "Sin nombre";

        $bucketName = "operativo";
        $keyname = $tenantName . "/cuadro_contable/soporte";
        $folders = Storage::disk('s3')->allDirectories($keyname);

        $search = [];

        foreach ($folders as $folder) {
            $search[] = basename($folder);
        }

        $lenght = count($search);

    //    $search = implode(",",$search);

       if(empty($search)){
        return Command::info("Tenancy ($tenantName) does not have any CCO attached");

       }
       
       TmCuadroContable::whereIn('id', $search)->update(['attached' => 'CCO']);

        return Command::info("Tenancy ($tenantName) updated rows: " . $lenght);
        return 0;
    } catch (\Exception $e) {
        return Command::info("Tenancy ($tenantName) updated exception: " . $e->getMessage());
        return 0;
    }
    }
}
