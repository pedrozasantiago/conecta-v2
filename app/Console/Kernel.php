<?php
namespace App\Console;

use Symfony\Component\Console\Output\ConsoleOutput;
use App\Console\Commands\CronConectaCarga;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreateTenant::class,
        Commands\DeleteTenant::class,
        Commands\CronConectaCarga::class,
        Commands\EmailNotificationsProcess::class,
        Commands\SendDailyScheduleReminder::class,
        Commands\UpdateAttachedCco::class,
        Commands\InsertBankItems::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        try{
            $schedule->command('tenancy:run tenant:cronconecta')->cron('30 9 * * *')
            ->sendOutputTo("/tmp/alerta_actividades_".date('YmdHis').".txt")
            ->onSuccess(function () {
                echo "exito";
            })
            ->onFailure(function () {
                echo "fallo";
            });

        }catch(Exception $e){
            echo $e->getMessage();
        }

        try{
            $schedule->command('tenancy:run tenant:email-notifications-process')->cron('30 12 * * 1-5')
            ->sendOutputTo("/tmp/email_notificacions_".date('YmdHi').".txt")
            ->onSuccess(function () {
                echo "exito";
            })
            ->onFailure(function () {
                echo "fallo";
            });

        }catch(Exception $e){
            echo $e->getMessage();
        }

        try{

            $schedule->command('tenancy:run schedule:send-daily-reminder')->hourly()
            ->sendOutputTo("/tmp/jornada_laboral_".date('YmdHis').".txt")
            ->onSuccess(function () {
                echo "exito";
            })
            ->onFailure(function () {
                echo "fallo";
            });

            
        }catch(Exception $e){

        }

     /*   try{

            $schedule->command('tenancy:run schedule:send-daily-reminder')->hourly()
            ->sendOutputTo("/tmp/jornada_laboral_".date('YmdHis').".txt")
            ->onSuccess(function () {
                echo "exito";
            })
            ->onFailure(function () {
                echo "fallo";
            });

            
        }catch(Exception $e){

        } */
        


        /*
        $schedule->command(CronConectaCarga::class)
        ->cron('* * * * *')
        ->onSuccess(function () {
            echo "exito";
        })
        ->onFailure(function () {
            echo "fallo";
        }); */



        // $schedule->call(function () {
        //     $command = new ConsoleOutput();
        //     $command->writeln("Hola ". date('Y-m-d H:i:s'));
        //     $results = Customer::with(['hostnames']);

            
        //     // $command->writeln("Hola ". date('Y-m-d H:i:s'));
        //     print_r($results);
        //     $command->writeln($results);

        // })->cron('* * * * *')->emailOutputTo('santyaristi@gmail.com');


        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
