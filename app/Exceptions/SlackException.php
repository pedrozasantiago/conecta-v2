<?php

namespace App\Exceptions;

use Monolog\Logger;
use Monolog\Handler\SlackWebhookHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Formatter\JsonFormatter;
use Auth;


class SlackException
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        $dateFormat = "Y-m-d H:i:s";
        $checkLocal = env('APP_ENV');

        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof SlackWebhookHandler) {
                $output = "[$checkLocal]: %datetime% > %level_name% - %message% `%extra% %context%` :poop: \n";
                $formatter = new LineFormatter($output, $dateFormat);

                $handler->setFormatter($formatter);
                $handler->pushProcessor(function ($record) {                    

                    $tenant = (get_hostname()) ?? 'Unidentified';                    

                    // URL
                    if(request()->fullUrl()){
                        $record['extra']['URL'] = request()->fullUrl();
                    }     
                    // Details               
                    $record['extra']['Details'] = $record['message'];
                    
                    // Request
                    if(request()->all()){
                        $record['extra']['Request'] = request()->all();
                    }
                    
                    if($user = Auth::user()){
                        $record['extra']['Name'] = $user->full_name;
                        $record['extra']['Username'] = $user->username;
                        $record['extra']['Email'] = $user->email;
                    }
                    
                    $record['message'] = '*Tenant*: '.$tenant;

                    return $record;
                });
            }
        }
    }

}