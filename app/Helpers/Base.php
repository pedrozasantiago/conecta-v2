<?php 

namespace App\Helpers;

use App\Models\Customer\Option;
use App\Models\System\Currency;
use App\Models\Customer\User;
use App\Models\Customer\Permission;
use App\Models\Customer\Warehouse;
use App\Models\Customer\Store;
use App\Models\Customer\UsersStore;
use App\Models\Customer\UsersCashRegister;
use App\Models\Customer\CashClosure;
use App\Models\System\Customer;
use App\Models\System\CustomersHostname;
use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Auth;
use DB;
use Ixudra\Curl\Facades\Curl;

class Base
{
	private $options = array();

	private function __construct()
    {

    }   


    // obtener el bucket de amazon
    public static function get_s3_image($filename, $thumb = false, $folder = false, $http = true)
    {

        $website = get_database_name();
        if($filename){

            $folder = ($folder) ? $website.'/'.$folder : $website.'/images';
            if($thumb){
                $route = $folder.'/thumbs/thumb_'.$filename;
            }else{
                $route = $folder.'/'.$filename;
            }

            if(!$http){
                return $route;
            }
        
            return Storage::disk('s3')->url($route);
        
        }

    }
    

    /**
     * Get all db options
     * @return array
     */
    public static function get_options()
    {
        $base = new BASE;
        return $base->options;
    }

    /**
     * Function that gets option based on passed name
     * @param  string $name
     * @return string
     */
    public static function get_option($name)
    {        

        /*$name = trim($name);
        
        $option = Option::select('value')->where('name', $name)->orderBy('value', 'ASC')->first();

        return ($option) ? $option->value : null;*/

        $base = new BASE;

        $name = trim($name);

        if (isset($base->options[$name])) {
            return $base->options[$name];
        }

        return '';

    }    

    /* moneda por defecto */
    public static function default_currency()
    {
        $currency = Currency::where('isdefault', 1)->first();
        return $currency;
    }

    /* zeros a la izquierda */
    public static function leading_zeros($type, $zeros = false, $value = false)
    {

        // verificar cuantos 0 se colocaran a la izquierda
        $qty = ($zeros) ? $zeros : Base::get_option($type.'_leading_zeros');

        // verificar si el valor se recibio y si no colocarlo por defecto desde la db
        $val = ($value) ? $value : Base::get_option('next_'.$type.'_number');

        return str_pad($val, $qty, '0', STR_PAD_LEFT);

    }


    /* bodegas de una tienda */
    public static function get_warehouses($id)
    {
        $warehouses = Warehouse::where('store_id', $id)->pluck('name','id')->all();
        
        return $warehouses;
    }

    /* tienda actual en la que esta el usuario  */
    public static function current_store()
    {
        return UsersStore::where('user_id', Auth::user()->id)->where('active', 1)->first();
    }

    /* tiendas asignadas de un usuario  */
    public static function users_stores($type = false, $id = false)
    {

        // obtener el id del usuario
        $user = ($id) ? $id : Auth::user()->id;
        $store_id = Auth::user()->store_id;

        // obtener las tiendas asignadas al usuario
        $stores = UsersStore::leftJoin('stores', 'stores.id', '=', 'users_stores.store_id')
        ->where('user_id', $user)
        ->where('stores.status', 1);

        // lista de tiendas en un array
        $select = $stores->pluck('name','store_id')->all();

        // quitar la tienda actual
        if($type == 'Change'){
            unset($select[$store_id]);
        }

        return $select;

    }    


    /* tienda actual en la que esta el usuario  */
    public static function current_register()
    {
        $user = Auth::user();

        return UsersCashRegister::where('user_id', $user->id)
        ->where('store_id', $user->store_id)
        ->where('active', 1)->first();
    }


    /* cajas asignadas de un usuario  */
    public static function users_registers($type = false, $id = false)
    {

        // obtener el id del usuario
        $user = ($id) ? $id : Auth::user()->id;
        $cash_register_id = Auth::user()->register_id;
        $store_id = Auth::user()->store_id;

        // obtener las cajas asignadas al usuario
        $registers = UsersCashRegister::leftJoin('cash_registers', 'cash_registers.id', '=', 'users_cash_registers.cash_register_id')
        ->where('user_id', $user)
        ->where('users_cash_registers.store_id', $store_id);
        //->where('stores.status', 1);

        // lista de tiendas en un array
        $select = $registers->pluck('name','cash_register_id')->all();

        // quitar la tienda actual
        if($type == 'Change'){
            unset($select[$cash_register_id]);
        }

        return $select;

    }


    /* caja registradora abierta actualente  */
    public static function current_closure()
    {
        $user = Auth::user();
        
        return CashClosure::with('store', 'user', 'register')->where('cash_register_id', $user->register_id)
        ->where('store_id', $user->store_id)
        ->where('status', 1)
        ->first();
    }    

    /* formatear monedas */
    public static function money_format($number, $currency = false)
    {
        $number_f = number_format($number, 0, ',','.');

        // agregar el simbolo y nombre de la moneda
        if($currency){
            $get_currency = Currency::where('currency', $currency)->first();
            $number_sym = $get_currency->symbol.' '.$number_f.' '.$get_currency->currency;
        }else{
            $number_sym = ' $ ' . $number_f;
        }

        return $number_sym;
    }


    /* Convertir moneda */
    public static function get_currency($value, $from, $to, $format = false)
    {
        // obtener la moneda
        $currency = Currency::where('currency', $from)->first();
        
        // verificar si la fecha de actualizacion ya paso
        $updated = $currency->updated_at;
        $now = Carbon::now();
        
        // obetner la diferencia de dias entre las fechas
        $diff = $now->diffInDays($updated);

        if($diff == 0){
            if($currency->rate){
                // obtener el valor de la moneda
                $exchange = $currency->rate;
            }else{
                $exchange = Base::rate_value($to, $from);
                $currency->update(['rate' => $exchange]);
            }
        }else{
            $exchange = Base::rate_value($to, $from);
            $currency->update(['rate' => $exchange]);
        }

        // multiplicar
        $total = $value/$exchange;

        // formatear moneda
        if($format == 1){
            $number = $total;
        }else{
            $number = Base::money_format($total, $to);
        }

        return $number;

    }


    /* obtener la cotizacion de una moneda frente al dolar */
    public static function rate_value($to, $from)
    {

        // obetener los valores actuales de las monedas
        $rate = Curl::to('http://www.apilayer.net/api/live')
        ->withData(array('access_key' => 'bb900c7895eef18b8df1b08f806d4b58'))
        ->get();

        // convertir el json en un array
        $json = json_decode($rate, true);

        // obtener las monedas
        $quote = $to.$from;

        // obtener el valor de la moneda
        $exchange = $json['quotes'][$quote];
    
        return $exchange;

    }

 
}