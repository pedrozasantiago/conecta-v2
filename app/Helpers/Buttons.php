<?php

namespace App\Helpers;
use Carbon\Carbon;
use PublikBase;

class Buttons {


    public static function href_link($href, $title = false, $icon = false, $target = false)
    {

        /* verificar que recibio el icono */
        $icon = ($icon) ? '<i class="'.$icon.'"></i>' : '<i class="fa fa-check-square-o"></i>';

        /* verificar si tiene target */
        $target = ($target) ? 'target="'.$target.'"' : '';

        /* renderizar el boton */
        $html = '<a class="btn btn-tables btn-edit" title="'.$title.'" href="'.url($href).'" '.$target.'>'.$icon.'</a>';

        return $html;

    }


    public static function create_modal($type, $url, $ref, $var = false, $size = false, $icon = false)
    {
        /* verificar que recibio el tamaño */
        $size = ($size) ? $size : "sm";

        /* verificar que recibio el icono */
        $icon = ($icon) ? $icon : "fa fa-check-square-o";

        /* renderizar el boton modal */
        $html = '<a class="vd-button btn btn-success" id="load_crud" href="#modal_crud" data-toggle="modal" data-title="'.trans($type.".create").'" data-backdrop="static" data-keyboard="false" data-size="'.$size.'" data-load-remote="'.url($url."/".$ref."/".$var).'" data-remote-target="#modal_crud .modal-body"><i class="'.$icon.'"></i> '.trans($type.".create").'</a>';

        return $html;

    } 


    public static function edit_modal($type, $url, $id, $size = false, $icon = false)
    {
        /* verificar que recibio el tamaño */
        $size = ($size) ? $size : "sm";

        /* verificar que recibio el icono */
        $icon = ($icon) ? $icon : "fa fa-pencil-alt";

        /* renderizar el boton modal */
        $html = '<a class="btn btn-tables btn-edit" id="load_crud" href="#modal_crud" data-toggle="modal" data-title="'.trans($type.".edit").'" data-backdrop="static" data-keyboard="false" data-size="'.$size.'" data-load-remote="'.url($url."/".$id."/edit").'" data-remote-target="#modal_crud .modal-body"><i class="'.$icon.'"></i></a>';

        return $html;

    }


    /* ventana modal de cualquier url */
    public static function url_modal($type, $url, $class, $icon = false, $text = false, $size = false)
    {
        /* verificar que recibio el tamaño */
        $size = ($size) ? $size : "sm";

        /* renderizar el boton modal */
        $html = '<a class="vd-button btn btn-'.$class.'" id="load_crud" href="#modal_crud" data-toggle="modal" data-title="'.trans($type).'" data-backdrop="static" data-keyboard="false" data-size="'.$size.'" data-load-remote="'.url($url).'" data-remote-target="#modal_crud .modal-body">';
        if($icon){
            $html.= '<i class="'.$icon.'"></i>';
        }
        if($text){
            $html.= $text;
        }
        $html.= '</a>';

        return $html;

    }


    /* boton restaurar eliminado */
    public static function restore_modal($attributes)
    {
    
        // id del boton
        $id = ($attributes['id']) ? 'id="'.$attributes['id'].'"' : '';
    
        // id para eliminar via ajax
        $data_id = ($attributes['item_id']) ? 'data-id='.$attributes['item_id'].'' : '';
    
        // ajax id
        $ajax = ($attributes['ajax']) ? 'id="ajax_restore"' : '';
    
        // icono
        $icon = isset($attributes['icon']) ? $attributes['icon'] : "fa fa-undo-alt";
    
        /* renderizar el boton modal */
        $html = '<form action="'.url($attributes['url']."/restore").'" method="POST" '.$ajax.' '.$data_id.'  class="d-inline-block"><input type="hidden" name="_token" value="'.csrf_token().'"><input type="hidden" name="id" value="'.$attributes['item_id'].'">';
        $html.= '<button type="submit" class="btn btn-tables btn-restore" '.$data_id.' '.$id.'><i class="'.$icon.'"></i></button>';
        $html.= '</form>';
    
        return $html;
    
    }


    /* boton eliminar */
    public static function destroy_modal($type, $url, $id, $id_attr = false, $form_id = false, $icon = false)
    {

        /* verificar que recibio el tamaño */
        $id_attr = ($id_attr) ? 'id='.$id_attr.'' : '';

        /* id para eliminar via ajax */
        $data_id = ($form_id == 'ajax_delete') ? 'data-id="'.$id.'"' : '';

        /* id del fomulario */
        $form_id = ($form_id) ? 'id="'.$form_id.'"' : '';

        /* verificar que recibio el icono */
        $icon = ($icon) ? $icon : "fa fa-trash";

        /* renderizar el boton modal */
        $html = '<form action="'.url($url."/destroy").'" method="POST" '.$form_id.' '.$data_id.'  class="d-inline-block"><input type="hidden" name="_token" value="'.csrf_token().'"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="id" value="'.$id.'"><button type="submit" class="btn btn-tables btn-delete" data-id="'.$id.'" '.$id_attr.'><i class="'.$icon.'"></i></button></form>';

        return $html;

    }

    /* input con icono */
    public static function date_picker($name, $label, $property = false, $dir = false, $icon = false, $class = false, $id = false, $value = false, $data_column = false)
    {

        // formatear la fecha si esta presente
        if($value){
            $value = Carbon::createFromFormat('Y-m-d', $value)->format(PublikBase::get_option('dateformat'));
        }

        /* verificar si el campo es requerido */
        $is_required = strpos($property, 'required');

        /* verificar que recibio la ubicacion del icono */
        $dir = ($dir) ? $dir : 'right';

        /* verificar que recibio el id */
        $id = ($id) ? 'id='.$id.'' : '';

        /* verificar que recibio el id de datatable */
        $data_column = ($data_column) ? 'data-column='.$data_column.'' : '';

        /*Verficar que recibio el $name */
        $name_input = ($name) ? 'name='.$name.'' : '';

        /* verificar el tipo de datepicker a usar*/
        $class = ($class) ? $class : "pickadate";

        /* verificar que recibio el icono */
        $icon = ($icon) ? $icon : "fa fa-calendar-alt";

        /* variable del icono */
        //<span class="input-group-addon">
        $icongroup = '<div class="input-group-append">';
        $icongroup.= '<span class="input-group-text">';
        $icongroup.= '<i class="'.$icon.'"></i>';
        $icongroup.= '</span>';
        $icongroup.= '</div>';

        $html = '';

        if($label){
            if ($is_required == true || $is_required == 0) {
                $html.= '<label for="'.$name.'" class="control-label vd-label">'.$label.'<span class="required">*</span></label>';
            }else{
                $html.= '<label for="'.$name.'" class="vd-label">'.$label.'</label>';
            }
        }
        $html.= '<div class="input-group">';

        if($dir == 'left'){
            $html.= $icongroup;
        }
        
        /* input */
        $html.= '<input type="text" '.$name_input.' '.$id.' class="form-control vd-input '.$class.'" style="padding: .83rem;" placeholder="'.$label.'" '.$property.' '.$data_column.' value="'.$value.'" />';
        /* end input */

        if($dir == 'right'){
            $html.= $icongroup;
        }

        $html.= '</div>';

        /* renderizar datepicker */
        return $html;

    }



    /* Radio y checkbox */
    public static function check_radio($type, $name, $label, $class = false)
    {

        /* verificar que recibio el id */
        $id_input = ($name) ? 'id='.$name.'' : '';

        /*Verficar que recibio el $name */
        $name_input = ($name) ? 'name='.$name.'' : '';

        $html = '<div class="skin '.$class.'">';
        $html.= '<input type="'.$type.'" '.$name_input.' '.$id_input.'>';
        $html.= '<label for="'.$name.'">'.$label.'</label>';
        $html.= '</div>';

        /* renderizar */
        return $html;

    }


    public static function switch($name = false, $label = false, $value = false, $attrs = false)
    {

        /* obtener el attr name del item */
        $name_attr = ($name) ? 'name="'.$name.'"' : '';

        /* verificar si esta activo o no */
        $value = ($value) ? 'value="'.$value.'" ' : '';

        /* renderizar el switch */
        $html = '';
        if($label){
            $html.= '<label class="label-switch pull-left m-r">'.trans($label).'</label>';
        }
        $html.= '<div class="checkbox-switch">';
        $html.= '<input type="checkbox" '.$name_attr.' '.$value;

        $label_for = $name;
        foreach ($attrs as $key => $attr) {
            if($key == 'checked' && $attr == ''){
                continue;   
            }
            $html.= $key.'="'.$attr.'" ';
            if($key == 'id'){
                $label_for = $attr;
            }
        }

        $html.= ' />';
        $html.= '<label for="'.$label_for.'">'.trans($label).'</label>';
        $html.= '</div>';

        return $html;

    }   


}