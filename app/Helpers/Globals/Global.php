<?php

 function array_group_by(array $array, $key) {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }

        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;

        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;

            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }

            if ($key === null) {
                continue;
            }

            $grouped[$key][] = $value;
        }

        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();

            foreach ($grouped as $key => $value) {
                $params = array_merge([$value], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }

        return $grouped;
    }


// checkear permisos
function check_perm($perm, $guard)
{
    if(!Auth::guard($guard)->user()->can($perm)){
        abort(403);
    }
}

// marcar notificaciones como vistas
function read_alert($guard, $type, $id)
{
    foreach(Auth::guard($guard)->user()->unreadNotifications as $notification){
        if($notification->type == $type && $notification->data['id'] == $id){
            $notification->markAsRead();
        }
    }
}  

// global json
function get_global_json()
{
    /* variable para crear el objeto */
    $global = new \stdClass();

    /* asignar las variables al objeto */
    $global->_root_ = url('/');
    $global->assets_url = asset('/');
    $global->loading_text = trans('base.loading_text');
    $global->error_load_datatable = trans('base.error_load_datatable');    
    $global->submit_processing_text = trans('base.submit_processing_text');
    $global->ajax_processing = trans('base.ajax_processing');
    $global->ajax_failure = trans('base.ajax_failure');
    $global->title_alert = trans('base.title_alert');
    $global->text_cancel = trans('base.cancel');
    $global->text_save = trans('base.save');        
    $global->delete_cancelled = trans('base.cancelled');
    $global->error_deleted = trans('base.error_deleted');
    $global->delete_error = trans('base.delete_error');
    $global->text_deleted = trans('base.deleted');
    $global->restore_cancelled = trans('base.cancelled');
    $global->error_restored = trans('base.error_restored');        
    $global->restore_error = trans('base.restore_error');
    $global->text_restored = trans('base.restored');        
    $global->dateformat = \PublikBase::get_option('js_dateformat');

    return 'var base_config = '. json_encode($global, JSON_PRETTY_PRINT) .';';
}


// configuration files
function get_config_data($name)
{
    $data = [];
    $configs = config('datatables_config.'.$name)['configs'];
    $items = $configs['config'];
    $routes = $configs['routes'];
    $trans = $configs['trans'];

    foreach($items as $key => $item){                
        $data[$key] = $item;
    }

    foreach($routes as $key => $route){                
        $data[$key] = route($route);
    }

    foreach($trans as $key => $tran){                
        $data[$key] = trans($tran);
    }

    return json_decode(
        json_encode($data)
    );

}


// orden de las columnas
function get_columns_order($name){

    $data = [];
    $order = (config('datatables_config.'.$name)['columns']['order']) ?? null;    

    $data['column'] = 0;
    $data['dir'] = 'desc';

    if(isset($order) && count($order)>0){
        $data['column'] = (int)$order['column'];
        $data['dir'] = $order['dir'];
    }

    return json_decode(
        json_encode($data)
    );;
}


// columnas para datatable jquery
function get_columns_dt($name)
{

    $config = json_decode(
        json_encode(config('datatables_config.'.$name)['columns'])
    );

    $columns = ($config->select) ?? [];
    $classnames = ($config->classnames) ?? [];
    $searchable = ($config->searchable) ?? [];
    $orderable = ($config->sortable) ?? [];
    $hiddens = ($config->hidden) ?? [];

    $columns_dt = [];
    if(!$searchable){
        $searchable = $columns;
    }

    if(!$orderable){
        $orderable = $searchable;
    }

    // crear el array de columnas datatable
    foreach ($columns as $key => $value) {

        $column = new \stdClass();
        $column->data = $value;
        $column->name = $value;
        $column->visible = true;
        $column->searchable = false;
        $column->orderable = false;

        // asignar clases
        foreach ($classnames as $key => $classname) {
            if($value == $key){
                $column->className = $classname;
            }
        }

        // columnas permitidas en la busqueda               
        foreach ($searchable as $key => $search) {
            if($value == $search){
                $column->searchable = true;
            }
        }

        // columnas que se pueden ordenar
        foreach ($orderable as $key => $order) {
            if($value == $order){
                $column->orderable = true;
            }
        }

        // columnas que se van a ocultar
        foreach ($hiddens as $key => $hidden) {
            if($value == $hidden){
                $column->visible = false;
            }
        }
            
        $columns_dt[] = $column;
    }

    return $columns_dt;

}


// papelera de usuarios
function dt_trashed()
{
    $columns = request()->input('columns');
    $items = [];
    foreach($columns as $column){
        array_push($items, $column['search']['value']);
    }
    $items = array_filter($items);
    $search = array_search('trashed', $items);
    if($search){
        return true;
    }

    return false;
}


// variables independientes de cada vista
function get_json_vars($name)
{

    $data = get_config_data($name);
    $data->columns_dt = get_columns_dt($name);
    $data->columns_order = get_columns_order($name);

    return 'var page_config = '. json_encode($data, JSON_PRETTY_PRINT) .';';

}


/* variables para las variaciones de los submit */
function get_submit_vars($type, $type_error, $redirect = false, $tiny = false)
{

    $vars = [
        'type_submit' => $type,
        'type_error' => $type_error,
        'redirect' => $redirect,
        'tiny' => $tiny
    ];

    /* convertir array en json */
    return 'var submit_config = '. json_encode($vars, JSON_PRETTY_PRINT) .';';

}


/* script para ejecutar el submit form via ajax */
function get_submit_ajax($type, $type_error, $redirect = false, $tiny = false)
{

    $tab = "\11";
    $break = "\12";

    $js = '$(document).ready(function(){'.$break;
    $js.= $tab.'$("#send_form").click( function(e){'.$break;
    $js.= $tab.$tab.'e.preventDefault();'.$break;
    $js.= $tab.$tab.'SendAjaxForm("'.$type.'", "'.$type_error.'", "'.$redirect.'", "'.$tiny.'");'.$break;
    $js.= $tab.'});'.$break;
    $js.= '});'.$break;

    return $js;

}

// zonas horarias
function generate_timezone_list()
{
    static $regions = array(
        DateTimeZone::AFRICA,
        DateTimeZone::AMERICA,
        DateTimeZone::ANTARCTICA,
        DateTimeZone::ASIA,
        DateTimeZone::ATLANTIC,
        DateTimeZone::AUSTRALIA,
        DateTimeZone::EUROPE,
        DateTimeZone::INDIAN,
        DateTimeZone::PACIFIC,
    );

    $timezones = array();
    foreach( $regions as $region )
    {
        $timezones = array_merge( $timezones, DateTimeZone::listIdentifiers( $region ) );
    }

    $timezone_offsets = array();
    foreach( $timezones as $timezone )
    {
        $tz = new DateTimeZone($timezone);
        $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
    }

    // sort timezone by offset
    asort($timezone_offsets);

    $timezone_list = array();
    foreach( $timezone_offsets as $timezone => $offset )
    {
        $offset_prefix = $offset < 0 ? '-' : '+';
        $offset_formatted = gmdate( 'H:i', abs($offset) );

        $pretty_offset = "UTC{$offset_prefix}{$offset_formatted}";

        $timezone_list[$timezone] = "({$pretty_offset}) $timezone";
    }

    return $timezone_list;
}


/* breadcrumbs */
function breadcrumb()
{

    $request = request();
    $segments = array_filter(array($request->segment(1), $request->segment(2), $request->segment(3)));
    foreach($segments as $key => $segment){
        $validdate = \DateTime::createFromFormat('m-d-Y', $segment);
        if(is_numeric($segment) || $validdate != ''){
            unset($segments[$key]);
        }
    }
    $base = $request->segment(1);
    $last_key = end($segments);

    $html = '<ol class="breadcrumb">';
    $html.= '<li class="breadcrumb-item">';
    $html.= '<a href="'.url('/').'">'.trans('base.home').'</a>';
    $html.= '<i class="fa fa-angle-right"></i>';
    $html.= '</li>';

    foreach($segments as $key => $segment){

        $number = is_numeric($segment);

        if(!$number){
                                    
            if($base == $segment){
                $url = $base;
            }else{
                $url = $base.'/'.$segment;
            }

            $html.= '<li class="breadcrumb-item">';
            $html.= '<a href="'.url($url).'">';
            $html.= trans($base.'.'.$segment);
            $html.= '</a>';

            if($segment != $last_key){
                $html.= '<i class="fa fa-angle-right"></i>';
            }
            $html.= '</li>';

        }
        
    }

    $html.= '</ol>';

    return $html;

}





// Imgen de fondo del login
function get_login_bg()
{

    if(request()->is('app/login') || request()->is('app/password/reset') || request()->is('app/password/reset/'.request()->route('token'))){
        return 'bg-login-image';
    }

    return false;
}


// identificar el hostname
function get_hostname()
{
    // obtener los datos del host
    $tenancy = app(\Hyn\Tenancy\Environment::class);
    $hostname = ($tenancy->hostname()) ? explode('.', $tenancy->hostname()->fqdn)[0] : null;

    // retornar el usuario
    return $hostname;

}  


// identificar un tenant pasando un subdomio
function get_tenant_by_host($hostname)
{
    $url = $hostname.'.'.config('app.url_base');
    $tenancy = \Hyn\Tenancy\Models\Hostname::where('fqdn', $url)->first();        
    return optional($tenancy->website)->uuid;
}


// identificar el id del tenant
function get_tenant_customer()
{
    // obtener los datos del host
    $tenancy = app(\Hyn\Tenancy\Environment::class);
    $tenancy_id = optional($tenancy->hostname())->id;
    $customer = \App\Models\System\CustomersHostname::where('hostname_id', $tenancy_id)->first();

    // retornar el usuario
    return ($customer->customer) ?? null;

}

// nombre de la base de datos
function get_database_name()
{
    $tenancy = app(\Hyn\Tenancy\Environment::class)->website();
    return $tenancy->uuid;
}


// fecha utc desde fecha local
function utc_datetime($date)
{
    $timezone = config('options.timezone');
    $format = config('options.datetime_format');

    $date = Carbon\Carbon::createFromFormat($format, $date, $timezone)
    ->setTimezone('UTC');

    return $date->format('Y-m-d H:i:s');
}


// convertir datetime a date
function datetime_to_date($datetime)
{    
    $timezone = config('options.timezone');
    $format = config('options.datetime_format');

    if($datetime){
        $date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $datetime, 'UTC')
        ->setTimezone($timezone)
        ->format('Y-m-d');  
    }else{
        $date = now()->format('Y-m-d');  
    }

    return $date;
}


// tamaños de imagenes
function image_sizes(){
    return [
        'thumbnail' => [
            'width' => 150,
            'height' => 150
        ],
        'medium' => [
            'width' => 300,
            'height' => null
        ],
        'medium_large' => [
            'width' => 768,
            'height' => null
        ],         
        'large' => [
            'width' => 1280,
            'height' => null
        ],
    ];
}

// Subir imagen
function upload_file($file, $folder, $resize = true)
{

    $filePath = '/'.get_database_name().'/'.$folder;

    $tmpname = md5($file->getClientOriginalName().time());
    $extension = $file->getClientOriginalExtension();
    $filename = $tmpname.'.'.$extension;

    Storage::disk('s3')->put($filePath.'/'.$filename, file_get_contents($file), 'public');

    // validar si es una imagen
    if(substr($file->getMimeType(), 0, 5) == 'image') {        
        foreach(image_sizes() as $key => $size){
            
            $thumb_name = $filePath.'/'.$tmpname.'_'.$key.'.'.$extension;

            $image = \Image::make($file->getRealPath());   
            $resize_image = $image->resize($size['width'], $size['height'], function($constraint) {
                $constraint->aspectRatio();
            });            

            // guardar en s3
            Storage::disk('s3')->put($thumb_name, $resize_image->stream()->detach(), 'public');

        }
    }

    return $filename;
}

// set upload file json encode
function upload_file_json($filename, $title)
{
    $image = [
        'image_url' => $filename,
        'image_title' => $title ?? null,
        'image_alt' => $title ?? null
    ];            
    return json_encode($image);
}


// Get S3 file
function get_s3_file($link)
{
    return \Storage::disk('s3')->url($link);
}

// img src
function get_image_src($image, $folder, $size = null, $class = false, $no_found = false)
{
    $data = json_decode($image);

    if(!$data){
        $data = json_decode(json_encode([
            'image_url' => ($no_found) ? $no_found : asset('images/no-picture.png'),
            'image_title' => null,
            'image_alt' => null,
            'image_local' => true
        ]));
    }

    $size = $size ? '_'.$size : null;
    $class = $class ? $class : 'img-fluid img-block';

    if(isset($data->image_local)){
        $html = '<img src="' . $data->image_url .'"';
    }else{
        $image_url = explode('.', $data->image_url);
        $file = get_database_name().'/'.$folder.'/'.$image_url[0].$size.'.'.$image_url[1];
        
        $html = '<img src="' . get_s3_file($file) .'"';
    }    
    
    // title
    if($data->image_title){
        $html.= ' title="' . $data->image_title .'"';
    }

    // alt
    if($data->image_alt){
        $html.= ' alt="' . $data->image_alt .'"';
    }

    $html.= ' class="'.$class.'"';
    $html.= ' />';
    
    return $html;
}
