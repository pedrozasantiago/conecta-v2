<?php

namespace App\Helpers;
use App\Models\Customer\Contact;
use App\Models\Customer\User;
use App\Models\Customer\Role;
use PublikBase;

class Selects {


    public static function get_pluck($type, $id = false)
    {

        $id = ($id) ? $id : 1;
        
        // lista de Clientes/Proveedores
        if($type == 'Contacts'){

            $list = Contact::where('active', 1)->where('type', $id)->get();
            $list->each(function ($model) { $model->setAppends(['full_name']); });
            $lists = $list->pluck('full_name','id')->all();

        // lista de Vendedores/Administradores/Empleados
        }else if($type == 'Users'){

            $list = Role::where('id', $id)->first()->users()->get();
            $list->each(function ($model) { $model->setAppends(['full_name']); });
            $lists = $list->pluck('full_name','id')->all();

        }

        

        return $lists;

    }


}