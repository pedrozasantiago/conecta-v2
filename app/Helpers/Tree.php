<?php 

namespace App\Helpers;

use App\Models\Customer\Option;

class Tree {

    private $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    // obtener el nivel superior
    public function itemsTree($byKey = 'name', $childKey = 'childs', $fields = false) {

        $result = array();
        foreach($this->items as $key => $item) {
            if ($item->parent_id == 0) {
                if($fields){
                    foreach($fields as $field){
                        $result[$item->{$byKey}][$field] = $item->{$field};
                    }
                }else{
                    $result[$item->{$byKey}] = $item;
                }
                $result[$item->{$byKey}][$childKey] = $this->itemWithChildren($item, $byKey, $childKey, $fields);
            }
        }
        return $result;
        
    }

    // niveles inferiores
    public function itemWithChildren($item, $byKey, $childKey, $fields) {
        $result = [];
        $children = $this->childrenOf($item);
        foreach ($children as $key => $child) {
            if($fields){
                foreach($fields as $field){
                    $result[$child->{$byKey}][$field] = $child->{$field};
                }
            }else{
                $result[$child->{$byKey}] = $child;
            }
            $result[$child->{$byKey}][$childKey] = $this->itemWithChildren($children[$key], $byKey, $childKey, $fields);
        }
        return $result;
    }

    // verificar si contiene niveles superiores
    public function childrenOf($item) {
        $result = array();
        foreach($this->items as $i) {
            if ($i->parent_id == $item->id) {
                $result[] = $i;
            }
        }
        return $result;
    }


    // renderizar como html > ul > li
    public function htmlList() {
      return $this->htmlFromArray($this->itemArray());
    }

    public function itemArray() {
      $result = array();
      foreach($this->items as $item) {
        if ($item->parent_id == 0) {
          $result[$item->name] = $this->itemWithChildren($item);
        }
      }
      return $result;
    }

    public function htmlFromArray($array) {
        $html = '';
        foreach($array as $k=>$v) {
            $html .= "<ul>";
            $html .= "<li>".$k."</li>";
            if(count($v) > 0) {
                $html .= $this->htmlFromArray($v);
            }
            $html .= "</ul>";
        }
        return $html;
    }

    /*public function itemWithChildren($item) {
        $result = array();
        $children = $this->childrenOf($item);
        foreach ($children as $child) {
            $result[$child->name] = $this->itemWithChildren($child);
        }
        return $result;
    }*/

    public function resetKeys( $items ) {
        $items = array_values( $items );
        $results = [];
        foreach($items as $key => $item){
            $results[$key]['id'] = $item['id'];
            $results[$key]['name'] = $item['name'];
            $results[$key]['children'] = $this->resetKeys($item['children']);
        }
        return $results;
    }   


}