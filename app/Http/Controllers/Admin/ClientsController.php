<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CreateTenant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\Customer;
use App\Models\Customer\User;
use App\Models\System\User as SystemUser;
use Artisan;
use Asana\Client as AsanaClient;
use Hyn\Tenancy\Environment;
use Hyn\Tenancy\Models\Website;
use Illuminate\Support\Facades\DB;

class ClientsController extends Controller {

    private $valid = false;
    private $ok = [];
    private $error = [];

    public function index() {
        return view("admin.clients.index");
    }

    public function store(Request $request) {
        $nit = $request->input('txtNit');
        $name_company = $request->input('txtNombre');
        $subdomain = $request->input('txtSubdomain');
        $prefix = $request->input('txtPrefix');

        $email = $request->input('txtCorreo');
        $next_billing_at = $request->input('txtFechaCobro');
        $ends_at = $request->input('txtFechaFin');

        $licence = $request->input('txtLicencias');
        $free_licence = $request->input('txtLicenciasGratis');


        $createTenant = new CreateTenant();

        $resp = $createTenant->handle($nit, $name_company, $subdomain, $prefix, $email , $next_billing_at , $ends_at , $licence, $free_licence);

        $this->valid = true;

        if(is_array($resp)){
            $this->valid = false;
            $this->error = $resp['error'] ?? "Error";
        }
        $message = ["Tenant creado correctamente"];
        
        if($this->valid){
            $asana = $this->createTaskAsana($request);    

            if($asana){
                array_push($message, "Asana creado correctamente");
            }
        }
        
        
        

        return response()->json([
                    "valid" => $this->valid,
                    "error" => $this->valid ? [] : [$this->error],
                    "OK" => $this->valid ? $message : [],
                    "data" => [],
        ]);
    }

    public function edit($id) {

//        $client = Customer::firstOrFail($id);
        // $client = new Client;
        // $client->firstOrFail($id);

        return view("admin.clients.index");
    }

    public function update(Request $request, Customer $client) {
        try {


            $client->update([
                'nit' => $request->input('txtNit'),
                'name_company' => $request->input('txtNit'),
                'nit' => $request->input('txtNit'),
                'name_company' => $request->input('txtNombre'),
                'subdomain' => $request->input('txtSubdomain'),
                'status' => $request->input('lstEstado'),
                'email' => $request->input('txtCorreo'),
                'next_billing_at' => $request->input('txtFechaCobro'),
                'ends_at' => $request->input('txtFechaFin'),
                'licencias' => $request->input('txtLicencias'),
                'licencias_gratis' => $request->input('txtLicenciasGratis')
            ]);

//           $client->save();

            array_push($this->ok, "Registro actualizado correctamente");
            $this->valid = true;
        } catch (Exception $e) {
            array_push($this->error, "No se pudo actualizar el registro", $e->getMessage());
            $this->valid = false;
        }


        return response()->json([
                    "valid" => $this->valid,
                    "error" => $this->error,
                    "OK" => $this->ok,
                    "data" => $client
        ]);
    }

    public function destroy(Request $request, Customer $client) {

        $msj = [];
        $msj['error'] = [];
        $msj['OK'] = [];
        $msj['customer'] = $client;

        try {

            $client->delete();
            array_push($msj['OK'], "Registro borrado correctamente");
        } catch (Exception $e) {
            array_push($msj['error'], "No se pudo borrar el registro");
            array_push($msj['error'], $e->getMessage());
        };

        return response()->json(
                        $msj
        );

        //
    }

    public function grid() {
        $grid = TmLista::all();

        return response()->json($grid);
    }

    public function clientes_frm(Request $request) {

//         return response()->json(
//         [
//         'request' => $request->all()
// ]
// ); 
//        $results = Customer::query();
//        if($id = $request->input('cliente_activo')) {
//         $results->where('id', $request->input('cliente_activo'));
// }

        $arreglo = Customer::where('id', $request->input('cliente_activo'))->withTrashed()->first();





        //    $arreglo = $results->first();
        // for ($i = 0; $i < count($arreglo); $i++) {
        //     array_push($msj['arreglo'], array_map("utf8_encode", $arreglo[$i]));
        // }


        return response()->json([
                    'arreglo' => $arreglo
        ]);
    }

    public function grid_ppal(Request $request) {

//        $results = Customer::query();
        $results = Customer::with(['hostnames.hostname.website']);
        //  dd($results->get());
        // $results = $results->with("hostname");
         // Acceder al atributo dinámico 'website'
        //  dd($results->with("hostname"));
        if ($name = $request->input('txtNombreBC')) {
            $results->where('name_company', 'LIKE', '%' . $name . '%');
        }

        if ($domain = $request->input('txtDominioBC')) {
            $results->where('domain', 'LIKE', '%' . $domain . '%');
        }

        $status = $request->input('lstEstadoCBC');
        if ($status != "") {
            if($status == 2){
                $results->onlyTrashed();
            }else{
                $results->where('status', 'LIKE', '%' . $status . '%');
            }
            
        }

        // $results->append(['users']);

        $arreglo = $results->get();

        foreach ($arreglo as $post) {
            $post['data'] = $post->getData();
        }

        // $arreglo->append('users');

        // dd($arreglo);

        // dd($arreglo);

        // for ($i = 0; $i < count($arreglo); $i++) {
        //     array_push($msj['arreglo'], array_map("utf8_encode", $arreglo[$i]));
        // }
        // foreach($arreglo as $key => $tenant){
        //     $arreglo[$key] =   $tenant->append('users');
        // }
        // $arreglo->each->append('users');


        return response()->json([
                    'arreglo' => $arreglo
        ]);
    }

    public function getUsers(Customer $client) {
        
        $users = $client->getUsers();
        
        return response()->json([
            'users' => $users
        ]);
        
    }

    public function getConfigs(Customer $client) {
        $configs = $client->getConfigs();

        return response()->json([
            'configs' => $configs
        ]);
    }


    public function createTaskAsana($request){
        try{

            $nit = $request->input('txtNit');
            $name_company = $request->input('txtNombre');
            $subdomain = $request->input('txtSubdomain');
      
    
            $email = $request->input('txtCorreo');
            $next_billing_at = $request->input('txtFechaCobro');
            $ends_at = $request->input('txtFechaFin');
    
            $licence = $request->input('txtLicencias');
            $free_licence = $request->input('txtLicenciasGratis');
    

            $client = AsanaClient::accessToken(env('ASANA_TOKEN'),);

            $result = $client->tasks->createTask(
                [
                    "name" => "Nuevo cliente ". $nit . " - " . $name_company,
                    "resource_subtype" => "default_task",
                    "approval_status" => "pending",
                    "assignee_status" => "upcoming",
                    "completed" => false,
                    "liked" => false,
                    "assignee" => env('ASANA_ASSIGNEE'),
                    "notes" => "Creado desde ADMIN",
                    "assignee_section" => env('ASANA_ASSIGNEE_SECTION'),
                    "custom_fields" => [
                        "1208812272459910" => $subdomain,
                        "1208812272459912" => $name_company,
                    ],
                    "projects" => [
                        env('ASANA_MAIN_PROJECT')
                    ],
                    "workspace" => "1202453996325960"
                ],
                [
                    "opt_pretty" => "true",
                    "headers" => [
                        "Asana-Disable" => "new_user_task_lists,new_goal_memberships",
                    ]
                ]
            );

            return true;

        }catch(\Exception $e){
            return false;
        }
    }

 
}
