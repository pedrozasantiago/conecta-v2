<?php

namespace App\Http\Controllers\Master\Configs;

use Illuminate\Http\Request;
use App\Http\Requests\Pos\EditSettingRequest;
use App\Models\Customer\Option;
use Hyn\Tenancy\Environment;
use App\Models\System\Customer;
use Illuminate\Support\Facades\Hash;

use Auth;
use App;
use DB;
use PublikButtons;
use PublikBase;
use \stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /* lista de clientes */
    public function index($id)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-customers')){
            abort(403);
        }

        // configuraciones necesarias
        $configs = $this->configs();

        // obtener el cliente
        $customer = Customer::findOrFail($id);

        // identificar la base de datos del tentant
        app(Environment::class)->tenant($customer->website);

        // cargar la vista de usuarios
        return view('master.configs.settings.index')
        ->with('customer', $customer)
        ->with('configs', $configs);
    }


    /* guardar nuevo registro en la db */
    public function store(EditSettingRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-customers')){
            abort(403);
        }

        $inputs = $request->all();
        
        // obtener el cliente
        $customer = Customer::findOrFail($id);

        // identificar la base de datos del tentant
        app(Environment::class)->tenant($customer->website);

        // obtener las configuraciones
        $options = Option::all();

        // lista de todas las opciones
        foreach ($options as $key => $option) {
            // variables enviadas en el formulario
            foreach ($inputs as $key_i => $input) {
                if($key_i == $option->name){
                    $getoption = Option::where('name', $option->name)->update(['value' => $input]);
                }
            }
        }

        // respuesta json
        return response()->json(['message' => trans('settings.update_success'), 'url' => url('admin/customers/config/settings/'.$customer->id), 'status' => 200]);
    }


    /* Config Json */
    public function configs()
    {

        // cargar las varibles necesarias para datatable
        $global = new \stdClass();
        $global->form_id = 'settings_submit';

        // convertir las variables en json
        return PublikBase::get_json_vars($global);

    }


}