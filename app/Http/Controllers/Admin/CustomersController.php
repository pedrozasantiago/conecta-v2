<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Master\CreateAdminCustomerRequest;
use App\Http\Requests\Master\EditAdminCustomerRequest;

use App\Models\Customer\User;
use App\Models\Customer\Role;
use App\Models\Customer\Option;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Environment;
use App\Models\System\Customer;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Illuminate\Support\Facades\Hash;

use Artisan;
use Auth;
use App;
use DB;
use PublikButtons;
use PublikBase;
use \stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    /* lista de clientes */
    public function index()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-customers')){
            abort(403);
        }

        return view('master.customers.index')
        ->with([
            'configs' => get_json_vars('master.customers')
        ]);
    }


    /* ver usuario */
    public function show($id)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-customers')){
            abort(403);
        }        

        // obtener el cliente
        $customer = Customer::findOrFail($id);        

        // cargar la vista de usuarios
        return view('master.customers.show')
        ->with('customer', $customer);

    }


    /* crear usuario */
    public function create()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-customers')){
            abort(403);
        }

        return view('master.customers.create')
        ->with([
            'configs' => get_json_vars('master.customers')
        ]);
    }

    /* guardar nuevo registro en la db */
    public function store(CreateAdminCustomerRequest $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-customers')){
            abort(403);
        }

        // setear datos
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $username = $request->input('username');
        $email = $request->input('email');
        $hostname = $request->input('hostname');
        $password = $request->input('password');

        // crear tenant
        Artisan::call('tenant:create', [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'hostname' => $hostname,
            'email' => $email,
            'password' => $password
        ]);
        $output = Artisan::output();

        // respuesta json
        return response()->json([
            'message' => trans('customers.create_success'),
            'url' => url('admin/customers'), 'status' => 200
        ]);
    }


    /* editar cliente */
    public function edit($id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-customers')){
            abort(403);
        }

        // obtener el cliente
        $customer = Customer::findOrFail($id);

        // configuraciones necesarias
        $configs = $this->configs();

        // show the edit form and pass the role
        return view('master.customers.edit')
        ->with([
            'customer' => $customer,
            'configs' => get_json_vars('master.customers')
        ]);
    }

    /* actualizar roles en la db */
    public function update(EditAdminCustomerRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-customers')){
            abort(403);
        }

        // obtener el cliente
        $customer = Customer::findOrFail($id); 
        $customer->update($request->all());

        // respuesta json
        return response()->json([
            'message' => trans('customers.edit_success'),
            'url' => url('admin/customers/'.$customer->id),
            'status' => 200
        ]);

    }


    /* eliminar un cliente */
    public function destroy(Request $request)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('delete-customers')){
            abort(403);
        }

        // obtener el cliente
        $customer = Customer::where('id', $request->input('id'))->firstOrFail();

        // eliminar tenant
        Artisan::call('tenant:delete', ['username' => $customer->username, 'force' => false]);
        $output = Artisan::output();

        // respuesta json
        return response()->json([
            'message' => trans('customers.destroy_success'),
            'status' => 200
        ]);

    }


    /* Lista de clientes con datatables */
    public function getCustomers(Request $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-customers')){
            abort(403);
        }

		return DataTables::of(Customer::query())
        ->addColumn('name', function ($customer) {
            return '<a href="'.url('admin/customers/'.$customer->id).'">'.$customer->full_name.'</a>';
        })
        ->addColumn('hostname', function ($customer) {
            return $customer->hostname->fqdn;
        })
        ->editColumn('id', function ($customer) {
            $buttons = '';
            if(Auth::guard('web_admin')->user()->can('update-customers')){
                $buttons.= PublikButtons::href_link('admin/customers/'.$customer->id.'/edit', trans('customers.edit'), 'fa fa-pencil-alt');
            }
            if(Auth::guard('web_admin')->user()->can('delete-customers')){
                $buttons.= PublikButtons::destroy_modal('customers', 'admin/customers', $customer->id, 'destroy_customer', 'ajax_delete');
            }

            return $buttons;
        })
        ->rawColumns(config('datatables_config.master.customers')['columns']['raw'])
		->make(true);
    }

}