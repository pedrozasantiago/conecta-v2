<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    /* Dashboard */
    public function index()
    {
        return view('master.dashboard');
    }


}
