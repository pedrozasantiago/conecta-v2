<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\Faq;
use Artisan;

class FaqsController extends Controller {

    public function index() {
        return view("admin.faqs.index");
    }

    public function store(Request $request) {
        dd("nuevo");
        return response()->json(
                        [
                            'request' => $request->all()
                        ]
        );
    }

    public function edit($id) {

//        $client = Customer::firstOrFail($id);
        // $client = new Client;
        // $client->firstOrFail($id);

        return view("admin.faqs.index");
    }

    public function update(Request $request, Faq $faq) {
        $msj = [];
        $msj['error'] = [];
        $msj['OK'] = [];
        $msj['faq'] = [];
        
        try{
            $faq->update($request->all());
            array_push($msj['OK'], "Registro actualizado correctamente");
        } catch (Exception $e) {
            array_push($msj['error'], "No se pudo actualizar el registro");
            array_push($msj['error'], $e->getMessage());
        };
 
        return response()->json(
                        $msj
        );
    }

    public function destroy(Request $request) {
        dd("borrar");
        //
    }

    public function grid() {
        $grid = TmLista::all();

        return response()->json($grid);
    }
 

    public function clientes_frm(Request $request) {

        $arreglo = Customer::where('id', $request->input('cliente_activo'))->first();


        return response()->json([
                    'arreglo' => $arreglo
        ]);
    }

    public function grid_ppal(Request $request) {

        $results = Faq::query();
//
        $results->where('software', 'LIKE', $request->modulo);

        $arreglo = $results->get();

        return response()->json([
                    'preguntas' => array_group_by($arreglo->toArray(), 'id_pantalla'),
        ]);
    }

    public function response(Request $request) {

        $arreglo = Faq::where('id', $request->input('lstPregunta'))->first();

        return response()->json([
                    'respuesta' => $arreglo
        ]);
    }

    public function uploadImages(Request $request) {


        $images = ($request->file()) ?? [];


        foreach ($images as $key => $image) {

            // crear la ruta de subida de las imagenes                
            $filePath = get_hostname() . '/faqs';

            // guardar en s3
            $result = Storage::disk('s3')->put($filePath, $image, 'public');
        }

        $url = Storage::disk('s3')->url($result);

        return response()->json(
                        [
                            'location' => $url,
                        ]
        );
    }

}
