<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    /* Inicio */
    public function index()
    {
        return redirect()->route('admin.dashboard');
    }

}
