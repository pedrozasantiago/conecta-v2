<?php

namespace App\Http\Controllers\Master;
use Illuminate\Http\Request;
use App\Http\Requests\Master\CreateAdminPermissionRequest;
use App\Http\Requests\Master\EditAdminPermissionRequest;
use Illuminate\Support\Str as Str;
use Spatie\Permission\Models\Permission;
use Auth;
use PublikButtons;
use PublikBase;
use \stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class PermissionsController extends Controller
{
    /* lista de permisos */
    public function index()
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-permissions')){
            abort(403);
        }

        // configuraciones necesarias
        $configs = $this->configs();

        // cargar la vista de permisos
        return view('master.permissions.index')
        ->with('configs', $configs);
    }


    /* ver permiso */
    public function show()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-permissions')){
            abort(403);
        }

    }


    /* crear permiso */
    public function create()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-permissions')){
            abort(403);
        }

        // lista de permisos padres
        $permissions = Permission::where('parent_id', 0)->pluck('display_name','id')->all();

        // renderizar la vista
        return view('master.permissions.create')
        ->with('permissions', $permissions);
    }

    /* guardar nuevo registro en la db */
    public function store(CreateAdminPermissionRequest $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-permissions')){
            abort(403);
        }

        /* si no hay errores insertar en la db */
        $permission = new Permission;
        $permission->create($request->all());

        return response()->json(['message' => trans('permissions.create_success'), 'status' => 200]);
    }


    /* editar permiso */
    public function edit($id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-permissions')){
            abort(403);
        }

        // get the role
        $permission = Permission::findOrFail($id);

        // lista de permisos padres
        $permissions = Permission::where('parent_id', 0)->pluck('display_name','id')->all();

        // show the edit form and pass the permissions
        return view('master.permissions.edit')
        ->with('permissions', $permissions)
        ->with('permission', $permission);
    }

    /* actualizar permisos en la db */
    public function update(EditAdminPermissionRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-permissions')){
            abort(403);
        }

        $permission = Permission::findOrFail($id);

        /* si no hay errores insertar en la db */
        $permission->update($request->all());

        return response()->json(['message' => trans('permissions.edit_success'), 'status' => 200]);

    }


    /* eliminar un permiso */
    public function destroy(Request $request)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('delete-permissions')){
            abort(403);
        }

        // eliminar el permiso
        Permission::destroy($request->input('id'));

        return response()->json(['message' => trans('permissions.destroy_success'), 'status' => 200]);
    }


    /* Lista de permisos con datatables */
    public function getPermissions(Request $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-permissions')){
            abort(403);
        }

        // datatables
    	$permissions = Permission::select($this->getColumns());
		return DataTables::of($permissions)
        ->editColumn('parent_id', function ($permission) {
                if($permission->parent_id == 0){
                    return '<span class="tag tag-success">'.trans('permissions.parent').'</span>';
                }else{
                    return '<span class="tag tag-warning">'.trans('permissions.child').'</span>';
                }
                
            })
        ->editColumn('id', function ($permission) {
                $buttons = '';
                if(Auth::guard('web_admin')->user()->can('update-permissions')){
                    $buttons.= PublikButtons::edit_modal('permissions', 'admin/permissions', $permission->id, 'md');
                }
                if(Auth::guard('web_admin')->user()->can('delete-permissions')){
                    $buttons.= PublikButtons::destroy_modal('permissions', 'admin/permissions', $permission->id, 'destroy_permission', 'ajax_delete');
                }

                return $buttons;
            })
        ->rawColumns(['parent_id','id'])
		->make(true);
    }

    /* columnas a usar en datatables */
    protected function getColumns()
    {
        return [
            'display_name',
            'name',
            'description',
            'parent_id',
            'id',
        ];
    }


    /* Config Json */
    public function configs()
    {

        // columns datatable
        $columns = PublikBase::get_columns_dt($this->getColumns());

        // cargar las varibles necesarias para datatable
        $global = new \stdClass();
        $global->ajax_route = route('admin.permissions.ajax');
        $global->table_id = 'permissions';
        $global->form_id = 'permissions_submit';
        $global->delete_id = 'destroy_permission';

        /* sweetalert config */
        $global->text_alert = trans('permissions.text_alert');
        $global->text_confirm_delete = trans('permissions.confirm_delete');
        $global->delete_success = trans('permissions.delete_success');

        /* datatable js vars */
        $global->columns_dt = $columns;

        // convertir las variables en json
        return PublikBase::get_json_vars($global);

    }


}