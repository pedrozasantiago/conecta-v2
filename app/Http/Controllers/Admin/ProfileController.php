<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Master\EditAdminProfileRequest;
use App\Models\System\User;
use Spatie\Permission\Models\Role;
use Auth;
use PublikBase;
use \stdClass;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /* perfil */
    public function index()
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-profile')){
            abort(403);
        }

        $user = User::findOrFail(Auth::guard('web_admin')->user()->id);

        // configuraciones necesarias
        $configs = $this->configs();

        // cargar la vista de usuarios
        return view('master.profile.index')
        ->with('user', $user)
        ->with('configs', $configs);
    }

    /* actualizar perfil en la db */
    public function update(EditAdminProfileRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-profile')){
            abort(403);
        }

        // obtener el usiario actual
        $user = Auth::guard('web_admin')->user();

        // verificar que el id que se envio es el mismo del usuario
        if($user->id != $id){
            return redirect()->back()->with('error', trans('profile.error_id'));
        }

        $inputs = $request->all();

        $file = $request->file('image');
        if($file){
            $destinationPath = public_path('/images/profile/');
            $filename = $user->id.'.'.$file->getClientOriginalExtension();

            // guardar imagen en la carpeta
            $file->move($destinationPath, $filename);

            $inputs['image'] = $filename;
        }

        /* si no hay errores insertar en la db */
        $user->update($inputs);

        return response()->json(['message' => trans('profile.update_success'), 'url' => url('admin/profile'), 'status' => 200]);

    }


    /* Config Json */
    public function configs()
    {
        // cargar las varibles necesarias para datatable
        $global = new \stdClass();
        $global->form_id = 'profile_submit';

        // convertir las variables en json
        return PublikBase::get_json_vars($global);
    }


}