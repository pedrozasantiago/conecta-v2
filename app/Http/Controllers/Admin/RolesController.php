<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Master\CreateAdminRoleRequest;
use App\Http\Requests\Master\EditAdminRoleRequest;
use Illuminate\Support\Str as Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;
use PublikButtons;
use PublikBase;
use PublikTree;
use \stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    /* lista de roles */
    public function index()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-roles')){
            abort(403);
        }

        // configuraciones necesarias
        $configs = $this->configs();

        // cargar la vista de roles
        return view('master.roles.index')
        ->with('configs', $configs);
    }


    /* ver rol */
    public function show()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-roles')){
            abort(403);
        }

    }


    /* crear rol */
    public function create()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-roles')){
            abort(403);
        }

        // renderizar la vista
        return view('master.roles.create');
    }

    /* guardar nuevo registro en la db */
    public function store(CreateAdminRoleRequest $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-roles')){
            abort(403);
        }

        /* si no hay errores insertar en la db */
        $role = new Role;
        $role->create($request->all());

        return response()->json(['message' => trans('roles.create_success'), 'status' => 200]);
        
    }


    /* editar rol */
    public function edit($id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-roles')){
            abort(403);
        }

        // get the role
        $role = Role::findOrFail($id);

        // show the edit form and pass the role
        return view('master.roles.edit')
        ->with('role', $role);
    }

    /* actualizar roles en la db */
    public function update(EditAdminRoleRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-roles')){
            abort(403);
        }

        $role = Role::findOrFail($id);

        /* si no hay errores insertar en la db */
        $role->update($request->all());

        return response()->json(['message' => trans('roles.edit_success'), 'status' => 200]);

    }


    /* permisos del rol */
    public function permissions($id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-roles')){
            abort(403);
        }

        // get the role
        $role = Role::findOrFail($id);

        // lista de permisos en forma de arbol
        $perms_lists = new PublikTree(Permission::all());
        $permissions = $perms_lists->itemsTree();

        // show the edit form and pass the role
        return view('master.roles.permissions')
        ->with('permissions', $permissions)
        ->with('role', $role);
    }


    /* cambiar permiso del role */
    public function perm(Request $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-roles')){
            abort(403);
        }

        // obtener el rol
        $role = Role::findOrFail($request->input('role_id'));

        // verificar si vamos a agregar o quitar un permiso
        if($request->input('checked') == 1){
            // agregar el permiso
            $role->givePermissionTo($request->input('perm_id'));
            $message = trans('roles.permision_attach_success');
        }else{
            // quitar el permiso
            $role->revokePermissionTo($request->input('perm_id'));
            $message = trans('roles.permision_detach_success');
        }

        return response()->json(['message' => $message, 'status' => 200]);
        
    }


    /* eliminar un rol */
    public function destroy(Request $request)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('delete-roles')){
            abort(403);
        }

        // eliminar el rol
        Role::destroy($request->input('id'));

        return response()->json(['message' => trans('roles.destroy_success'), 'status' => 200]);
    }


    /* Lista de roles con datatables */
    public function getRoles(Request $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-roles')){
            abort(403);
        }

        // datatables
    	$roles = Role::select($this->getColumns());
		return DataTables::of($roles)
        ->editColumn('display_name', function ($role){
                return '<a href="'.url('admin/roles/'.$role->id.'/permissions').'">'.$role->display_name.'</a>';
            })
        ->editColumn('id', function ($role) {
                $buttons = '';
                if(Auth::guard('web_admin')->user()->can('update-roles')){
                    $buttons.= PublikButtons::edit_modal('roles', 'admin/roles', $role->id, 'md');
                }
                if(Auth::guard('web_admin')->user()->can('delete-roles')){
                    $buttons.= PublikButtons::destroy_modal('roles', 'admin/roles', $role->id, 'destroy_role', 'ajax_delete');
                }

                return $buttons;
            })
        ->rawColumns(['display_name', 'id'])
		->make(true);
    }

    /* columnas a usar en datatables */
    protected function getColumns()
    {
        return [
            'display_name',
            'name',
            'description',
            'id',
        ];
    }


    /* Config Json */
    public function configs()
    {

        // columns datatable
        $columns = PublikBase::get_columns_dt($this->getColumns());

        // cargar las varibles necesarias para datatable
        $global = new \stdClass();
        $global->ajax_route = route('admin.roles.ajax');
        $global->table_id = 'roles';
        $global->form_id = 'roles_submit';
        $global->delete_id = 'destroy_role';

        /* sweetalert config */
        $global->text_alert = trans('roles.text_alert');
        $global->text_confirm_delete = trans('roles.confirm_delete');
        $global->delete_success = trans('roles.delete_success');

        /* datatable js vars */
        $global->columns_dt = $columns;

        // convertir las variables en json
        return PublikBase::get_json_vars($global);

    }


}