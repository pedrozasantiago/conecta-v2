<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Master\CreateAdminUserRequest;
use App\Http\Requests\Master\EditAdminUserRequest;
use Illuminate\Support\Str as Str;
use App\Models\System\User;
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use PublikButtons;
use PublikBase;
use \stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /* lista de usuarios */
    public function index()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-users')){
            abort(403);
        }

        // configuraciones necesarias
        $configs = $this->configs();

        // cargar la vista de usuarios
        return view('master.users.index')
        ->with('configs', $configs);
    }


    /* ver usuario */
    public function show()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-users')){
            abort(403);
        }
    }


    /* crear usuario */
    public function create()
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-users')){
            abort(403);
        }

        // lista de roles
        $roles = Role::pluck('display_name','id')->all();

        // renderizar la vista
        return view('master.users.create')
        ->with('roles', $roles);
    }

    /* guardar nuevo registro en la db */
    public function store(CreateAdminUserRequest $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('create-users')){
            abort(403);
        }

        /* si no hay errores insertar en la db */
        $user = new User;
        $user->fill($request->all());
        $user->save();

        // asignar role
        $user->assignRole($request->input('role_id'));

        return response()->json(['message' => trans('users.create_success'), 'status' => 200]);
    }


    /* editar usuario */
    public function edit($id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-users')){
            abort(403);
        }

        // get the user
        $user = User::with('roles')->findOrFail($id);

        // lista de roles
        $roles = Role::pluck('display_name','id')->all();

        // show the edit form and pass the role
        return view('master.users.edit')
        ->with('user', $user)
        ->with('roles', $roles);
    }

    /* actualizar roles en la db */
    public function update(EditAdminUserRequest $request, $id)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('update-users')){
            abort(403);
        }

        $user = User::findOrFail($id);        

        // eliminar role anterior
        $user->removeRole($user->roles->first()->id);

        // asignar role
        $user->assignRole($request->input('role_id'));

        /* si no hay errores insertar en la db */
        $user->update($request->all());

        return response()->json(['message' => trans('users.edit_success'), 'status' => 200]);

    }


    /* eliminar un usuario */
    public function destroy(Request $request)
    {
        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('delete-users')){
            abort(403);
        }

        // eliminar el usuario
        User::destroy($request->input('id'));

        return response()->json(['message' => trans('users.destroy_success'), 'status' => 200]);
    }


    /* Lista de usuarios con datatables */
    public function getUsers(Request $request)
    {

        // verificar si tiene permisos
        if(!Auth::guard('web_admin')->user()->can('read-users')){
            abort(403);
        }

        // datatables
    	$users = User::with('roles')->select($this->getColumns());
		return DataTables::of($users)
        ->addColumn('full_name', function ($user) {
                return $user->full_name;
            })
        ->addColumn('role', function ($user) {
                $role = $user->roles->first();
                return $role['display_name'];
            })
        ->editColumn('id', function ($user) {
                $buttons = '';
                if(Auth::guard('web_admin')->user()->can('update-users')){
                    $buttons.= PublikButtons::edit_modal('users', 'admin/users', $user->id, 'md');
                }
                if(Auth::guard('web_admin')->user()->can('delete-users')){
                    $buttons.= PublikButtons::destroy_modal('users', 'admin/users', $user->id, 'destroy_user', 'ajax_delete');
                }

                return $buttons;
            })
        ->rawColumns(['id'])
		->make(true);
    }

    /* columnas a usar en datatables */
    protected function getColumns()
    {
        return [
            'first_name',
            'last_name',
            'username',
            'email',
            'id',
        ];
    }


    /* Config Json */
    public function configs()
    {

        // columns datatable
        $column = array('full_name', 'username', 'email', 'role', 'id');
        $columns = PublikBase::get_columns_dt($column);

        // cargar las varibles necesarias para datatable
        $global = new \stdClass();
        $global->ajax_route = route('admin.users.ajax');
        $global->table_id = 'users';
        $global->form_id = 'users_submit';
        $global->delete_id = 'destroy_user';

        /* sweetalert config */
        $global->text_alert = trans('users.text_alert');
        $global->text_confirm_delete = trans('users.confirm_delete');
        $global->delete_success = trans('users.delete_success');

        /* Column orders */
        $order = array('column' => '1', 'dir' => 'asc');
        
        /* datatable js vars */
        $global->columns_dt = $columns;
        $global->columns_order = $order;

        // convertir las variables en json
        return PublikBase::get_json_vars($global);

    }


}