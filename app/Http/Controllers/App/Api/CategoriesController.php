<?php

namespace App\Http\Controllers\Pos\Api;

use Illuminate\Http\Request;
use App\Models\Customer\ProductsCategory;
use Auth;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /* lista de categorias */
    public function index($parent_id = false)
    {

        // si esta vacio el padre colocarle 0
        $parent_id = ($parent_id) ? $parent_id : 0;
        $vars = $parent_id;

        // obtener las categorias
        $categories = ProductsCategory::where('parent_id', $parent_id)->get();

        // respuesta json
        return $categories;
    }


}