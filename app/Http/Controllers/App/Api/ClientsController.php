<?php

namespace App\Http\Controllers\Pos\Api;

use Illuminate\Http\Request;
use App\Models\Customer\Contact;
use Auth;
use DB;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
    /* lista de clientes */
    public function index()
    {

        // obtener los clientes
        $clients = Contact::select('*', DB::raw('CONCAT(last_name, ", ", first_name) AS full_name'))
        ->where('type', 1)->where('active', 1)
        ->get();

        // respuesta json
        return $clients;
    }


}