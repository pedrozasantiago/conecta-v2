<?php

namespace App\Http\Controllers\Pos\Api;

use Illuminate\Http\Request;
use App\Models\Customer\PaymentMethod;
use Auth;
use App\Http\Controllers\Controller;

class PaymentMethodsController extends Controller
{
    /* lista de metodos de pago */
    public function index()
    {
        return PaymentMethod::get();
    }


}