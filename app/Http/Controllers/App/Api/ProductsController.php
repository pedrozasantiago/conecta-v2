<?php

namespace App\Http\Controllers\Pos\Api;

use Illuminate\Http\Request;
use PublikProducts;
use Auth;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /* lista de productos */
    public function index()
    {
        return PublikProducts::productsvariants();
    }


}