<?php

namespace App\Http\Controllers\Pos\Api;

use Illuminate\Http\Request;
use App\Models\Customer\Store;
use Auth;
use App\Http\Controllers\Controller;

class StoresController extends Controller
{
    /* lista de tiendas */
    public function index()
    {
        return Store::where('status', 1)->get();
    }


}