<?php

namespace App\Http\Controllers\Pos\Auth;

use Illuminate\Http\Request;
use App\Http\Requests\Pos\EditProfileRequest;
use App\Models\Customer\User;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Auth;
use PublikBase;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:read-profile',   ['only' => ['index']]);
        $this->middleware('permission:update-profile',   ['only' => ['update']]);
    }

    /* perfil */
    public function index()
    {
        return view('pos.profile.index')
        ->with([
            'user' => Auth::user()
        ]);
    }

    /* actualizar perfil en la db */
    public function update(EditProfileRequest $request, User $profile)
    {

        $file = $request->file('image');
        $inputs = $request->all();
        if($file){

            // crear la ruta de subida de las imagenes
            $filePath = '/'.get_database_name().'/images/profile/';

            // generar el nombre de la imagen
            $filename = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

            // guardar la imagen
            Storage::disk('s3')->put($filePath.$filename, file_get_contents($file), 'public');

            $inputs['image'] = $filename;
        }

        $profile->update($inputs);

        return response()->json([
            'message' => trans('profile.update_success'),
            'url' => url('app/profile'),
            'status' => 200
        ]);

    }

    /* Eliminar logo */
    public function remove_image(Request $request)
    {
        $user = Auth::user();
    
        Storage::disk('s3')->delete(PublikBase::get_s3_image($user->image, false, 'images/profile', false));
            
        $user->update(['image' => null]);
    
        return redirect()->back()->with('error', trans('profile.image_removed_success'));
    }

}