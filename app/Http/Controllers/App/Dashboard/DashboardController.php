<?php

namespace App\Http\Controllers\Pos\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str as Str;
use App\Models\Customer\Product;
use App\Models\Customer\ProductsCategory;
use App\Http\Resources\WooCommerce\CategoryCollection;
use App\Http\Resources\WooCommerce\Category as CategoryResource;
use \stdClass;
use Woocommerce;
use Yajra\Datatables\Facades\Datatables;

class DashboardController extends Controller
{

    /* Dashboard */
    public function index()
    {
        return view('pos.dashboard.index');
    }


    // mercadolibre
    public function mercadolibre()
    {
        $product = Product::all();
        $product = $product->first();

        // get user data
        $user = app('meli_api')->setAccessToken('APP_USR-5023174239311608-051905-d4ccd1d268cd3215a020868bb6eda4d7-421786298');
        $user_data = $user->userShow('421786298');

        // site_id: MCO
        $categories = $user->categoryList($user_data->site_id); 
        
        // predecir categoria
        $category = app('meli_api')->categoryPredict($user_data->site_id, $product->name);


        $item = new \Zephia\MercadoLibre\Entity\Item;
        $item->title = $product->name;
        $item->description = $product->desc;
        $item->price = $product->price;
        $item->currency_id = 'COP';
        $item->available_quantity = $product->inventories->sum('quantity');
        $item->category_id = $category->id;
        $item->site_id = $user_data->site_id;        
        dd($item);

        //crear producto
        $product = $user->itemCreate($item);

        dd($product);

        // user_id: id
        $products = $user->itemList($user_data->id);

        dd($products);
    }


    // woocommerce api
    public function woocommerce()
    {
        $product = Product::all();
        $product = $product->first();

        $params = [
            'per_page' => 25,
            'page' => 5
        ];

        //$orders = Woocommerce::get('orders', $params);

        $categories = $this->apiCategories();
        //$categories = $this->diffCategories();
        dd($categories);
        $category = Woocommerce::post('products/categories/batch', $categories);
        dd($category);

        //dd($product);
        //dd(Woocommerce::get('products/44'));

        // datos del producto
        $data = [
            'name' => $product->name,
            'sku' => ($product->sku) ?? $product->barcode,
            'type' => $product->getProductType('woocommerce'),
            'regular_price' => $product->price,
            'description' => $product->desc,
            'manage_stock' => ($product->has_inventory) ? true : false,
            'stock_quantity' => $product->inventories->sum('quantity'),
            'categories' => [
                [
                    'id' => 9
                ],
                [
                    'id' => 14
                ]
            ],
            'images' => [
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
                ],
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg'
                ]
            ]
        ];
        
        $item = Woocommerce::post('products', $data);

        dd($item);
    }


    // categorias locales
    public function localCategories()
    {

        $categories = ProductsCategory::where('status', 1)->get();
        $create = [];
        foreach($categories as $category){
            if($category->id != 1){
                $data = [
                    'name' => $category->name,
                    //'description' => $category->desc,
                ];
                array_push($create, $data);
            }            
        }
        $create_categories['create'] = $create;

        return $create;

    }



    // categorias woocommerce
    public function apiCategories()
    {
        $categories = Woocommerce::get('products/categories');
        $create = [];
        foreach($categories as $category){
            if($category['name'] != 'Sin categorizar'){
                $data = [
                    'name' => $category['name'],
                    //'description' => $category['description'],
                ];
                array_push($create, $data); 
            }           
        }
        $create_categories['create'] = $create;

        return $create;

    }

    // categorias sin sincronizar
    public function diffCategories()
    {

        // categorias locales
        $local = $this->localCategories();
        
        // woocommerce categories
        $api = $this->apiCategories();
        
        $diff_categories = [];
        foreach($local as $category){
            foreach($api as $apicategory){
                if($category['name'] != $apicategory['name']){
                    $diff_categories[$category['name']] = $category;
                }
            }
        }

        $categories['create'] = array_values($diff_categories);

        return $categories;
    
    }


}
