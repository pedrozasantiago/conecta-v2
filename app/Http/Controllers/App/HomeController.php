<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    /* Inicio */
    public function index($file)
    {   
//        return redirect()->route('app.login');
        return view("app.crm.$file.index",['menu' => config('datatables_config.app.menu')]);
    }

}
