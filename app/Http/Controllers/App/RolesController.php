<?php

namespace App\Http\Controllers\Pos;

use Illuminate\Http\Request;
use App\Http\Requests\Pos\CreateRoleRequest;
use App\Http\Requests\Pos\EditRoleRequest;
use App\Models\System\Permission;
use App\Models\Customer\Role;
use Auth;
use PublikButtons;
use PublikTree;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:create-roles', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-roles', ['only' => ['index', 'show', 'getRoles']]);
        $this->middleware('permission:update-roles', ['only' => ['edit', 'update', 'permissions', 'perm']]);
        $this->middleware('permission:delete-roles', ['only' => ['destroy']]);
    }

    /* lista de roles */
    public function index()
    {
        return view('pos.roles.index');
    }


    /* ver rol */
    public function show()
    {

    }


    /* crear rol */
    public function create()
    {
        return view('pos.roles.create');
    }

    /* guardar nuevo registro en la db */
    public function store(CreateRoleRequest $request)
    {

        Role::create($request->all());        

        return response()->json([
            'message' => trans('roles.create_success'),
            'status' => 200
        ]);
  
    }


    /* editar rol */
    public function edit(Role $role)
    {
        return view('pos.roles.edit')
        ->with([
            'role' => $role
        ]);
    }

    /* actualizar rol en la db */
    public function update(EditRoleRequest $request, Role $role)
    {

        $role->update($request->all());

        return response()->json([
            'message' => trans('roles.edit_success'),
            'status' => 200
        ]);

    }


    /* permisos del rol */
    public function permissions(Role $role)
    {

        $permissions = new PublikTree(Permission::all());

        return view('pos.roles.permissions')
        ->with([
            'role' => $role,
            'permissions' => $permissions->itemsTree()
        ]);
    }


    /* cambiar permiso del role */
    public function perm(Request $request)
    {        

        // obtener el rol
        $inputs = $request->all();
        $role = Role::where('uuid', $inputs['role_id'])->firstOrFail();
        $permission = Permission::where('uuid', $inputs['perm_id'])->firstOrFail();

        // verificar si vamos a agregar o quitar un permiso
        if($inputs['checked'] == 1){
            $role->attachPermission($permission->id);
        }else{
            $role->detachPermission($permission->id);
        }

        return response()->json([
            'message' => trans('roles.permision_success'),
            'status' => 200
        ]);

    }


    /* eliminar un rol */
    public function destroy(Request $request)
    {

        Role::where('uuid', $request->input('id'))->delete();

        return response()->json([
            'message' => trans('roles.destroy_success'),
            'status' => 200
        ]);
    }


    /* Lista de roles con datatables */
    public function getRoles(Request $request)
    {

		return DataTables::of(Role::query())
        ->editColumn('display_name', function ($role){
            if($role->id == 1 || $role->id == 2){
                $name = trans('roles.'.$role->name);
            }else{
                $name = $role->display_name;
            }
            return '<a href="'.url('app/roles/'.$role->uuid.'/permissions').'">'.$name.'</a>';
        })
        ->editColumn('description', function ($role){
            if($role->id == 1 || $role->id == 2){
                $desc = trans('roles.'.$role->name.'-desc');
            }else{
                $desc = $role->description;
            }
            return $desc;
        })
        ->editColumn('id', function ($role) {
          
            $buttons = '';
            
            if($role->id != 1 && $role->id != 2){
            
                // si tiene permisos para editar un rol
                if(Auth::user()->can('update-roles')){                
                    $buttons.= PublikButtons::edit_modal('roles', 'app/roles', $role->uuid, 'md');
                }

                // si tiene permisos para eliminar un rol
                if(Auth::user()->can('delete-roles')){
                    $buttons.= PublikButtons::destroy_modal('roles', 'app/roles', $role->uuid, 'destroy_role', 'ajax_delete');
                }

            }

            return $buttons;
        })
        ->rawColumns(config('datatables_config.pos.roles')['columns']['raw'])
		->make(true);
    }


}