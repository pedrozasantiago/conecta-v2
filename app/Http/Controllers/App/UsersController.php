<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Requests\Pos\CreateUserRequest;
use App\Http\Requests\Pos\EditUserRequest;
use App\Http\Requests\Pos\EditUserLoginDataRequest;
use App\Models\Customer\User;
use App\Models\Customer\Role;
use App\Models\Customer\Store;
use App\Models\Customer\UsersStore;
use Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:create-users', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-users', ['only' => ['index', 'show', 'getUsers']]);
        $this->middleware('permission:update-users', ['only' => ['edit', 'update', 'login_data', 'update_login_data']]);
        $this->middleware('permission:delete-users', ['only' => ['destroy']]);
    }

    /* lista de usuarios */
    public function index()
    {
        return view('pos.users.index')
        ->with([
            'title' => 'users'
        ]);
    }


    /* ver usuario */
    public function show()
    {

    }


    /* crear usuario */
    public function create()
    {
        return view('pos.users.create')
        ->with([
            'title' => 'users',
            'roles' => Role::whereNotIn('name', ['seller'])->pluck('display_name','id')->all(),
            'stores' => Store::pluck('name','id')->all()
        ]);
    }

    /* guardar nuevo registro en la db */
    public function store(CreateUserRequest $request)
    {

        $inputs = $request->all();

        if(!isset($inputs['store_id'])){
            $inputs['store_id'] = array();
        }

        /* si no hay errores insertar en la db */
        $user = new User;
        $user->fill($inputs);
        $user->save();

        // asignar role
        $user->attachRole($inputs['role_id']);

        // asignar las tiendas
        $stores = $inputs['store_id'];
        foreach ($stores as $key => $store) {
            UsersStore::create([
                'store_id' => $store,
                'user_id' => $user->id,
                'active' => 0
            ]);
        }

        return response()->json([
            'message' => trans('users.create_success'),
            'status' => 200
        ]);
    }


    /* editar usuario */
    public function edit(User $user)
    {
        return view('pos.users.edit')
        ->with([
            'title' => 'users',
            'user' => $user,
            'store_selected' => PublikBase::users_stores('Selected', $user->id),
            'roles' => Role::whereNotIn('name', ['seller'])->pluck('display_name','id')->all(),
            'stores' => Store::pluck('name','id')->all()
        ]);
    }

    /* actualizar usuario en la db */
    public function update(EditUserRequest $request, User $user)
    {

        $inputs = $request->all();

        if(!isset($inputs['store_id'])){
            $inputs['store_id'] = array();
        }

        $user->update($inputs);

        // eliminar role anterior
        $user->detachRole($user->roles->first()->id);

        // asignar role
        $user->attachRole($inputs['role_id']);

        // eliminar tiendas anteriormente asignadas
        UsersStore::where('user_id', $user->id)->delete();

        // asignar las tiendas
        $stores = $inputs['store_id'];
        foreach ($stores as $key => $store) {
            UsersStore::create([
                'store_id' => $store,
                'user_id' => $user->id,
                'active' => 0
            ]);
        }

        // respuesta json
        return response()->json([
            'message' => trans('users.edit_success'),
            'status' => 200
        ]);

    }


    /* cambiar usuario y clave */
    public function login_data($title, User $user)
    {
        return view('pos.users.login-data')
        ->with([
            'title' => $title,
            'user' => $user
        ]);
    }
    
    /* actualizar datos de personas en la db */
    public function update_login_data(EditUserLoginDataRequest $request, User $user)
    {

        $user->update($request->all());
                
        return response()->json([
            'message' => trans('users.edit_login_data_success'),
            'status' => 200
        ]);
    
    }


    /* eliminar un usuario */
    public function destroy(Request $request)
    {

        User::where('uuid', $request->input('id'))->delete();

        return response()->json([
            'message' => trans('users.destroy_success'),
            'status' => 200
        ]);
    }


    /* Lista de usuarios con datatables */
    public function getUsers(Request $request)
    {

        // datatables
        $users = Role::whereNotIn('name', ['seller'])->first()->users()->get();
		return DataTables::of($users)
        ->addColumn('full_name', function ($user) {
            return $user->full_name;
        })
        ->addColumn('role', function ($user) {
            $role = $user->roles->first();
            return $role['display_name'];
        })
        ->editColumn('id', function ($user) {
            $buttons = '';
            
            // si tiene permisos para editar usuarios
            if(Auth::user()->can('update-users')){

                $buttons.= PublikButtons::url_modal('users.update-login-data', 'app/users/login-data/users/'.$user->uuid, 'edit btn-tables', 'fa fa-key', null, 'md');

                $buttons.= PublikButtons::edit_modal('users', 'app/users', $user->uuid, 'md');

            }

            // si tiene permisos para eliminar usuarios
            if(Auth::user()->can('delete-users')){
                $buttons.= PublikButtons::destroy_modal('users', 'app/users', $user->uuid, 'destroy_user', 'ajax_delete');
            }

            return $buttons;
        })
        ->rawColumns(config('datatables_config.pos.users')['columns']['raw'])
		->make(true);
    }


}