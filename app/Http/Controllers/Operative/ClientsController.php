<?php

namespace App\Http\Controllers\Operative;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\Customer;
use Artisan;

class ClientsController extends Controller {

    private $ok = [];
    private $error = [];

    public function index() {
        return view("operative.clients.index");
    }

    public function store(Request $request) {
        $nit = $request->input('txtNit');
        $name_company = $request->input('txtNombre');
        $subdomain = $request->input('txtSubdomain');
        $prefix = $request->input('txtPrefix');


        // crear tenant
        Artisan::call('tenant:create', [
            'nit' => $nit,
            'name_company' => $name_company,
            'hostname' => $subdomain,
            'prefijo' => $prefix
        ]);

        $output = [];

        array_push($output, Artisan::output());
//$output = Artisan::output();

        return response()->json([
                    "error" => [],
                    "OK" => ["Tenant creado correctamente"],
                    "data" => [],
                        ]);
    }

    public function edit($id) {

//        $client = Customer::firstOrFail($id);
        // $client = new Client;
        // $client->firstOrFail($id);

        return view("operative.clients.index");
    }

    public function update(Request $request, Customer $client) {
        try {
            $client->nit = $request->input('txtNit');
            $client->name_company = $request->input('txtNombre');
            $client->subdomain = $request->input('txtSubdomain');
            $client->status = $request->input('lstEstado');

            $client->save();

            array_push($this->ok, "Registro actualizado correctamente");
        } catch (Exception $e) {
            array_push($this->error, "No se pudo actualizar el registro", $e->getMessage());
        }


        return response()->json([
                    "error" => $this->error,
                    "OK" => $this->ok,
                    "data" => $client
                        ]);
    }

    public function destroy(Request $request, Customer $client) {

        $msj = [];
        $msj['error'] = [];
        $msj['OK'] = [];
        $msj['customer'] = $client;

        try {

            $client->delete();
            array_push($msj['OK'], "Registro borrado correctamente");
        } catch (Exception $e) {
            array_push($msj['error'], "No se pudo borrar el registro");
            array_push($msj['error'], $e->getMessage());
        };

        return response()->json(
                        $msj
        );

        //
    }

    public function grid() {
        $grid = TmLista::all();

        return response()->json($grid);
    }

    public function clientes_frm(Request $request) {

//         return response()->json(
//         [
//         'request' => $request->all()
// ]
// ); 
//        $results = Customer::query();
//        if($id = $request->input('cliente_activo')) {
//         $results->where('id', $request->input('cliente_activo'));
// }

        $arreglo = Customer::where('id', $request->input('cliente_activo'))->first();





        //    $arreglo = $results->first();
        // for ($i = 0; $i < count($arreglo); $i++) {
        //     array_push($msj['arreglo'], array_map("utf8_encode", $arreglo[$i]));
        // }


        return response()->json([
                    'arreglo' => $arreglo
        ]);
    }

    public function grid_ppal(Request $request) {
        
        $results = Customer::query();

        if ($name = $request->input('txtNombreBC')) {
            $results->where('name_company', 'LIKE', '%' . $name . '%');
        }

        if ($domain = $request->input('txtDominioBC')) {
            $results->where('domain', 'LIKE', '%' . $domain . '%');
        }


        $arreglo = $results->get();


        // for ($i = 0; $i < count($arreglo); $i++) {
        //     array_push($msj['arreglo'], array_map("utf8_encode", $arreglo[$i]));
        // }


        return response()->json([
                    'arreglo' => $arreglo
        ]);
    }

}
