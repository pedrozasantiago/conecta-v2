<?php

namespace App\Http\Controllers;


use SevenSpan\WhatsApp\WhatsApp;
use Illuminate\Http\Request;

class WhatsappController extends Controller
{
    public function index($to){
        $response = new WhatsApp();
        // $response = $response->getPhoneNumbers();
        $components = $this->setComponents();
        // $to = "573002149224"; // Phone number with country code where we want to send message(Required).
        $templateName = "hello_world"; // Template name of your template (Required).
        $languageCode = "en_us"; // Template language code of your template (Required).
        


        $response = $response->sendTemplateMessage( $to,  $templateName,  $languageCode);

        
        
        dd($response);
        return false;
    }

    public function getPhoneNumbers($WhatsAppBusinessAccountId = ""){
        $whatsappObject = new WhatsApp();
        
        $response = $whatsappObject->getPhoneNumbers($WhatsAppBusinessAccountId);
        dd($response);
    }


    private function setComponents(){

       return [
            [
                'type' => 'header',
                'parameters' => [
                    [
                        'type' => 'image',
                        'image' => [
                        'link' => 'http(s)://URL',
                        ]
                    ]
                ]
            ],
            [
                'type' => 'body',
                'parameters' =>[
                    [
                        'type' => 'text',
                        'text' => 'TEXT_STRING',
                    ],
                    [
                        'type' => 'currency',
                        'currency' => [
                        'fallback_value' => 'VALUE',
                        'code' => 'USD',
                        'amount_1000' => 100,
                        ]
                    ],
                    [
                        'type' => 'date_time',
                        'date_time' => [
                        'fallback_value' => 'MONTH DAY, YEAR',
                        ]
                    ],
                ],
            ],
            [
                'type' => 'button',
                'sub_type' => 'quick_reply',
                'index' => '0',
                'parameters' => [
                    [
                        'type' => 'payload',
                        'payload' => 'PAYLOAD',
                    ],
                ],
            ]
        ];

    }
}
