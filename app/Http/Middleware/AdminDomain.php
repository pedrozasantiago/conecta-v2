<?php

namespace App\Http\Middleware;

use Closure;

//Auth Facade
use Auth;

class AdminDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {

          // validar si es un subdominio
          /*$subdomain = explode('.', $request->getHost());

          // si es un subdomio redireccionar
          if(count($subdomain)>1 && $subdomain[0] != 'www') {

              // quitar el subdomio
              unset($subdomain[0]);
              $domain = implode('.', $subdomain);

              $redirect = str_replace($request->getHost(), $domain, $request->url());

              return redirect($redirect);
          }*/

          return $next($request);
     }
}
