<?php

namespace App\Http\Middleware;

use Closure;
use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Config;

class EnforceTenancy
{
    public function handle($request, Closure $next)
    {

    	$config = config();
    	$url_base = $config['app']['url_base'];

        // asignar el nombre de la base de datos a laratrust
        $tenancy = app(Environment::class);
        $database = ($tenancy->tenant()->uuid) ?? null;
        
        // si no existe el tenant enviar un error
        if(!$tenancy->tenant()){
            abort(404);
        }

//        // configs
//        config(['laratrust.tables.teams' => $database.'.teams']);
//        config(['laratrust.tables.roles' => $database.'.roles']);
//        config(['laratrust.tables.permission_role' => $database.'.permission_role']);
//        config(['laratrust.tables.permission_user' => $database.'.permission_user']);
//        config(['laratrust.tables.role_user' => $database.'.role_user']);

    	// si no tiene un hostname enviar error 404
    	if($request->getHost() === $url_base){
    		abort(404);
    	}
    	
    	// usar la base de datos del cliente
        Config::set('database.default', 'tenant');
        return $next($request);

    }
}