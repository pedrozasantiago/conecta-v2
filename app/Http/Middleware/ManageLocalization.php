<?php

namespace App\Http\Middleware;

use Closure, Session, PublikBase;

class ManageLocalization
{

    protected $languages = ['en','es'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $lang = PublikBase::get_option('active_language');    
        /*if(!Session::has('userLang'))
        {
            Session::put('userLang', $request->getPreferredLanguage($this->languages));
        }*/
        //app()->setLocale(Session::get('userLang'));
        app()->setLocale($lang);

        return $next($request);
    }
}
