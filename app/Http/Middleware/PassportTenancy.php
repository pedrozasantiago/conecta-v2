<?php

namespace App\Http\Middleware;

use Closure;
use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Config;

class PassportTenancy
{
    public function handle($request, Closure $next)
    {
        	
        Config::set('database.default', 'tenant');
        
        return $next($request);

    }
}