<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateAdminCustomerRequest extends FormRequest
{


    public function __construct(Request $request)
    {
        $baseUrl = config('app.url_base');
        $request->request->add(['hostname' => $request->input('fqdn')]);
        $request->request->add(['fqdn' => $request->input('fqdn').'.'.$baseUrl]);
        $request->request->add(['username' => $request->input('username')]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [            
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:customers',
            'email' => 'required|unique:customers',
            'fqdn' => 'required|unique:hostnames',
            'password' => 'required|confirmed'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            $this->response($validator->errors()->getMessages())
        );
    }


    // respuesta json
    public function response(array $errors)
    {

        if ($this->expectsJson()) {
            return new JsonResponse(['errors' => $errors, 'status' => 422]);
        }

        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}