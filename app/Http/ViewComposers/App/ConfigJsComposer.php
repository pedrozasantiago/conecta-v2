<?php

namespace App\Http\ViewComposers\Pos;

use Illuminate\Contracts\View\View;
use Route;

class ConfigJsComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([

            'config_pos_profile' => get_json_vars('pos.profile'),

            // config
            'config_pos_users' => get_json_vars('pos.users'),
            'config_pos_roles' => get_json_vars('pos.roles'),
        ]);
    }

}