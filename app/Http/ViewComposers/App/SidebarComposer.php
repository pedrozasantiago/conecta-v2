<?php

namespace App\Http\ViewComposers\Pos;

use Illuminate\Contracts\View\View;
use App\Models\System\SidebarTenant;
use Route;

class SidebarComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {   
        $view->with([
            'sidebar_tenant' => $this->sidebar_tenant(),
            'active_url' => Route::currentRouteName()
        ]);
    }

    public function sidebar_tenant(){
        $menu = new SidebarTenant;
        return $menu->items();
    }


}