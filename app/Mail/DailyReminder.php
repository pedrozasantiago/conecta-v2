<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $process;
    public $type_process;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$process,$type_process,$link)
    {
        $this->user = $user;
        $this->process = $process;
        $this->type_process = $type_process;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.daily_reminder')
                ->with([
                    'userName' => $this->user->nombre ?? "Sin nombre",
                    'process' => $this->process ?? [],
                    'type_process' => $this->type_process ?? "Vacio",
                    'link' => $this->link
                ])
                ->subject('Recordatorio diario de '.$this->type_process);
    }
}
