<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ScheduleReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $schedule;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $schedule)
    {
        $this->user = $user;
        $this->schedule = $schedule;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.schedule_reminder')
        ->subject('Recordatorio de horario')
        ->with([
            'userName' => $this->user->nombre ?? "Sin nombre",
            'dia' => $this->schedule->dia,
            'horaEntrada' => $this->schedule->hora_entrada,
            'horaSalida' => $this->schedule->hora_salida,
        ]);
    }
}
