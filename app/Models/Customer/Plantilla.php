<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 

 

// plantillas
class Plantilla extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
    'tipo', 'texto'
    ];
    
    public $timestamps = false;
    
}
