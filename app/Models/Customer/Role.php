<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Laratrust\Models\LaratrustRole;
use App\Traits\Uuids;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends LaratrustRole
{
	use UsesTenantConnection;
	use SoftDeletes;
    //use Uuids;
	
	protected $fillable = [
		'uuid',
		'name',
		'display_name',
		'description'
	];

	// cambiar id por uuid
    public function getRouteKeyName() {
        return 'uuid';
	}
	
	public static function boot()
    {
        
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
            $model->name = Str::slug($model->display_name);
		});

        static::saving(function ($model) {
            $model->name = Str::slug($model->display_name);
        });

    }
}
