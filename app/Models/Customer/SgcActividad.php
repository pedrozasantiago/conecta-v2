<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// sgc_actividades
class SgcActividad extends Model
{
    use UsesTenantConnection;
    
      protected $table = 'sgc_actividades';
    
    protected $fillable = [
    'ACTIVIDAD_ID', 'DESCRIPCION', 'SECUENCIA', 'TIPO', 'ACTIVO', 
    ];
    
    public $timestamps = false;
    
}
