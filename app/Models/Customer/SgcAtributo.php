<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 



// sgc_atributos 
class SgcAtributo extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
   'atributo', 'descrip', 'descrip_ingles', 'orden', 'grupo', 
        'lista', 'orden_grp', 'estado', 'tipo_campo', 'obligatorio', 'tipo', 'sistema'
    ];
    
    public $timestamps = false;
    
}
