<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcCampana extends Model
{
    use UsesTenantConnection;
    use HasFactory;

    protected $table = 'sgc_campana';

    protected $fillable = [
        'ID', 'CAMPANA_ID', 'DESCRIPCION', 'TIPO', 'ESTADO', 'ANO', 'COLOR_ALERTA', 'USUARIO', 'MAQUINA', 'FECHA_MOD', 'FECHA_INICIO', 'FECHA_FIN', 'FECHA_ALERTA', 'OBSERVACIONES', 'destinatarios'
    ];

    public $timestamps = false;
}
