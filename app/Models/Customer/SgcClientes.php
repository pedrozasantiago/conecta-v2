<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// tm_listas
class SgcClientes extends Model
{
    use UsesTenantConnection;
    
    //   protected $primaryKey = 'your_key_name'; // or null
    public $incrementing = false;

    protected $primaryKey = 'CEDULA';

    protected $keyType = 'string';

    
    protected $fillable = [
        'TIPO_ID',
        'CEDULA',
        'id_externo',
        'NOMBRE',
        'TIPO_CLIENTE',
        'asesor_id',
        'user',
        'ESTADO',
        'ESTADO_INFO',
        'CATEGORIA',
        'ORIGEN',
        'CARGA',
        'password',
        'tracking',
        'FECHA_VERIFICACION',
        'OBSERVACIONES',
        'USUARIO',
        'MAQUINA',
        'FECHA_MOD',
        'fecha_creacion',
    ];
    
    public $timestamps = false;
    
}


//  protected $table = 'tm_pantallas';