<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcCotizaTarifario extends Model
{
    use UsesTenantConnection;
    use HasFactory;

    protected $table = 'sgc_cotiza_tarifario';

    protected $fillable = [
        'id', 'estado', 'cotizacion_id', 'version', 'id_tarifario', 'id_proveedor', 'pais_origen', 'ciudad_origen', 'pais_destino', 'ciudad_destino', 'vigencia'
    ];

    public $timestamps = false;

}
