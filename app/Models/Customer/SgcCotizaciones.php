<?php

namespace App\Models\Customer;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class SgcCotizaciones extends Model
{
    use HasFactory;
    use UsesTenantConnection;

    protected $table = 'sgc_cotizaciones';

    protected $primaryKey = 'COTIZACION_ID';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'COTIZACION_ID',
        'CAMPANA_ID',
        'ASESOR_ID',
        'CLIENTE_ID',
        'PROVEEDOR_ID',
        'FECHA_INGRESO',
        'ESTADO',
        'MONEDA',
        'PROFILE',
        'OFICINA',
        'MOTIVO_RECHAZO',
        'ARCHIVO',
        'ARCH_INFO',
        'DESCRIPCION',
        'DESTINO',
        'ORIGEN',
        'incoterm',
        'contacto',
        'cargo_contacto',
        'consecutivo',
        'USUARIO',
        'MAQUINA',
        'FECHA_MOD',
        'FECHA_INS',
        'SUBTIPO_OPER',
        'SERVICIO',
        'chk_origen',
        'chk_seguro',
        'chk_flete',
        'chk_destino',
        'chk_aduana',
        'chk_terrestre',
        'chk_condiciones',
        'do_asociado',
        'idioma',
        'CONSIGNATARIO_ID',
        'SHIPPER_ID',
        'NOTIFICADOR_ID',
        'shipping_observaciones',
        'contacto_id',
        'version',
        'tarifario_id',
    ];
 
}
