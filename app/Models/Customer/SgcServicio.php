<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// sgc_servicios 
class SgcServicio extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
    'descripcion', 'descripcion_ingles', 'activo', 'venta', 'moneda_venta', 
        'impuesto', 'tipo_servicio', 'compra', 'moneda_compra', 'notas', 'notas_internas', 
        'creado_por', 'fecha_creacion', 'modificado_por', 'fecha_modificacion', 'orden'
    ];
    
    public $timestamps = false;
    
}
