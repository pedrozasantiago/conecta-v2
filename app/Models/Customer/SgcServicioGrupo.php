<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// sgc_servicios_grupo
class SgcServicioGrupo extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'sgc_servicios_grupos';
    
    protected $fillable = [
    'id_grupo', 'id_servicio'
    ];
    
    public $timestamps = false;
    
}
