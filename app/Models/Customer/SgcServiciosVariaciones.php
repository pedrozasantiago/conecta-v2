<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcServiciosVariaciones extends Model
{
    use HasFactory;

    protected $table = 'sgc_servicios_variaciones';

    protected $fillable = [
        'id_servicio',
        'nombre',
        'modificado_por',
        'fecha_mod',
        'creado_por',
        'fecha_creacion',
        'orden'
    ];

    public $timestamps = false; // Desactivamos los timestamps por defecto, ya que manejas tus propios campos de fecha
}
