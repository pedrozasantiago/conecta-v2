<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcTarifarioProv extends Model
{
    use UsesTenantConnection;
    use HasFactory;

    protected $table = 'sgc_tarifario_prov';

    protected $fillable = [
        'id', 'estado', 'tipo', 'id_tarifario', 'id_proveedor', 'pais_origen', 'ciudad_origen', 'pais_destino', 'ciudad_destino', 'vigencia', 'id_lista'
    ];

    public $timestamps = false;

}
