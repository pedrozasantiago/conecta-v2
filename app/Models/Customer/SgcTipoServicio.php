<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// sgc_tipo_servicios
class SgcTipoServicio extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
    'descripcion', 'descripcion_ingles', 'activo', 'orden'
    ];
    
    public $timestamps = false;
    
}
