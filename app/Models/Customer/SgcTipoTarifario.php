<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcTipoTarifario extends Model
{
    use HasFactory;

    protected $table = 'sgc_tipo_tarifario';

    protected $fillable = [
        'nombre',
        'subtipo',
        'servicio',
        'estado'
    ];

    public $timestamps = false; // Si no tienes columnas created_at o updated_at
}
