<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcTipoTarifarioAdicionales extends Model
{
    use HasFactory;

    protected $table = 'sgc_tipo_tarifario_adicionales';

    protected $fillable = [
        'id_tarifario',
        'nombre',
        'estado',
        'tipo',
        'orden',
        'modificado_por',
        'fecha_mod',
        'creado_por',
        'fecha_creacion'
    ];

    public $timestamps = false; // Las fechas son manejadas por campos específicos
}
