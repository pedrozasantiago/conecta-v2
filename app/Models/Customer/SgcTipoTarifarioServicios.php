<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SgcTipoTarifarioServicios extends Model
{
    use HasFactory;

    protected $table = 'sgc_tipo_tarifario_servicios';

    protected $fillable = [
        'id_tarifario',
        'id_servicio',
        'id_variacion',
        'estado',
        'orden',
        'modificado_por',
        'fecha_mod',
        'creado_por',
        'fecha_creacion'
    ];

    public $timestamps = false; // Las fechas son manejadas por campos específicos

    // Definir la clave única compuesta
    protected $primaryKey = null;
    public $incrementing = false;
    protected $keyType = 'string';
    
    // Reglas de la clave única
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->timestamps = false; // Deshabilitar timestamps automáticos
        });
    }
}
