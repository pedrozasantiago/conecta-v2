<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 






// sgc_trm
class SgcTrm extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'sgc_trm';
    
    public $incrementing = false;
    
    protected $fillable = [
    'moneda', 'valor', 'estado', 'usuario', 'fecha'
    ];
    
    public $timestamps = false;
    
}
