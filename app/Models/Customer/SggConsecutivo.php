<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 

// sgg_consecutivos
class SggConsecutivo extends Model
{
    use UsesTenantConnection;
    
    public $incrementing = false;
    
    protected $fillable = [
    'TIPO', 'CODIGO', 'ANO', 'SECUENCIA', 'PRE'
    ];
    
    public $timestamps = false;
    
}

