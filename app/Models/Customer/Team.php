<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Laratrust\Models\LaratrustTeam;

class Team extends LaratrustTeam
{
    use UsesTenantConnection;
}
