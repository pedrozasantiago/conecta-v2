<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// tm_actividades_oper
class TmActividadesOper extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'tm_actividades_oper';
    
    protected $fillable = [
    'descripcion', 'secuencia', 'tipo', 'subtipo_oper', 
        'servicio', 'incoterm', 'estado_oper', 'activo', 'row_num'
    ];
    
    public $timestamps = false;
    
}
