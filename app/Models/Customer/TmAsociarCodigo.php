<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// tm_asociar_codigos
class TmAsociarCodigo extends Model
{
    use UsesTenantConnection;
    
    public $incrementing = false;
    
    protected $fillable = [
    'tipo', 'codigo', 'codasoc', 'grupo', 'orden', 'estado'
    ];
    
    public $timestamps = false;
    
}
