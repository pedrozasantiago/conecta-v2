<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 



// tm_atributos
class TmAtributo extends Model
{
    use UsesTenantConnection;
    
    public $incrementing = false;
    
    protected $fillable = [
    'atributo', 'descrip','descrip_ingles', 'orden', 'grupo', 'lista', 'orden_grp', 'estado', 
        'tipo', 'obligatorio', 'tipo_campo', 'sistema', 'reporte'
    ];
    
    public $timestamps = false;
    
}

