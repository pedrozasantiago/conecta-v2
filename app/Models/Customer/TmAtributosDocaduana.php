<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 




// tm_atributos_docaduana
class TmAtributosDocaduana extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'tm_atributos_docaduana';
    
    protected $fillable = [
    'proveedor_id', 'ciudad', 'descripcion', 'estado', 'creado_por', 'fecha_creacion', 'modificado_por', 'fecha_modificacion'
    ];
    
    public $timestamps = false;
    
}
