<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 



// tm_consecutivo
class TmConsecutivo extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'tm_consecutivo';
    
    public $incrementing = false;
    
    protected $fillable = [
    'SERVICIO', 'CODIGO_SERV', 'ANO', 'PRE', 'SECUENCIA'
    ];
    
    public $timestamps = false;
    
}
