<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmCuadroContable extends Model
{
    use HasFactory;
    use UsesTenantConnection;

    protected $table = 'tm_cuadro_contable';

    public $timestamps = false;

    protected $fillable = [
        'cotizacion_id',
        'do_id',
        't_pago',
        'tipo_ingreso',
        'concepto',
        'id_grupo',
        'variacion',
        'proveedor_id',
        'aplica_por',
        'unidad',
        'tarifa_venta',
        'tipo_venta',
        'minima',
        'subtotal_venta',
        'monedaventa',
        'trmventa',
        'tasa_impuesto',
        'venta_total',
        'venta_total_mn',
        'impuesto',
        'impuesto_mn',
        'subtotal_venta_mn',
        'fv',
        'fecha_fv',
        'minimacompra',
        'tarifa_compra',
        'tipo_compra',
        'compra',
        'monedacompra',
        'trmcompra',
        'compra_mn',
        'fc',
        'fecha_fc',
        'comision',
        'valor_comision',
        'valor_comision_mn',
        'opcion',
        'cotizacion_obs',
        'do_obs',
        'usuario',
        'maquina',
        'fecha_mod',
        'attached',
    ];

    protected $casts = [
        'unidad' => 'double',
        'tarifa_venta' => 'double',
        'minima' => 'double',
        'subtotal_venta' => 'double',
        'trmventa' => 'double',
        'tasa_impuesto' => 'double',
        'venta_total' => 'double',
        'venta_total_mn' => 'double',
        'impuesto' => 'double',
        'impuesto_mn' => 'double',
        'subtotal_venta_mn' => 'double',
        'minimacompra' => 'double',
        'tarifa_compra' => 'double',
        'compra' => 'double',
        'trmcompra' => 'double',
        'compra_mn' => 'double',
        'comision' => 'double',
        'valor_comision' => 'double',
        'valor_comision_mn' => 'double',
    ];
}