<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmDo extends Model
{
    use HasFactory;
    use UsesTenantConnection;

    protected $table = 'tm_do';

    protected $primaryKey = 'DO';

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'ID',
        'DO',
        'CONTENEDOR',
        'CLIENTE_ID',
        'CLIENTE',
        'TIPO',
        'SUBTIPO_OPER',
        'ESTADO',
        'ESTADO_INFO',
        'FECHA',
        'FECHA_INGRESO',
        'SERVICIO_ID',
        'SERVICIO',
        'MES_CO',
        'OFICINA',
        'ANO',
        'MONEDA',
        'CONTROL_ID',
        'FECHA_ALERTA',
        'FECHA_CIERRE',
        'REGISTRADO_CO',
        'COLOR_ALERTA',
        'ASESOR',
        'ASESOR_ID',
        'AGENTE',
        'AGENTE_ID',
        'INCOTERM',
        'PEDIDO',
        'PROVEEDOR',
        'BL_CONSIGNATARIO',
        'CONSIGNATARIO_ID',
        'SHIPPER1',
        'BL_SHIPPER',
        'BL_NOTIFY',
        'LT_ESTIMADO',
        'FECHA_BITACORAF',
        'FECHA_FACTURA',
        'PROFILE_CONTABLE',
        'PROFILE_ESTIMADO',
        'FECHA_BITACORAI',
        'NOTIFICADOR',
        'FLETE_COBRO',
        'FECHA_ZARPE',
        'FECHA_PROG',
        'REGISTRO_ACT',
        'CONSECUTIVO',
        'PROVAGENTE',
        'NOM_CLIENTE',
        'PLACA',
        'PROVTRANSP',
        'ORIGEN',
        'DESTINO',
        'FLETE_PREPAGADO',
        'HIJO',
        'ADICIONAL',
        'MASTER',
        'PESO',
        'VOL_LARGO',
        'VOL_ANCHO',
        'CANT_CAJAS',
        'CANT_CONTENEDORES',
        'VOL_TOTAL_VOL',
        'VOL_ALTO',
        'CANT_BULTOS',
        'CANT_NO_CONTENEDORES',
        'MONTA_CARGUE',
        'AEREA',
        'IMO',
        'MONTA_DESCARGUE',
        'MOTONAVE',
        'PRODUCTO',
        'SUCURSAL',
        'UN',
        'USUARIO',
        'DIGITALIZADO',
        'ASUNTO_STATUS',
        'VIAJE',
        'COMERCIAL',
        'OBSERVACIONES',
        'MAXBIT',
        'MIGRADO',
        'MAQUINA',
        'FECHA_MOD',
        'arch_infogral',
        'coordinador',
        'COTIZACION',
        'fletes',
        'valor',
    ];
}
