<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

// pantallas
class TmPantalla extends Model
{
    use UsesTenantConnection;

//  protected $table = 'tm_pantallas';
    protected $fillable = [
        'cod', 'nombre', 'descripcion', 'orden'
    ];
    public $timestamps = false;
    
}