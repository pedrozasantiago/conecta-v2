<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;

// tm_permisos
class TmPermiso extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
        'cod', 'pantalla', 'id_tab', 'nombre_tab', 'nombre', 'descripcion'
    ];
    
    public $timestamps = false;
    
}

