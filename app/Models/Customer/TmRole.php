<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// tm_roles
class TmRole extends Model
{
    use UsesTenantConnection;
    
    protected $fillable = [
    'nombre', 'descripcion', 'maquina', 'usuario'
    ];
    
    public $timestamps = false;
    
}
