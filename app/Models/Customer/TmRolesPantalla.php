<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 


// tm_roles_pantalla
class TmRolesPantalla extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'tm_roles_pantalla';
    
    public $incrementing = false;
    
    protected $fillable = [
    'rol', 'pantalla', 'valor', 'usuario', 'maquina', 'fecha'
    ];
    
    public $timestamps = false;
    
}
