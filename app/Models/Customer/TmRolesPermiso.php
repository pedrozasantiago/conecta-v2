<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 

// tm_roles_permisos
class TmRolesPermiso extends Model
{
    use UsesTenantConnection;
    
    public $incrementing = false;
    
    protected $fillable = [
    'id_rol', 'id_permiso', 'valor', 'usuario', 'maquina', 'fecha'
    ];
    
    public $timestamps = false;
    
}

