<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;
 

 

// tm_trayectos
class TmTrayecto extends Model
{
    use UsesTenantConnection;
    
    public $incrementing = false;
    
    protected $fillable = [
    'PAIS', 'CIUDAD', 'ESTADO', 'USUARIO', 'MAQUINA', 'FECHA_MOD'
    ];
    
    public $timestamps = false;
    
}
