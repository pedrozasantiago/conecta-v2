<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmUsersSchedule extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'tm_users_schedule';
    
    protected $fillable = [
        'user_id',
        'dia',
        'hora_entrada',
        'hora_salida',
        'creado_el'
    ];
    
    public $timestamps = false;

}
