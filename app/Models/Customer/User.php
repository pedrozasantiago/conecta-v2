<?php

namespace App\Models\Customer;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Hash;
use App\Support\Database\CacheQueryBuilder;
use PublikBase;
use Illuminate\Support\Str;
use App\Traits\Uuids;
use App\Traits\Versioning;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use UsesTenantConnection;
    use LaratrustUserTrait;
    use Notifiable;
    // use HasApiTokens;
    use CacheQueryBuilder;
    use SoftDeletes;
    
    protected $table = 'tm_users';

    protected $primaryKey = 'susuario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cedula',
        'nombre',
        'susuario',
        'correo',
        'cargo',
        'telefono',
        'perfil',
        'rol',
        'coordinador',
        'comercial',
        'adtivo',
        'pricing',
        'whatsapp',
        'slogin',
        'password',
        'api_token',
    ];
                    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ]; 

    // encriptar la contraseña
    public function setPasswordAttribute($password){
        if($password != ''){
            $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
        }
    }

    // obtener el nombre completo
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    // guardar api token    
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->api_token = Str::random(60);
        });
    }

}
