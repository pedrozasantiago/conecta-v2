<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingDocument extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

	protected $fillable = [
        'uuid',
        'name',
        'code',
        'prefix',
        'cufe_algorithm',
        'country_id'
    ];

}
