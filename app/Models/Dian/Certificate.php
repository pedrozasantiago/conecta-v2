<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

	protected $fillable = [
        'uuid',
        'customer_id',
        'certificate',
        'password'
    ];

    // findorfail por uuid
    public function getRouteKeyName() {
        return 'uuid';
    }

    // cliente
    public function customer()
    {
        return $this->belongsTo(\App\Models\System\Customer::class);
    }

}
