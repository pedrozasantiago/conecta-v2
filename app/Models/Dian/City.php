<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

// Modelo de ciudades
class City extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'name',
		'code',
		'state_id'
	];

	// departamento relacionado con la ciudad
	public function state()
	{
		return $this->belongsTo(App\Models\Dian\State::class);
	}

	// pais relacionado con la ciudad
	public function country()
	{
		return $this->hasManyThrough(
			App\Models\Dian\Country::class,
			App\Models\Dian\State::class
		);
	}
	
}
