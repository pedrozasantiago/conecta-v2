<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyType extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

    protected $table = 'companies_types';
	protected $fillable = [
        'uuid',
        'name',
        'code',
        'country_id'
    ];

}
