<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

// Modelo de paises
class Country extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'iso2',
		'short_name',
		'long_name',
		'iso3',
		'numcode',
		'un_member',
		'calling_code',
		'cctld',
		'active'
	];
 
}
