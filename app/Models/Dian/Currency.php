<?php

namespace App\Models\Dian;

use PublikTree;
use Hyn\Tenancy\Traits\UsesSystemConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Support\Str as Str;
use Illuminate\Database\Eloquent\SoftDeletes;

// monedas
class Currency extends Model
{
    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;
    
	protected $fillable = [
        'uuid',
        'symbol',
        'currency',
        'description',
        'isdefault',
        'active',
        'rate'
    ];
    public $timestamps = false;
    
}
