<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentType extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

	protected $fillable = [
        'uuid',
        'name',
        'short_name',
        'code',
        'country_id'
    ];

}
