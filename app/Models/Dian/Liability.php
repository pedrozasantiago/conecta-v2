<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Liability extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

	protected $fillable = [
        'uuid',
        'name',
        'code',
        'country_id'
    ];

}
