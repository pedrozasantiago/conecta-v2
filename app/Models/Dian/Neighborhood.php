<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

// Modelo de barrios
class Neighborhood extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;
	
	protected $fillable = [
		'uuid',
		'name',
		'code',
		'polygon',
		'city_id'
	];

	// departamento relacionado con la ciudad
	public function city()
	{
		return $this->belongsTo(App\Models\Dian\City::class);
	}
	
}
