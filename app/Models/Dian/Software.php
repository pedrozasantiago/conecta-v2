<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Software extends Model
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;

    protected $table = 'dian_software';
	protected $fillable = [
        'uuid',
        'customer_id',
        'identifier',
        'test',
        'pin',
        'url'
    ];

    // findorfail por uuid
    public function getRouteKeyName() {
        return 'uuid';
    }

    // cliente
    public function customer()
    {
        return $this->belongsTo(\App\Models\Dian\Customer::class);
    }

}
