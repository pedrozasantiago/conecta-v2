<?php

namespace App\Models\Dian;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

// Modelo de departamentos
class State extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'name',
		'code',
		'country_id'
	];

	// pais relacionado con el departamento
	public function country()
	{
		return $this->belongsTo(\App\Models\Dian\Country::class);
	}

	// ciudades del departamento
	public function cities()
	{
		return $this->hasMany(\App\Models\Dian\City::class);
	}
	
}
