<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use PublikTree;
use Illuminate\Database\Eloquent\SoftDeletes;


// sidebar tenant
class SidebarTenant extends Model
{
    use UsesSystemConnection;
    use CacheQueryBuilder;

    protected $fillable = [
        'name',
        'url',
        'permission',
        'parent_id',
        'order',
        'active',
        'params',
    ];


    public function items()
    {
        $menu = $this->where('active', 1)
        ->orderby('parent_id')
        ->orderby('order')
        ->orderby('name')
        ->get();

        $items = new PublikTree($menu);
        return $items->itemsTree();
    }
        
}

// sidebar tenant
class SidebarAdmin extends Model
{
    use UsesSystemConnection;
    use CacheQueryBuilder;

    protected $fillable = [
        'name',
        'url',
        'permission',
        'parent_id',
        'order',
        'active',
        'params',
    ];

    public function items()
    {
        $menu = $this->where('active', 1)
        ->orderby('parent_id')
        ->orderby('order')
        ->orderby('name')
        ->get();

        $items = new PublikTree($menu);
        return $items->itemsTree();
    }
        
}

// ajustes
class Option extends Model
{
    use UsesSystemConnection;

	protected $fillable = ['name', 'value'];
    public $timestamps = false;
}

