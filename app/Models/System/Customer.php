<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Environment;
use \App\Models\Customer\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// clientes
class Customer extends Model {

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;
    
//    protected $primaryKey = "id";
    protected $appends = ['fqdn','hostname','website'];
    
    protected $fillable = [
        'uuid',
        'nit',
        'name_company',
        'ends_at',
        'next_billing_at',
        'email',
        'licencias',
        'licencias_gratis',
        'status'
    ];

    // hostnames
    public function hostnames() {
        return $this->hasOne(CustomersHostname::class);
    }
    
    // obtener el fqdn
    public function getFqdnAttribute() {
        return $this->hostnames->hostname->fqdn;
    }

    // obtener el hostname
    public function getHostnameAttribute() {
        return $this->hostnames->hostname;
    }

    // obtener la base de datos
    public function getWebsiteAttribute() {
        return $this->hostnames->hostname->website;
    }

    ///SCOPES
    //Buscar por nombre
    public function scopeName($query, $name) {
        if ($name) {
            return $query->where('name_company', 'like', '%' . $name . '%');
        }
    }

    //Buscar por nombre
    public function scopeDomain($query, $domain) {
        if ($domain) {
            return $query->where('name_company', 'like', '%' . $domain . '%');
        }
    }

    public function getUsersCount() {
        
        $website = $this->website;

        return DB::table($website->uuid.'.tm_users')->where("perfil","S")->count();
        
    }

    public function getUsers() {
        
        $website = $this->hostnames->hostname->website;

        return DB::table($website->uuid.'.tm_users')->where("perfil","S")->select('id', 'cedula', 'nombre', 'susuario', 'correo', 'cargo', 'telefono', 'perfil', 'rol', 'coordinador', 'comercial', 'adtivo', 'pricing', 'whatsapp', 'created_at', 'updated_at', 'deleted_at')->get();
        
    }

    public function getConfigs() {
        
        $website = $this->hostnames->hostname->website;
        $results = DB::table('conecta_master.options as opt')
        ->leftJoin($website->uuid.'.configuraciones as conf', 'opt.name', '=', 'conf.configuracion')
        ->select('opt.name', 'conf.valor', 'opt.type', 'opt.value', 'opt.default')
        ->orderBy('opt.name', 'asc')
        ->get();

        return $results;
        // return DB::table($website->uuid.'.tm_users')->where("perfil","S")->select('id', 'cedula', 'nombre', 'susuario', 'correo', 'cargo', 'telefono', 'perfil', 'rol', 'coordinador', 'comercial', 'adtivo', 'pricing', 'whatsapp', 'created_at', 'updated_at', 'deleted_at')->get();
        
    }

    public function getData() {
        
        $website = $this->website;
        $data = [
            'users' => DB::table($website->uuid.'.tm_users')->where("perfil","S")->count(),
            'shipments' => DB::table($website->uuid.'.tm_do')->whereBetween(DB::raw("str_to_date(fecha_ingreso, '%d/%m/%Y')"), [Carbon::now()->subDays(30), Carbon::now()])->count(),
            'requests' => DB::table($website->uuid.'.sgc_requerimientos')->whereBetween(DB::raw("str_to_date(fecha_creacion, '%d/%m/%Y')"), [Carbon::now()->subDays(30), Carbon::now()])->count(),
            'invoices' => DB::table($website->uuid.'.tm_facturas')->whereBetween(DB::raw("str_to_date(fecha, '%d/%m/%Y')"), [Carbon::now()->subDays(30), Carbon::now()])->count(),
            'quotes' => DB::table($website->uuid.'.sgc_cotizaciones')->whereBetween(DB::raw("str_to_date(fecha_ingreso, '%d/%m/%Y')"), [Carbon::now()->subDays(30), Carbon::now()])->count(),
            'purchases' => DB::table($website->uuid.'.tm_compras')->whereBetween(DB::raw("str_to_date(fecha, '%d/%m/%Y')"), [Carbon::now()->subDays(30), Carbon::now()])->count(),
            'containers' => DB::table($website->uuid.'.tm_bl')->whereIn('do', function ($query) use ($website) { $query->select('do')->from($website->uuid.'.tm_do')->whereBetween(DB::raw("str_to_date(fecha_ingreso, '%d/%m/%Y')"), [Carbon::now()->subDays(60), Carbon::now()]);})->sum('cant_contenedores'),
            'logs' => DB::table($website->uuid.'.log')->whereBetween(DB::raw("fecha"), [Carbon::now()->subDays(8), Carbon::now()])->count()
        ];

        return $data;
        
    }



}
