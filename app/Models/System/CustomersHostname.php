<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Environment;
use \App\Models\Customer\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


// hostnames de un cliente
class CustomersHostname extends Model {

    use UsesSystemConnection;

    protected $fillable = ['hostname_id', 'customer_id'];
    public $timestamps = false;

    // hostname
    public function hostname() {
        return $this->belongsTo('Hyn\Tenancy\Models\Hostname', 'hostname_id');
        // return $this->hasOne('Hyn\Tenancy\Models\Hostname', 'id', 'hostname_id');
    }

    // cliente
    public function customer() {
        return $this->belongsTo('App\Models\System\Customer', 'customer_id');
        // return $this->hasOne('App\Models\System\Customer', 'id', 'customer_id');
    }
    

}
 