<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use App\Traits\Uuids;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Authenticatable
{
    use UsesSystemConnection;
    use HasRoles;
    use Notifiable;
//    use SoftDeletes;
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'id_pantalla', 
        'orden_pantalla', 
        'orden_pregunta', 
        'pregunta', 
        'img', 
        'respuesta', 
        'fecha_creacion', 
        'fecha_modificacion', 
        'usuario', 
        'software'
    ];
    
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
//        'password', 'remember_token',
    ];

   

}
