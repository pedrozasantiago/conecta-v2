<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

// Modelo de continentes
class Continent extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'code',
		'name'
	];
	public $timestamps = false;

	// paises relacionados con el continente
	public function countries()
	{
		return $this->hasMany('App\Models\System\Country');
	}

}

// Modelo de paises
class Country extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'iso2',
		'short_name',
		'long_name',
		'iso3',
		'numcode',
		'un_member',
		'calling_code',
		'cctld',
		'continent_id'
	];
	public $timestamps = false;

	// continente relacionado con el pais
	public function continent()
	{
		return $this->hasOne('App\Models\System\Continent');
	}
	
}


// Modelo de departamentos
class State extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'name',
		'country_id'
	];
	public $timestamps = false;

	// pais relacionado con el departamento
	public function country()
	{
		return $this->hasOne('App\Models\System\Country');
	}
	
}


// Modelo de ciudades
class City extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;

	protected $fillable = [
		'uuid',
		'name',
		'state_id'
	];
	public $timestamps = false;

	// departamento relacionado con la ciudad
	public function state()
	{
		return $this->hasOne('App\Models\System\State');
	}
	
}


// Modelo de barrios
class Neighborhood extends Model
{

	use UsesSystemConnection;
	use SoftDeletes;
    use Uuids;
	
	protected $fillable = [
		'uuid',
		'name',
		'polygon',
		'city_id'
	];
	public $timestamps = false;

	// departamento relacionado con la ciudad
	public function city()
	{
		return $this->hasOne('App\Models\System\City');
	}
	
}