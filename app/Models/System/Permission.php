<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Laratrust\Models\LaratrustPermission;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends LaratrustPermission
{

    use UsesSystemConnection;
    use SoftDeletes;
    use Uuids;
	
    protected $fillable = [
        'uuid',
        'name',
        'display_name',
        'description',
        'parent_id',
        'icon'
    ];
}