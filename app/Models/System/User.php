<?php

namespace App\Models\System;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use UsesSystemConnection;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'first_name',
        'last_name',
        'username',
        'email',
        'image',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // encriptar la contraseña
    public function setPasswordAttribute($password){
        if($password != ''){
            $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
        }
    }

    // obtener el nombre completo
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

}
