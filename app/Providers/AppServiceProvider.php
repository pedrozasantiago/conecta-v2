<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Hyn\Tenancy\Environment;
use App\Models\Customer\Option;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {        
        Schema::defaultStringLength(191);

        if (app()->environment('production')) {
            URL::forceScheme('https');
        }

        $env = app(Environment::class);

        if ($fqdn = optional($env->hostname())->fqdn) {
            config(['database.default' => 'tenant']);
        }

        // configuracion de sistema
       /* if(get_hostname()){
            Config::set([
                'options' => Option::pluck('value', 'name')->all()
            ]);
        } */

        // mayor que (validation)
        Validator::extend('greater_than', function($attribute, $value, $params){            
            $other = Input::get($params[0]);
            return intval($value) <= intval($other);
        });

        Validator::replacer('greater_than', function($message, $attribute, $rule, $params) {
            return str_replace('_', ' ' , 'The '. $attribute .' must be greater than the ' .$params[0]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
