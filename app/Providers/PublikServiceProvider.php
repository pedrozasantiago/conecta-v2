<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PublikServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // globals
        require_once app_path() . '/Helpers/Globals/Global.php';

        // 
        require_once app_path() . '/Helpers/Base.php';
//        require_once app_path() . '/Helpers/Tree.php';
//        require_once app_path() . '/Helpers/Buttons.php';
//        require_once app_path() . '/Helpers/Selects.php';
    }
}
