<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

     /*   // sidebar
        View::composer(
            'app.layouts.sidebar', 'App\Http\ViewComposers\Pos\SidebarComposer'
        );

        // config for jquery
        View::composer(
            'app.*', 'App\Http\ViewComposers\Pos\ConfigJsComposer'
        ); */

    }
}
