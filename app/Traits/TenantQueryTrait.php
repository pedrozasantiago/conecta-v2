<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use App\Models\Customer\User;

trait TenantQueryTrait
{
    /**
     * Obtiene la lista de tenants activos con la información necesaria.
     *
     * @param array|null $websiteIds Lista opcional de website_ids para filtrar.
     * @return \Illuminate\Support\Collection
     */
    public function getActiveTenants(array $websiteIds = null)
    {
        config(['database.default' => 'system']);
        $query = DB::table('customers as a')
            ->join('customers_hostnames as b', 'a.id', '=', 'b.customer_id')
            ->join('hostnames as c', 'c.id', '=', 'b.hostname_id')
            ->join('websites as d', 'c.website_id', '=', 'd.id')
            ->select([
                'd.id as website_id',
                'd.*',
                'c.fqdn',
                'd.uuid as db_name',
                'd.managed_by_database_connection',
                'a.status',
            ]);
            // ->where('a.status', 1); // Solo tenants activos

        // Filtrar por website_id si se proporciona
        if (!empty($websiteIds)) {
            $query->whereIn('d.id', $websiteIds);
        }

        
        return $query->first()->status ?? false;
    }


    public function getUsers(){

        $operativeUsers = User::where('coordinador', 'S')->where('perfil', 'S')->get();
        $commercialUsers = User::where('comercial', 'S')->where('perfil', 'S')->get();
        $pricingUsers = User::where('pricing', 'S')->where('perfil', 'S')->get();
        $administrativeUsers = User::where('coordinador', 'S')->where('perfil', 'S')->get();

        return [
              $operativeUsers,
              $commercialUsers,
              $pricingUsers,
             $administrativeUsers
        ];

    }

}
