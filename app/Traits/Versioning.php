<?php
namespace App\Traits;

use Carbon\Carbon;

trait Versioning
{

    /**
     * Version.
     */
    public function getVersionAttribute()
    {
        return $this->updated_at->timestamp;
    }

}
