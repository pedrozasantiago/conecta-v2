<?php

return [
    [
        'id' => 'DASHBOARD',
        'href' => 'MDI_CCIAL.php?enl_id=DASHBOARD',
        'ref' => 'dashboard',
        'icon' => 'fas fa-chart-bar',
        'titulo' => 'Dashboard',
        'hijo' => [
        ],
    ],
    [
        'id' => 'ESTADISX',
        'href' => 'MDI_CCIAL.php?enl_id=ESTADISX',
        'ref' => 'statistics',
        'icon' => 'fas fa-globe-americas',
        'titulo' => 'Estadísticas',
        'hijo' => [
        ],
    ],
    [
        'nuevo' => '',
        'id' => 'CONTACTOS',
        'href' => '',
        'ref' => 'clients',
        'icon' => 'fa fa-book',
        'titulo' => 'Libreta de contactos',
        'hijo' => [
            [
                'id' => 'CONTACTOS_CLIE',
                'href' => 'MDI_CCIAL.php?enl_id=CONTACTOS_CLIE',
                'ref' => 'clients-contacts',
                'icon' => 'fas fa-child',
                'titulo' => 'Contactos de Clientes',
            ],
            [
                'id' => 'CONTACTOS_PROV',
                'href' => 'MDI_CCIAL.php?enl_id=CONTACTOS_PROV',
                'ref' => 'suppliers-contacts',
                'icon' => 'fas fa-child',
                'titulo' => 'Contactos de Proveedores',
            ],
        ],
    ],
    [
        'id' => 'CCAMP',
        'href' => 'MDI_CCIAL.php?enl_id=CCAMP',
        'ref' => 'clients',
        'icon' => 'fas fa-city',
        'titulo' => 'Clientes',
        'hijo' => [
        ],
    ],
    [
        'id' => 'ICAMP',
        'href' => 'MDI_CCIAL.php?enl_id=ICAMP',
        'ref' => 'clients',
        'icon' => 'fa fa-bullhorn',
        'titulo' => 'Campañas',
        'hijo' => [
        ],
    ], 
    [
        'id' => 'G_CLIENTE',
        'href' => 'index.html',
        'ref' => 'clients',
        'icon' => 'fa fa-handshake',
        'titulo' => 'Gestión cliente',
        'hijo' => [
            [
                'id' => 'GCAMP',
                'href' => 'MDI_CCIAL.php?enl_id=GCAMP',
                'ref' => 'clients',
                'icon' => 'fas fa-phone-volume',
                'titulo' => 'Seguimiento al cliente',
            ],
            [
                'id' => 'CORREOS',
                'href' => 'MDI_CCIAL.php?enl_id=CORREOS',
                'ref' => 'mailing',
                'icon' => 'fa fa-inbox',
                'titulo' => 'Correos',
            ], 
            [
                'id' => 'SMS',
                'href' => 'MDI_CCIAL.php?enl_id=SMS',
                'ref' => 'sms',
                'icon' => 'fa fa-envelope',
                'titulo' => 'SMS',
            ],
        ],
    ],
    [
        'nuevo' => '',
        'id' => 'REQUEST',
        'href' => 'MDI_CCIAL.php?enl_id=REQUEST',
        'ref' => 'requests',
        'icon' => 'fas fa-hands-helping',
        'titulo' => 'Requerimientos',
        'hijo' => [
        ],
    ], 
    [
        'id' => 'COTIZA_NEW',
        'href' => 'MDI_CCIAL.php?enl_id=COTIZA_NEW',
        'ref' => 'quotations',
        'icon' => 'fas fa-file-invoice',
        'titulo' => 'Gestión cotizaciones',
        'hijo' => [
        ],
    ],
    [
        'nuevo' => '',
        'id' => 'REQ_TARIFAS',
        'href' => 'MDI_CCIAL.php?enl_id=REQ_TARIFAS',
        'ref' => 'req_rates',
        'icon' => 'fas fa-hand-holding-usd',
        'titulo' => 'Solicitar tarifas',
        'hijo' => [
        ],
    ],
    [
        'nuevo' => '',
        'id' => 'TARIFARIO',
        'href' => 'MDI_CCIAL.php?enl_id=TARIFARIO',
        'ref' => 'rates',
        'icon' => 'fas fa-calculator',
        'titulo' => 'Tarifarios',
        'hijo' => [
            [
                'id' => 'TARIFARIO_C',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=TARIFARIO_C',
                'ref' => 'buy_rates',
                'icon' => 'fa fa-user-cog',
                'titulo' => 'Compra',
            ],
            [
                'id' => 'TARIFARIO_V',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=TARIFARIO_V',
                'ref' => 'sell_rates',
                'icon' => 'fas fa-calculator',
                'titulo' => 'Venta',
            ],
            [
                'id' => 'LISTA_PRECIOS',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=LISTA_PRECIOS',
                'ref' => 'rate_lists',
                'icon' => 'fas fa-calculator',
                'titulo' => 'Lista precios',
            ],
        ],
    ],
    [
        'id' => 'REPORT',
        'href' => 'MDI_CCIAL.php?enl_id=REPORT',
                'ref' => 'report',
        'icon' => 'fas fa-chart-line',
        'titulo' => 'Reportes',
        'hijo' => [
        ],
    ],
    [
        'id' => 'CONFIG',
        'href' => 'index.html',
                'ref' => 'config',
        'icon' => 'fa fa-cogs',
        'titulo' => 'Configuraciones',
        'hijo' => [
            [
                'id' => 'ROLES',
                'href' => '../operativo/MDI_SSO.php?enl_id=ROLES',
                'ref' => 'config_roles',
                'icon' => 'fa fa-user-cog',
                'titulo' => 'Roles',
            ],
            [
                'id' => 'USERS',
                'href' => '../operativo/MDI_SSO.php?enl_id=USERS',
                'ref' => 'users',
                'icon' => 'fa fa-user',
                'titulo' => 'Usuarios',
            ],
            [
                'id' => 'PLANTILLAS',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=PLANTILLAS',
                'ref' => 'templates',
                'icon' => 'fas fa-sticky-note',
                'titulo' => 'Plantillas',
            ],
            [
                'id' => 'TRAYECTOS',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=TRAYECTOS',
                'ref' => 'ports',
                'icon' => 'fa fa-ship',
                'titulo' => 'Trayectos',
            ],
            [
                'id' => 'SERVICIOS',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=SERVICIOS',
                'ref' => 'services',
                'icon' => 'fas fa-layer-group',
                'titulo' => 'Servicios',
                'hijo' => [
                    [
                        'id' => 'T_SERVICIOS',
                        'href' => '../comercial/MDI_CCIAL.php?enl_id=T_SERVICIOS',
                        'ref' => 'group_services',
                        'icon' => 'fas fa-boxes',
                        'titulo' => 'Grupos',
                    ],
                    [
                        'id' => 'SERVICIOS',
                        'href' => '../comercial/MDI_CCIAL.php?enl_id=SERVICIOS',
                        'ref' => 'services',
                        'icon' => 'fas fa-box-open',
                        'titulo' => 'Conceptos',
                    ],
                ]
            ],
            [
                'id' => 'IMPUESTOS',
                'href' => '../comercial/MDI_CCIAL.php?enl_id=IMPUESTOS',
                        'ref' => 'taxes',
                'icon' => 'fas fa-percentage',
                'titulo' => 'Impuestos',
            ],
            [
                'id' => 'TIPOSCARGO',
                'href' => 'MDI_CCIAL.php?enl_id=TIPOSCARGO',
                        'ref' => 'type_charges',
                'icon' => 'fas fa-chart-bar',
                'titulo' => 'Tipos de cargo',
            ],
            [
                'id' => 'CARGOS',
                'href' => 'MDI_CCIAL.php?enl_id=CARGOS',
                'ref' => 'charges',
                'icon' => 'fas fa-chart-bar',
                'titulo' => 'Cargos',
            ],
            [
                'id' => 'CONFIG_TARIFARIO',
                'href' => 'MDI_CCIAL.php?enl_id=CONFIG_TARIFARIO',
                        'ref' => 'config_rates',
                'icon' => 'fas fa-calculator',
                'titulo' => 'Tarifario',
            ],
            [
                'id' => 'GALLERY',
                'href' => 'MDI_CCIAL.php?enl_id=GALLERY',
                        'ref' => 'gallery',
                'icon' => 'fa fa-picture-o',
                'titulo' => 'Galeria',
            ],
        ],
    ],
    [
        'id' => 'SOPORTE',
        'href' => 'MDI_CCIAL.php?enl_id=SOPORTE',
                        'ref' => 'manual',
        'icon' => 'fa fa-question',
        'titulo' => 'Manual de usuarios',
        'hijo' => [],
    ],
      
];
