<?php

return [

    /* Columns */
    'columns' => [
        'select' => [
            'name',
            'email',
            'hostname',
            'id'
        ],
        'order' => [
            'column' => '1',
            'dir' => 'asc'
        ],
        'raw' => [
            'name',
            'id'
        ],
        'hidden' => [],
        'sortable' => [],
        'searchable' => [],
    ],

    /* Configs */
    'configs' => [

        'config' => [
            'table_id' => 'customers',
            'form_id' => 'customers_submit',
            'delete_id' => 'destroy_customer'
        ],

        'routes' => [
            'ajax_route' => 'admin.customers.ajax'
        ],        

        'trans' => [
            'text_alert' => 'customers.text_alert',
            'text_confirm_delete' => 'customers.confirm_delete',
            'delete_success' => 'customers.delete_success',
            'delete_cancelled' => 'customers.delete_cancelled',
        ]
        
    ],

];
