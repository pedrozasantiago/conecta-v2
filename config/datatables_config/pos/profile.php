<?php

return [

    /* Columns */
    'columns' => [
        'select' => [],
        'order' => [],
        'raw' => [],
        'hidden' => [],
        'sortable' => [],
        'searchable' => [],
    ],

    /* Configs */
    'configs' => [

        'config' => [
            'form_id' => 'profile_submit',
        ],

        'routes' => [],
        'trans' => []
        
    ],

];
