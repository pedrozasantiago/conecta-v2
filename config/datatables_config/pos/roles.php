<?php

return [

    /* Columns */
    'columns' => [
        'select' => [
            'display_name',
            'description',
            'id',
        ],
        'order' => [
            'column' => '0',
            'dir' => 'desc'
        ],
        'raw' => [
            'display_name',
            'id',
        ],
        'hidden' => [],
        'sortable' => [],
        'searchable' => [],
    ],

    /* Configs */
    'configs' => [

        'config' => [
            'table_id' => 'roles',
            'form_id' => 'roles_submit',
            'delete_id' => 'destroy_role'
        ],

        'routes' => [
            'ajax_route' => 'roles.datatables'
        ],        

        'trans' => [
            'text_alert' => 'roles.text_alert',
            'text_confirm_delete' => 'roles.confirm_delete',
            'delete_success' => 'roles.delete_success',
            'delete_cancelled' => 'roles.delete_cancelled',
        ]
        
    ],

];
