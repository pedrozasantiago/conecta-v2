<?php

return [

    /* Columns */
    'columns' => [
        'select' => [
            'full_name',
            'username',
            'email',
            'role',
            'id',
        ],
        'order' => [
            'column' => '1',
            'dir' => 'asc'
        ],
        'raw' => [
            'id',
        ],
        'hidden' => [],
        'sortable' => [],
        'searchable' => [],
    ],

    /* Configs */
    'configs' => [

        'config' => [
            'table_id' => 'users',
            'form_id' => 'users_submit',
            'delete_id' => 'destroy_user'
        ],

        'routes' => [
            'ajax_route' => 'users.datatables'
        ],        

        'trans' => [
            'text_alert' => 'users.text_alert',
            'text_confirm_delete' => 'users.confirm_delete',
            'delete_success' => 'users.delete_success',
            'delete_cancelled' => 'users.delete_cancelled',
        ]
        
    ],

];
