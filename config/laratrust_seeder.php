<?php

return [
    'role_structure' => [
        'admin' => [
            'users' => 'c,r,u,d',
            'profile' => 'r,u',
            'roles' => 'c,r,u,d',
            'reports' => 'r',
            'config' => 'r,u',
        ]
    ],
    'permission_structure' => [

    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'i' => 'import',
        'e' => 'export',
        'd' => 'delete'
    ]
];
