<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeographyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // paises 
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->char('iso2', 2);
            $table->string('short_name', 80);
            $table->string('long_name', 80)->nullable();
            $table->char('iso3', 3)->nullable();
            $table->string('numcode', 6)->nullable();
            $table->string('un_member', 12)->nullable();
            $table->string('calling_code', 8)->nullable();
            $table->string('cctld', 5)->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();           
            $table->softDeletes();
        });

        // departamentos
        Schema::create('states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
            $table->softDeletes();
        });

        // ciudades
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('code')->nullable();
            $table->unsignedBigInteger('state_id');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
            $table->softDeletes();
        });

        // barrios
        Schema::create('neighborhoods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->string('code')->nullable();
            $table->text('polygon')->nullable();
            $table->unsignedBigInteger('city_id');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('neighborhoods');
    }
}
