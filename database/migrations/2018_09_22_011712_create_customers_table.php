<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // clientes
        Schema::create('customers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('nit')->nullable();
            $table->string('name_company');
            $table->string('subdomain');
            $table->string('prefix');
            $table->string('next_billing_at');
            $table->string('ends_at');
            $table->unsignedInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->string('wp_account');
            $table->string('wp_phone_id');

            //$table->foreign('customer_id')->references('id')->on('customers_hostnames')->onDelete('set null');
        });

        // relacion de hostnames
        Schema::create('customers_hostnames', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hostname_id');
            $table->unsignedBigInteger('customer_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('customers_hostnames');
        Schema::dropIfExists('customers');
    }
}
