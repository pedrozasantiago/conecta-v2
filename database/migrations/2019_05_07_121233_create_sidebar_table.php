<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSidebarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // menu tenants
        Schema::create('sidebar_tenants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('url');
            $table->string('permission')->nullable();
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->unsignedBigInteger('order')->default(0);
            $table->unsignedBigInteger('active')->default(1);
            $table->longText('params')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // menu administrador
        Schema::create('sidebar_admin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('url');
            $table->string('permission')->nullable();
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->unsignedBigInteger('order')->default(0);
            $table->unsignedBigInteger('active')->default(1);
            $table->longText('params')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('sidebar_tenants');
//        Schema::dropIfExists('sidebar_admin');
    }
}
