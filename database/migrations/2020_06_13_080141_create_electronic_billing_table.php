<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectronicBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // tipo de empresa (natural o juridica)
        Schema::create('companies_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->char('code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->softDeletes();
        });

        // tipos de regimen
        Schema::create('regimes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->char('code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->softDeletes();
        });

        // tipos de responsabilidades
        Schema::create('liabilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->char('code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->softDeletes();
        });

        // tipos de facturas (Venta, Nota credito, etc)
        Schema::create('billing_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->char('code')->nullable();
            $table->char('prefix')->nullable();
            $table->char('cufe_algorithm')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->softDeletes();
        });

        // descuentos
        Schema::create('discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->char('code')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->softDeletes();
        });

        // software dian
        Schema::create('dian_software', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('customer_id');
            $table->string('identifier');
            $table->string('test');
            $table->string('pin');
            $table->string('url');
            $table->timestamps();
            $table->softDeletes();
        });

        // certificados digitales p12
        Schema::create('certificates', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('customer_id');
            $table->string('certificate');
            $table->string('password');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_types');
        Schema::dropIfExists('regimes');
        Schema::dropIfExists('liabilities');
        Schema::dropIfExists('billing_documents');
        Schema::dropIfExists('discounts');
        Schema::dropIfExists('dian_software');
        Schema::dropIfExists('certificates');
    }
}
