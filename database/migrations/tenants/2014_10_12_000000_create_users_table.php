<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('tm_roles'))
        Schema::create('tm_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('maquina');
            $table->string('usuario');
        });
        
         
        if (!Schema::hasTable('tm_users'))
        Schema::create('tm_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cedula');
            $table->string('nombre');
            $table->string('susuario')->unique();
            $table->string('correo');
            $table->string('cargo')->nullable();
            $table->string('telefono')->nullable();
            $table->string('perfil')->nullable();
            $table->unsignedBigInteger('rol');
            $table->string('coordinador')->nullable();
            $table->string('comercial')->nullable();
            $table->string('slogin');
            $table->string('password');
            $table->string('api_token', 60)->unique();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            
            
            $table->foreign('rol')->references('id')->on('tm_roles')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('users');
    }
}
