<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        // configuraciones
        if (!Schema::hasTable('configuraciones'))
        Schema::create('configuraciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('configuracion')->unique();
            $table->string('tipo');
            $table->text('valor');
            $table->string('icon')->nullable();
            $table->string('group')->nullable();
            $table->json('params')->nullable();
        });
        
        // Plantillas
        if (!Schema::hasTable('plantillas'))
        Schema::create('plantillas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('tipo')->unique();
            $table->text('texto');
            
            
        });
        
        // tm_trayectos
        if (!Schema::hasTable('tm_trayectos'))
        Schema::create('tm_trayectos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('PAIS');
            $table->string('CIUDAD');
            $table->string('ESTADO');
            $table->string('USUARIO');
            $table->string('MAQUINA');
            $table->string('FECHA_MOD');
            $table->primary(['PAIS', 'CIUDAD']);
        });
        
        if (!Schema::hasTable('inicio_sesion'))
        Schema::create('inicio_sesion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('row_num');
            $table->string('correo')->nullable();
            $table->string('ip')->nullable();
            $table->string('fecha')->nullable();
            $table->string('hora')->nullable();
        });
        
        if (!Schema::hasTable('log'))
        Schema::create('log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
           
            $table->bigIncrements('idlog');
            $table->string('pantalla', 45)->nullable()->collation('utf8mb4_unicode_ci');
            $table->text('detalle_nuevo')->collation('utf8mb4_unicode_ci');
            $table->text('detalle_viejo')->collation('utf8mb4_unicode_ci');
            $table->string('usuario', 45)->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('maquina', 45)->nullable()->collation('utf8mb4_unicode_ci');
            $table->dateTime('fecha')->nullable();
            $table->string('accion', 45)->nullable()->collation('utf8mb4_unicode_ci');
            $table->text('request')->collation('utf8mb4_unicode_ci');
            $table->string('user_agent', 45)->nullable()->collation('utf8mb4_unicode_ci');
            
            // Índices
            $table->index('pantalla');
            $table->index('fecha');
            $table->index('usuario');
            
            
            
        });
        
        if (!Schema::hasTable('mensajes'))
        Schema::create('mensajes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('tipo')->nullable();
            $table->string('msg')->nullable();
            $table->string('Fecha', 50)->nullable();
            $table->bigIncrements('row_num');
            $table->string('colNueva', 2)->nullable();
        });
                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('configuraciones');
//        Schema::dropIfExists('plantillas');
//        Schema::dropIfExists('tm_trayectos');
    }
}
