<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComercialConfigTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('sgc_actividades'))
        Schema::create('sgc_actividades', function (Blueprint $table) {
            $table->string('ACTIVIDAD_ID')->primary();
            $table->string('DESCRIPCION');
            $table->integer('SECUENCIA');
            $table->string('TIPO');
            $table->string('ACTIVO');
        });
        
        if (!Schema::hasTable('sgc_atributos'))
        Schema::create('sgc_atributos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('atributo');
            $table->string('descrip');
            $table->string('descrip_ingles')->nullable();
            $table->integer('orden');
            $table->string('grupo');
            $table->string('lista')->nullable();
            $table->string('orden_grp')->nullable();
            $table->string('estado');
            $table->string('tipo_campo')->nullable();
            $table->string('obligatorio')->nullable();
            $table->string('placeholder')->nullable();;
            $table->string('default')->nullable();
            $table->string('tipo')->nullable();
            $table->string('sistema')->nullable()->comment('Columna que define las campos que no se pueden eliminar del sistema');
            $table->string('reporte')->nullable();
            
            $table->unique(['atributo','grupo']);
            
        });
        
                if (!Schema::hasTable('sgc_tipo_servicios'))
        Schema::create('sgc_tipo_servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion');
            $table->string('descripcion_ingles');
            $table->string('activo');
            $table->integer('orden');
            $table->string('id_externo')->nullable();
        });
        
        if (!Schema::hasTable('sgc_servicios'))
        Schema::create('sgc_servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_externo')->nullable();
            $table->string('tipo_ingreso')->nullable();
            $table->string('descripcion');
            $table->string('descripcion_ingles');
            $table->string('activo');
            $table->string('aplica_por')->nullable();
            $table->double('minima')->nullable();
            $table->string('venta');
            $table->string('tipo_venta')->default("V");
            $table->string('moneda_venta');
            $table->string('impuesto');
            $table->unsignedBigInteger('tipo_servicio');
            $table->string('compra');
            $table->double('minimacompra')->nullable();
            $table->string('tipo_compra')->default("V");
            $table->string('moneda_compra');
            $table->text('notas');
            $table->text('notas_internas');
            $table->string('creado_por');
            $table->dateTime('fecha_creacion')->useCurrent();
            $table->string('modificado_por')->nullable();
            $table->dateTime('fecha_modificacion')->useCurrent();            
            $table->integer('orden');  
            
            $table->index(['id', 'tipo_servicio']);
            
            $table->foreign('tipo_servicio')->references('id')->on('sgc_tipo_servicios');
        });
        
        
        if (!Schema::hasTable('sgc_impuestos'))
        Schema::create('sgc_impuestos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('impuesto');
            $table->string('activo');
        });
        
        if (!Schema::hasTable('sgc_trm'))
        Schema::create('sgc_trm', function (Blueprint $table) {
            $table->string('moneda')->primary();
            $table->string('valor');
            $table->string('estado');
            $table->string('usuario');
            $table->dateTime('fecha')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
        
        if (!Schema::hasTable('sgg_consecutivos'))
        Schema::create('sgg_consecutivos', function (Blueprint $table) {
            $table->string('TIPO')->primary();
            $table->string('CODIGO')->nullable();
            $table->string('ANO')->nullable();
            $table->integer('SECUENCIA');
            $table->string('PRE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
//        Schema::dropIfExists('sgc_actividades');
//        Schema::dropIfExists('sgc_atributos');
//        Schema::dropIfExists('sgc_tipo_servicios');
//        Schema::dropIfExists('sgc_impuestos');
//        Schema::dropIfExists('sgc_trm');
//        Schema::dropIfExists('sgg_consecutivos');
    }

}
