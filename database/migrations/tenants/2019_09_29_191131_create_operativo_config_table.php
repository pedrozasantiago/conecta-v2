<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperativoConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        
        Schema::create('tm_actividades_oper', function (Blueprint $table) {
            $table->bigIncrements('row_num');
            $table->string('descripcion');
            $table->integer('secuencia');
            $table->string('tipo');
            $table->string('subtipo_oper');
            $table->string('servicio')->nullable();
            $table->string('incoterm')->nullable();
            $table->string('estado_oper')->nullable();
            $table->string('activo');
            $table->string('icono')->nullable();
            $table->string('interna')->nullable();
        });
        
        
        Schema::create('tm_asociar_codigos', function (Blueprint $table) {
            $table->string('tipo');
            $table->string('codigo');
            $table->string('codasoc');
            $table->string('grupo');
            $table->integer('orden');
            $table->string('estado');
            $table->primary(['tipo', 'codigo','codasoc']);
        });
        
        
        Schema::create('tm_atributos', function (Blueprint $table) {
            $table->string('module')->nullable();
            $table->string('atributo');
            $table->string('descrip');
            $table->string('descrip_ingles')->nullable();
            $table->integer('orden');
            $table->string('grupo');
            $table->string('lista')->nullable();
            $table->integer('orden_grp')->nullable();
            $table->string('estado');
            $table->string('tipo')->nullable();
            $table->string('obligatorio')->nullable();
            $table->string('tipo_campo')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('default')->nullable();
            $table->string('sistema')->nullable();
            $table->string('reporte')->nullable();
            $table->primary(['atributo', 'grupo']);
        });
        
        
        Schema::create('tm_atributos_docaduana', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('proveedor_id');
            $table->string('ciudad');
            $table->string('descripcion');
            $table->string('estado');
            $table->string('creado_por');
            $table->datetime('fecha_creacion')->useCurrent();
            $table->string('modificado_por')->nullable();
            $table->datetime('fecha_modificacion')->useCurrent();
        });
        
        
        Schema::create('tm_consecutivo', function (Blueprint $table) {
            $table->string('SUBTIPO_OPER')->primary();
            $table->string('SERVICIO')->nullable();
            $table->string('CODIGO_SERV')->nullable();
            $table->string('ANO')->nullable();
            $table->string('PRE')->nullable();
            $table->integer('SECUENCIA');
        });
        
        
        Schema::create('tm_listas', function (Blueprint $table) {
            $table->string('tipo');
            $table->string('codigo');
            $table->string('descrip');
            $table->string('descrip_ingles')->nullable();
            $table->integer('orden');
            $table->string('activo');
            $table->string('sistema')->nullable();
            $table->string('tipo_parent')->nullable();
            $table->string('cod_parent')->nullable();
            $table->string('id_externo')->nullable();
            $table->string('publico')->nullable();
            $table->primary(['tipo', 'codigo']);
        });
        
               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_actividades_oper');
        Schema::dropIfExists('tm_asociar_codigos');
        Schema::dropIfExists('tm_atributos');
        Schema::dropIfExists('tm_atributos_docaduana');
        Schema::dropIfExists('tm_consecutivo');
        Schema::dropIfExists('tm_listas');
    }
}
