<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAclTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_pantallas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cod');
            $table->string('nombre');
            $table->string('descripcion');
            $table->integer('orden')->nullable();
        });
        
         Schema::create('tm_permisos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cod');
            $table->string('pantalla');
            $table->string('id_tab');
            $table->string('nombre_tab');
            $table->string('nombre');
            $table->string('descripcion');
            
            $table->unique(['cod', 'pantalla','id_tab']);
            
        });
        
         
        
         Schema::create('tm_roles_pantalla', function (Blueprint $table) {
            $table->string('rol');
            $table->string('pantalla');
            $table->string('valor');
            $table->string('usuario');
            $table->string('maquina');
            $table->string('fecha');
            
            $table->unique(['rol', 'pantalla']);
        });
        
         Schema::create('tm_roles_permisos', function (Blueprint $table) {
            $table->string('id_rol');
            $table->string('id_permiso');
            $table->string('valor')->default('N');
            $table->string('usuario');
            $table->string('maquina');
            $table->string('fecha');
            
            $table->unique(['id_rol', 'id_permiso']);
        });
               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_pantallas');
        Schema::dropIfExists('tm_permisos');
        Schema::dropIfExists('tm_roles');
        Schema::dropIfExists('tm_roles_pantalla');
        Schema::dropIfExists('tm_roles_permisos');
    }
}
