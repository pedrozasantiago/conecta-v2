<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperativoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
                 
        
        Schema::create('tm_banco_egresos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('prefijo', 45)->nullable();
            $table->string('consecutivo', 45)->nullable();
            $table->string('proveedor_id', 45);
            $table->integer('factura_id')->nullable();
            $table->integer('cuenta_acreditar')->nullable();
            $table->integer('cuenta_debitar')->nullable();
            $table->string('estado', 45)->nullable();
            $table->string('metodo', 45)->nullable();
            $table->string('fecha', 45)->nullable();
            $table->double('valor')->nullable();
            $table->double('retencion')->nullable();
            $table->string('notas')->nullable();
            $table->string('motivo_anulacion')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->unique(["consecutivo"]);
        });

        Schema::create('tm_banco_ingresos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('prefijo', 45)->nullable();
            $table->string('consecutivo', 45)->nullable();
            $table->string('cliente_id', 45);
            $table->integer('factura_id')->nullable();
            $table->integer('cuenta_acreditar')->nullable();
            $table->integer('cuenta_debitar')->nullable();
            $table->string('estado', 45)->nullable();
            $table->string('metodo', 45)->nullable();
            $table->string('fecha', 45)->nullable();
            $table->double('valor')->nullable();
            $table->double('retencion')->nullable();
            $table->string('notas')->nullable();
            $table->string('motivo_anulacion')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

//            $table->unique(["consecutivo"], 'consecutivo_UNIQUE');
            $table->unique(["consecutivo"]);
        });

        Schema::create('tm_banco_items', function (Blueprint $table) {
            $table->id();
            $table->string('do', 191)->nullable();
            $table->unsignedBigInteger('itemable_id')->nullable();
            $table->string('itemable_type', 45)->nullable();
            $table->unsignedBigInteger('billable_id')->nullable();
            $table->string('billable_type', 45)->nullable();
            $table->unsignedBigInteger('concepto')->nullable();
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('notas', 191)->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->string('modificado_por', 45)->nullable();
            $table->timestamp('fecha_modificacion')->useCurrent();
            $table->string('maquina', 191)->nullable();

        });


        Schema::create('tm_bitacora_do', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('DO');
            $table->integer('CONSOLIDADO')->nullable();
            $table->string('CONTENEDOR')->nullable();
            $table->string('FECHA_STR')->nullable();
            $table->date('FECHA_OLD1')->nullable();
            $table->text('DETALLE')->nullable();
            $table->string('DETALLE_INTERNO')->nullable();
            $table->string('RETRASO')->nullable();
            $table->string('RESPONSABLE')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('INTERNA')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->string('NUM_INCUMP')->nullable();
            $table->date('FECHA2')->nullable();
            $table->string('ACTIVIDAD', 200)->nullable();
            $table->double('SEC_ACTIV')->nullable();
            $table->string('ANO', 25)->nullable();
            $table->string('ACTIVIDAD_ID', 20)->nullable();
            $table->string('HORA', 20)->nullable();
            $table->string('FECHA_BIT', 20)->nullable();
            $table->string('FECHA', 45)->nullable();
            $table->string('DESTINATARIO', 45)->nullable();
            $table->integer('id_actividad')->nullable();
            $table->json('changes')->nullable();

            $table->index(["DO"]);

            $table->index(["FECHA_BIT"]);

        });

        Schema::create('tm_bl', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do');
            $table->string('cant_contenedores')->nullable();
            $table->string('tipo_contenedor')->nullable();
            $table->string('num_contenedor')->nullable();
            $table->string('marcas')->nullable();
            $table->string('cant_empaques')->nullable();
            $table->string('tipo_empaque')->nullable();
            $table->text('mercancia')->nullable();
            // $table->string('mercancia')->nullable();
            $table->string('peso')->nullable();
            $table->string('u_peso')->nullable();
            $table->string('volumen')->nullable();
            $table->string('u_volumen')->nullable();
            $table->string('maquina')->nullable();
            $table->string('usuario')->nullable();
            $table->string('fecha_modificacion')->nullable();
            $table->string('fletes')->nullable();
            $table->string('valor')->nullable();
            $table->integer('dias_libres')->default(0);
            $table->string('fecha_devolucion')->nullable();
            $table->string('fecha_salida_puerto')->nullable();
            $table->string('lugar_entrega')->nullable();
            $table->string('lugar_dev_vacio')->nullable();
            $table->string('id_externo')->nullable();
        });

        Schema::create('tm_compras', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do')->nullable();
            $table->string('proveedor_id')->nullable();
            $table->string('factura_compra', 45)->nullable();
            $table->string('estado', 45);
            $table->string('estado_info', 45)->default('PENDING');
            $table->string('fecha', 45)->nullable();
            $table->string('fecha_vencimiento', 45)->nullable();
            $table->integer('cuenta_debitar')->nullable();
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('concepto')->nullable();
            $table->string('observacion')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::create('tm_compras_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('factura_id')->nullable();
            $table->string('tipo_ingreso', 45)->nullable();
            $table->unsignedBigInteger('concepto')->nullable();
            $table->integer('id_grupo')->nullable();
            $table->integer('variacion');
            $table->string('proveedor_id', 191);
            $table->double('unidad');
            $table->double('tarifa_compra');
            $table->string('monedacompra', 45)->nullable();
            $table->double('trmcompra')->nullable();
            $table->double('retencion');
            $table->double('valor_retencion');
            $table->double('valor_retencion_mn');
            $table->double('subtotal_compra')->nullable();
            $table->double('subtotal_compra_mn')->nullable();
            $table->string('tipo_descuento', 191)->nullable();
            $table->double('descuento');
            $table->double('valor_descuento');
            $table->double('valor_descuento_mn');
            $table->double('tasa_impuesto')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('compra_total')->nullable();
            $table->double('compra_total_mn')->nullable();
            $table->string('notas', 191)->nullable();
            $table->string('usuario', 191)->nullable();
            $table->string('maquina', 191)->nullable();
            $table->timestamp('fecha_mod')->useCurrent();

        });

        Schema::create('tm_consolidado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('nombre', 45)->nullable();
            $table->string('subtipo', 45)->nullable();
            $table->string('servicio', 45)->nullable();
            $table->string('estado', 45)->nullable();
            $table->string('observaciones')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->string('fecha_creacion', 45)->nullable();
            $table->string('modificado_por', 45)->nullable();
            $table->string('fecha_modificacion', 45)->nullable();
        });

        Schema::create('tm_consolidado_do', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('id_consolidado');
            $table->string('do', 45);
            $table->string('creado_por', 45)->nullable();
            $table->string('fecha_creacion', 45)->nullable();

            $table->unique(["id_consolidado", "do"]);
        });

        Schema::create('tm_detalle_expo', function (Blueprint $table) {
            $table->engine = 'InnoDB';            
            $table->bigIncrements('row_num');
            $table->string('DO');
            $table->string('CONTENEDOR')->nullable();
            $table->double('SECUENCIA')->nullable();
            $table->string('ACTIVIDAD_ID');
            $table->string('INFORMACION')->nullable();
            $table->string('FECHA_ESTIMADA')->nullable();
            $table->string('ESTADO_ACTIV')->nullable()->default('N');
            $table->string('OBLIGATORIA')->nullable()->default('N');
            $table->string('OBSERV_INTERNA')->nullable();
            $table->string('HORA')->nullable();
            $table->string('COLOR_ALERTA_ACTIVIDAD')->nullable();
            $table->string('FECHA_ALERTA')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->double('SEC')->nullable();
            $table->string('TIPO', 30)->nullable();
            $table->string('ANO', 25)->nullable();
            $table->string('COLOR_ALR', 45)->nullable();
            $table->string('FECHA_ALR', 45)->nullable();
            $table->string('ACTIVIDAD_CTL', 45)->nullable();
            $table->string('FECHA_CTL', 45)->nullable();
            $table->string('TIEMPO_E', 45)->nullable();
            $table->string('modo', 45)->nullable();
            $table->integer('id_actividad')->nullable();

            $table->index(["DO","id_actividad"]);
            
        });

        Schema::create('tm_detalle_impo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('row_num');
            $table->string('DO');
            $table->string('CONTENEDOR')->nullable();
            $table->double('SECUENCIA')->nullable();
            $table->string('ACTIVIDAD_ID');
            $table->string('INFORMACION')->nullable();
            $table->string('FECHA_ESTIMADA')->nullable();
            $table->string('ESTADO_ACTIV')->nullable();
            $table->string('OBSERV_INTERNA')->nullable();
            $table->string('HORA')->nullable();
            $table->string('OBLIGATORIA')->nullable();
            $table->string('COLOR_ALERTA_ACTIVIDAD')->nullable();
            $table->string('FECHA_ALERTA')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->double('SEC')->nullable();
            $table->string('TIPO', 30)->nullable();
            $table->string('ANO', 25)->nullable();
            $table->string('COLOR_ALR', 45)->nullable();
            $table->string('FECHA_ALR', 45)->nullable();
            $table->string('ACTIVIDAD_CTL', 45)->nullable();
            $table->string('FECHA_CTL', 45)->nullable();
            $table->integer('TIEMPO_E')->nullable();
            $table->string('modo', 45)->nullable();
            $table->integer('id_actividad')->nullable();

            $table->index(["DO","id_actividad"]);
            
        });
        
     
        
        Schema::create('sgc_clientes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('TIPO_ID', 45);
//            $table->integer('CEDULA')->primary();
            $table->string('CEDULA')->primary();
            $table->string('NOMBRE')->nullable();
            $table->string('TIPO_CLIENTE')->nullable();
            $table->string('CORREO')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('ESTADO_INFO')->nullable();
            $table->string('CATEGORIA')->nullable();
            $table->string('ORIGEN')->nullable();
            $table->string('CARGA')->nullable();
            $table->string('CARGO')->nullable();
            $table->string('FECHA_VERIFICACION', 45)->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
        });
        
        Schema::create('tm_proveedores', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('tipo_id', 45);
            $table->bigInteger('cedula')->unique();
            $table->string('correo')->nullable();
            $table->string('nombre')->nullable();
            $table->string('chk_visita')->nullable();
            $table->string('tipo')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('estado')->nullable();
            $table->string('arch_credito')->nullable();
            $table->string('arch_prov')->nullable();
            $table->string('estado_info')->nullable();
            $table->string('fecha_verificado')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('arch_infogral', 20)->nullable();
            $table->string('arch_doclegal', 20)->nullable();
            $table->string('arch_doccred', 20)->nullable();
            $table->string('MIGRADO', 45)->nullable();
            $table->string('coordinador')->nullable();

        });

        Schema::create('tm_do', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('ID', 20)->nullable();
            $table->string('DO');
            $table->string('shipment_name')->nullable();
//            $table->integer('CLIENTE_ID');
            $table->string('CLIENTE_ID');
            $table->string('CLIENTE')->nullable();
            $table->string('TIPO')->nullable();
            $table->string('SUBTIPO_OPER', 30)->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('ESTADO_INFO')->nullable();
            $table->string('FECHA', 55)->nullable();
            $table->string('FECHA_INGRESO', 20)->nullable();
            $table->string('SERVICIO_ID', 20)->nullable();
            $table->string('SERVICIO')->nullable();
            $table->string('MES_CO')->nullable();
            $table->string('OFICINA')->nullable();
            $table->string('ANO')->nullable();
            $table->string('MONEDA', 20)->nullable();
            $table->string('CONTROL_ID')->nullable();
            $table->string('FECHA_ALERTA', 55)->nullable();
            $table->string('FECHA_CIERRE', 55)->nullable();
            $table->string('REGISTRADO_CO')->nullable();
            $table->string('COLOR_ALERTA')->nullable();
            $table->string('ASESOR')->nullable();
            $table->bigInteger('ASESOR_ID')->nullable();
            $table->string('AGENTE')->nullable();
            $table->bigInteger('AGENTE_ID')->nullable();
            $table->string('INCOTERM')->nullable();
            $table->string('PEDIDO')->nullable();
            $table->string('PROVEEDOR')->nullable();
            $table->text('BL_CONSIGNATARIO')->nullable();
            $table->string('CONSIGNATARIO_ID', 45)->nullable();
            $table->string('SHIPPER1')->nullable();
            $table->text('BL_SHIPPER')->nullable();
            $table->text('BL_NOTIFY')->nullable();
            $table->string('LT_ESTIMADO')->nullable();
            $table->string('FECHA_BITACORAF')->nullable();
            $table->string('FECHA_FACTURA', 50)->nullable();
            $table->integer('PROFILE_CONTABLE')->nullable();
            $table->integer('PROFILE_ESTIMADO')->nullable();
            $table->string('FECHA_BITACORAI')->nullable();
            $table->string('NOTIFICADOR')->nullable();
            $table->string('FLETE_COBRO')->nullable();
            $table->string('FECHA_ZARPE')->nullable();
            $table->string('FECHA_PROG', 20)->nullable();
            $table->string('REGISTRO_ACT', 5)->nullable();
            $table->string('CONSECUTIVO')->nullable();
            $table->string('PROVAGENTE')->nullable();
            $table->string('NOM_CLIENTE')->nullable();
            $table->string('PLACA')->nullable();
            $table->string('PROVTRANSP')->nullable();
            $table->string('ORIGEN')->nullable();
            $table->string('DESTINO')->nullable();
            $table->string('FLETE_PREPAGADO')->nullable();
            $table->string('HIJO')->nullable();
            $table->string('ADICIONAL')->nullable();
            $table->string('MASTER')->nullable();
            $table->string('PESO')->nullable();
            $table->string('VOL_LARGO')->nullable();
            $table->string('VOL_ANCHO')->nullable();
            $table->string('CANT_CAJAS')->nullable();
            $table->string('CANT_CONTENEDORES')->nullable();
            $table->string('VOL_TOTAL_VOL')->nullable();
            $table->string('VOL_ALTO')->nullable();
            $table->string('CANT_BULTOS')->nullable();
            $table->string('CANT_NO_CONTENEDORES')->nullable();
            $table->string('MONTA_CARGUE')->nullable();
            $table->string('AEREA')->nullable();
            $table->string('IMO')->nullable();
            $table->string('MONTA_DESCARGUE')->nullable();
            $table->string('MOTONAVE')->nullable();
            $table->string('PRODUCTO')->nullable();
            $table->string('SUCURSAL')->nullable();
            $table->string('UN')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('DIGITALIZADO', 30)->nullable();
            $table->string('ASUNTO_STATUS', 5)->nullable();
            $table->string('VIAJE')->nullable();
            $table->string('COMERCIAL')->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->text('MAXBIT')->nullable();
            $table->string('MIGRADO', 25)->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD', 55)->nullable();
            $table->string('arch_infogral')->nullable();
            $table->string('coordinador')->nullable();
            $table->string('COTIZACION', 45)->nullable();

            $table->index(["shipment_name"]);

            $table->index(["CLIENTE_ID"]);

            $table->index(["CONTROL_ID"]);

            $table->index(["ESTADO", "SUBTIPO_OPER","TIPO"]);

            $table->index(["ASESOR_ID", "AGENTE_ID"]);

            $table->index(["TIPO", "SUBTIPO_OPER"]);

            $table->index(["AGENTE_ID"]);

//            $table->unique(["DO", "ESTADO"]);

            $table->unique(["DO"]);

            $table->unique(["SUBTIPO_OPER","CONSECUTIVO","ANO"]);

            $table->unique(['SUBTIPO_OPER', 'CONSECUTIVO','HIJO','DO']);  


            $table->foreign('CLIENTE_ID')
                    ->references('CEDULA')->on('sgc_clientes')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            // $table->foreign('AGENTE_ID')
            //         ->references('cedula')->on('tm_proveedores')
            //         ->onDelete('restrict')
            //         ->onUpdate('restrict');

            // $table->foreign('ASESOR_ID')
            //         ->references('cedula')->on('tm_proveedores')
            //         ->onDelete('restrict')
            //         ->onUpdate('restrict');
        });
        
                 
        
        Schema::create('tm_empleados', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->bigInteger('CEDULA')->unique();            
            $table->string('CLAVE', 45)->nullable();
            $table->string('NOMBRE')->nullable();
            $table->string('TIPO_CONTRATO', 25)->nullable();
            $table->string('CORREO')->nullable();
            $table->string('ESTADO_INFO')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('FECHA_VERIFICADO')->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('COORDINADOR', 45)->nullable();
        });
        
        
        Schema::create('tm_empleados_doclegal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('cedula');
            $table->integer('atributo'); 
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('chk', 5)->nullable();
            $table->string('chk_ant', 5)->nullable();
            $table->string('maquina')->nullable();
            $table->string('usuario')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('observaciones')->nullable();
            
            $table->unique(["atributo", "cedula"]);
            
        });
        
         Schema::create('tm_empleados_infogral', function (Blueprint $table) {
            $table->engine = 'InnoDB';            
            $table->unsignedBigInteger('cedula');
            $table->integer('atributo');            
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->date('fecha_mod')->nullable();
            
            $table->unique(["atributo", "cedula"]);
            
        });
        
        
        Schema::create('tm_exportaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('DO')->unique();
            $table->string('CONTENEDOR', 30);
            $table->string('SUBTIPO_OPER', 30);
            $table->string('ESTADO');
            $table->string('FECHA', 50);
            $table->string('ANO', 25)->nullable();
            $table->string('OBSERV_EXTERNA')->nullable();
            $table->string('PEDIDO', 30)->nullable();
            $table->string('LEAD_TIME_VACIO', 30)->nullable();
            $table->string('LEAD_TIME_RECORRIDO', 30)->nullable();
            $table->string('INCOTERM', 45)->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();

            
        });
        
         Schema::create('tm_facturas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('tipo_doc', 45)->nullable();
            $table->string('do')->nullable();
            $table->string('prefijo', 45)->nullable();
            $table->string('consecutivo', 45);
            $table->string('estado', 45);
            $table->string('estado_info', 45)->default('PENDING');
            $table->string('cliente_id', 45)->nullable();
            $table->string('contacto_id', 45)->nullable();
            $table->string('fecha', 45)->nullable();
            $table->string('fecha_vencimiento', 45)->nullable();
            $table->string('forma_pago', 45)->nullable();
            $table->string('moneda', 45)->nullable();
            $table->double('trm')->nullable();
            $table->string('monedapago')->nullable();
            $table->string('concepto')->nullable();
            $table->string('observacion')->nullable();
            $table->string('motivo_anulacion')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('maquina', 45)->nullable();
        });
        
        
        Schema::create('tm_factura_detalle', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('factura_id')->nullable();
            $table->string('tipo_ingreso', 45)->nullable();
            $table->unsignedBigInteger('concepto')->nullable();
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('notas')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

             
           $table->foreign('factura_id')
                ->references('id')->on('tm_facturas')
                ->onDelete('no action')
                ->onUpdate('no action');
            

            $table->foreign('concepto')
                ->references('id')->on('sgc_servicios')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
        
        
        Schema::create('tm_importaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('DO')->unique();
            $table->string('CONTENEDOR', 30);
            $table->string('SUBTIPO_OPER', 30);
            $table->string('ESTADO');
            $table->string('FECHA', 50);
            $table->string('ANO', 25)->nullable();
            $table->string('NO_MBL')->nullable();
            $table->string('NO_HBL')->nullable();
            $table->string('EMISION_MBL')->nullable();
            $table->string('EMISION_HBL')->nullable();
            $table->string('LEAD_TIME_RECORRIDO', 30)->nullable();
            $table->string('LEAD_TIME_VACIO', 30)->nullable();
            $table->string('DOC_OPERACION', 20)->nullable();
            $table->string('PEDIDO', 30)->nullable();
            $table->string('OBSERV_EXTERNA')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();

            
        });
        
        Schema::create('tm_notas_credito', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do')->nullable();
            $table->string('prefijo', 45)->nullable();
            $table->string('consecutivo', 45);
            $table->string('estado', 45);
            $table->string('cliente_id', 45)->nullable();
            $table->string('contacto_id', 45)->nullable();
            $table->string('fecha', 45)->nullable();
            $table->string('moneda', 45)->nullable();
            $table->string('concepto')->nullable();
            $table->string('observacion')->nullable();
            $table->string('motivo_anulacion')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        
        Schema::create('tm_notas_credito_detalle', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('nc_id')->nullable();
            $table->unsignedBigInteger('factura_id')->nullable();
            $table->unsignedBigInteger('concepto')->nullable();
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('notas')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('maquina')->nullable();

            $table->index(["concepto"]);

            $table->index(["nc_id"]);

            $table->index(["factura_id"]);


            $table->foreign('factura_id')
                ->references('id')->on('tm_facturas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('nc_id')
                ->references('id')->on('tm_notas_credito')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('concepto')
                ->references('id')->on('sgc_servicios')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
        
        Schema::create('tm_ordenes_det', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('do');
            $table->string('contenedor')->nullable();
            $table->string('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->date('fecha_mod')->nullable();
            $table->string('sga', 20)->nullable();
        });
        
         Schema::create('tm_ordenes_doc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('do');
            $table->string('contenedor')->nullable();
            $table->string('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod', 45)->nullable();
            $table->string('observaciones')->nullable();
            
            $table->unique(["do", "atributo"]);
        });
        
        Schema::create('tm_ordenes_infogral', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('do');
            $table->string('contenedor')->nullable();
            $table->string('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->date('fecha_mod')->nullable();
            
            
            $table->unique(["do", "atributo"]);
            
        });
        
        
        Schema::create('tm_cuadro_contable', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('cotizacion_id')->nullable();
            $table->string('do_id');
            $table->string('t_pago')->nullable();
            $table->string('tipo_ingreso', 45)->nullable();
            $table->string('concepto')->nullable();
            $table->integer('id_grupo')->nullable();
            $table->integer('variacion')->nullable();
            $table->unsignedBigInteger('proveedor_id');
            $table->string('tercero_id', 45)->nullable();
            $table->string('aplica_por')->nullable();
            $table->double('unidad')->nullable()->default('1');
            $table->double('tarifa_venta')->nullable();
            $table->string('tipo_venta')->default("V");;
            $table->double('minima');
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('fv')->nullable();
            $table->string('fecha_fv', 45)->nullable();
            $table->double('minimacompra')->nullable();
            $table->double('tarifa_compra')->nullable();
            $table->string('tipo_compra')->default("V");
            $table->double('compra')->nullable();
            $table->string('monedacompra', 45)->nullable();
            $table->double('trmcompra')->nullable();
            $table->double('compra_mn')->nullable();
            $table->string('fc')->nullable();
            $table->string('fecha_fc', 45)->nullable();
            $table->double('comision');
            $table->double('valor_comision');
            $table->double('valor_comision_mn');
            $table->string('opcion', 45)->nullable();
            $table->string('cotizacion_obs')->nullable();
            $table->string('do_obs')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('attached',1000)->nullable();
        });
        
                
        Schema::create('tm_proveedores_contactos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('cedula_proveedor')->nullable();
            $table->string('empresa')->nullable();
            $table->string('nom_contacto')->nullable();
            $table->string('cargo')->nullable();
            $table->string('correo')->nullable();
            $table->string('celular')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('telefono')->nullable();
            $table->string('skype')->nullable();
            $table->string('notas')->nullable();
            $table->string('comercial', 45)->nullable();
            $table->string('operaciones', 45)->nullable();
            $table->string('tipo')->nullable();
            $table->string('administrativo')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["cedula_proveedor"]);
            //$table->unique('correo');
        });
        
        Schema::create('tm_proveedores_doclegal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('cedula');
            $table->integer('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('chk', 5)->nullable();
            $table->string('chk_ant', 5)->nullable();
            $table->string('maquina')->nullable();
            $table->string('usuario')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('observaciones')->nullable();
            
            $table->unique(["atributo", "cedula"]);
        });
        
        Schema::create('tm_proveedores_infogral', function (Blueprint $table) {
            $table->engine = 'InnoDB';            
            $table->unsignedBigInteger('cedula');
            $table->integer('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
//            $table->bigIncrements('rownum');

            $table->unique(["atributo", "cedula"]);
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('operativo');
    }

}
