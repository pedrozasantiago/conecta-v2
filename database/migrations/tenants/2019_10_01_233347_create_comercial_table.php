<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComercialTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sgc_actividades_seg', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('CAMPANA_ID');
            $table->string('COTIZACION_ID')->nullable();
            $table->string('CONSECUTIVO')->nullable();
            $table->string('CLIENTE_ID');
            $table->string('ESTADO')->nullable();
            $table->string('ACTIVIDAD_ID');
            $table->double('SECUENCIA')->nullable();
            $table->string('FECHA_REAL')->nullable();
            $table->string('FECHA_ESTIMADA')->nullable();
            $table->string('ESTADO_ACTIV')->nullable();
            $table->string('ORDEN_IMPRE')->nullable();
            $table->string('OBLIGATORIA')->nullable();
            $table->string('COLOR_ALERTA_ACTIV')->nullable();
            $table->string('FECHA_ALERTA_ACTIV')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
        });

        Schema::create('sgc_campana', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->bigInteger('CAMPANA_ID')->nullable();
            $table->string('DESCRIPCION')->nullable();
            $table->string('TIPO')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('ANO')->nullable();
            $table->string('COLOR_ALERTA')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->dateTime('FECHA_MOD')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('FECHA_INICIO', 25)->nullable();
            $table->string('FECHA_FIN', 25)->nullable();
            $table->string('FECHA_ALERTA', 25)->nullable();
            $table->string('OBSERVACIONES')->nullable();
            $table->text('destinatarios')->nullable();
        });

        Schema::create('sgc_campana_bitacora', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('row_num');
            $table->string('CAMPANA_ID',10);
            $table->string('tarea_id');
            $table->string('requerimiento_id',10);
            $table->string('CLIENTE_ID')->nullable();
            $table->unsignedBigInteger('contacto_id');
            // $table->bigInteger('contacto_id',false,false)->unsigned();
            $table->string('COTIZACION_ID', 10)->nullable();
            $table->string('ACTIVIDAD_ID', 10);
            $table->string('FECHA_BIT')->nullable();
            $table->string('FECHA', 20)->nullable();
            $table->text('DETALLE')->nullable();
            $table->string('RESPONSABLE')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('INTERNA')->nullable();
            $table->string('USUARIO')->nullable();
//            $table->string('FECHA_MOD')->nullable();
            $table->dateTime('FECHA_MOD')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->double('ANO')->nullable();
            $table->string('HORA', 20)->nullable();
            $table->string('ASESOR_ID', 45)->nullable();
            $table->string('INDICADOR', 45)->nullable();
            $table->string('INDIAGENDA', 45)->nullable();
            $table->text('destinatarios')->nullable();
            $table->json('changes');

            // $table->index(["CAMPANA_ID","tarea_id","requerimiento_id","cliente_id","contacto_id","cotizacion_id","actividad_id"]);
            $table->index(["CAMPANA_ID", "tarea_id", "requerimiento_id", "cliente_id", "contacto_id", "cotizacion_id", "actividad_id"], 'cmp_tsk_req_cli_ctc_cot_act_idx');

            // $table->index(["CAMPANA_ID", "tarea_id"]);
            // $table->index(["requerimiento_id", "cliente_id"]);
            // $table->index(["contacto_id", "cotizacion_id", "actividad_id"]);
            
        });

        Schema::create('sgc_campana_clientes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('CAMPANA_ID');
            $table->string('CONSECUTIVO')->nullable();
//            $table->Integer('CLIENTE_ID');
            $table->string('CLIENTE_ID');
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->dateTime('FECHA_MOD')->nullable();
            $table->string('SEGUIMIENTO', 20)->nullable();
            $table->string('CONTROL', 20)->nullable();
            $table->string('COLOR_ALERTA', 20)->nullable();
            $table->string('FECHA_ALERTA', 20)->nullable();
            $table->string('ESTADO', 20)->nullable();
            $table->string('FECHA_A', 50)->nullable();
            $table->string('ARCHIVO_V')->nullable();
            $table->string('FECHA_V', 50)->nullable();
            $table->string('CONCEPTO_V', 50)->nullable();
            $table->string('CALIFICACION_V', 50)->nullable();

            $table->unique(["CLIENTE_ID","CAMPANA_ID"]);


            $table->foreign('CAMPANA_ID')
                    ->references('ID')->on('sgc_campana')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('CLIENTE_ID')
                    ->references('CEDULA')->on('sgc_clientes')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });

        Schema::create('sgc_campana_comercial', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('CAMPANA_ID');
            $table->string('COMERCIAL_ID');
        });
        
         Schema::create('sgc_tipo_cargos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');          
            $table->string('nombre');         
            $table->string('tipo');         
            $table->string('estado');
            $table->integer('orden');   
            $table->string('operacion');
            $table->string('base');
        });
        
        Schema::create('sgc_cargos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('id_tipo');            
            $table->string('nombre');
            $table->double('valor');
            $table->string('estado');
            $table->integer('orden');   
        });
        
               
        Schema::create('sgc_cargo_servicios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id_cargo');
            $table->integer('id_servicio');
            
            $table->unique(['id_cargo','id_servicio']);
            
        });
        
        Schema::create('sgc_cotiza_cargos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id_preliquidacion');
            $table->integer('id_cargo');
            $table->double('subtotal');
            $table->double('tasa');
            $table->double('total_cargo');
            $table->string('moneda');
            $table->double('trm');
            $table->double('total_cargo_mn');
            
            $table->unique(['id_preliquidacion','id_cargo']);
            
        });
        
         Schema::create('sgc_cotiza_impuestos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cotizacion_id')->primary();
            $table->double('valor_mercancia_usd');
            $table->double('valor_fletes_usd');
            $table->double('tasa_seguro');
            $table->double('minima');
            $table->double('valor_seguro_usd');
            $table->double('valor_gorigen_usd');
            $table->double('valor_total_cif_usd');
            $table->string('moneda');
            $table->double('trm');
            $table->double('valor_total_cif_mn');
            $table->double('tasa_arancel');
            $table->double('valor_arancel');
            $table->double('tasa_iva');
            $table->double('valor_impuestos');
            $table->double('valor_total_tributos');
            $table->integer('version');            
            $table->string('usuario');
            $table->string('maquina');
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            
            $table->unique(['cotizacion_id','version']);
            
        });
        

        Schema::create('sgc_citas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cita')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('campana_id', 45)->nullable()->comment('Asociar la tarea a una campana');
            $table->string('cliente_id', 45)->nullable()->comment('Asociar la tarea a un cliente');
            $table->string('fecha_ini', 45)->nullable();
            $table->string('hora_ini', 45)->nullable();
            $table->string('fecha_fin', 45)->nullable();
            $table->string('hora_fin', 45)->nullable();
            $table->string('dia_entero', 45)->nullable();
            $table->string('estado', 45)->nullable()->comment('COMPLETA\\nINCOMPLETA\\nPENDIENTE\\nVENCIDA');
            $table->string('privado', 2)->nullable()->comment('Indicar si la tarea puede ser vista por otros usuarios o no.');
            $table->string('usuario', 45)->nullable();
            $table->string('maquina', 45)->nullable();
            $table->string('fecha_creacion', 45)->nullable();
            $table->string('ultima_modificacion', 45)->nullable();
            $table->string('modificado_por', 45)->nullable();
            $table->string('maquina_ultima_modificacion', 45)->nullable();
        });

        Schema::create('sgc_clientes_contactos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cedula_cliente')->nullable();
            $table->string('empresa')->nullable();
            $table->string('nom_contacto')->nullable();
            $table->string('cargo')->nullable();
            $table->string('tipo')->nullable();
            $table->string('correo')->nullable();
            $table->string('celular')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('telefono')->nullable();
            $table->string('skype')->nullable();
            $table->string('cumpleanos')->nullable();
            $table->string('notas')->nullable();
            $table->string('comercial', 45)->nullable();
            $table->string('operaciones', 45)->nullable();
            $table->string('administrativo')->nullable();
            $table->string('wp_status', 45)->nullable();
            $table->string('ig_user_id', 45)->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["cedula_cliente"]);
            //$table->unique('correo');
        });

        Schema::create('sgc_clientes_docaduana', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cedula');
            $table->string('atributo_id');
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('modificado_por', 45)->nullable();
            $table->dateTime('fecha_modificacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["cedula", "atributo_id"]);
            $table->unique(["cedula", "atributo_id"]);
        });

        Schema::create('sgc_clientes_doccred', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cedula');
            $table->bigInteger('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('maquina')->nullable();
            $table->string('usuario')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('observaciones')->nullable();

            $table->index(["cedula", "atributo"]);
            $table->unique(["cedula", "atributo"]);
        });

        Schema::create('sgc_clientes_doclegal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cedula');
            $table->bigInteger('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_anterior')->nullable();
            $table->string('chk', 5)->nullable();
            $table->string('chk_ant', 5)->nullable();
            $table->string('maquina')->nullable();
            $table->string('usuario')->nullable();
            $table->string('fecha_mod')->nullable();
            $table->string('observaciones')->nullable();

            $table->index(['cedula', 'atributo']);
            $table->unique(["cedula", "atributo"]);
        });

        Schema::create('sgc_clientes_infogral', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cedula');
            $table->bigInteger('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(['cedula', 'atributo']);
            $table->unique(["cedula", "atributo"]);
        });

        Schema::create('sgc_cotiza_infogral', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cotizacion');
            $table->bigInteger('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->integer('version');
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod')->nullable();

            $table->index(['cotizacion', 'atributo']);
            $table->unique(["cotizacion", "atributo","version"]);
        });

        Schema::create('sgc_cotiza_instruccion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cotizacion');
            $table->string('do')->nullable();
            $table->string('atributo');
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod')->nullable();

            $table->index(['cotizacion', 'atributo']);
            $table->unique(["cotizacion", "atributo"]);
        });

        Schema::create('sgc_cotiza_segactiv', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('CAMPANA_ID')->nullable();
            $table->string('requerimiento_id');
            $table->string('COTIZACION_ID');
            $table->string('CONSECUTIVO')->nullable();
            $table->string('CLIENTE_ID')->nullable();
            $table->string('contacto_id')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('ACTIVIDAD_ID');
            $table->double('SECUENCIA')->nullable();
            $table->string('FECHA_REAL')->nullable();
            $table->string('FECHA_ESTIMADA')->nullable();
            $table->string('ESTADO_ACTIV')->nullable();
            $table->string('ORDEN_IMPRE')->nullable();
            $table->string('OBLIGATORIA')->nullable();
            $table->string('COLOR_ALERTA_ACTIV')->nullable();
            $table->string('FECHA_ALERTA_ACTIV')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->string('FECHA_INS')->nullable();
            $table->index(['COTIZACION_ID', 'ACTIVIDAD_ID']);
            $table->unique(['COTIZACION_ID', 'ACTIVIDAD_ID']);
        });

        Schema::create('sgc_cotizaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('COTIZACION_ID')->primary();
            $table->string('CAMPANA_ID')->nullable();
            $table->string('ASESOR_ID')->nullable();
            $table->string('CLIENTE_ID');
            $table->string('PROVEEDOR_ID')->nullable();
            $table->string('FECHA_INGRESO')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('MONEDA')->nullable();
            $table->string('PROFILE')->nullable();
            $table->string('OFICINA')->nullable();
            $table->string('MOTIVO_RECHAZO')->nullable();
            $table->string('ARCHIVO')->nullable();
            $table->string('ARCH_INFO')->nullable();
            $table->text('DESCRIPCION')->nullable();
            $table->string('DESTINO')->nullable();
            $table->string('ORIGEN')->nullable();
            $table->string('contacto')->nullable();
            $table->string('cargo_contacto')->nullable();
            $table->string('consecutivo')->nullable();
            $table->string('USUARIO')->nullable();
            $table->string('MAQUINA')->nullable();
            $table->string('FECHA_MOD')->nullable();
            $table->string('FECHA_INS')->nullable();
            $table->string('SUBTIPO_OPER', 45)->nullable();
            $table->string('SERVICIO', 45)->nullable();
            $table->string('chk_origen', 45)->nullable();
            $table->string('chk_seguro', 45)->nullable();
            $table->string('chk_flete', 45)->nullable();
            $table->string('chk_destino', 45)->nullable();
            $table->string('chk_aduana', 45)->nullable();
            $table->string('chk_terrestre', 45)->nullable();
            $table->string('chk_condiciones', 45)->nullable();
            $table->string('do_asociado', 45)->nullable();
            $table->string('idioma', 45)->nullable();
            $table->string('CONSIGNATARIO_ID')->nullable();
            $table->string('SHIPPER_ID')->nullable();
            $table->string('NOTIFICADOR_ID')->nullable();
            $table->text('shipping_observaciones')->nullable();            
            $table->string('contacto_id', 45)->nullable();
            $table->integer('version')->nullable()->comment('Muestra la version aprobada en la cotización');

            $table->index(['CAMPANA_ID', 'CLIENTE_ID']);
        });

       /* Schema::create('sgc_cotizaciones_detalle', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('id_preliquidacion')->nullable();            
            $table->string('cotizacion_id');
            $table->string('atributo_id');
            $table->string('agrupador', 45);
            $table->string('grp');
            $table->string('valor')->nullable()->default('-');
            $table->string('fecha_creacion')->nullable();
            $table->string('maquina_creacion')->nullable();
            $table->string('usuario_creacion')->nullable();
            $table->string('fecha_modificacion')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();

            $table->index(["atributo_id", "cotizacion_id", "grp"]);
        });*/

        Schema::create('sgc_cotizaciones_obs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id')->nullable();
            $table->string('cotizacion_id');
            $table->text('valor')->nullable();
            $table->integer('version');
            $table->string('usuario', 45)->nullable();
            $table->string('maquina', 45)->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["cotizacion_id"]);
            
            $table->unique(["cotizacion_id", "version"]);
        });

        Schema::create('sgc_cotizaciones_versiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cotizacion_id');
            $table->integer('version');
            $table->string('fecha', 45)->nullable();
            $table->string('notas')->nullable();
            $table->string('usuario', 45)->nullable();
            $table->string('maquina', 45)->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            
            $table->unique(['cotizacion_id','version']);
            
        });

        Schema::create('sgc_mailing_response', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('id_empresa', 45)->nullable()->comment('\'columna para confirmar que el correo fue enviado por la empresa\'');
            $table->string('evento', 45);
            $table->string('correo')->nullable();
            $table->string('ip_destinatario', 45)->nullable();
            $table->string('link', 45)->nullable();
            $table->string('user_agent', 45)->nullable();
            $table->string('detalle', 45)->nullable();
            $table->text('arreglo')->nullable();
            $table->dateTime('fecha')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });


        Schema::create('sgc_preliquidacion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cotizacion_id');
            $table->string('do_id')->nullable();
            $table->unsignedBigInteger('concepto')->nullable();
            $table->integer('id_grupo')->nullable();
            $table->integer('variacion')->nullable();
            $table->bigInteger('proveedor_id')->nullable();
            $table->string('tercero_id')->nullable();
            $table->string('aplica_por')->nullable();
            $table->double('unidad')->nullable()->default('1');
            $table->double('tarifa_venta')->nullable();
            $table->double('minima');
            $table->string('tipo_venta')->default("V");
            $table->double('subtotal_venta')->nullable();
            $table->string('monedaventa', 45)->nullable();
            $table->double('trmventa')->nullable();
            $table->double('tasa_impuesto')->nullable();
            $table->double('venta_total')->nullable();
            $table->double('venta_total_mn')->nullable();
            $table->double('impuesto')->nullable();
            $table->double('impuesto_mn')->nullable();
            $table->double('subtotal_venta_mn')->nullable();
            $table->string('fv')->nullable();
            $table->string('fecha_fv', 45)->nullable();
            $table->double('minimacompra')->nullable();
            $table->double('tarifa_compra')->nullable();
            $table->string('tipo_compra')->default("V");;
            $table->double('compra')->nullable();
            $table->string('monedacompra', 45)->nullable();
            $table->double('trmcompra')->nullable();
            $table->double('compra_mn')->nullable();
            $table->string('fc')->nullable();
            $table->string('fecha_fc', 45)->nullable();
            $table->string('opcion', 45)->nullable();
            $table->text('cotizacion_obs');
            $table->string('do_obs')->nullable();
            $table->integer('version')->nullable();
            $table->string('usuario')->nullable();
            $table->string('maquina')->nullable();
            $table->string('fecha_mod')->nullable();

            $table->index(["proveedor_id"]);

            $table->index(["concepto"]);

//            $table->index(["cotizacion_id"]);


            $table->foreign('cotizacion_id')
                    ->references('COTIZACION_ID')->on('sgc_cotizaciones')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('concepto')
                    ->references('id')->on('sgc_servicios')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('proveedor_id')
                    ->references('cedula')->on('tm_proveedores')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });

        Schema::create('sgc_qt', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cliente_id', 45)->nullable();
            $table->string('nom_empresa', 45)->nullable();
            $table->integer('contacto_id')->nullable();
            $table->string('asesor_id', 45)->nullable();
            $table->string('tipo_intercambio', 45)->nullable();
            $table->string('origen')->nullable();
            $table->string('destino')->nullable();
            $table->string('comercial_id', 45)->nullable();
            $table->string('fecha_actualizacion', 45)->nullable();
            $table->string('fecha_creacion', 45)->nullable();
            $table->string('usuario', 45)->nullable();
            $table->string('maquina', 45)->nullable();
        });

        Schema::create('sgc_requerimientos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('campana_id', 45)->nullable();
            $table->string('cliente_id', 45)->nullable();
            $table->string('contacto_id', 45)->nullable();
            $table->string('oficina', 45)->nullable();
            $table->string('estado', 45)->nullable();
            $table->string('subtipo', 45)->nullable();
            $table->string('servicio', 45)->nullable();
            $table->string('origen')->nullable();
            $table->string('destino')->nullable();
            $table->string('comercial', 45)->nullable();
            $table->string('moneda', 45)->nullable();
            $table->string('cotizacion_id', 45)->nullable();
            $table->text('observaciones')->nullable();
            $table->string('fecha_creacion', 45)->nullable();
            $table->string('creado_por', 45)->nullable();
            $table->string('fecha_modificacion', 45)->nullable();
            $table->string('modificado_por', 45)->nullable();
        });

        Schema::create('sgc_requerimientos_detalle', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('requerimiento_id')->nullable();
            $table->string('atributo', 45)->nullable();
            $table->string('valor')->nullable();
            $table->string('valor_ant')->nullable();
            $table->string('usuario', 45)->nullable();
            $table->string('fecha_mod', 45)->nullable();

            $table->unique(["requerimiento_id", "atributo"], 'requerimiento_id_UNIQUE');
        });

        Schema::create('sgc_sms_response', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mensaje', 160)->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('group_id', 45)->nullable();
            $table->string('group_name')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('id_description')->nullable();
            $table->string('sms_count', 45)->nullable();
            $table->string('messageId')->nullable();
            $table->string('fecha')->nullable();
            $table->string('hora')->nullable();
            $table->string('usuario', 45)->nullable();
            $table->string('ip', 45)->nullable();
        });


        Schema::create('sgc_sms_saldo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('descripcion', 45)->nullable();
            $table->string('cantidad', 45)->nullable();
            $table->dateTime('fecha')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('comercial');
    }

}
