<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSgcCotizaImpuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sgc_cotiza_impuestos', function (Blueprint $table) {
            $table->dropPrimary('cotizacion_id');
            $table->dropUnique('sgc_cotiza_impuestos_cotizacion_id_version_unique');
            
        });
        
        Schema::table('sgc_cotiza_impuestos', function (Blueprint $table) {
            
            $table->bigIncrements('id')->first();
            $table->string('subpartida')->nullable()->after('cotizacion_id');
        });
        
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
