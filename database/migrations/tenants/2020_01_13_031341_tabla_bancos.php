<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaBancos extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasTable('tm_bancos')) {
            Schema::create('tm_bancos', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('tipo')->nullable();
                $table->string('nombre')->nullable();
                $table->string('cuenta_numero');
                $table->double('saldo_inicial')->nullable();
                $table->string('fecha')->nullable();
                $table->string('estado');
                $table->string('descripcion')->nullable();
                $table->string('modificado_por')->nullable();
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por', 45)->nullable();
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }



        Schema::table('tm_banco_egresos', function (Blueprint $table) {

            $table->string('moneda', 45)->nullable();
            $table->double('trm')->nullable();
            $table->double('valor_mn')->nullable();
            $table->double('retencion_mn')->nullable();
        });



        Schema::table('tm_banco_ingresos', function (Blueprint $table) {

            $table->string('moneda', 45)->nullable();
            $table->double('trm')->nullable();
            $table->double('valor_mn')->nullable();
            $table->double('retencion_mn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists('tm_bancos');

        Schema::table('tm_banco_egresos', function (Blueprint $table) {
            $table->dropColumn(['moneda', 'trm', 'valor_mn', 'retencion_mn']);
        });

        Schema::table('tm_banco_ingresos', function (Blueprint $table) {
            $table->dropColumn(['moneda', 'trm', 'valor_mn', 'retencion_mn']);
        });
    }

}
