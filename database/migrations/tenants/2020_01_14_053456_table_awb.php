<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableAwb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tm_awb', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do')->unique();
            $table->string('agent_iata_code')->nullable();
            $table->string('account_num')->nullable();
            $table->string('airport_departure')->nullable();
            $table->string('first_to')->nullable();
            $table->string('first_carrier')->nullable();
            $table->string('second_to')->nullable();
            $table->string('second_carrrier')->nullable();
            $table->string('third_to')->nullable();
            $table->string('third_carrier')->nullable();
            $table->string('airport_destination')->nullable();
            $table->string('currency')->nullable();
            $table->string('trm')->nullable();
            $table->string('chgs_code')->nullable();
            $table->string('wt_val')->nullable();
            $table->string('other')->nullable();
            $table->string('declared_v_customs')->nullable();
            $table->string('declared_v_carriage')->nullable();
            $table->string('amount_insurance')->nullable();
            $table->text('handling_information')->nullable();
            $table->text('goods')->nullable();
            $table->text('other_charges')->nullable();
            $table->string('type_weight_charge')->nullable();
            $table->double('weight_charge')->nullable();
            $table->string('type_valuation_charge')->nullable();
            $table->double('valuation_charge')->nullable();
            $table->string('type_tax')->nullable();
            $table->double('tax')->nullable();
            $table->string('type_charges_due_agent')->nullable();
            $table->double('charges_due_agent')->nullable();
            $table->string('type_charges_due_carrier')->nullable();
            $table->double('charges_due_carrier')->nullable();
          
            $table->string('modificado_por')->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('creado_por')->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_bancos');
    }
}
