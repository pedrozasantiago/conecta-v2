<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class LlenarTmAsociarCodigos extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        $selects = DB::select(DB::raw("SELECT * FROM tm_listas  where tipo = 'SUBTIPO_OPER' and activo  = 'SI';"));

        foreach ($selects as $select) {
            $query = "insert ignore into tm_asociar_codigos (tipo, codigo, codasoc, grupo, estado) select  'INFORDEN',atributo,'$select->codigo','$select->codigo','ACT' from tm_atributos where grupo = 'INFORDEN'  and estado = 'ACT' ";
            DB::statement($query);
        }
        
        Schema::dropIfExists('tm_awb');
        
        Schema::create('tm_awb', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do');
            $table->double('cant_piezas')->nullable();
            $table->double('peso_bruto')->nullable();
            $table->string('kg_lb')->nullable();
            $table->double('rate_charge')->nullable();
            $table->double('largo')->nullable();
            $table->double('ancho')->nullable();
            $table->double('alto')->nullable();
            $table->double('peso_volumetrico')->nullable();
            $table->string('dim_unidad')->nullable();
            $table->double('total')->nullable();
              
            $table->string('modificado_por')->nullable();
            $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('creado_por')->nullable();
            $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
