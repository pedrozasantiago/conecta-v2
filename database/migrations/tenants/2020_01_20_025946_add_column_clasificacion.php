<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnClasificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tm_banco_egresos', function (Blueprint $table) {
            $table->string('clasificacion', 45)->nullable()->after('motivo_anulacion');
        });
        
         Schema::table('tm_banco_ingresos', function (Blueprint $table) {
            $table->string('clasificacion', 45)->nullable()->after('motivo_anulacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
