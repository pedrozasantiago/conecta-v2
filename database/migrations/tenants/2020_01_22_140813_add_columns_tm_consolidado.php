<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTmConsolidado extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::table('tm_consolidado')->truncate();

        DB::table('tm_consolidado_do')->truncate();

        if (Schema::hasColumn('tm_consolidado', 'consecutivo')) {
            Schema::table('tm_consolidado', function (Blueprint $table) {
                $table->dropColumn('consecutivo');
            });
        }
        
        if (Schema::hasColumn('tm_consolidado', 'do')) {
            Schema::table('tm_consolidado', function (Blueprint $table) {
                $table->dropColumn('do');
            });
        }
        
        if (Schema::hasColumn('tm_consolidado', 'shipper_id')) {
            Schema::table('tm_consolidado', function (Blueprint $table) {
                $table->dropColumn('shipper_id');
            });
        }
        
        if (Schema::hasColumn('tm_consolidado', 'consignatario_id')) {
            Schema::table('tm_consolidado', function (Blueprint $table) {
                $table->dropColumn('consignatario_id');
            });
        }
        if (Schema::hasColumn('tm_consolidado', 'notify_id')) {
            Schema::table('tm_consolidado', function (Blueprint $table) {
                $table->dropColumn('notify_id');
            });
        }

        Schema::table('tm_consolidado', function (Blueprint $table) {
            $table->string('consecutivo', 45)->nullable();
            $table->string('do', 45)->nullable();
            $table->string('shipper_id')->nullable();
            $table->string('consignatario_id')->nullable();
            $table->string('notify_id')->nullable();
        });


        DB::table('sgc_sms_saldo')->insertOrIgnore(['descripcion' => 'COMPRA', 'CANTIDAD' => '1000']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
