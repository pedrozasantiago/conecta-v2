<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableImpuestosDo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('tm_do_impuestos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('do');
            $table->string('subpartida');
            $table->string('descripcion');            
            $table->double('valor_mercancia_usd');
            $table->double('valor_fletes_usd');
            $table->double('tasa_seguro');
            $table->double('minima');
            $table->double('valor_seguro_usd');
            $table->double('valor_gorigen_usd');
            $table->double('valor_total_cif_usd');
            $table->string('moneda'); 
            $table->double('trm');
            $table->double('valor_total_cif_mn');  
            $table->double('tasa_arancel'); 
            $table->double('valor_arancel'); 
            $table->double('tasa_iva'); 
            $table->double('valor_impuestos'); 
            $table->double('valor_total_tributos'); 
            $table->string('usuario');
            $table->string('maquina');
            $table->dateTime('fecha_mod')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('fecha_creacion')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
         
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
