<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPermissionTmDo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tm_permisos')->insertOrIgnore([
                "cod" => "C",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Cerrar",
                "descripcion" => "Cerrar el DO"
            ]);
        
        DB::table('tm_permisos')->insertOrIgnore([
                "cod" => "ANULAR",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Anular",
                "descripcion" => "Anular el DO"
            ]);
        
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
