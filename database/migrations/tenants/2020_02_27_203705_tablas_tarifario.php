<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablasTarifario extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {


        if (!Schema::hasColumn('sgc_cotizaciones', 'tarifario_id')) {
            Schema::table('sgc_cotizaciones', function (Blueprint $table) {
                $table->string('tarifario_id')->nullable()->after('version');
            });
        }




        if (!Schema::hasTable('sgc_tipo_tarifario')) {
            Schema::create('sgc_tipo_tarifario', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('nombre');
                $table->string('subtipo');
                $table->string('servicio');
                $table->string('estado');
            });
        }


        if (!Schema::hasTable('sgc_tipo_tarifario_adicionales')) {
            Schema::create('sgc_tipo_tarifario_adicionales', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_tarifario');
                $table->string('nombre');
                $table->string('estado');
                $table->string('orden');
                $table->string('modificado_por');
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por');
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }

        if (!Schema::hasTable('sgc_tipo_tarifario_servicios')) {
            Schema::create('sgc_tipo_tarifario_servicios', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_tarifario');
                $table->string('id_servicio');
                $table->string('id_variacion');
                $table->string('estado');
                $table->string('orden');
                $table->string('modificado_por');
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por');
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));

                $table->unique(['id_tarifario', 'id_servicio', 'id_variacion'], 'sgc_tarifario_servicios_unique');
            });
        }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

        if (!Schema::hasTable('sgc_cotiza_tarifario')) {
            Schema::create('sgc_cotiza_tarifario', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('cotizacion_id');
                $table->string('version', 10);
                $table->string('id_tarifario', 10);
                $table->string('id_proveedor');
                $table->string('pais_origen', 100);
                $table->string('ciudad_origen', 100);
                $table->string('pais_destino', 100);
                $table->string('ciudad_destino', 100);
            });
        }

        if (!Schema::hasTable('sgc_cotiza_tarifario_adicionales')) {
            Schema::create('sgc_cotiza_tarifario_adicionales', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_cotiza_tarifario');
                $table->string('id_adicional');
                $table->string('valor');

                $table->unique(['id_cotiza_tarifario', 'id_adicional'], 'sgc_c_tarifario_adicionales_unique');
            });
        }

        if (!Schema::hasTable('sgc_cotiza_tarifario_servicios')) {
            Schema::create('sgc_cotiza_tarifario_servicios', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_cotiza_tarifario');
                $table->string('id_servicio');
                $table->string('id_variacion');
                $table->string('valor');

                $table->unique(['id_cotiza_tarifario', 'id_servicio', 'id_variacion'], 'sgc_c_tarifario_servicios_unique');
            });
        }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

        if (!Schema::hasTable('sgc_tarifario')) {
            Schema::create('sgc_tarifario', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('tipo',10);
                $table->string('id_tarifario', 10);
                $table->string('id_proveedor');
                $table->string('pais_origen', 100);
                $table->string('ciudad_origen', 100);
                $table->string('pais_destino', 100);
                $table->string('ciudad_destino', 100);
                
                $table->unique(['tipo', 'id_tarifario','id_proveedor','pais_origen','ciudad_origen','pais_destino','ciudad_destino'],'sgc_tarifario_unique');
                
            });
        }

        if (!Schema::hasTable('sgc_tarifario_adicionales')) {
            Schema::create('sgc_tarifario_adicionales', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_row_tarifario');
                $table->string('id_adicional');
                $table->string('valor');

                $table->unique(['id_row_tarifario', 'id_adicional'],'sgc_tarifario_adicionales_unique');
            });
        }

        if (!Schema::hasTable('sgc_tarifario_servicios')) {
            Schema::create('sgc_tarifario_servicios', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_row_tarifario');
                $table->string('id_servicio');
                $table->string('id_variacion');
                $table->string('valor');

                $table->unique(['id_row_tarifario', 'id_servicio', 'id_variacion'],'sgc_tarifario_servicios_unique');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
