<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPreliquidacion extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        

        if (!Schema::hasTable('sgc_cotiza_caracteristicas')) {
            Schema::create('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->string('cotizacion_id')->primary();
                $table->string('contenedores')->nullable();
                $table->string('piezas')->nullable();
                $table->string('peso_bruto')->nullable();
                $table->string('unidad_peso')->nullable();
                $table->string('largo')->nullable();
                $table->string('ancho')->nullable();
                $table->string('alto')->nullable();
                $table->string('unidad_dimension')->nullable();
                $table->string('peso_volumetrico')->nullable();
                $table->string('usuario')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
