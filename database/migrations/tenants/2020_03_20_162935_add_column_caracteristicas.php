<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCaracteristicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotiza_caracteristicas')) {
            
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'tipo')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->string('tipo')->nullable()->after('cotizacion_id');
                });
            }
            
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'valor_fob')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->string('valor_fob')->nullable()->after('tipo');
                });
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
