<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TarifarioCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('sgc_tipo_tarifario_compras')) {
            Schema::create('sgc_tipo_tarifario_compras', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_tarifario');
                $table->string('nombre');
                $table->string('estado');
                $table->string('orden');
                $table->string('modificado_por');
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por');
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
        
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        if (!Schema::hasTable('sgc_tarifario_prov')) {
            Schema::create('sgc_tarifario_prov', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('tipo',10);
                $table->string('id_tarifario', 10);
                $table->string('id_proveedor');
                $table->string('pais_origen', 100);
                $table->string('ciudad_origen', 100);
                $table->string('pais_destino', 100);
                $table->string('ciudad_destino', 100);
                
                $table->unique(['tipo', 'id_tarifario','id_proveedor','pais_origen','ciudad_origen','pais_destino','ciudad_destino'],'sgc_tarifario_prov_unique');
                
            });
        }
        
        if (!Schema::hasTable('sgc_tarifario_prov_compras')) {
            Schema::create('sgc_tarifario_prov_compras', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_row_tarifario');
                $table->string('id_adicional');
                $table->string('valor');

                $table->unique(['id_row_tarifario', 'id_adicional'],'sgc_tarifario_prov_compras_unique');
            });
        }
        
        
        if (!Schema::hasTable('sgc_tarifario_prov_adicionales')) {
            Schema::create('sgc_tarifario_prov_adicionales', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_row_tarifario');
                $table->string('id_adicional');
                $table->string('valor');

                $table->unique(['id_row_tarifario', 'id_adicional'],'sgc_tarifario_prov_adicionales_unique');
            });
        }

        if (!Schema::hasTable('sgc_tarifario_prov_servicios')) {
            Schema::create('sgc_tarifario_prov_servicios', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('id_row_tarifario');
                $table->string('id_servicio');
                $table->string('id_variacion');
                $table->string('valor');

                $table->unique(['id_row_tarifario', 'id_servicio', 'id_variacion'],'sgc_tarifario_prov_servicios_unique');
            });
        }
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
