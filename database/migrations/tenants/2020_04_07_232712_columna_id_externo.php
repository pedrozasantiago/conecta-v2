<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ColumnaIdExterno extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasTable('sgc_clientes')) {

            if (!Schema::hasColumn('sgc_clientes', 'id_externo')) {
                Schema::table('sgc_clientes', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('CEDULA');
                });
            }
        }
        
        if (Schema::hasTable('tm_proveedores')) {
            if (!Schema::hasColumn('tm_proveedores', 'id_externo')) {
                Schema::table('tm_proveedores', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('cedula');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
