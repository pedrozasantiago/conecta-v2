<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDescuento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_factura_detalle')) {
            
            
            if (!Schema::hasColumn('tm_factura_detalle', 'unidad')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('unidad')->after('concepto');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'tarifa_venta')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('tarifa_venta')->after('unidad');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'tipo_descuento')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->string('tipo_descuento')->nullable()->after('subtotal_venta');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'descuento')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('descuento')->after('tipo_descuento');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'valor_descuento')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('valor_descuento')->after('descuento');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'valor_descuento_mn')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('valor_descuento_mn')->after('valor_descuento');
                });
            }
               
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
