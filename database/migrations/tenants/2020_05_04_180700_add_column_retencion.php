<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRetencion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_factura_detalle')) {
            
            
            
            if (!Schema::hasColumn('tm_factura_detalle', 'retencion')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('retencion')->after('tarifa_venta');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'valor_retencion')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('valor_retencion')->after('retencion');
                });
            }
            
            if (!Schema::hasColumn('tm_factura_detalle', 'valor_retencion_mn')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->double('valor_retencion_mn')->after('valor_retencion');
                });
            }
               
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
