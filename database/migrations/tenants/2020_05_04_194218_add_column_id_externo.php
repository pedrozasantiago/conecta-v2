<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdExterno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (Schema::hasTable('tm_empleados')) {
            if (!Schema::hasColumn('tm_empleados', 'id_externo')) {
                Schema::table('tm_empleados', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('cedula');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
