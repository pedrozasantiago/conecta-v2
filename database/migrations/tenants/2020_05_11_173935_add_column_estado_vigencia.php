<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnEstadoVigencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (Schema::hasTable('sgc_tarifario')) {
            if (!Schema::hasColumn('sgc_tarifario', 'estado')) {
                Schema::table('sgc_tarifario', function (Blueprint $table) {
                    $table->string('estado')->nullable()->default(1)->after('id');
                });
            }
            
            if (!Schema::hasColumn('sgc_tarifario', 'vigencia')) {
                Schema::table('sgc_tarifario', function (Blueprint $table) {
                    $table->string('vigencia')->nullable()->after('ciudad_destino');
                });
            }
        }
        
         if (Schema::hasTable('sgc_tarifario_prov')) {
            if (!Schema::hasColumn('sgc_tarifario_prov', 'estado')) {
                Schema::table('sgc_tarifario_prov', function (Blueprint $table) {
                    $table->string('estado')->nullable()->default(1)->after('id');
                });
            }
            
            if (!Schema::hasColumn('sgc_tarifario_prov', 'vigencia')) {
                Schema::table('sgc_tarifario_prov', function (Blueprint $table) {
                    $table->string('vigencia')->nullable()->after('ciudad_destino');
                });
            }
        }
        
         if (Schema::hasTable('sgc_cotiza_tarifario')) {
            if (!Schema::hasColumn('sgc_cotiza_tarifario', 'estado')) {
                Schema::table('sgc_cotiza_tarifario', function (Blueprint $table) {
                    $table->string('estado')->nullable()->default(1)->after('id');
                });
            }
            
            if (!Schema::hasColumn('sgc_cotiza_tarifario', 'vigencia')) {
                Schema::table('sgc_cotiza_tarifario', function (Blueprint $table) {
                    $table->string('vigencia')->nullable()->after('ciudad_destino');
                });
            }
        }
        
        //
//        
//        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
