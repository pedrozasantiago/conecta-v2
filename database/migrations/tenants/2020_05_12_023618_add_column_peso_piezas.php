<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPesoPiezas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_awb')) {
            if (!Schema::hasColumn('tm_awb', 'peso_piezas')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->string('peso_piezas')->nullable()->after('cant_piezas');
                });
            }            
        }
        
        if (Schema::hasTable('sgc_cotiza_caracteristicas')) {
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'volumen')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->string('volumen')->nullable()->after('unidad_dimension');
                });
            }            
        }
        
        if (Schema::hasTable('sgc_cotiza_caracteristicas')) {
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'valor_cif')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->string('valor_cif')->nullable()->after('valor_fob');
                });
            }            
        }
        
        if (Schema::hasTable('sgc_cotiza_caracteristicas')) {
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'peso_piezas')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->string('peso_piezas')->nullable()->after('piezas');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('tm_awb')) {
            if (Schema::hasColumn('tm_awb', 'peso_piezas')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->dropColumn('peso_piezas');
                });
            }
        }
        
    }
}
