<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSgcCitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_citas')) {
            if (!Schema::hasColumn('sgc_citas', 'contacto_id')) {
                Schema::table('sgc_citas', function (Blueprint $table) {
                    $table->string('contacto_id')->nullable()->after('cliente_id');
                });
            }  
            
            if (!Schema::hasColumn('sgc_citas', 'uuid')) {
                Schema::table('sgc_citas', function (Blueprint $table) {
                    $table->string('uuid')->nullable()->after('id');
                });
            }
            if (!Schema::hasColumn('sgc_citas', 'secuencia')) {
                Schema::table('sgc_citas', function (Blueprint $table) {
                    $table->string('secuencia')->nullable()->after('uuid');
                });
            } 
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
