<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAsesor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_clientes')) {
            if (!Schema::hasColumn('sgc_clientes', 'asesor_id')) {
                Schema::table('sgc_clientes', function (Blueprint $table) {
                    $table->string('asesor_id')->nullable()->after('TIPO_CLIENTE');
                });
            }
        }
        
        if (Schema::hasTable('tm_consolidado')) {
            if (!Schema::hasColumn('tm_consolidado', 'transportista')) {
                Schema::table('tm_consolidado', function (Blueprint $table) {
                    $table->string('transportista')->nullable()->after('servicio');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
