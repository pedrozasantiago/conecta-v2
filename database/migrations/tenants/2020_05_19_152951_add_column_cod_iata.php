<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCodIata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_proveedores')) {
            if (!Schema::hasColumn('tm_proveedores', 'cod_iata')) {
                Schema::table('tm_proveedores', function (Blueprint $table) {
                    $table->string('cod_iata')->nullable()->after('tipo');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
