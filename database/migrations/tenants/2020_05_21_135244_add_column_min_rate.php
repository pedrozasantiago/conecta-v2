<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMinRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_awb')) {
            if (!Schema::hasColumn('tm_awb', 'rate_charge_class')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->string('rate_charge_class')->nullable()->after('rate_charge');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
