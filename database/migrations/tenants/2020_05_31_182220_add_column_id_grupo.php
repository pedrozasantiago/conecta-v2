<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdGrupo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

 
        if (Schema::hasTable('sgc_servicios')) {
            Schema::table('sgc_servicios', function (Blueprint $table) {
                $foreignKeys = $this->listTableForeignKeys('sgc_servicios');
                if (in_array('sgc_servicios_tipo_servicio_foreign', $foreignKeys))
                    $table->dropForeign('sgc_servicios_tipo_servicio_foreign');
            });
        }


        if (!Schema::hasTable('sgc_servicios_grupos')) {
            Schema::create('sgc_servicios_grupos', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('id_grupo')->nullable();
                $table->integer('id_servicio')->nullable();
                $table->unique(["id_grupo", "id_servicio"]);
            });
        }

 
        DB::statement("INSERT IGNORE INTO `sgc_servicios_grupos` (`id_grupo`,`id_servicio`) select tipo_servicio,id from sgc_servicios; ");
        
        DB::statement("UPDATE sgc_preliquidacion a INNER JOIN  sgc_servicios  b ON a.concepto = b.id  SET   a.id_grupo = b.tipo_servicio ; ");
        
        DB::statement("UPDATE tm_cuadro_contable a INNER JOIN  sgc_servicios  b  ON a.concepto = b.id  SET  a.id_grupo = b.tipo_servicio ; ");
        
        
    }

    public function listTableForeignKeys($table) {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
