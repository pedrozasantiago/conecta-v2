<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFacturaIdGrupo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_factura_detalle')) {
            if (!Schema::hasColumn('tm_factura_detalle', 'id_grupo')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->integer('id_grupo')->nullable()->after('concepto');
                });
            }
            
            DB::statement("UPDATE tm_factura_detalle a INNER JOIN  sgc_servicios  b ON a.concepto = b.id  SET   a.id_grupo = b.tipo_servicio ; ");
        }
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
