<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModuleContable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        
        if (!Schema::hasTable('tm_registro_diario')) {
            Schema::create('tm_registro_diario', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('registro')->nullable();
                $table->string('tipo_transaccion')->nullable();
                $table->string('fecha')->nullable();
                $table->string('ano')->nullable();
                $table->string('mes')->nullable();
                $table->string('dia')->nullable();
                $table->string('cuenta')->nullable();
                $table->string('naturaleza')->nullable();
                $table->string('comprobante')->nullable();
                $table->string('centro_costos')->nullable();
                $table->string('subcentro_costos')->nullable();
                $table->double('valor')->nullable();
                $table->string('tercero')->nullable();
                $table->string('observaciones')->nullable();
                $table->string('modificado_por')->nullable();
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por')->nullable();
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }

        if (!Schema::hasTable('tm_puc')) {
            Schema::create('tm_puc', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('cuenta')->nullable()->unique();
                $table->string('descripcion')->nullable();
                $table->string('clase')->nullable();
                $table->string('saldo_inicial')->nullable();
                $table->string('fecha')->nullable();
                $table->string('orden')->nullable();
                $table->integer('id_parent');
            });
        }

        if (!Schema::hasTable('sgc_servicios_mapping')) {
            Schema::create('sgc_servicios_mapping', function (Blueprint $table) {
                $table->string('canal')->nullable();
                $table->string('tipo')->nullable();
                $table->integer('id_servicio');
                $table->string('valor')->nullable();
                $table->unique(['canal','id_servicio','tipo']);
            });
        }
        
         if (!Schema::hasTable('tm_puc_config')) {
            Schema::create('tm_puc_config', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('tipo_transac')->nullable();
                $table->string('cuenta')->nullable();
                $table->string('naturaleza')->nullable();
            }); 
            
      
        } 
        
        if (Schema::hasTable('tm_puc_config')) {
            
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('FV', '42353010', 'C'); ");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('FV', '130505', 'D'); ");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('NC', '417505', 'D'); ");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('NC', '130505', 'C');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('RC', '130505', 'C');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('RC', '111005', 'D');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('FC', '511095', 'D');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('FC', '220505', 'C');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('CE', '220505', 'D');");
            DB::statement("INSERT IGNORE INTO `tm_puc_config` (`tipo_transac`, `cuenta`, `naturaleza`) VALUES ('CE', '111005', 'C');");
            
        }
            
            
         
        
        
        
        if (Schema::hasTable('sgc_cargos')) {
            if (!Schema::hasColumn('sgc_cargos', 'id_externo')) {
                Schema::table('sgc_cargos', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('orden');
                });
            }
        }
        
         if (Schema::hasTable('tm_bancos')) {
            if (!Schema::hasColumn('tm_bancos', 'id_externo')) {
                Schema::table('tm_bancos', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('id');
                });
            }
        }
         
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
//        Schema::dropIfExists('tm_registro_diario');
//        Schema::dropIfExists('tm_puc');
//        Schema::dropIfExists('sgc_servicios_mapping');
        
    }

}
