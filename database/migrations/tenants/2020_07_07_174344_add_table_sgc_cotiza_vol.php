<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableSgcCotizaVol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sgc_cotiza_volumen')) {
            Schema::create('sgc_cotiza_volumen', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('cotizacion_id')->nullable();
                $table->integer('version')->default(0);
                $table->double('cant_piezas')->nullable();
                $table->string('peso_piezas')->nullable()->default(0);
                $table->double('peso_bruto')->nullable();
                $table->string('kg_lb')->nullable();
                $table->double('rate_charge')->nullable();
                $table->double('largo')->nullable();
                $table->double('ancho')->nullable();
                $table->double('alto')->nullable();
                $table->double('peso_volumetrico')->nullable();
                $table->string('dim_unidad')->nullable();
                $table->double('total')->nullable();
                $table->string('modificado_por')->nullable();
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por')->nullable();
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('rate_charge_class')->nullable();
                $table->double('rate_charge_min')->nullable();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
