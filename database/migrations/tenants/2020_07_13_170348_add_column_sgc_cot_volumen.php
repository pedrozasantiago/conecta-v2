<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSgcCotVolumen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('sgc_cotiza_volumen')) {
             
                if (!Schema::hasColumn('sgc_cotiza_volumen', 'volumen')) {
                Schema::table('sgc_cotiza_volumen', function (Blueprint $table) {
                    $table->double('volumen')->nullable()->after('alto');
                });
            }    
            
            if (!Schema::hasColumn('sgc_cotiza_volumen', 'peso_cobrable')) {
                Schema::table('sgc_cotiza_volumen', function (Blueprint $table) {
                    $table->double('peso_cobrable')->nullable()->after('peso_volumetrico');
                });
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
