<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertActivCotiza extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasTable('sgc_actividades')) {

            DB::statement("INSERT IGNORE INTO `sgc_actividades` (`ACTIVIDAD_ID`, `DESCRIPCION`, `SECUENCIA`, `TIPO`, `ACTIVO`) 
                        VALUES ('FAPER', 'Fecha apertura', '0', 'COTIZA', 'S');");
            DB::statement("INSERT IGNORE INTO `sgc_actividades` (`ACTIVIDAD_ID`, `DESCRIPCION`, `SECUENCIA`, `TIPO`, `ACTIVO`) 
                        VALUES ('FPRINT', 'Fecha impresion cotizacion', '3', 'COTIZA', 'S');");
            DB::statement("INSERT IGNORE INTO `sgc_actividades` (`ACTIVIDAD_ID`, `DESCRIPCION`, `SECUENCIA`, `TIPO`, `ACTIVO`) 
                        VALUES ('FSHIP', 'Fecha impresion shipping', '4', 'COTIZA', 'S');");
            DB::statement("INSERT IGNORE INTO `sgc_actividades` (`ACTIVIDAD_ID`, `DESCRIPCION`, `SECUENCIA`, `TIPO`, `ACTIVO`) 
                        VALUES ('FAPRO', 'Fecha aprobado', '5', 'COTIZA', 'S');");
            DB::statement("INSERT IGNORE INTO `sgc_actividades` (`ACTIVIDAD_ID`, `DESCRIPCION`, `SECUENCIA`, `TIPO`, `ACTIVO`) 
                        VALUES ('FCIERRE', 'Fecha cierre', '6', 'COTIZA', 'S');");
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
