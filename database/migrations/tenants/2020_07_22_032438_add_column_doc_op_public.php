<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDocOpPublic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_ordenes_doc')) {
             
                if (!Schema::hasColumn('tm_ordenes_doc', 'publico')) {
                Schema::table('tm_ordenes_doc', function (Blueprint $table) {
                    $table->string('publico')->nullable()->after('observaciones');
                });
            }    
            
            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
