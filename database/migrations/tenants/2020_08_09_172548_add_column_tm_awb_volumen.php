<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTmAwbVolumen extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasTable('tm_awb')) {

            if (!Schema::hasColumn('tm_awb', 'volumen')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->double('volumen')->nullable()->after('alto');
                });
            }

            if (!Schema::hasColumn('tm_awb', 'peso_cobrable')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->double('peso_cobrable')->nullable()->after('peso_volumetrico');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
