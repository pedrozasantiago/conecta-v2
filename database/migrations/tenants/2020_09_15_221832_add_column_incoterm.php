<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIncoterm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotizaciones')) {
            if (!Schema::hasColumn('sgc_cotizaciones', 'incoterm')) {
                Schema::table('sgc_cotizaciones', function (Blueprint $table) {
                    $table->string('incoterm')->nullable()->after('ORIGEN');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
