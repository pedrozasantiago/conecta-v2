<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableParancelaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tm_parancelarias')) {
            Schema::create('tm_parancelarias', function (Blueprint $table) {
                $table->string('subpartida')->primary();
                $table->string('seccion')->nullable();
                $table->string('capitulo')->nullable();
                $table->string('partida')->nullable();
                $table->string('subpartida_6')->nullable();
                $table->string('subpartida_8')->nullable();
                $table->text('mercancias')->nullable();
                $table->text('observaciones')->nullable();
                $table->double('iva');
                $table->double('arancel');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
