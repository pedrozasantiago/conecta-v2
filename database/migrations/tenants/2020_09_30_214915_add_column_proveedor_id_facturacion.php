<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnProveedorIdFacturacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_factura_detalle')) {
            if (!Schema::hasColumn('tm_factura_detalle', 'proveedor_id')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->string('proveedor_id')->after('id_grupo');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
