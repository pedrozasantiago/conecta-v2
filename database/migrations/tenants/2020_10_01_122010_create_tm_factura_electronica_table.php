<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmFacturaElectronicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tm_factura_electronica')) {
            Schema::create('tm_factura_electronica', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->longText('cufe');
            $table->longText('xml_file');
            $table->string('xml_name');
            $table->longText('qr_code')->nullable();
            $table->longText('response')->nullable();
            $table->unsignedBigInteger('documento_id');
            $table->string('consecutivo');
            $table->string('tipo_documento');
            $table->timestamps();
        });
        }
        
        
         // resoluciones de facturacion
        if (!Schema::hasTable('tm_resoluciones_fe')) {
            Schema::create('tm_resoluciones_fe', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('numero');
            $table->string('consecutivo')->default(0);
            $table->string('desde')->nullable();
            $table->string('hasta')->nullable();
            $table->string('prefijo')->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->string('clave_tecnica')->nullable();
            $table->timestamps();
        });
        }
        
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('electronic_billing');
    }
}
