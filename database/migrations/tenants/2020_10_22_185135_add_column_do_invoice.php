<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDoInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_banco_ingresos')) {
            if (!Schema::hasColumn('tm_banco_ingresos', 'do')) {
                Schema::table('tm_banco_ingresos', function (Blueprint $table) {
                    $table->string('do')->nullable()->after('factura_id');
                });
            }
        }
        
         if (Schema::hasTable('tm_banco_egresos')) {
            if (!Schema::hasColumn('tm_banco_egresos', 'do')) {
                Schema::table('tm_banco_egresos', function (Blueprint $table) {
                    $table->string('do')->nullable()->after('factura_id');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
