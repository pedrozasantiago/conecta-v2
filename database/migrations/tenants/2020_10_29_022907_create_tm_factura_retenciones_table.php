<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmFacturaRetencionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('tm_factura_retenciones')) {
            Schema::create('tm_factura_retenciones', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('factura_id');
                $table->unsignedBigInteger('id_retencion');
                $table->double('retencion');
                $table->double('subtotal_mn');
                $table->double('valor_retencion_mn');
                $table->string('usuario');
                $table->string('maquina');
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
//        Schema::dropIfExists('tm_factura_retenciones');
    }

}
