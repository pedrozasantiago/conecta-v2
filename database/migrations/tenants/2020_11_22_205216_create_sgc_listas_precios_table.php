<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSgcListasPreciosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        if (!Schema::hasTable('sgc_lista_precios')) {
            Schema::create('sgc_lista_precios', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nombre');
                $table->string('estado');
                $table->string('id_externo');
                $table->text('notas')->nullable();
                $table->timestamps();
            });
        }
        
        if (!Schema::hasTable('sgc_lista_precios_detalle')) {
            Schema::create('sgc_lista_precios_detalle', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('lista_id');
                $table->unsignedBigInteger('concepto');
                $table->integer('id_grupo');  
                $table->integer('variacion');  
                $table->bigInteger('proveedor_id');  
                $table->double('tarifa_venta')->nullable();
                $table->string('tipo_venta');
                $table->double('minima')->nullable();
                $table->string('monedaventa');
                $table->double('tasa_impuesto')->nullable();
                $table->double('minimacompra')->nullable();
                $table->double('tarifa_compra')->nullable();
                $table->string('tipo_compra');
                $table->string('monedacompra');
                $table->text('observaciones')->nullable();
                $table->string('usuario');
                $table->string('maquina');                
                
                $table->timestamps();
            });
            
        }
        
        if (Schema::hasTable('sgc_tarifario')) {
            if (!Schema::hasColumn('sgc_tarifario', 'id_lista')) {
                Schema::table('sgc_tarifario', function (Blueprint $table) {
                    $table->unsignedBigInteger('id_lista')->nullable()->after('vigencia');
                });
            }
        }
        
        if (Schema::hasTable('sgc_tarifario_prov')) {
            if (!Schema::hasColumn('sgc_tarifario_prov', 'id_lista')) {
                Schema::table('sgc_tarifario_prov', function (Blueprint $table) {
                    $table->unsignedBigInteger('id_lista')->nullable()->after('vigencia');
                });
            }
        }
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
//        Schema::dropIfExists('sgc_listas_precios');
    }

}
