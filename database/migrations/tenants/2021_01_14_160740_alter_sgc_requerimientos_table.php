<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSgcRequerimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_requerimientos')) {
                
            if (!Schema::hasColumn('sgc_requerimientos', 'incoterm')) {
                Schema::table('sgc_requerimientos', function (Blueprint $table) {
                    $table->string('incoterm')->nullable()->after('cotizacion_id');
                });
            }
            
            
            
        }
        
         if (Schema::hasTable('plantillas')) {
                
            if (!Schema::hasColumn('plantillas', 'grupo')) {
                Schema::table('plantillas', function (Blueprint $table) {
                    $table->string('grupo')->nullable()->after('id');
                });
            }
            
            
            
        }
         
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
