<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmDoImpuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('tm_do_impuestos')) {
                
            
            
            if (!Schema::hasColumn('tm_do_impuestos', 'pieza_id')) {
                Schema::table('tm_do_impuestos', function (Blueprint $table) {
                    $table->unsignedBigInteger('pieza_id')->nullable()->after('do');
                    
                    
                     $table->foreign('pieza_id')
                    ->references('id')->on('tm_awb')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
                    
                });
                 
            }
            
            if (!Schema::hasColumn('tm_do_impuestos', 'fecha_ingreso')) {
                Schema::table('tm_do_impuestos', function (Blueprint $table) {
                    $table->string('fecha_ingreso')->nullable()->after('pieza_id');
                });
            }
            
             
            
            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
