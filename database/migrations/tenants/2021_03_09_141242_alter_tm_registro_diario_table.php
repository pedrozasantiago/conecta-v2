<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmRegistroDiarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
      
            if (!Schema::hasColumn('tm_registro_diario', 'id_documento')) {
                Schema::table('tm_registro_diario', function (Blueprint $table) {
                    $table->unsignedInteger('id_documento')->nullable()->after('tipo_transaccion');
                });
            }
      
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
