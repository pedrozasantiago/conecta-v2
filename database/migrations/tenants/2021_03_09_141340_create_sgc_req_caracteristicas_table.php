<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSgcReqCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('sgc_req_caracteristicas')) {
            Schema::create('sgc_req_caracteristicas', function (Blueprint $table) {
                 
                $table->unsignedInteger('id_requerimiento')->primary();
                $table->string('tipo')->nullable();
                $table->string('valor_fob')->nullable();
                $table->string('valor_cif')->nullable();
                $table->string('contenedores')->nullable();
                $table->string('piezas')->nullable();
                $table->string('peso_piezas')->nullable();
                $table->string('peso_bruto')->nullable();
                $table->string('unidad_peso')->nullable();
                $table->string('largo')->nullable();
                $table->string('ancho')->nullable();
                $table->string('alto')->nullable();
                $table->string('unidad_dimension')->nullable();
                $table->string('volumen')->nullable();
                $table->string('peso_volumetrico')->nullable();
                $table->integer('dias_libres')->default(0);
                $table->string('usuario')->nullable();
                $table->timestamps();
            });
        }
        
        
         if (!Schema::hasTable('sgc_req_volumen')) {
            Schema::create('sgc_req_volumen', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('id_requerimiento');
                $table->double('cant_piezas')->nullable();
                $table->string('peso_piezas')->nullable()->default(0);
                $table->double('peso_bruto')->nullable();
                $table->string('kg_lb')->nullable();
                $table->double('rate_charge')->nullable();
                $table->double('largo')->nullable();
                $table->double('ancho')->nullable();
                $table->double('alto')->nullable();
                $table->double('volumen')->nullable();
                $table->double('peso_volumetrico')->nullable();
                $table->double('peso_cobrable')->nullable();
                $table->string('dim_unidad')->nullable();
                $table->double('total')->nullable();
                $table->string('modificado_por')->nullable();
                $table->dateTime('fecha_mod')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por')->nullable();
                $table->dateTime('fecha_creacion')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                
            });
        }
         
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
