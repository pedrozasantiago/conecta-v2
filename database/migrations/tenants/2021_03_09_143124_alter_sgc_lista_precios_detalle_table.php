<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSgcListaPreciosDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
            if (!Schema::hasColumn('sgc_lista_precios_detalle', 'aplica_por')) {
                Schema::table('sgc_lista_precios_detalle', function (Blueprint $table) {
                    $table->string('aplica_por')->nullable()->after('proveedor_id');
                });
            }
            
             if (!Schema::hasColumn('sgc_lista_precios_detalle', 'unidad')) {
                Schema::table('sgc_lista_precios_detalle', function (Blueprint $table) {
                    $table->double('unidad')->nullable()->default('1')->after('aplica_por');
                });
            }
            
            
       
          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
