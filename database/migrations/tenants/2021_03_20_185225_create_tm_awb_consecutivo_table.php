<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmAwbConsecutivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tm_awb_consecutivo')) {
           Schema::create('tm_awb_consecutivo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proveedor_id');
            $table->string('consecutivo');
            $table->string('creado_por')->nullable();
            $table->timestamps();
            
            $table->unique(['proveedor_id', 'consecutivo']);
            
            
        }); 
        }
        
        
     if (Schema::hasTable('tm_consolidado')) {
            Schema::table('tm_consolidado', function(Blueprint $table) {
                $table->unique("nombre");
            });
        }  
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('tm_awb_consecutivo');
    }
}
