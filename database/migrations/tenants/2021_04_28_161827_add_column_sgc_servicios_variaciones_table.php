<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSgcServiciosVariacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         if (Schema::hasTable('sgc_servicios_variaciones')) {
            if (!Schema::hasColumn('sgc_servicios_variaciones', 'orden')) {
                Schema::table('sgc_servicios_variaciones', function (Blueprint $table) {
                    $table->integer('orden');  
                });
            }
             
        }
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
