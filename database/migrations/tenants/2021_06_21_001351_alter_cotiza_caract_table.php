<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCotizaCaractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotiza_caracteristicas')) {
            if (!Schema::hasColumn('sgc_cotiza_caracteristicas', 'dias_libres')) {
                Schema::table('sgc_cotiza_caracteristicas', function (Blueprint $table) {
                    $table->integer('dias_libres')->default(0);
                });
            }
             
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
