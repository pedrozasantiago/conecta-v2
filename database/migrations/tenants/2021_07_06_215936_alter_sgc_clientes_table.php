<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSgcClientesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasTable('sgc_clientes')) {
            Schema::table('sgc_clientes', function (Blueprint $table) {
                $table->renameColumn('CORREO', 'user')->after('CARGA');
                $table->renameColumn('CARGO', 'password')->after('user');
                $table->string('tracking')->after('CARGO')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
