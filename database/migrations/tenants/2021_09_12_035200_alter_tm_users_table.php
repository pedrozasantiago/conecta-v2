<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_users')) {
            
            if (!Schema::hasColumn('tm_users', 'password')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('password')->nullable();
                });
            }
            
            if (!Schema::hasColumn('tm_users', 'api_token')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('api_token')->nullable();
                });
            }
            
            if (!Schema::hasColumn('tm_users', 'remember_token')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('remember_token')->nullable();
                });
            }
            
            if (!Schema::hasColumn('tm_users', 'created_at')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                });
            }
            
            if (!Schema::hasColumn('tm_users', 'updated_at')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                });
            }
            
            if (!Schema::hasColumn('tm_users', 'deleted_at')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->timestamp('deleted_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
                });
            }
            
            
            
             if (Schema::hasColumn('tm_users', 'susuario')) {
                
                
                Schema::table('tm_users', function (Blueprint $table) {
                    $sm = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $sm->listTableIndexes('tm_users');
                    
                    if(array_key_exists("tm_users_susuario_unique", $indexesFound))
                    $table->dropUnique('tm_users_susuario_unique');
                    
                    $table->unique('susuario');
                });
            }
            
            if (Schema::hasColumn('tm_users', 'cedula')) {
                
                
                
                Schema::table('tm_users', function (Blueprint $table) {
                    $sm = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $sm->listTableIndexes('tm_users');
                    
                    if(array_key_exists("tm_users_cedula_unique", $indexesFound))
                    $table->dropUnique('tm_users_cedula_unique');
                    
                    $table->unique('cedula');
                });
            }
            
            
        }
        
    }
    
    
    public function listTableIndexes($table) {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableIndexes($table));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
