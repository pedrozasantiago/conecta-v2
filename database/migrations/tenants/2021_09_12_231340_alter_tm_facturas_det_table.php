<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmFacturasDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_factura_detalle')) {
            
            if (!Schema::hasColumn('tm_factura_detalle', 'variacion')) {
                Schema::table('tm_factura_detalle', function (Blueprint $table) {
                    $table->integer('variacion')->after('id_grupo');
                });
            }
          
            
           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
