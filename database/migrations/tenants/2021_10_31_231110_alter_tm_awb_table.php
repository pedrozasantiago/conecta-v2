<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmAwbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_awb')) {
            if (!Schema::hasColumn('tm_awb', 'tipo_empaque')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->string('tipo_empaque');
                });
            }
            if (!Schema::hasColumn('tm_awb', 'mercancia')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->text('mercancia')->nullable();
                });
            }
            if (!Schema::hasColumn('tm_awb', 'id_contenedor')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->unsignedBigInteger('id_contenedor');
                    
                });
            }
             
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
