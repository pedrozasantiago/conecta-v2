<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        
        if (Schema::hasTable('log')) {
            
            if (!Schema::hasColumn('log', 'request')) {
                Schema::table('log', function (Blueprint $table) {
                    $table->text('detalle_nuevo')->change();
                    $table->text('detalle_viejo')->change();
                    $table->text('request')->nullable();
                });
            }
             
        }
        
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
