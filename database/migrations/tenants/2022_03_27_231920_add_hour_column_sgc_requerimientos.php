<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHourColumnSgcRequerimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_requerimientos')) {
            if (!Schema::hasColumn('sgc_requerimientos', 'hora_creacion')) {
                Schema::table('sgc_requerimientos', function (Blueprint $table) {
                    $table->string('hora_creacion')->after('fecha_creacion');
                });
                 
            }
            
            if (!Schema::hasColumn('sgc_requerimientos', 'hora_modificacion')) {
                Schema::table('sgc_requerimientos', function (Blueprint $table) {
                    $table->string('hora_modificacion')->after('fecha_modificacion');
                });
                 
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
