<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTipoTarifario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_tipo_tarifario_adicionales')) {
            if (!Schema::hasColumn('sgc_tipo_tarifario_adicionales', 'tipo')) {
                Schema::table('sgc_tipo_tarifario_adicionales', function (Blueprint $table) {
                    $table->string('tipo')->after('estado');
                });
                 
            }
            
        }
        
        if (Schema::hasTable('sgc_tipo_tarifario_compras')) {
            if (!Schema::hasColumn('sgc_tipo_tarifario_compras', 'tipo')) {
                Schema::table('sgc_tipo_tarifario_compras', function (Blueprint $table) {
                    $table->string('tipo')->after('estado');
                });
                 
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
