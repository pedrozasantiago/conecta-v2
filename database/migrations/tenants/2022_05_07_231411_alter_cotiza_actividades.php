<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCotizaActividades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotiza_segactiv')) {
            
            if (Schema::hasColumn('sgc_cotiza_segactiv', 'requerimiento_id')) {
                
                
                Schema::table('sgc_cotiza_segactiv', function (Blueprint $table) {
                    $sm = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $sm->listTableIndexes('sgc_cotiza_segactiv');
                    
                    if(array_key_exists("sgc_cotiza_segactiv_cotizacion_id_actividad_id_unique", $indexesFound))
                    $table->dropUnique('sgc_cotiza_segactiv_cotizacion_id_actividad_id_unique');
                    
                    $table->unique(['requerimiento_id','cotizacion_id','actividad_id'],'sgc_cotiza_segactiv_req_id_cotizacion_id_actividad_id_unique');
                });
            }
             
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
