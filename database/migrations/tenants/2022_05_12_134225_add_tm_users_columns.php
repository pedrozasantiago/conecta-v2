<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTmUsersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('tm_users')) {
             
             
            if (!Schema::hasColumn('tm_users', 'pricing')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('pricing')->default('N')->after('comercial');
                });
                 
            }
            
            if (!Schema::hasColumn('tm_users', 'adtivo')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('adtivo')->default('N')->after('comercial');
                });
                 
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
