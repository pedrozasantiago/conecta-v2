<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSgcWpMessageTable extends Migration
{
    public function up()
    {
       if (!Schema::hasTable('sgc_wp_message')){
           
           Schema::create('sgc_wp_message', function (Blueprint $table) {

			$table->bigIncrements('id');
			$table->string('channel')->nullable();
			$table->string('wp_phone_id')->nullable();
			$table->string('id_contacto',45)->nullable();
			$table->string('wamid',191)->nullable();
			$table->string('status',45)->nullable();
			$table->string('profile_name',45)->nullable();
			$table->bigInteger('wp_number')->unsigned()->nullable();
			$table->string('wa_media_id')->nullable();
			$table->string('type_media')->nullable();
			$table->text('message');
			$table->text('body');
			$table->string('timestamp',45)->nullable();
			$table->string('conversation_id',45)->nullable();
			$table->string('expiration_timestamp',45)->nullable();
			$table->string('initiated_by',45)->nullable();
			$table->string('type_id_row',45)->nullable();
			$table->string('id_row',45)->nullable();
			$table->string('assigned_to',45)->nullable();
			$table->string('context_id',191)->nullable();
			$table->string('context_from',45)->nullable();
			$table->string('created_at',45)->nullable();
			$table->string('created_by',45)->nullable();
			$table->string('read_at',45)->nullable();
			$table->string('delivered_at',45)->nullable();
			$table->string('s3_media_url',1000)->nullable();
			$table->string('id_template',1000)->nullable();
		
		
		
                $table->unique(['wamid', 'status']);

        });
       }

        if (!Schema::hasTable('sgc_wp_interactive')){
            Schema::create('sgc_wp_interactive', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name', 45)->nullable();
                $table->string('type', 45)->nullable();
                $table->string('status', 45)->nullable();
                $table->text('components')->nullable();
                $table->string('created_at', 45)->nullable();
                $table->string('created_by', 45)->nullable();
                $table->string('id_process', 45)->nullable();
            });
        }


        
    }

    public function down()
    {
        
    }
}