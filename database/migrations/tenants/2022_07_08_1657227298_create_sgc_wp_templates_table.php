<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSgcWpTemplatesTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('sgc_wp_templates')){
            Schema::create('sgc_wp_templates', function (Blueprint $table) {

		$table->increments('id');
		$table->string('id_wp',45)->nullable();
		$table->string('name',45)->nullable();
		$table->string('category',45)->nullable();
		$table->string('status',45)->nullable();
		$table->string('language',45)->nullable();
		$table->text('components');
		$table->string('created_at',45)->nullable();
		$table->string('created_by',45)->nullable();
		$table->string('id_process',45)->nullable();
		
                $table->unique(['name', 'language']);
                $table->unique(['id_process']);

        });
            
        }

        if (!Schema::hasTable('sgc_wp_templates_reply')){
            Schema::create('sgc_wp_templates_reply', function (Blueprint $table) {
                $table->string('id_wp', 45)->primary();
                $table->string('reply_1', 45)->nullable();
                $table->string('reply_2', 45)->nullable();
                $table->string('reply_3', 45)->nullable();
            });
            
        }
        
    }

    public function down()
    {
        
    }
}