<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWpColumnTmUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_users')) {
             
             
            if (!Schema::hasColumn('tm_users', 'whatsapp')) {
                Schema::table('tm_users', function (Blueprint $table) {
                    $table->string('whatsapp')->default('N')->after('pricing');
                });
                 
            }
           
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
