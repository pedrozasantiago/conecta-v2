<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('sgc_clientes_contactos_operador')) {
            Schema::create('sgc_clientes_contactos_operador', function (Blueprint $table) {
            
                $table->bigInteger('contact_id');
                $table->string('user_id');
            });
        }

        if (!Schema::hasTable('sgc_clientes_contactos_tags')) {

            Schema::create('sgc_clientes_contactos_tags', function (Blueprint $table) {
            
                $table->bigInteger('contact_id');
                $table->string('tag_id');
            });
        }
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
