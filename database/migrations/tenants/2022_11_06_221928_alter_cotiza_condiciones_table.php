<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCotizaCondicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotizaciones_obs')) {
            
            if (Schema::hasColumn('sgc_cotizaciones_obs', 'valor')) {
                Schema::table('sgc_cotizaciones_obs', function (Blueprint $table) {
                    $table->longText('valor')->change();
                });
                
                
            }
             
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
