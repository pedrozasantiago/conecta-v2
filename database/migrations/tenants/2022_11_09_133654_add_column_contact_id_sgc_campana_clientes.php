<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnContactIdSgcCampanaClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_campana_clientes')) {
             
            if (!Schema::hasColumn('sgc_campana_clientes', 'contacto_id')) {
                Schema::table('sgc_campana_clientes', function (Blueprint $table) {
                    $table->unsignedBigInteger('contacto_id')->after('CLIENTE_ID');
                });
                 
            }

            

            Schema::table('sgc_campana_clientes', function (Blueprint $table) { 

                $foreignKeys = $this->listTableForeignKeys('sgc_campana_clientes');

                if (in_array('sgc_campana_clientes_campana_id_foreign', $foreignKeys))
                    $table->dropForeign('sgc_campana_clientes_campana_id_foreign');
                if (in_array('sgc_campana_clientes_cliente_id_foreign', $foreignKeys))
                    $table->dropForeign('sgc_campana_clientes_cliente_id_foreign');


                $indexesKeys = $this->listTableIndexes('sgc_campana_clientes');

            if (in_array('sgc_campana_clientes_cliente_id_campana_id_unique', $indexesKeys))
                    $table->dropUnique('sgc_campana_clientes_cliente_id_campana_id_unique');


                $table->unique(['CAMPANA_ID', 'CLIENTE_ID','contacto_id']);  

              /*  $table->foreign('CAMPANA_ID')
                    ->references('ID')->on('sgc_campana')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

                $table->foreign('CLIENTE_ID')
                        ->references('CEDULA')->on('sgc_clientes')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');


                $table->foreign('contacto_id')
                        ->references('id')->on('sgc_clientes_contactos')
                        ->onDelete('restrict')
                        ->onUpdate('restrict'); */
                
            });


            
        }




        if (Schema::hasTable('sgc_actividades_seg')) {
             
            if (!Schema::hasColumn('sgc_actividades_seg', 'contacto_id')) {
                Schema::table('sgc_actividades_seg', function (Blueprint $table) {
                    $table->unsignedBigInteger('contacto_id')->after('CLIENTE_ID');
                });
                 
            }
            
        }

      


    }

    public function listTableIndexes($table) {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableIndexes($table));
    }


    public function listTableForeignKeys($table) {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
