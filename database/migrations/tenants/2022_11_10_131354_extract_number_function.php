<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtractNumberFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $sql = "drop FUNCTION IF EXISTS ExtractNumber;

        
        
        CREATE  FUNCTION `ExtractNumber`(in_string VARCHAR(50)) 
        RETURNS BIGINT
        NO SQL
        BEGIN
            DECLARE ctrNumber VARCHAR(50);
            DECLARE finNumber VARCHAR(50) DEFAULT '';
            DECLARE sChar VARCHAR(1);
            DECLARE inti BIGINT DEFAULT 1;
        
            IF LENGTH(in_string) > 0 THEN
                WHILE(inti <= LENGTH(in_string)) DO
                    SET sChar = SUBSTRING(in_string, inti, 1);
                    SET ctrNumber = FIND_IN_SET(sChar, '0,1,2,3,4,5,6,7,8,9'); 
                    IF ctrNumber > 0 THEN
                        SET finNumber = CONCAT(finNumber, sChar);
                    END IF;
                    SET inti = inti + 1;
                END WHILE;
                RETURN CAST(finNumber AS UNSIGNED);
            ELSE
                RETURN 0;
            END IF;    
        END ;";


        \DB::connection()->getPdo()->exec($sql);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
