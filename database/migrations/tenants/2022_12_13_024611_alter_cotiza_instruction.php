<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCotizaInstruction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_cotiza_instruccion')) {
            
                
                Schema::table('sgc_cotiza_instruccion', function (Blueprint $table) {
                    $sm = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $sm->listTableIndexes('sgc_cotiza_instruccion');
                    
                    if(array_key_exists("sgc_cotiza_instruccion_do_atributo_unique", $indexesFound))
                    $table->dropUnique('sgc_cotiza_instruccion_do_atributo_unique');
                    
                    $table->unique(['cotizacion','do','atributo']);
                });
         
             
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
