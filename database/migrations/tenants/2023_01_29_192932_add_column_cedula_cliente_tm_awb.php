<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCedulaClienteTmAwb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        if (Schema::hasTable('sgc_req_caracteristicas')) {
            if (!Schema::hasColumn('sgc_req_caracteristicas', 'dias_libres')) {
                Schema::table('sgc_req_caracteristicas', function (Blueprint $table) {
                    $table->integer('dias_libres')->default(0)->after('peso_volumetrico');
                });
            }  
            
        }


        if (Schema::hasTable('tm_awb')) {
            if (!Schema::hasColumn('tm_awb', 'cedula_cliente')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->string('cedula_cliente')->nullable()->after('do');
                });
            }  
            
            if (!Schema::hasColumn('tm_awb', 'id_externo')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->string('id_externo')->nullable()->after('do');
                });
            } 
            
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
