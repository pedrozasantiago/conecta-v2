<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateCreationClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_clientes')) {
            if (!Schema::hasColumn('sgc_clientes', 'fecha_creacion')) {
                Schema::table('sgc_clientes', function (Blueprint $table) {
                    $table->string('fecha_creacion')->nullable()->after('FECHA_MOD');
                });
            }  
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
