<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdPiezaTmAwb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_awb')) {
            if (!Schema::hasColumn('tm_awb', 'id_pieza')) {
                Schema::table('tm_awb', function (Blueprint $table) {
                    $table->unsignedBigInteger('id_pieza')->after('id');
                });
            }  
            
            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
