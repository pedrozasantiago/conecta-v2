<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tm_pieces')) {
            Schema::create('tm_pieces', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->string('id_externo')->nullable();
                $table->string('cedula_cliente')->nullable();
                $table->double('cant_piezas')->nullable();
                $table->string('peso_piezas')->nullable();
                $table->double('peso_bruto')->nullable();
                $table->string('kg_lb')->nullable();
                $table->double('largo')->nullable();
                $table->double('ancho')->nullable();
                $table->double('alto')->nullable();
                $table->double('volumen')->nullable();
                $table->string('dim_unidad')->nullable();
                $table->string('tipo_empaque');
                $table->text('mercancia');
                $table->bigInteger('id_location',)->unsigned();
                $table->string('modificado_por')->nullable();
                $table->datetime('fecha_mod')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->string('creado_por')->nullable();
                $table->datetime('fecha_creacion')->default(DB::raw('CURRENT_TIMESTAMP'));
        
        
        });
        }
        
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
