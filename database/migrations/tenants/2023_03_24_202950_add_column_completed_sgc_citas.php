<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCompletedSgcCitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sgc_citas')) {
            if (!Schema::hasColumn('sgc_citas', 'fecha_completado')) {
                Schema::table('sgc_citas', function (Blueprint $table) {
                    
                    $table->string('fecha_completado', 45)->nullable();
                    $table->string('completado_por', 45)->nullable();
                    
                });
            }  
            
            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
