<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTmPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tm_pieces')) {
            if (!Schema::hasColumn('tm_pieces', 'status')) {
                Schema::table('tm_pieces', function (Blueprint $table) {
                    
                    $table->string('status', 45)->nullable();
                    
                });
            }  
            
            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
