<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSgcReqCotizaVolumenTable extends Migration
{
    public function up()
    {
        Schema::create('sgc_req_cotiza_volumen', function (Blueprint $table) {

		$table->bigIncrements('id');
		$table->string('id_row_req',45)->nullable();
		$table->string('id_row_cot',45)->nullable();
		
        });
    }

    public function down()
    {
    
    }
}