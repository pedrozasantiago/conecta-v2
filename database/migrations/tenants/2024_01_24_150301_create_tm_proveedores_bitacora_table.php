<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmProveedoresBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    if (!Schema::hasTable('tm_proveedores_bitacora')){
        Schema::create('tm_proveedores_bitacora', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('proveedor_id', 191)->default(null);
            $table->string('contacto_id', 45)->default(null);
            $table->string('actividad_id', 200);
            $table->string('fecha_bit', 191)->default(null);
            $table->text('detalle');
            $table->string('user', 191)->default(null);
            $table->timestamps();
            
        });
    }
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_proveedores_bitacora');
    }
}
