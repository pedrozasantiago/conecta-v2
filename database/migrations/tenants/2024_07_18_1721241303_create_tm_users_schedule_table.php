<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmUsersScheduleTable extends Migration
{
    public function up()
    {
        Schema::create('tm_users_schedule', function (Blueprint $table) {

        $table->integer('id')->autoIncrement();
		$table->string('user_id');
		$table->string('dia',10);
		$table->time('hora_entrada');
		$table->time('hora_salida');
		$table->timestamp('creado_el')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		// $table->primary('id');

        $table->unique(['user_id', 'dia','hora_entrada']);

        });
    }

    public function down()
    {
        Schema::dropIfExists('tm_users_schedule');
    }
}