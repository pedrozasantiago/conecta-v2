<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ElectronicBillingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->command->info('Truncating Electronic Billing tables');
        $this->truncateTables();
        
        $this->command->info('Insert Document Types data');
        
        foreach ($this->DocumentTypes() as $key => $type) {
            \App\Models\Dian\DocumentType::create($type);
        }

        $this->command->info('Insert Regimes data');
        
        foreach ($this->Regimes() as $key => $regime) {
            \App\Models\Dian\Regime::create($regime);
        }

        $this->command->info('Insert Liabilities data');
        
        foreach ($this->Liabilities() as $key => $liability) {
            \App\Models\Dian\Liability::create($liability);
        }

        $this->command->info('Insert Company Types data');
        
        foreach ($this->CompaniesTypes() as $key => $type) {
            \App\Models\Dian\CompanyType::create($type);
        }

        $this->command->info('Insert Billing Documents data');
        
        foreach ($this->BillingDocuments() as $key => $document) {
            \App\Models\Dian\BillingDocument::create($document);
        }

        $this->command->info('Insert Discounts data');
        
        foreach ($this->Discounts() as $key => $discount) {
            \App\Models\Dian\Discount::create($discount);
        }

    }

    // tipos de documentos
    public function DocumentTypes()
    {
        return [
            [
                'name' => 'Registro civil',
                'short_name' => 'RC',
                'code' => '11',
                'country_id' => 46
            ],
            [
                'name' => 'Tarjeta de identidad',
                'short_name' => 'TI',
                'code' => '12',
                'country_id' => 46
            ],
            [
                'name' => 'Cédula de ciudadanía',
                'short_name' => 'CC',
                'code' => '13',
                'country_id' => 46
            ],
            [
                'name' => 'Tarjeta de extranjería',
                'short_name' => 'TE',
                'code' => '21',
                'country_id' => 46
            ],
            [
                'name' => 'Cédula de extranjería',
                'short_name' => 'CE',
                'code' => '22',
                'country_id' => 46
            ],
            [
                'name' => 'NIT',
                'short_name' => 'NIT',
                'code' => '31',
                'country_id' => 46
            ],
            [
                'name' => 'Pasaporte',
                'short_name' => 'Pasaporte',
                'code' => '41',
                'country_id' => 46
            ],
            [
                'name' => 'Documento de identificación extranjero',
                'short_name' => 'DIE',
                'code' => '42',
                'country_id' => 46
            ],
            [
                'name' => 'NIT de otro país',
                'short_name' => 'NIT de otro país',
                'code' => '50',
                'country_id' => 46
            ],
            [
                'name' => 'NUIP',
                'short_name' => 'NUIP',
                'code' => '91',
                'country_id' => 46
            ]
          ];
    }


    // regimenes
    public function Regimes()
    {
        return [
            [
              'name' => 'Régimen Simple',
              'code' => '04',
              'country_id' => 49
            ],
            [
              'name' => 'Régimen Ordinario',
              'code' => '05',
              'country_id' => 49
            ]
        ];
    }


    // responsabilidades
    public function Liabilities()
    {
        return [
          [
            'name' => 'Ingresos y patrimonio',
            'code' => 'O-06',
            'country_id' => 49
          ],
          [
            'name' => 'Retención en la fuente a título de renta',
            'code' => 'O-07',
            'country_id' => 49
          ],
          [
            'name' => 'Retención timbre nacional',
            'code' => 'O-08',
            'country_id' => 49
          ],
          [
            'name' => 'Retención en la fuente en el impuesto sobre las ventas',
            'code' => 'O-09',
            'country_id' => 49
          ],
          [
            'name' => 'Ventas régimen común',
            'code' => 'O-11',
            'country_id' => 49
          ],
          [
            'name' => 'Ventas régimen simplificado',
            'code' => 'O-12',
            'country_id' => 49
          ],
          [
            'name' => 'Gran contribuyente',
            'code' => 'O-13',
            'country_id' => 49
          ],
          [
            'name' => 'Informante de exógena',
            'code' => 'O-14',
            'country_id' => 49
          ],
          [
            'name' => 'Autorretenedor',
            'code' => 'O-15',
            'country_id' => 49
          ],
          [
            'name' => 'Obligación de facturar por ingresos de bienes y/o servicios excluidos',
            'code' => 'O-16',
            'country_id' => 49
          ],
          [
            'name' => 'Profesionales de compra y venta de divisas',
            'code' => 'O-17',
            'country_id' => 49
          ],
          [
            'name' => 'Productor y/o exportador de bienes exentos',
            'code' => 'O-19',
            'country_id' => 49
          ],
          [
            'name' => 'Obligado a cumplir deberes formales a nombre de terceros',
            'code' => 'O-22',
            'country_id' => 49
          ],
          [
            'name' => 'Agente de retención en el impuesto sobre las ventas',
            'code' => 'O-23',
            'country_id' => 49
          ],
          [
            'name' => 'Impuesto Nacional a la Gasolina y al ACPM',
            'code' => 'O-32',
            'country_id' => 49
          ],
          [
            'name' => 'Impuesto Nacional al consumo',
            'code' => 'O-33',
            'country_id' => 49
          ],
          [
            'name' => 'Régimen simplificado impuesto nacional consumo rest y bares',
            'code' => 'O-34',
            'country_id' => 49
          ],
          [
            'name' => 'Establecimiento Permanente',
            'code' => 'O-36',
            'country_id' => 49
          ],
          [
            'name' => 'Obligado a Facturar Electrónicamente Modelo 2242',
            'code' => 'O-37',
            'country_id' => 49
          ],
          [
            'name' => 'Facturación Electrónica Voluntaria Modelo 2242',
            'code' => 'O-38',
            'country_id' => 49
          ],
          [
            'name' => 'Proveedor de Servicios Tecnológicos PST Modelo 2242',
            'code' => 'O-39',
            'country_id' => 49
          ],
          [
            'name' => 'Otro tipo de obligado',
            'code' => 'O-99',
            'country_id' => 49
          ],
          [
            'name' => 'Factor PN',
            'code' => 'R-12-PN',
            'country_id' => 49
          ],
          [
            'name' => 'Mandatario PN',
            'code' => 'R-16-PN',
            'country_id' => 49
          ],
          [
            'name' => 'Agente Interventor PN',
            'code' => 'R-25-PN',
            'country_id' => 49
          ],
          [
            'name' => 'Apoderado especial PJ',
            'code' => 'R-06-PJ',
            'country_id' => 49
          ],
          [
            'name' => 'Apoderado general PJ',
            'code' => 'R-07-PJ',
            'country_id' => 49
          ],
          [
            'name' => 'Factor PJ',
            'code' => 'R-12-PJ',
            'country_id' => 49
          ],
          [
            'name' => 'Mandatario PJ',
            'code' => 'R-16-PJ',
            'country_id' => 49
          ],
          [
            'name' => 'Otro tipo de responsable PJ',
            'code' => 'R-99-PJ',
            'country_id' => 49
          ],
          [
            'name' => 'Agente de carga internacional',
            'code' => 'A-01',
            'country_id' => 49
          ],
          [
            'name' => 'Agente marítimo',
            'code' => 'A-02',
            'country_id' => 49
          ],
          [
            'name' => 'Almacén general de depósito',
            'code' => 'A-03',
            'country_id' => 49
          ],
          [
            'name' => 'Comercializadora internacional (C.I.)',
            'code' => 'A-04',
            'country_id' => 49
          ],
          [
            'name' => 'Comerciante de la zona aduanera especial de Inírida, Puerto Carreño, Cumaribo y Primavera',
            'code' => 'A-05',
            'country_id' => 49
          ],
          [
            'name' => 'Comerciantes de la zona de régimen aduanero especial de Leticia',
            'code' => 'A-06',
            'country_id' => 49
          ],
          [
            'name' => 'Comerciantes de la zona de régimen aduanero especial de Maicao, Uribia y Manaure',
            'code' => 'A-07',
            'country_id' => 49
          ],
          [
            'name' => 'Comerciantes de la zona de régimen aduanero especial de Urabá, Tumaco y Guapí','code' => 'A-08',
            'country_id' => 49
          ],
          [
            'name' => 'Comerciantes del puerto libre de San Andrés, Providencia y Santa Catalina',
            'code' => 'A-09',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito público de apoyo logístico internacional',
            'code' => 'A-10',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado para procesamiento industrial',
            'code' => 'A-11',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado de transformación o ensamble',
            'code' => 'A-12',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito franco',
            'code' => 'A-13',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado aeronáutico',
            'code' => 'A-14',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado para distribución internacional',
            'code' => 'A-15',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado de provisiones de a bordo para consumo y para llevar',
            'code' => 'A-16',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado para envíos urgentes',
            'code' => 'A-17',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito privado',
            'code' => 'A-18',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito público',
            'code' => 'A-19',
            'country_id' => 49
          ],
          [
            'name' => 'Depósito público para distribución internacional',
            'code' => 'A-20',
            'country_id' => 49
          ],
          [
            'name' => 'Exportador de café',
            'code' => 'A-21',
            'country_id' => 49
          ],
          [
            'name' => 'Exportador',
            'code' => 'A-22',
            'country_id' => 49
          ],
          [
            'name' => 'Importador',
            'code' => 'A-23',
            'country_id' => 49
          ],
          [
            'name' => 'Intermediario de tráfico postal y envíos urgentes',
            'code' => 'A-24',
            'country_id' => 49
          ],
          [
            'name' => 'Operador de transporte multimodal',
            'code' => 'A-25',
            'country_id' => 49
          ],
          [
            'name' => 'Sociedad de intermediación aduanera',
            'code' => 'A-26',
            'country_id' => 49
          ],
          [
            'name' => 'Titular de puertos y muelles de servicio público o privado',
            'code' => 'A-27',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador nfor régimen de importación y/o exportación',
            'code' => 'A-28',
            'country_id' => 49
          ],
          [
            'name' => 'Transportista nacional para operaciones del régimen de tránsito aduanero',
            'code' => 'A-29',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario comercial zona franca',
            'code' => 'A-30',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario industrial de bienes zona franca',
            'code' => 'A-32',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario industrial de servicios zona franca',
            'code' => 'A-34',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario operador de zona franca',
            'code' => 'A-36',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario aduanero permanente',
            'code' => 'A-37',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario altamente exportador',
            'code' => 'A-38',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario de zonas económicas especiales de exportación',
            'code' => 'A-39',
            'country_id' => 49
          ],
          [
            'name' => 'Deposito privado de instalaciones industriales',
            'code' => 'A-40',
            'country_id' => 49
          ],
          [
            'name' => 'Beneficiarios de programas especiales de exportación PEX',
            'code' => 'A-41',
            'country_id' => 49
          ],
          [
            'name' => 'Depósitos privados para mercancías en tránsito San Andrés',
            'code' => 'A-42',
            'country_id' => 49
          ],
          [
            'name' => 'Observadores de las operaciones de importación',
            'code' => 'A-43',
            'country_id' => 49
          ],
          [
            'name' => 'Usuarios sistemas especiales Importación exportación',
            'code' => 'A-44',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador nformac régimen de importación y/o exportación',
            'code' => 'A-46',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador terrestre régimen de importación y/o exportación',
            'code' => 'A-47',
            'country_id' => 49
          ],
          [
            'name' => 'Aeropuerto de servicio publico o privado',
            'code' => 'A-48',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador fluvial régimen de importación',
            'code' => 'A-49',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario industrial zona franca especial',
            'code' => 'A-50',
            'country_id' => 49
          ],
          [
            'name' => 'Agencias de aduanas 1',
            'code' => 'A-53',
            'country_id' => 49
          ],
          [
            'name' => 'Usuario Operador Zona Franca Especial',
            'code' => 'A-54',
            'country_id' => 49
          ],
          [
            'name' => 'Agencias de aduanas 2',
            'code' => 'A-55',
            'country_id' => 49
          ],
          [
            'name' => 'Agencias de aduanas 3',
            'code' => 'A-56',
            'country_id' => 49
          ],
          [
            'name' => 'Agencias de aduanas 4',
            'code' => 'A-57',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador aéreo nacional',
            'code' => 'A-58',
            'country_id' => 49
          ],
          [
            'name' => 'Transportador aéreo, marítimo o fluvial modalidad Cabotaje',
            'code' => 'A-60',
            'country_id' => 49
          ],
          [
            'name' => 'Importador de alimentos de consumo humano y animal',
            'code' => 'A-61',
            'country_id' => 49
          ],
          [
            'name' => 'Importador Ocasional',
            'code' => 'A-62',
            'country_id' => 49
          ],
          [
            'name' => 'Importador de maquinaría y sus partes Decreto 2261 de 2012',
            'code' => 'A-63',
            'country_id' => 49
          ],
          [
            'name' => 'Beneficiario Programa de Fomento Industria Automotriz-PROFIA',
            'code' => 'A-64',
            'country_id' => 49
          ],
          [
            'name' => 'Otro tipo de agente aduanero',
            'code' => 'A-99',
            'country_id' => 49
          ],
          [
            'name' => 'Agencia',
            'code' => 'E-01',
            'country_id' => 49
          ],
          [
            'name' => 'Establecimiento de comercio',
            'code' => 'E-02',
            'country_id' => 49
          ],
          [
            'name' => 'Centro de explotación agrícola',
            'code' => 'E-03',
            'country_id' => 49
          ],
          [
            'name' => 'Centro de explotación animal',
            'code' => 'E-04',
            'country_id' => 49
          ],
          [
            'name' => 'Centro de explotación minera',
            'code' => 'E-05',
            'country_id' => 49
          ],
          [
            'name' => 'Centro de explotación de transformación',
            'code' => 'E-06',
            'country_id' => 49
          ],
          [
            'name' => 'Centro de explotación de servicios',
            'code' => 'E-07',
            'country_id' => 49
          ],
          [
            'name' => 'Oficina',
            'code' => 'E-08',
            'country_id' => 49
          ],
          [
            'name' => 'Sede',
            'code' => 'E-09',
            'country_id' => 49
          ],
          [
            'name' => 'Sucursal',
            'code' => 'E-10',
            'country_id' => 49
          ],
          [
            'name' => 'Consultorio',
            'code' => 'E-11',
            'country_id' => 49
          ],
          [
            'name' => 'Administraciones',
            'code' => 'E-12',
            'country_id' => 49
          ],
          [
            'name' => 'Seccionales',
            'code' => 'E-13',
            'country_id' => 49
          ],
          [
            'name' => 'Regionales',
            'code' => 'E-14',
            'country_id' => 49
          ],
          [
            'name' => 'Intendencias',
            'code' => 'E-15',
            'country_id' => 49
          ],
          [
            'name' => 'Local o negocio',
            'code' => 'E-16',
            'country_id' => 49
          ],
          [
            'name' => 'Punto de venta',
            'code' => 'E-17',
            'country_id' => 49
          ],
          [
            'name' => 'Fábrica',
            'code' => 'E-18',
            'country_id' => 49
          ],
          [
            'name' => 'Taller',
            'code' => 'E-19',
            'country_id' => 49
          ],
          [
            'name' => 'Cantera',
            'code' => 'E-20',
            'country_id' => 49
          ],
          [
            'name' => 'Pozo de Petróleo y Gas',
            'code' => 'E-21',
            'country_id' => 49
          ],
          [
            'name' => 'Otro lug de tipo de extrac explotación de recursos naturales',
            'code' => 'E-22',
            'country_id' => 49
          ],
          [
            'name' => 'Otro tipo de establecimiento',
            'code' => 'E-99',
            'country_id' => 49
          ]
        ];
    }


    // documentos de facturacion
    public function CompaniesTypes()
    {
        return [
            [
              'name' => 'Persona Jurídica',
              'code' => '1',
              'country_id' => 49
            ],
            [
              'name' => 'Persona Natural',
              'code' => '2',
              'country_id' => 49
            ]
        ];
    }
    

    // documentos de facturacion
    public function BillingDocuments()
    {
        return [
            [
              'name' => 'Factura de Venta Nacional',
              'code' => '01',
              'prefix' => 'FV',
              'cufe_algorithm' => 'CUFE-SHA384',
              'country_id' => 49
            ],
            [
              'name' => 'Factura de Exportación',
              'code' => '02',
              'prefix' => 'FV',
              'cufe_algorithm' => 'CUFE-SHA384',
              'country_id' => 49
            ],
            [
              'name' => 'Factura de Contingencia',
              'code' => '03',
              'prefix' => 'FV',
              'cufe_algorithm' => 'CUFE-SHA384',
              'country_id' => 49
            ],
            [
              'name' => 'Nota Crédito',
              'code' => '91',
              'prefix' => 'NC',
              'cufe_algorithm' => 'CUDE-SHA384',
              'country_id' => 49
            ],
            [
              'name' => 'Nota Débito',
              'code' => '92',
              'prefix' => 'ND',
              'cufe_algorithm' => 'CUDE-SHA384',
              'country_id' => 49
            ],
            [
              'name' => 'ZIP',
              'code' => null,
              'prefix' => 'Z',
              'country_id' => 49
            ]
          ];

    }


    // descuentos
    public function Discounts()
    {
        return [
            [
              'name' => 'Descuento por impuesto asumido',
              'code' => '00'
            ],
            [
              'name' => 'Pague uno lleve otro',
              'code' => '01'
            ],
            [
              'name' => 'Descuentos contractulales',
              'code' => '02'
            ],
            [
              'name' => 'Descuento por pronto pago',
              'code' => '03'
            ],
            [
              'name' => 'Envío gratis',
              'code' => '04'
            ],
            [
              'name' => 'Descuentos escpecíficos por inventarios',
              'code' => '05'
            ],
            [
              'name' => 'Descuento por monto de compras',
              'code' => '06'
            ],
            [
              'name' => 'Descuento de temporada',
              'code' => '07'
            ],
            [
              'name' => 'Descuento por acturalización de productos / servicios',
              'code' => '08'
            ],
            [
              'name' => 'Descuento general',
              'code' => '09'
            ],
            [
              'name' => 'Descuento por volumen',
              'code' => '10'
            ],
            [
              'name' => 'Otro descuento',
              'code' => '11'
            ]
        ];
    }


    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateTables()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
//        \App\Models\Dian\DocumentType::truncate();
//        \App\Models\Dian\Regime::truncate();
//        \App\Models\Dian\Liability::truncate();
//        \App\Models\Dian\CompanyType::truncate();
//        \App\Models\Dian\BillingDocument::truncate();
//        \App\Models\Dian\Discount::truncate();
//        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
