<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemCurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->command->info('Truncating Currencies table');
        $this->truncateCurrencyTable();

        $this->command->info('Insert Currencies data');
        
        foreach ($this->Currencies() as $key => $currency) {
            \App\Models\Dian\Currency::create($currency);
        }

    }

    // array de monedas
    public function Currencies()
    {

        return [
            ['symbol' => '$','currency' => 'USD','description' => 'US Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '؋','currency' => 'AFN','description' => 'Afghanistan, Afghani','isdefault' => '0','active' => '0'],
            ['symbol' => '€','currency' => 'EUR','description' => 'Euro','isdefault' => '0','active' => '0'],
            ['symbol' => 'Lek','currency' => 'ALL','description' => 'Albania, Lek','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.ج‏','currency' => 'DZD','description' => 'Algerian Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => 'Kz','currency' => 'AOA','description' => 'Angola, Kwanza','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'XCD','description' => 'East Caribbean Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'ARS','description' => 'Argentine Peso','isdefault' => '0','active' => '0'],
            ['symbol' => '&#1423;','currency' => 'AMD','description' => 'Armenian Dram','isdefault' => '0','active' => '0'],
            ['symbol' => 'ƒ','currency' => 'AWG','description' => 'Aruban Guilder','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'AUD','description' => 'Australian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '₼','currency' => 'AZN','description' => 'Azerbaijanian Manat','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'BSD','description' => 'Bahamian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '.د.','currency' => 'BHD','description' => 'Bahraini Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => '৳','currency' => 'BDT','description' => 'Bangladesh, Taka','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'BBD','description' => 'Barbados Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'BZ$','currency' => 'BZD','description' => 'Belize Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'F.CFA','currency' => 'XOF','description' => 'Franc CFA (XOF)','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'BMD','description' => 'Bermudian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '₹','currency' => 'INR','description' => 'Indian Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => 'Nu.','currency' => 'BTN','description' => 'Bhutan, Ngultrum','isdefault' => '0','active' => '0'],
            ['symbol' => 'Bs','currency' => 'BOB','description' => 'Bolivia, Boliviano','isdefault' => '0','active' => '1'],
            ['symbol' => 'КМ','currency' => 'BAM','description' => 'Bosnia and Herzegovina, Convertible Marks','isdefault' => '0','active' => '0'],
            ['symbol' => 'P','currency' => 'BWP','description' => 'Botswana, Pula','isdefault' => '0','active' => '0'],
            ['symbol' => 'kr','currency' => 'NOK','description' => 'Norwegian Krone','isdefault' => '0','active' => '0'],
            ['symbol' => 'R$','currency' => 'BRL','description' => 'Brazilian Real','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'BND','description' => 'Brunei Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'лв.','currency' => 'BGN','description' => 'Bulgarian Lev','isdefault' => '0','active' => '0'],
            ['symbol' => 'FBu','currency' => 'BIF','description' => 'Burundi Franc','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'CVE','description' => 'Cape Verde Escudo','isdefault' => '0','active' => '0'],
            ['symbol' => '៛','currency' => 'KHR','description' => 'Cambodia, Riel','isdefault' => '0','active' => '0'],
            ['symbol' => 'F.CFA','currency' => 'XAF','description' => 'Franc CFA (XAF)','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'CAD','description' => 'Canadian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'KYD','description' => 'Cayman Islands Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'CLP','description' => 'Chilean Peso','isdefault' => '0','active' => '0'],
            ['symbol' => '¥','currency' => 'CNY','description' => 'China Yuan Renminbi','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'COP','description' => 'Colombian Peso','isdefault' => '1','active' => '1'],
            ['symbol' => 'CF','currency' => 'KMF','description' => 'Comoro Franc','isdefault' => '0','active' => '0'],
            ['symbol' => 'FC','currency' => 'CDF','description' => 'Franc Congolais','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'NZD','description' => 'New Zealand Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '₡','currency' => 'CRC','description' => 'Costa Rican Colon','isdefault' => '0','active' => '0'],
            ['symbol' => 'kn','currency' => 'HRK','description' => 'Croatian Kuna','isdefault' => '0','active' => '0'],
            ['symbol' => '$MN','currency' => 'CUP','description' => 'Cuban Peso','isdefault' => '0','active' => '0'],
            ['symbol' => 'CUC','currency' => 'CUC','description' => 'Cuban Convertible Peso','isdefault' => '0','active' => '0'],
            ['symbol' => 'ƒ','currency' => 'ANG','description' => 'Netherlands Antillian Guilder','isdefault' => '0','active' => '0'],
            ['symbol' => 'Kč','currency' => 'CZK','description' => 'Czech Koruna','isdefault' => '0','active' => '0'],
            ['symbol' => 'kr.','currency' => 'DKK','description' => 'Danish Krone','isdefault' => '0','active' => '0'],
            ['symbol' => 'Fdj','currency' => 'DJF','description' => 'Djibouti Franc','isdefault' => '0','active' => '0'],
            ['symbol' => 'RD$','currency' => 'DOP','description' => 'Dominican Peso','isdefault' => '0','active' => '0'],
            ['symbol' => 'ج.م','currency' => 'EGP','description' => 'Egyptian Pound','isdefault' => '0','active' => '0'],
            ['symbol' => '₡','currency' => 'SVC','description' => 'El Salvador Colon','isdefault' => '0','active' => '0'],
            ['symbol' => 'Nfk','currency' => 'ERN','description' => 'Eritrea, Nakfa','isdefault' => '0','active' => '0'],
            ['symbol' => 'ETB','currency' => 'ETB','description' => 'Ethiopian Birr','isdefault' => '0','active' => '0'],
            ['symbol' => '£','currency' => 'FKP','description' => 'Falkland Islands Pound','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'FJD','description' => 'Fiji Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'F','currency' => 'XPF','description' => 'CFP Franc','isdefault' => '0','active' => '0'],
            ['symbol' => 'D','currency' => 'GMD','description' => 'Gambia, Dalasi','isdefault' => '0','active' => '0'],
            ['symbol' => 'Lari','currency' => 'GEL','description' => 'Georgia, Lari','isdefault' => '0','active' => '0'],
            ['symbol' => '₵','currency' => 'GHS','description' => 'Ghana Cedi','isdefault' => '0','active' => '0'],
            ['symbol' => '£','currency' => 'GIP','description' => 'Gibraltar Pound','isdefault' => '0','active' => '0'],
            ['symbol' => 'Q','currency' => 'GTQ','description' => 'Guatemala, Quetzal','isdefault' => '0','active' => '0'],
            ['symbol' => '£','currency' => 'GBP','description' => 'Pound Sterling','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'GYD','description' => 'Guyana Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'G','currency' => 'HTG','description' => 'Haiti, Gourde','isdefault' => '0','active' => '0'],
            ['symbol' => 'L.','currency' => 'HNL','description' => 'Honduras, Lempira','isdefault' => '0','active' => '0'],
            ['symbol' => 'HK$','currency' => 'HKD','description' => 'Hong Kong Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'Ft','currency' => 'HUF','description' => 'Hungary, Forint','isdefault' => '0','active' => '0'],
            ['symbol' => 'kr.','currency' => 'ISK','description' => 'Iceland Krona','isdefault' => '0','active' => '0'],
            ['symbol' => 'Rp','currency' => 'IDR','description' => 'Indonesia, Rupiah','isdefault' => '0','active' => '0'],
            ['symbol' => '﷼','currency' => 'IRR','description' => 'Iranian Rial','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.ع.‏','currency' => 'IQD','description' => 'Iraqi Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => '₪','currency' => 'ILS','description' => 'New Israeli Shekel','isdefault' => '0','active' => '0'],
            ['symbol' => 'J$','currency' => 'JMD','description' => 'Jamaican Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '¥','currency' => 'JPY','description' => 'Japan, Yen','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.ا.‏','currency' => 'JOD','description' => 'Jordanian Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => '₸','currency' => 'KZT','description' => 'Kazakhstan, Tenge','isdefault' => '0','active' => '0'],
            ['symbol' => 'S','currency' => 'KES','description' => 'Kenyan Shilling','isdefault' => '0','active' => '0'],
            ['symbol' => '₩','currency' => 'KPW','description' => 'North Korean Won','isdefault' => '0','active' => '0'],
            ['symbol' => '₩','currency' => 'KRW','description' => 'South Korea, Won','isdefault' => '0','active' => '0'],
            ['symbol' => 'دينار‎‎‏','currency' => 'KWD','description' => 'Kuwaiti Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => 'сом','currency' => 'KGS','description' => 'Kyrgyzstan, Som','isdefault' => '0','active' => '0'],
            ['symbol' => '₭','currency' => 'LAK','description' => 'Laos, Kip','isdefault' => '0','active' => '0'],
            ['symbol' => 'ل.ل.‏','currency' => 'LBP','description' => 'Lebanese Pound','isdefault' => '0','active' => '0'],
            ['symbol' => 'M','currency' => 'LSL','description' => 'Lesotho, Loti','isdefault' => '0','active' => '0'],
            ['symbol' => 'R','currency' => 'ZAR','description' => 'South Africa, Rand','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'LRD','description' => 'Liberian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.ل.‏','currency' => 'LYD','description' => 'Libyan Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => 'CHF','currency' => 'CHF','description' => 'Swiss Franc','isdefault' => '0','active' => '0'],
            ['symbol' => 'MOP$','currency' => 'MOP','description' => 'Macao, Pataca','isdefault' => '0','active' => '0'],
            ['symbol' => 'ден.','currency' => 'MKD','description' => 'Macedonia, Denar','isdefault' => '0','active' => '0'],
            ['symbol' => 'Ar','currency' => 'MGA','description' => 'Malagasy Ariary','isdefault' => '0','active' => '0'],
            ['symbol' => 'MK','currency' => 'MWK','description' => 'Malawi, Kwacha','isdefault' => '0','active' => '0'],
            ['symbol' => 'RM','currency' => 'MYR','description' => 'Malaysian Ringgit','isdefault' => '0','active' => '0'],
            ['symbol' => 'MVR','currency' => 'MVR','description' => 'Maldives, Rufiyaa','isdefault' => '0','active' => '0'],
            ['symbol' => 'UM','currency' => 'MRO','description' => 'Mauritania, Ouguiya','isdefault' => '0','active' => '0'],
            ['symbol' => '₨','currency' => 'MUR','description' => 'Mauritius Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'MXN','description' => 'Mexican Peso','isdefault' => '0','active' => '1'],
            ['symbol' => 'lei','currency' => 'MDL','description' => 'Moldovan Leu','isdefault' => '0','active' => '0'],
            ['symbol' => '₮','currency' => 'MNT','description' => 'Mongolia, Tugrik','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.م.‏','currency' => 'MAD','description' => 'Moroccan Dirham','isdefault' => '0','active' => '0'],
            ['symbol' => 'MT','currency' => 'MZN','description' => 'Mozambique Metical','isdefault' => '0','active' => '0'],
            ['symbol' => 'K','currency' => 'MMK','description' => 'Myanmar, Kyat','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'NAD','description' => 'Namibian Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '₨','currency' => 'NPR','description' => 'Nepalese Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => 'C$','currency' => 'NIO','description' => 'Nicaragua, Cordoba Oro','isdefault' => '0','active' => '0'],
            ['symbol' => '₦','currency' => 'NGN','description' => 'Nigeria, Naira','isdefault' => '0','active' => '0'],
            ['symbol' => '﷼','currency' => 'OMR','description' => 'Rial Omani','isdefault' => '0','active' => '0'],
            ['symbol' => '₨','currency' => 'PKR','description' => 'Pakistan Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => 'B/.','currency' => 'PAB','description' => 'Panama, Balboa','isdefault' => '0','active' => '0'],
            ['symbol' => 'K','currency' => 'PGK','description' => 'Papua New Guinea, Kina','isdefault' => '0','active' => '0'],
            ['symbol' => '₲','currency' => 'PYG','description' => 'Paraguay, Guarani','isdefault' => '0','active' => '0'],
            ['symbol' => 'S/.','currency' => 'PEN','description' => 'Peru, Nuevo Sol','isdefault' => '0','active' => '0'],
            ['symbol' => '₱','currency' => 'PHP','description' => 'Philippine Peso','isdefault' => '0','active' => '0'],
            ['symbol' => 'zł','currency' => 'PLN','description' => 'Poland, Zloty','isdefault' => '0','active' => '0'],
            ['symbol' => '﷼','currency' => 'QAR','description' => 'Qatari Rial','isdefault' => '0','active' => '0'],
            ['symbol' => 'lei','currency' => 'RON','description' => 'Romania, New Leu','isdefault' => '0','active' => '0'],
            ['symbol' => '₽','currency' => 'RUB','description' => 'Russian Ruble','isdefault' => '0','active' => '0'],
            ['symbol' => 'RWF','currency' => 'RWF','description' => 'Rwanda Franc','isdefault' => '0','active' => '0'],
            ['symbol' => '£','currency' => 'SHP','description' => 'Saint Helena Pound','isdefault' => '0','active' => '0'],
            ['symbol' => 'WS$','currency' => 'WST','description' => 'Samoa, Tala','isdefault' => '0','active' => '0'],
            ['symbol' => 'Db','currency' => 'STD','description' => 'Sao Tome and Principe, Dobra','isdefault' => '0','active' => '0'],
            ['symbol' => '﷼','currency' => 'SAR','description' => 'Saudi Riyal','isdefault' => '0','active' => '0'],
            ['symbol' => 'Дин.','currency' => 'RSD','description' => 'Serbian Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => '₨','currency' => 'SCR','description' => 'Seychelles Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => 'Le','currency' => 'SLL','description' => 'Sierra Leone, Leone','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'SGD','description' => 'Singapore Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'SBD','description' => 'Solomon Islands Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'S','currency' => 'SOS','description' => 'Somali Shilling','isdefault' => '0','active' => '0'],
            ['symbol' => '₨','currency' => 'LKR','description' => 'Sri Lanka Rupee','isdefault' => '0','active' => '0'],
            ['symbol' => '$','currency' => 'SRD','description' => 'Surinam Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'E','currency' => 'SZL','description' => 'Swaziland, Lilangeni','isdefault' => '0','active' => '0'],
            ['symbol' => 'kr','currency' => 'SEK','description' => 'Swedish Krona','isdefault' => '0','active' => '0'],
            ['symbol' => '£','currency' => 'SYP','description' => 'Syrian Pound','isdefault' => '0','active' => '0'],
            ['symbol' => 'NT$','currency' => 'TWD','description' => 'New Taiwan Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'TJS','currency' => 'TJS','description' => 'Tajikistan, Somoni','isdefault' => '0','active' => '0'],
            ['symbol' => 'TSh','currency' => 'TZS','description' => 'Tanzanian Shilling','isdefault' => '0','active' => '0'],
            ['symbol' => '฿','currency' => 'THB','description' => 'Thailand, Baht','isdefault' => '0','active' => '0'],
            ['symbol' => 'T$','currency' => 'TOP','description' => 'Tonga, Paanga','isdefault' => '0','active' => '0'],
            ['symbol' => 'TT$','currency' => 'TTD','description' => 'Trinidad and Tobago Dollar','isdefault' => '0','active' => '0'],
            ['symbol' => 'د.ت.‏','currency' => 'TND','description' => 'Tunisian Dinar','isdefault' => '0','active' => '0'],
            ['symbol' => 'TL','currency' => 'TRY','description' => 'New Turkish Lira','isdefault' => '0','active' => '0'],
            ['symbol' => 'm','currency' => 'TMT','description' => 'Turkmenistani New Manat','isdefault' => '0','active' => '0'],
            ['symbol' => 'USh','currency' => 'UGX','description' => 'Uganda Shilling','isdefault' => '0','active' => '0'],
            ['symbol' => '₴','currency' => 'UAH','description' => 'Ukraine, Hryvnia','isdefault' => '0','active' => '0'],
            ['symbol' => 'دإ‏','currency' => 'AED','description' => 'UAE Dirham','isdefault' => '0','active' => '0'],
            ['symbol' => '$U','currency' => 'UYU','description' => 'Peso Uruguayo','isdefault' => '0','active' => '0'],
            ['symbol' => 'сўм','currency' => 'UZS','description' => 'Uzbekistan Sum','isdefault' => '0','active' => '0'],
            ['symbol' => 'VT','currency' => 'VUV','description' => 'Vanuatu, Vatu','isdefault' => '0','active' => '0'],
            ['symbol' => 'Bs. F.','currency' => 'VEF','description' => 'Venezuela Bolivares Fuertes','isdefault' => '0','active' => '0'],
            ['symbol' => '₫','currency' => 'VND','description' => 'Viet Nam, Dong','isdefault' => '0','active' => '0'],
            ['symbol' => '﷼','currency' => 'YER','description' => 'Yemeni Rial','isdefault' => '0','active' => '0'],
            ['symbol' => 'ZK','currency' => 'ZMW','description' => 'Zambia Kwacha','isdefault' => '0','active' => '0'],
            ['symbol' => 'р.','currency' => 'BYR','description' => 'Belarussian Ruble','isdefault' => '0','active' => '0'],
            ['symbol' => '₤','currency' => 'MTL','description' => 'Maltese Lira','isdefault' => '0','active' => '0'],
            ['symbol' => 'LSd','currency' => 'SDD','description' => 'Sudanese Dinar','isdefault' => '0','active' => '0']
        ];

    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateCurrencyTable()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\Dian\Currency::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
