<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SystemDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ElectronicBillingSeeder::class);
        $this->call(SystemCurrenciesSeeder::class);
        $this->call(SystemGeographySeeder::class);
        $this->call(SystemRolesAndPermissionsSeeder::class);
        $this->call(TenantPermissionsSeeder::class);
        $this->call(SystemOptionSeeder::class);
    }
}
