<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Str as Str;

class SystemOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->command->info('Truncating System table');
        $this->truncateOptionTable();


        $this->command->info('Insert Option data');
        
        /* agregar las opciones */
        foreach ($this->OptionsArray() as $key => $option) {
            $option = \App\Models\System\Option::create([
                'id' => $option['id'],
                'name' => $option['name'],
                'value' => $option['value']
            ]);
        }
        

    }


    /* arreglo con los valores */
    public function OptionsArray()
    {

        $options = array(
            '0' => array(
                'id' => 1,
                'name' => 'dateformat',
                'value' => 'Y-m-d|%Y-%m-%d'
                ),
            '1' => array(
                'id' => 2,
                'name' => 'default_timezone',
                'value' => 'America/Bogota'
                ),
            '2' => array(
                'id' => 3,
                'name' => 'active_language',
                'value' => 'es'
                ),
            '3' => array(
                'id' => 4,
                'name' => 'aside_menu_active',
                'value' => '{"aside_menu_active":[{"name":"dashboard","url":"admin\/dashboard","permission":null,"icon":"fa fa-tachometer-alt","id":"dashboard"},{"name":"customers","url":"admin\/customers","permission":"read-customers","icon":"fa fa-users","id":"customers"},{"name":"config","url":"#","permission":"read-config","icon":"fa fa-cog","id":"config","children":[{"name":"users","url":"admin\/users","permission":"read-users","icon":null,"id":"child-users"},{"name":"roles","url":"admin\/roles","permission":"read-roles","icon":null,"id":"child-roles"},{"name":"permissions","url":"admin\/permissions","permission":"read-permissions","icon":null,"id":"child-permissions"}]}]}'
                ),
            '4' => array(
                'id' => 5,
                'name' => 'theme_style',
                'value' => '[{"id":"admin-menu","color":"#e34400"},{"id":"admin-menu-submenu-open","color":"#ffffff"},{"id":"admin-menu-links","color":"#421f1f"}]'
                ),
            '5' => array(
                'id' => 6,
                'name' => 'company_name',
                'value' => 'PublikDesign'
                ),
            '6' => array(
                'id' => 7,
                'name' => 'company_logo',
                'value' => 'logo.png'
                ),
            '7' => array(
                'id' => 8,
                'name' => 'company_address',
                'value' => 'Cra 46 # 79 - 163'
                ),
            '8' => array(
                'id' => 9,
                'name' => 'company_city',
                'value' => 'Medellin'
                ),
            '9' => array(
                'id' => 10,
                'name' => 'company_country',
                'value' => 'Colombia'
                ),
            '10' => array(
                'id' => 11,
                'name' => 'company_phone',
                'value' => '7548428'
                ),
            '11' => array(
                'id' => 12,
                'name' => 'company_cellphone',
                'value' => '3012503849'
                ),
            '12' => array(
                'id' => 13,
                'name' => 'company_email',
                'value' => 'soporte@publikdesign.co'
                ),
            '13' => array(
                'id' => 14,
                'name' => 'company_domain',
                'value' => 'www.publikdesign.co'
                ),
        );

        return $options;

    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateOptionTable()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\System\Option::truncate();       
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
