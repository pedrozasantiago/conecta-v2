<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\System\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SystemRolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {

        $this->command->info('Truncating Role and Permission tables');
        $this->truncateTables();

        $config = $this->PermissionsAndRoles();
        $role_structure = $config['role_structure'];
        $permission_map = $config['permissions_map'];

        // Create permission primary
        foreach ($role_structure['admin'] as $key => $perm) {
            Permission::firstOrCreate([
                'name' => $key,
                'guard_name' => 'web_admin',
                'display_name' => ucwords(str_replace('-', ' ', $key)),
                'description' => ucwords(str_replace('-', ' ', $key))
            ]);
        }


        // crear permisos secundario
        foreach ($role_structure as $key => $modules) {
            
            // Create a new role
            $role = Role::create([
                'name' => $key,
                'guard_name' => 'web_admin',
                'display_name' => ucwords(str_replace("_", " ", $key)),
                'description' => ucwords(str_replace("_", " ", $key))
            ]);

            $this->command->info('Creating Role '. strtoupper($key));


            // Reading role permission modules
            foreach ($modules as $module => $value) {
                
                $permissions = explode(',', $value);

                // verificar si el permiso principal existe
                $checkperm = Permission::where('name', $module)->first();

                foreach ($permissions as $p => $perm) {
                    
                    $permissionValue = $permission_map[$perm];

                    $permission = Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'guard_name' => 'web_admin',
                        'display_name' => ucfirst($permissionValue) . ' ' . ucwords(str_replace('-', ' ', $module)),
                        'description' => ucfirst($permissionValue) . ' ' . ucwords(str_replace('-', ' ', $module)),
                        'parent_id' => $checkperm->id
                    ]);

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                    
                    if (!$role->hasPermissionTo($permission->name)) {
                        $role->givePermissionTo($permission->name);
                    } else {
                        $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
                    }
                }
            }


            $this->command->info("Creating '{$key}' user");
            // Create default user for each role
            $user = User::create([
                'first_name' => ucwords(str_replace("_", " ", $key)),
                'last_name' => ucwords(str_replace("_", " ", $key)),
                'username' => strtolower(str_replace("_", "", $key)),
                'email' => $key.'@app.com',
                'password' => bcrypt('password'),
                'api_token' => Str::random(60)
            ]);
            $user->assignRole($role->name);

        }

    }


    public function PermissionsAndRoles()
    {
        return [
            'role_structure' => [
                'admin' => [
                    'users' => 'c,r,u,d',
                    'profile' => 'r,u',
                    'roles' => 'c,r,u,d',
                    'permissions' => 'c,r,u,d',
                    'customers' => 'c,r,i,e,u,d',
                    'config' => 'r,u'
                ],
                'user' => [
                    'profile' => 'r,u'
                ]
            ],
            'permissions_map' => [
                'c' => 'create',
                'r' => 'read',
                'u' => 'update',
                'i' => 'import',
                'e' => 'export',
                'd' => 'delete'
            ]
        ];
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('role_has_permissions')->truncate();
        User::truncate();
        Role::truncate();
        Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
