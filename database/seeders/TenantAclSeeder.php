<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Str as Str;

class TenantAclSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $this->command->info('Truncating System table');
        $this->truncateOptionTable();


        // insertar pantallas
        $this->command->info('Insertar datos de pantallas');

        foreach ($this->Pantallas() as $key => $pantalla) {
            \App\Models\Customer\TmPantalla::create($pantalla);
        }

        // insertar permisos
        $this->command->info('Insertar datos de permisos');

        foreach ($this->Permisos() as $key => $permiso) {
            \App\Models\Customer\TmPermiso::create($permiso);
        }

        // insertar roles
        $this->command->info('Insertar datos de Roles');

        foreach ($this->Roles() as $key => $rol) {
            \App\Models\Customer\TmRole::create($rol);
        }


        // insertar roles de pantallas
        $this->command->info('Insertar datos de roles de pantallas');

        foreach ($this->RolesPantalla() as $key => $value) {
            \App\Models\Customer\TmRolesPantalla::create($value);
        }

        // insertar roles de permisos
        $this->command->info('Insertar datos de roles de permisos');

        foreach ($this->RolesPermisos() as $key => $value) {
            \App\Models\Customer\TmRolesPermiso::create($value);
        }
    }

    // arreglo de pantallas
    public function Pantallas() {
        return
                array(
                    array(
                        'cod' => 'CLIE',
                        'nombre' => 'Clientes',
                        'descripcion' => 'Pantalla de clientes OPERATIVO',
                        'orden' => 1,
                    ),
                    array(
                        'cod' => 'PROV',
                        'nombre' => 'Proveedores',
                        'descripcion' => 'Pantalla de proveedores',
                        'orden' => 3,
                    ),
                    array(
                        'cod' => 'EMPLE',
                        'nombre' => 'Empleados',
                        'descripcion' => 'Pantalla de empleados',
                        'orden' => 3,
                    ),
                    array(
                        'cod' => 'ORDEN',
                        'nombre' => 'Ordenes',
                        'descripcion' => 'Pantalla de ordenes',
                        'orden' => 4,
                    ),
                    array(
                        'cod' => 'IMPO',
                        'nombre' => 'Importaciones',
                        'descripcion' => 'Pantalla de importaciones',
                        'orden' => 5,
                    ),
                    array(
                        'cod' => 'EXPO',
                        'nombre' => 'Exportaciones',
                        'descripcion' => 'Pantalla de exportaciones',
                        'orden' => 6,
                    ),
                    array(
                        'cod' => 'ROAALERT',
                        'nombre' => 'Actividades alertadas',
                        'descripcion' => 'Pantalla de actividades alertadas',
                        'orden' => 7,
                    ),
                    array(
                        'cod' => 'CUOP',
                        'nombre' => 'Cuadro contable',
                        'descripcion' => 'Pantalla de cuadro contable',
                        'orden' => 8,
                    ),
                    array(
                        'cod' => 'ROLES',
                        'nombre' => 'Roles',
                        'descripcion' => 'Pantalla de roles',
                        'orden' => 9,
                    ),
                    array(
                        'cod' => 'ICAMP',
                        'nombre' => 'Campañas',
                        'descripcion' => 'Pantalla de campañas',
                        'orden' => 10,
                    ),
                    array(
                        'cod' => 'GCAMP',
                        'nombre' => 'Clientes por campaña',
                        'descripcion' => 'Pantalla de clientes por campaña',
                        'orden' => 11,
                    ),
                    array(
                        'cod' => 'CORREOS',
                        'nombre' => 'Correos',
                        'descripcion' => 'Pantalla de Mailing',
                        'orden' => 12,
                    ),
                    array(
                        'cod' => 'SMS',
                        'nombre' => 'SMS',
                        'descripcion' => 'Pantalla de SMS',
                        'orden' => 13,
                    ),
                    array(
                        'cod' => 'COTIZA_NEW',
                        'nombre' => 'Cotizaciones',
                        'descripcion' => 'Pantalla de cotizaciones',
                        'orden' => 14,
                    ),
                     array(
                        'cod' => 'COTIZA',
                        'nombre' => 'Cotizaciones',
                        'descripcion' => 'Pantalla de cotizaciones',
                        'orden' => 14,
                    ),
                    array(
                        'cod' => 'USERS',
                        'nombre' => 'Usuarios',
                        'descripcion' => 'Pantalla de usuarios',
                        'orden' => 15,
                    ),
                    array(
                        'cod' => 'ESTADIS',
                        'nombre' => 'Estadisticas',
                        'descripcion' => 'Pantalla de estadisticas',
                        'orden' => 16,
                    ),
                    array(
                        'cod' => 'CCAMP',
                        'nombre' => 'Clientes',
                        'descripcion' => 'Pantalla de clientes COMERCIAL',
                        'orden' => 2,
                    ),
                    array(
                        'cod' => 'CONTACTOS_CLIE',
                        'nombre' => 'Contactos Clientes',
                        'descripcion' => 'Pantalla de contactos de clientes',
                        'orden' => 2,
                    ),
                    array(
                        'cod' => 'TRAYECTOS',
                        'nombre' => 'Trayectos',
                        'descripcion' => 'Pantalla de trayectos',
                        'orden' => 1,
                    ),
                    array(
                        'cod' => 'CONTACTOS_PROV',
                        'nombre' => 'Contactos proveedores',
                        'descripcion' => 'Pantalla de contactos de proveedores',
                        'orden' => 2,
                    ),
                    array(
                        'cod' => 'LISTAS',
                        'nombre' => 'Configuración de listas',
                        'descripcion' => 'Pantalla de Configuración de listas',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'SERVICIOS',
                        'nombre' => 'Configuración de servicios',
                        'descripcion' => 'Pantalla de Configuración de servicios',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'PLANTILLAS',
                        'nombre' => 'Configuración de plantillas',
                        'descripcion' => 'Pantalla de Configuración de plantillas',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'REPORT',
                        'nombre' => 'Reportes',
                        'descripcion' => 'Pantalla de reportes',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'WHATSAPP',
                        'nombre' => 'Chat de Whatsapp',
                        'descripcion' => 'Pantalla de chat de Whatsapp',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'LISTA_PRECIOS',
                        'nombre' => 'Lista de prcios',
                        'descripcion' => 'Pantalla de lista de precios',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'SEGMENTACION',
                        'nombre' => 'Segmentacion de clientes',
                        'descripcion' => 'Pantalla Segmentacion de clientes',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'REQUEST',
                        'nombre' => 'Requerimientos',
                        'descripcion' => 'Pantalla Requerimientos',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'ADJUSTMENT',
                        'nombre' => 'Ajustes contables',
                        'descripcion' => 'Pantalla de ajustes contables',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'DAILY_LOG',
                        'nombre' => 'Registro diario',
                        'descripcion' => 'Pantalla de registros diarios',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'BANCOS',
                        'nombre' => 'Bancos',
                        'descripcion' => 'Pantalla de bancos',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'CREDIT_NOTE',
                        'nombre' => 'Notas credito',
                        'descripcion' => 'Pantalla de notas credito',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'TRANSACTION',
                        'nombre' => 'Ingresos de dinero',
                        'descripcion' => 'Pantalla de ingresos de dinero',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'PAYMENT',
                        'nombre' => 'Egresos de dinero',
                        'descripcion' => 'Pantalla de egresos de dinero',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'PURCHASE',
                        'nombre' => 'Compras',
                        'descripcion' => 'Pantalla de compras',
                        'orden' => NULL,
                    ),
                    array(
                        'cod' => 'INVOICE',
                        'nombre' => 'Ventas',
                        'descripcion' => 'Pantalla de ventas',
                        'orden' => NULL,
                    ),
        );
    }

    public function Permisos() {
        return [
            [
                "id" => 1,
                "cod" => "INS",
                "pantalla" => "CLIE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cliente",
                "nombre" => "Nuevo",
                "descripcion" => "Crea un nuevo cliente"
            ],
            [
                "id" => 2,
                "cod" => "UPD",
                "pantalla" => "CLIE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cliente",
                "nombre" => "Modificar",
                "descripcion" => "Modifica un cliente existente"
            ],
            [
                "id" => 3,
                "cod" => "DEL",
                "pantalla" => "CLIE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cliente",
                "nombre" => "Borrar",
                "descripcion" => "Elimina un cliente existente"
            ],
            [
                "id" => 4,
                "cod" => "UPD",
                "pantalla" => "CLIE",
                "id_tab" => "1",
                "nombre_tab" => "Información general",
                "nombre" => "Modificar",
                "descripcion" => "Modifica la información general del cliente"
            ],
            [
                "id" => 5,
                "cod" => "UPD",
                "pantalla" => "CLIE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Modificar",
                "descripcion" => "Modifica el estado de la documentación legal de un cliente"
            ],
            [
                "id" => 6,
                "cod" => "INS",
                "pantalla" => "CLIE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Cargar",
                "descripcion" => "Permite subir documentos digitalizados de un cliente"
            ],
            [
                "id" => 7,
                "cod" => "DOWN",
                "pantalla" => "CLIE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Descargar",
                "descripcion" => "Permite descargar documentos digitalizados en el servidor de un cliente"
            ],
            [
                "id" => 8,
                "cod" => "DEL",
                "pantalla" => "CLIE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Borrar",
                "descripcion" => "Permite borrar documentos digitalizados en el servidor de un cliente"
            ],
            [
                "id" => 9,
                "cod" => "INS",
                "pantalla" => "PROV",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de proveedor",
                "nombre" => "Nuevo",
                "descripcion" => "Crea un nuevo proveedor"
            ],
            [
                "id" => 10,
                "cod" => "UPD",
                "pantalla" => "PROV",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de proveedor",
                "nombre" => "Modificar",
                "descripcion" => "Modifica un cliente existente"
            ],
            [
                "id" => 11,
                "cod" => "DEL",
                "pantalla" => "PROV",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de proveedor",
                "nombre" => "Borrar",
                "descripcion" => "Elimina un cliente existente"
            ],
            [
                "id" => 12,
                "cod" => "UPD",
                "pantalla" => "PROV",
                "id_tab" => "1",
                "nombre_tab" => "Información general",
                "nombre" => "Modificar",
                "descripcion" => "Modifica la información general del cliente"
            ],
            [
                "id" => 13,
                "cod" => "UPD",
                "pantalla" => "PROV",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Modificar",
                "descripcion" => "Modifica el estado de la documentación legal de un cliente"
            ],
            [
                "id" => 14,
                "cod" => "INS",
                "pantalla" => "PROV",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Cargar",
                "descripcion" => "Permite subir documentos digitalizados de un cliente"
            ],
            [
                "id" => 15,
                "cod" => "DOWN",
                "pantalla" => "PROV",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Descargar",
                "descripcion" => "Permite descargar documentos digitalizados en el servidor de un cliente"
            ],
            [
                "id" => 16,
                "cod" => "DEL",
                "pantalla" => "PROV",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Borrar",
                "descripcion" => "Permite borrar documentos digitalizados en el servidor de un cliente"
            ],
            [
                "id" => 17,
                "cod" => "INS",
                "pantalla" => "EMPLE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de empleado",
                "nombre" => "Nuevo",
                "descripcion" => "Crea un nuevo empleado"
            ],
            [
                "id" => 18,
                "cod" => "UPD",
                "pantalla" => "EMPLE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de empleado",
                "nombre" => "Modificar",
                "descripcion" => "Modifica un empleado existente"
            ],
            [
                "id" => 19,
                "cod" => "DEL",
                "pantalla" => "EMPLE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de empleado",
                "nombre" => "Borrar",
                "descripcion" => "Elimina un empleado existente"
            ],
            [
                "id" => 20,
                "cod" => "UPD",
                "pantalla" => "EMPLE",
                "id_tab" => "1",
                "nombre_tab" => "Información general",
                "nombre" => "Modificar",
                "descripcion" => "Modifica la información general del empleado"
            ],
            [
                "id" => 21,
                "cod" => "UPD",
                "pantalla" => "EMPLE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Modificar",
                "descripcion" => "Modifica el estado de la documentación legal de un empleado"
            ],
            [
                "id" => 22,
                "cod" => "INS",
                "pantalla" => "EMPLE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Nuevo",
                "descripcion" => "Permite subir documentos digitalizados de un empleado"
            ],
            [
                "id" => 23,
                "cod" => "DOWN",
                "pantalla" => "EMPLE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Descargar",
                "descripcion" => "Permite descargar documentos digitalizados en el servidor de un empleado"
            ],
            [
                "id" => 24,
                "cod" => "DEL",
                "pantalla" => "EMPLE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Borrar",
                "descripcion" => "Permite borrar documentos digitalizados en el servidor de un empleado"
            ],
            [
                "id" => 25,
                "cod" => "INS",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Nuevo",
                "descripcion" => "Crea una orden de servicio"
            ],
            [
                "id" => 26,
                "cod" => "DEL",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Borrar",
                "descripcion" => "Elimina una orden de servicio"
            ],
            [
                "id" => 27,
                "cod" => "UPD",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una orden de servicio existente"
            ],
            [
                "id" => 28,
                "cod" => "CON",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Imprimir",
                "descripcion" => "Permite imprimir el checklist del DO"
            ],
            [
                "id" => 29,
                "cod" => "UPD",
                "pantalla" => "ORDEN",
                "id_tab" => "1",
                "nombre_tab" => "Información adicional",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una orden de servicio existente"
            ],
            [
                "id" => 30,
                "cod" => "INS",
                "pantalla" => "ORDEN",
                "id_tab" => "2",
                "nombre_tab" => "Formulario de BL",
                "nombre" => "Nuevo",
                "descripcion" => "Crea un registro de BL"
            ],
            [
                "id" => 31,
                "cod" => "UPD",
                "pantalla" => "ORDEN",
                "id_tab" => "2",
                "nombre_tab" => "Formulario de BL",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un registro del BL"
            ],
            [
                "id" => 32,
                "cod" => "DEL",
                "pantalla" => "ORDEN",
                "id_tab" => "2",
                "nombre_tab" => "Formulario de BL",
                "nombre" => "Borrar",
                "descripcion" => "Eliminar un registro del BL"
            ],
            [
                "id" => 33,
                "cod" => "CON",
                "pantalla" => "ORDEN",
                "id_tab" => "2",
                "nombre_tab" => "Formulario de BL",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir un BL"
            ],
            [
                "id" => 34,
                "cod" => "UPD",
                "pantalla" => "ORDEN",
                "id_tab" => "3",
                "nombre_tab" => "Digitalizar documentos",
                "nombre" => "Modificar",
                "descripcion" => "Modifica el estado de la documentación del DO"
            ],
            [
                "id" => 35,
                "cod" => "INS",
                "pantalla" => "ORDEN",
                "id_tab" => "3",
                "nombre_tab" => "Digitalizar documentos",
                "nombre" => "Nuevo",
                "descripcion" => "Permite subir documentos digitalizados de un Embarque"
            ],
            [
                "id" => 36,
                "cod" => "DOWN",
                "pantalla" => "ORDEN",
                "id_tab" => "3",
                "nombre_tab" => "Digitalizar documentos",
                "nombre" => "Descargar",
                "descripcion" => "Permite descargar documentos digitalizados en el servidor de un Embarque"
            ],
            [
                "id" => 37,
                "cod" => "DEL",
                "pantalla" => "ORDEN",
                "id_tab" => "3",
                "nombre_tab" => "Digitalizar documentos",
                "nombre" => "Borrar",
                "descripcion" => "Permite borrar documentos digitalizados en el servidor de un Embarque"
            ],
            [
                "id" => 38,
                "cod" => "CON",
                "pantalla" => "ORDEN",
                "id_tab" => "3",
                "nombre_tab" => "Digitalizar documentos",
                "nombre" => "Imprimir",
                "descripcion" => "Permite imprimir el checklist de la documentación del DO"
            ],
            [
                "id" => 39,
                "cod" => "UPD",
                "pantalla" => "IMPO",
                "id_tab" => "0",
                "nombre_tab" => "Actividades",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una actividad de la lista"
            ],
            [
                "id" => 40,
                "cod" => "INS",
                "pantalla" => "IMPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una bitacora"
            ],
            [
                "id" => 41,
                "cod" => "UPD",
                "pantalla" => "IMPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una bitacora existente"
            ],
            [
                "id" => 42,
                "cod" => "DEL",
                "pantalla" => "IMPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Borrar",
                "descripcion" => "Elimina una bitacora existente"
            ],
            [
                "id" => 43,
                "cod" => "UPD",
                "pantalla" => "EXPO",
                "id_tab" => "0",
                "nombre_tab" => "Actividades",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una actividad "
            ],
            [
                "id" => 44,
                "cod" => "INS",
                "pantalla" => "EXPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una bitacora"
            ],
            [
                "id" => 45,
                "cod" => "UPD",
                "pantalla" => "EXPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una bitacora existente"
            ],
            [
                "id" => 46,
                "cod" => "DEL",
                "pantalla" => "EXPO",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Borrar",
                "descripcion" => "Elimina una bitacora existente"
            ],
            [
                "id" => 47,
                "cod" => "UPD",
                "pantalla" => "EXPO",
                "id_tab" => "2",
                "nombre_tab" => "Programación",
                "nombre" => "Modificar",
                "descripcion" => "Programación de actividades de la operación"
            ],
            [
                "id" => 48,
                "cod" => "UPD",
                "pantalla" => "IMPO",
                "id_tab" => "2",
                "nombre_tab" => "Programación",
                "nombre" => "Modificar",
                "descripcion" => "Programación de actividades de la operación"
            ],
            [
                "id" => 49,
                "cod" => "INS",
                "pantalla" => "CUOP",
                "id_tab" => "0",
                "nombre_tab" => "Cuadro contable",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un registro "
            ],
            [
                "id" => 50,
                "cod" => "UPD",
                "pantalla" => "CUOP",
                "id_tab" => "0",
                "nombre_tab" => "Cuadro contable",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un registro existente"
            ],
            [
                "id" => 51,
                "cod" => "DEL",
                "pantalla" => "CUOP",
                "id_tab" => "0",
                "nombre_tab" => "Cuadro contable",
                "nombre" => "Borrar",
                "descripcion" => "Eliminar un registro existente"
            ],
            [
                "id" => 52,
                "cod" => "INS",
                "pantalla" => "ROLES",
                "id_tab" => "0",
                "nombre_tab" => "Formulario roles",
                "nombre" => "Nuevo",
                "descripcion" => "Crea un nuevo rol"
            ],
            [
                "id" => 53,
                "cod" => "UPD",
                "pantalla" => "ROLES",
                "id_tab" => "0",
                "nombre_tab" => "Formulario roles",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un rol existente"
            ],
            [
                "id" => 54,
                "cod" => "DEL",
                "pantalla" => "ROLES",
                "id_tab" => "0",
                "nombre_tab" => "Formulario roles",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un rol existente"
            ],
            [
                "id" => 55,
                "cod" => "UPD",
                "pantalla" => "ROLES",
                "id_tab" => "1",
                "nombre_tab" => "Pantallas",
                "nombre" => "Modificar",
                "descripcion" => "Asignar permisos de pantallas"
            ],
            [
                "id" => 56,
                "cod" => "UPD",
                "pantalla" => "ROLES",
                "id_tab" => "2",
                "nombre_tab" => "Permisos",
                "nombre" => "Modificar",
                "descripcion" => "Asignar permisos de permisos"
            ],
            [
                "id" => 57,
                "cod" => "UPD",
                "pantalla" => "USERS",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de usuario",
                "nombre" => "Modificar",
                "descripcion" => "Modificar información de un usuario existente"
            ],
            [
                "id" => 58,
                "cod" => "INS",
                "pantalla" => "ICAMP",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de campaña",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una nueva campaña"
            ],
            [
                "id" => 59,
                "cod" => "UPD",
                "pantalla" => "ICAMP",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de campaña",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una campaña existente"
            ],
            [
                "id" => 60,
                "cod" => "DEL",
                "pantalla" => "ICAMP",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de campaña",
                "nombre" => "Borrar",
                "descripcion" => "Borrar una campaña existente"
            ],
            [
                "id" => 61,
                "cod" => "INS",
                "pantalla" => "ICAMP",
                "id_tab" => "1",
                "nombre_tab" => "Clientes campaña",
                "nombre" => "Nuevo",
                "descripcion" => "Agregar un cliente a una campaña"
            ],
            [
                "id" => 62,
                "cod" => "DEL",
                "pantalla" => "ICAMP",
                "id_tab" => "1",
                "nombre_tab" => "Clientes campaña",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un cliente de una campaña"
            ],
            [
                "id" => 63,
                "cod" => "INS",
                "pantalla" => "ICAMP",
                "id_tab" => "2",
                "nombre_tab" => "Carga desde base de datos",
                "nombre" => "Nuevo",
                "descripcion" => "Cargar clientes a una campaña desde una base de datos de excel"
            ],
            [
                "id" => 64,
                "cod" => "INS",
                "pantalla" => "GCAMP",
                "id_tab" => "0",
                "nombre_tab" => "Bitacora",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una bitacora"
            ],
            [
                "id" => 65,
                "cod" => "UPD",
                "pantalla" => "GCAMP",
                "id_tab" => "0",
                "nombre_tab" => "Bitacora",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una bitacora existente"
            ],
            [
                "id" => 66,
                "cod" => "DEL",
                "pantalla" => "GCAMP",
                "id_tab" => "0",
                "nombre_tab" => "Bitacora",
                "nombre" => "Borrar",
                "descripcion" => "Elimina una bitacora existente"
            ],
            [
                "id" => 67,
                "cod" => "SEND",
                "pantalla" => "CORREOS",
                "id_tab" => "0",
                "nombre_tab" => "Correos",
                "nombre" => "Enviar",
                "descripcion" => "Enviar un correo"
            ],
            [
                "id" => 68,
                "cod" => "SEND",
                "pantalla" => "SMS",
                "id_tab" => "0",
                "nombre_tab" => "SMS",
                "nombre" => "Enviar",
                "descripcion" => "Enviar SMS"
            ],
            [
                "id" => 69,
                "cod" => "INS",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una nueva cotización"
            ],
            [
                "id" => 70,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una cotización existente"
            ],
            [
                "id" => 71,
                "cod" => "DEL",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Borrar",
                "descripcion" => "Borrar una cotización existente"
            ],
            [
                "id" => 72,
                "cod" => "D",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Duplicar",
                "descripcion" => "Duplicar una cotización existente"
            ],
            [
                "id" => 73,
                "cod" => "MIGR",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Crear DO",
                "descripcion" => "Migrar una cotización a un DO"
            ],
            [
                "id" => 74,
                "cod" => "CON",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir la cotización"
            ],
            [
                "id" => 75,
                "cod" => "INS",
                "pantalla" => "COTIZA",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Nuevo",
                "descripcion" => "Crear una bitacora"
            ],
            [
                "id" => 76,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Modificar",
                "descripcion" => "Modifica una bitacora existente"
            ],
            [
                "id" => 77,
                "cod" => "DEL",
                "pantalla" => "COTIZA",
                "id_tab" => "1",
                "nombre_tab" => "Bitacora",
                "nombre" => "Borrar",
                "descripcion" => "Elimina una bitacora existente"
            ],
            [
                "id" => 78,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "2",
                "nombre_tab" => "Información adicional",
                "nombre" => "Modificar",
                "descripcion" => "Modificar la información adicional de una cotización"
            ],
            [
                "id" => 79,
                "cod" => "INS",
                "pantalla" => "COTIZA",
                "id_tab" => "3",
                "nombre_tab" => "Preliquidación",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un registro de preliquidación"
            ],
            [
                "id" => 80,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "3",
                "nombre_tab" => "Preliquidación",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un registro de preliquidación existente"
            ],
            [
                "id" => 81,
                "cod" => "DEL",
                "pantalla" => "COTIZA",
                "id_tab" => "3",
                "nombre_tab" => "Preliquidación",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un registro de preliquidación existente"
            ],
            [
                "id" => 82,
                "cod" => "CON",
                "pantalla" => "COTIZA",
                "id_tab" => "3",
                "nombre_tab" => "Preliquidación",
                "nombre" => "Imprimir",
                "descripcion" => "Imprmir la preliquidación"
            ],
            [
                "id" => 83,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "4",
                "nombre_tab" => "Tipos de gastos",
                "nombre" => "Modificar",
                "descripcion" => "Modificar los tipos de gastos"
            ],
            [
                "id" => 84,
                "cod" => "UPD",
                "pantalla" => "COTIZA",
                "id_tab" => "12",
                "nombre_tab" => "Instrucción de embarque",
                "nombre" => "Modificar",
                "descripcion" => "Modificar la información de una instrucción de embarque"
            ],
            [
                "id" => 85,
                "cod" => "CON",
                "pantalla" => "COTIZA",
                "id_tab" => "12",
                "nombre_tab" => "Instrucción de embarque",
                "nombre" => "Imprimir",
                "descripcion" => "Generar una instrucción de embarque"
            ],
            [
                "id" => 86,
                "cod" => "CON",
                "pantalla" => "CLIE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir check list del cliente"
            ],
            [
                "id" => 87,
                "cod" => "CON",
                "pantalla" => "PROV",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir check list del cliente"
            ],
            [
                "id" => 88,
                "cod" => "CON",
                "pantalla" => "EMPLE",
                "id_tab" => "2",
                "nombre_tab" => "Documentación legal",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir check list del cliente"
            ],
            [
                "id" => 89,
                "cod" => "INS",
                "pantalla" => "COTIZA",
                "id_tab" => "4",
                "nombre_tab" => "Tipos de gastos",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un registro de la cotización"
            ],
            [
                "id" => 90,
                "cod" => "DEL",
                "pantalla" => "COTIZA",
                "id_tab" => "4",
                "nombre_tab" => "Tipos de gastos",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un registro de la cotización"
            ],
            [
                "id" => 91,
                "cod" => "CON",
                "pantalla" => "CLIE",
                "id_tab" => "3",
                "nombre_tab" => "Reporte",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir reporte"
            ],
            [
                "id" => 92,
                "cod" => "CON",
                "pantalla" => "PROV",
                "id_tab" => "3",
                "nombre_tab" => "Reporte",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir reporte"
            ],
            [
                "id" => 93,
                "cod" => "CON",
                "pantalla" => "EMPLE",
                "id_tab" => "3",
                "nombre_tab" => "Reporte",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir reporte"
            ],
            [
                "id" => 94,
                "cod" => "CON",
                "pantalla" => "ORDEN",
                "id_tab" => "4",
                "nombre_tab" => "Reporte",
                "nombre" => "Imprimir",
                "descripcion" => "Imprimir reporte"
            ],
            [
                "id" => 95,
                "cod" => "INS",
                "pantalla" => "CONTACTOS_CLIE",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un nuevo contacto"
            ],
            [
                "id" => 96,
                "cod" => "UPD",
                "pantalla" => "CONTACTOS_CLIE",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un contacto"
            ],
            [
                "id" => 97,
                "cod" => "DEL",
                "pantalla" => "CONTACTOS_CLIE",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un contacto"
            ],
            [
                "id" => 98,
                "cod" => "INS",
                "pantalla" => "PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un nuevo contacto"
            ],
            [
                "id" => 99,
                "cod" => "UPD",
                "pantalla" => "PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un contacto"
            ],
            [
                "id" => 100,
                "cod" => "DEL",
                "pantalla" => "PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un contacto"
            ],
            [
                "id" => 104,
                "cod" => "UPD_APROBA",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una cotización APROBADA"
            ],
            [
                "id" => 105,
                "cod" => "INS",
                "pantalla" => "CONTACTOS_PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un nuevo contacto"
            ],
            [
                "id" => 106,
                "cod" => "UPD",
                "pantalla" => "CONTACTOS_PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un contacto"
            ],
            [
                "id" => 107,
                "cod" => "DEL",
                "pantalla" => "CONTACTOS_PROV",
                "id_tab" => "4",
                "nombre_tab" => "Información general/ Contactos",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un contacto"
            ],
            [
                "id" => 108,
                "cod" => "SEND",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de cotización",
                "nombre" => "Enviar",
                "descripcion" => "Enviar cotización al cliente"
            ],
            [
                "id" => 109,
                "cod" => "C",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Cerrar",
                "descripcion" => "Cerrar el DO"
            ],
            [
                "id" => 110,
                "cod" => "ANULAR",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Anular",
                "descripcion" => "Anular el DO"
            ],
            [
                "id" => 111,
                "cod" => "D",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Duplicar",
                "descripcion" => "Duplicar un DO"
            ],
            [
                "id" => 112,
                "cod" => "ABRIR",
                "pantalla" => "COTIZA",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de Cotizacion",
                "nombre" => "Reabrir",
                "descripcion" => "Reabrir una cotizacion"
            ],
            [
                "id" => 113,
                "cod" => "INS",
                "pantalla" => "SERVICIOS",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de servicios",
                "nombre" => "Crear",
                "descripcion" => "Crear un servicio"
            ],
            [
                "id" => 114,
                "cod" => "UPD",
                "pantalla" => "SERVICIOS",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de servicios",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un servicio"
            ],
            [
                "id" => 115,
                "cod" => "DEL",
                "pantalla" => "SERVICIOS",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de servicios",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un servicio"
            ],
            [
                "id" => 116,
                "cod" => "INS",
                "pantalla" => "SERVICIOS",
                "id_tab" => "1",
                "nombre_tab" => "Formulario de variaciones",
                "nombre" => "Crear",
                "descripcion" => "Crear una variacion"
            ],
            [
                "id" => 117,
                "cod" => "UPD",
                "pantalla" => "SERVICIOS",
                "id_tab" => "1",
                "nombre_tab" => "Formulario de variaciones",
                "nombre" => "Modificar",
                "descripcion" => "Modificar una variacion"
            ],
            [
                "id" => 118,
                "cod" => "DEL",
                "pantalla" => "SERVICIOS",
                "id_tab" => "1",
                "nombre_tab" => "Formulario de variaciones",
                "nombre" => "Borrar",
                "descripcion" => "Borrar una variacion"
            ],
            [
                "id" => 119,
                "cod" => "INS",
                "pantalla" => "USERS",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de usuario",
                "nombre" => "Nuevo",
                "descripcion" => "Crear un nuevo un usuario"
            ],

            [
                "id" => 120,
                "cod" => "UPD",
                "pantalla" => "CLIE",
                "id_tab" => "6",
                "nombre_tab" => "Portal de clientes",
                "nombre" => "Modificar",
                "descripcion" => "Habilitar acceso Portal de clientes"
            ],
            [
                "id" => 121,
                "cod" => "H",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "House",
                "descripcion" => "Crear embarque House"
            ],

            
            [
                "id" => 122,
                "cod" => "UPD_CLOSE",
                "pantalla" => "ORDEN",
                "id_tab" => "0",
                "nombre_tab" => "Formulario de DO",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un DO cerrado"
            ],
            [
                "id" => 123,
                "cod" => "INS_CLIE_VER",
                "pantalla" => "INVOICE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario facturacion",
                "nombre" => "Nuevo,Modificar,Duplicar",
                "descripcion" => "Restringir facturacion con clientes no VERIFICADOS"
            ],
            [
                "id" => 124,
                "cod" => "INS_CLIE_PTE",
                "pantalla" => "INVOICE",
                "id_tab" => "0",
                "nombre_tab" => "Formulario facturacion",
                "nombre" => "Nuevo,Modificar,Duplicar",
                "descripcion" => "Restringir facturacion con clientes en estado de informacion PENDIENTE"
            ],
            [
                "id" => 125,
                "cod" => "INS",
                "pantalla" => "LISTA_PRECIOS",
                "id_tab" => "1",
                "nombre_tab" => "Lista de precios servicios",
                "nombre" => "Nuevo",
                "descripcion" => "Agregar un servicio a la lista de precios"
            ],
            [
                "id" => 126,
                "cod" => "UPD",
                "pantalla" => "LISTA_PRECIOS",
                "id_tab" => "1",
                "nombre_tab" => "Lista de precios servicios",
                "nombre" => "Modificar",
                "descripcion" => "Modificar un servicio de la lista de precios"
            ],
            [
                "id" => 127,
                "cod" => "DEL",
                "pantalla" => "LISTA_PRECIOS",
                "id_tab" => "1",
                "nombre_tab" => "Lista de precios servicios",
                "nombre" => "Borrar",
                "descripcion" => "Borrar un servicio de la lista de precios"
            ],
            [
                "id" => 128,
                "cod" => "EMPTY_VAL",
                "pantalla" => "REQUEST",
                "id_tab" => "0",
                "nombre_tab" => "Restringir Campos en blanco",
                "nombre" => "Modificar",
                "descripcion" => "No guarda el requerimiento si hay algun campo en blanco"
            ],
            [
                "id" => 129,
                "cod" => "SPLIT",
                "pantalla" => "CUOP",
                "id_tab" => "0",
                "nombre_tab" => "Cuadro contable",
                "nombre" => "Dividir",
                "descripcion" => "Separar la venta y la compra en dos registros"
            ],
            [
                "id" => 130,
                "cod" => "NOTIFICATIONS",
                "pantalla" => "WHATSAPP",
                "id_tab" => "0",
                "nombre_tab" => "Chat Whatsapp",
                "nombre" => "Chat",
                "descripcion" => "Notificaciones chat de whatsapp"
            ],
        ];
    }

    public function Roles() {

        return [
            [
                "id" => 1,
                "nombre" => 'Administrador',
                "descripcion" => 'Todos los permisos',
                "maquina" => '::1',
                "usuario" => 'SOPORTE'
            ],
            [
                "id" => 2,
                "nombre" => 'Comercial',
                "descripcion" => 'Permisos modulo comercial',
                "maquina" => '::1',
                "usuario" => 'SOPORTE'
            ],
            [
                "id" => 3,
                "nombre" => 'Operaciones',
                "descripcion" => 'Permisos modulo operativo',
                "maquina" => '::1',
                "usuario" => 'SOPORTE'
            ]
        ];
    }

    //tm_roles_pantalla
    public function RolesPantalla() {
        return [
            [
                "rol" => "1",
                "pantalla" => "CCAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CONTACTOS_CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CONTACTOS_PROV",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CORREOS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "COTIZA_NEW",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "COTIZA",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CUOP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "EMPLE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ESTADIS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "EXPO",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "GCAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ICAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "IMPO",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "LISTAS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ORDEN",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "PROV",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ROAALERT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ROLES",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "SMS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "TRAYECTOS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "USERS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "REPORT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            
            
            // COMERCIAL 
            
            [
                "rol" => "2",
                "pantalla" => "CCAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "CONTACTOS_CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "CONTACTOS_PROV",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "CORREOS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "COTIZA_NEW",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            
             
            [
                "rol" => "2",
                "pantalla" => "ESTADIS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             
            [
                "rol" => "2",
                "pantalla" => "GCAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "2",
                "pantalla" => "ICAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             
            [
                "rol" => "2",
                "pantalla" => "SMS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             [
                "rol" => "2",
                "pantalla" => "REPORT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            
            
            // OPERATIVO
            
            
            
            [
                "rol" => "3",
                "pantalla" => "CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "CONTACTOS_CLIE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "CONTACTOS_PROV",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "CORREOS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             
             
            [
                "rol" => "3",
                "pantalla" => "CUOP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "EMPLE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             
            [
                "rol" => "3",
                "pantalla" => "EXPO",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "GCAMP",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
             
            [
                "rol" => "3",
                "pantalla" => "IMPO",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "ORDEN",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "PROV",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "3",
                "pantalla" => "ROAALERT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            
            [
                "rol" => "3",
                "pantalla" => "REPORT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "ADJUSTMENT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "DAILY_LOG",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "BANCOS",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "PAYMENT",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "TRANSACTION",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "CREDIT_NOTE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "PURCHASE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "rol" => "1",
                "pantalla" => "INVOICE",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            
        ];
    }

    //tm_roles_pantallas
    public function RolesPermisos() {
        return [
            [
                "id_rol" => "1",
                "id_permiso" => "1",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "2",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "3",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "4",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "5",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "6",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "7",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "8",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "9",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "10",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "11",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "12",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "13",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "14",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "15",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "16",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "17",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "18",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "19",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "20",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "21",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "22",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "23",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "24",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "25",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "26",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "27",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "28",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "29",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "30",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "31",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "32",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "33",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "34",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "35",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "36",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "37",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "38",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "39",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "40",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "41",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "42",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "43",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "44",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "45",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "46",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "47",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "48",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "49",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "50",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "51",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "52",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "53",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "54",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "55",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "56",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "57",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "58",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "59",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "60",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "61",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "62",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "63",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "64",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "65",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "66",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "67",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "68",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "69",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "70",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "71",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "72",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "73",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "74",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "75",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "76",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "77",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "78",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "79",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "80",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "81",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "82",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "83",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "84",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "85",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "86",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "87",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "88",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "89",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "90",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "91",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "92",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "93",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "94",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "95",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "96",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "97",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "98",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "99",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "100",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "101",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "102",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "103",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "104",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "105",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "106",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "107",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "108",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "109",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "110",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "111",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "112",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "113",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "114",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "115",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "116",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "117",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "118",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "119",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ],
            [
                "id_rol" => "1",
                "id_permiso" => "121",
                "valor" => "S",
                "usuario" => "SOPORTE",
                "maquina" => "::1",
                "fecha" => now()
            ]
        ];
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateOptionTable() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\Customer\TmPantalla::truncate();
        \App\Models\Customer\TmPermiso::truncate();
        \App\Models\Customer\TmRole::truncate();
        \App\Models\Customer\TmRolesPantalla::truncate();
        \App\Models\Customer\TmRolesPermiso::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
