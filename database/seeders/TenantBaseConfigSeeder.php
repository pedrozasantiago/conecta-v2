<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Str as Str;

class TenantBaseConfigSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $this->command->info('Truncating System table');
        $this->truncateOptionTable();

        // insertar plantillas
        $this->command->info('Insertar datos de configuraciones');

        foreach ($this->Configuraciones() as $key => $value) {
            \App\Models\Customer\Configuracion::create($value);
        }
        
        // insertar plantillas
        $this->command->info('Insertar datos de plantillas');

        foreach ($this->Plantillas() as $key => $value) {
            \App\Models\Customer\Plantilla::create($value);
        }

        // insertar trayectos
        $this->command->info('Insertar datos de trayectos');

        foreach ($this->Trayectos() as $key => $value) {
            \App\Models\Customer\TmTrayecto::create($value);
        }
    }

    public function Configuraciones() {
        return [
            [
                "configuracion" => "CLIE_CONFIG",
                "tipo" => "array",
                "valor" => '{"PAIS":"","CIUDAD":"1003","DIRECCION":"1001","TELEFONO":"1002"}'
            ],
            [
                "configuracion" => "AWB_CONFIG",
                "tipo" => "array",
                "valor" => '["4665","4667","4672","5000","5001","5002","5003","5004","5005","5006","5007","5008","5009","5010","5011","5012","5013","5014","5015","5016","5017","5018","5019","5020","5021","5022","5023","5024","5025","5026"]'
            ],
            [
                "configuracion" => "SEP_DEC",
                "tipo" => "string",
                "valor" => ","
            ],
            
            [
                "configuracion" => "SEP_MIL",
                "tipo" => "string",
                "valor" => "."
            ],
            [
                "configuracion" => "WHATSAPP_NOTIFICATIONS",
                "tipo" => "string",
                "valor" => "false"
            ],
            [
                "configuracion" => "TRACKING_CLIENT",
                "tipo" => "string",
                "valor" => "false"
            ],
            [
                "configuracion" => "MONEDA",
                "tipo" => "string",
                "valor" => "COP"
            ],
            [
                "configuracion" => "INVOICE_DECIMAL",
                "tipo" => "string",
                "valor" => "0"
            ],
             [
                "configuracion" => "COTIZA_RATE_DET",
                "tipo" => "string",
                "valor" => "true"
            ],
            [
                "configuracion" => "CONF_COTIZA_OBS",
                "tipo" => "string",
                "valor" => "true"
            ],
            [
                "configuracion" => "MULTISESSION",
                "tipo" => "string",
                "valor" => "true"
            ],
            [
                "configuracion" => "BL_CONFIG",
                "tipo" => "array",
                "valor" => '["4676","4602","4677","4667","4631","4678","4665","4653","4611","4632","4672","4675"]'
            ],
            [
                "configuracion" => "PLANTILLA_COTIZACION",
                "tipo" => "string",
                "valor" => "1"
            ],
            [
                "configuracion" => "TOTAL_COT_MN",
                "tipo" => "string",
                "valor" => "SI"
            ],
            [
                "configuracion" => "CONSEC_DO",
                "tipo" => "array",
                "valor" => "{\"SERVICIO\":\"NO\",\"SUBTIPO\":\"NO\",\"OFICINA\":\"SI\",\"ANO\":\"SI\"}"
            ],
            [
                "configuracion" => "SERVIDOR",
                "tipo" => "string",
                "valor" => "AMAZON"
            ],
            [
                "configuracion" => "MENU",
                "tipo" => "array",
                "valor" => "[\"COTIZA\",\"FACTURA_ELECTRONICA\",\"WP_TEMPLATES\",\"WP_INTERACTIVE\"]"
            ],
            [
                "configuracion" => "LOGO_EMPRESA",
                "tipo" => "string",
                "valor" => "https://s3.us-east-2.amazonaws.com/operativo/operativo1.csyp.co/galeria/logo_conecta.jpeg"
            ],
            [
                "configuracion" => "LOGO_EMPRESA_NEGRO",
                "tipo" => "string",
                "valor" => "https://s3.us-east-2.amazonaws.com/operativo/operativo1.csyp.co/galeria/logo_conecta.jpeg"
            ],
            [
                "configuracion" => "LOGO_SECUNDARIO",
                "tipo" => "string",
                "valor" => "https://s3.us-east-2.amazonaws.com/operativo/operativo1.csyp.co/galeria/logo_conecta.jpeg"
            ],
            [
                "configuracion" => "NOMBRE_EMPRESA",
                "tipo" => "string",
                "valor" => config('tenancy.name_company')
            ],
            [
                "configuracion" => "NIT_EMPRESA",
                "tipo" => "string",
                "valor" => config('tenancy.nit')
            ],
            [
                "configuracion" => "RAZON_SOCIAL",
                "tipo" => "string",
                "valor" => config('tenancy.name_company')
            ],
            [
                "configuracion" => "PAIS",
                "tipo" => "string",
                "valor" => "COLOMBIA"
            ],
            [
                "configuracion" => "CIUDAD",
                "tipo" => "string",
                "valor" => "Medellin"
            ],
            [
                "configuracion" => "FUENTE",
                "tipo" => "string",
                "valor" => "Tahoma, Geneva, sans-serif"
            ],
            [
                "configuracion" => "TAMANO_FUENTE",
                "tipo" => "string",
                "valor" => "12px"
            ],
            [
                "configuracion" => "COLOR",
                "tipo" => "string",
                "valor" => "#7FB384"
            ],
            [
                "configuracion" => "PAGINA_WEB",
                "tipo" => "string",
                "valor" => "https://www.conectacarga.co"
            ],
            [
                "configuracion" => "MAILING_USER",
                "tipo" => "string",
                "valor" => env('SUPPORT_ACCOUNT_EMAIL')
            ],
            [
                "configuracion" => "MAILING_PASS",
                "tipo" => "string",
                "valor" => env('SUPPORT_ACCOUNT_EMAIL_PASSWORD'),
            ],
            [
                "configuracion" => "MAILING_FROM",
                "tipo" => "string",
                "valor" => "info@csyp.co"
            ],
            [
                "configuracion" => "MAILING_FROM_TITLE",
                "tipo" => "string",
                "valor" => config('tenancy.name_company')
            ],
            [
                "configuracion" => "REPORT_TIPO_SERVIDOR",
                "tipo" => "string",
                "valor" => "normal"
            ],
            [
                "configuracion" => "REPORT_USUARIO",
                "tipo" => "string",
                "valor" => env('SUPPORT_ACCOUNT_EMAIL')
            ],
            [
                "configuracion" => "REPORT_CONTRASENA",
                "tipo" => "string",
                "valor" => env('SUPPORT_ACCOUNT_EMAIL_PASSWORD'),
            ],
            [
                "configuracion" => "REPORT_HOST",
                "tipo" => "string",
                "valor" => "mail.csyp.co"
            ],
            [
                "configuracion" => "REPORT_SMTPAUTH",
                "tipo" => "string",
                "valor" => "true"
            ],
            [
                "configuracion" => "REPORT_SMTPSECURE",
                "tipo" => "string",
                "valor" => "ssl"
            ],
            [
                "configuracion" => "REPORT_PORT",
                "tipo" => "string",
                "valor" => "465"
            ],
            [
                "configuracion" => "REPORT_FROM",
                "tipo" => "string",
                "valor" => env('SUPPORT_ACCOUNT_EMAIL')
            ],
            [
                "configuracion" => "REPORT_FROM_TITLE",
                "tipo" => "string",
                "valor" => config('tenancy.name_company')
            ],
            [
                "configuracion" => "REPORT_PATH",
                "tipo" => "string",
                "valor" => "{mail.csyp.co:993/imap/ssl/novalidate-cert}INBOX.Sent"
            ],
            [
                "configuracion" => "PLANTILLA_REPORT",
                "tipo" => "string",
                "valor" => "1"
            ],
            [
                "configuracion" => "ASUNTO_REPORTE",
                "tipo" => "array",
                "valor" => "[\"NOM_EMPRESA\",\"NOM_CLIENTE\",\"DO\",\"8009\",\"NOM_SERVICIO\"]"
            ],
            [
                "configuracion" => "TELEFONO",
                "tipo" => "string",
                "valor" => "3001235678"
            ],
            [
                "configuracion" => "DIRECCION",
                "tipo" => "string",
                "valor" => "Calle 19 #43g80"
            ],
            [
                "configuracion" => "CORREO_CORPORATIVO",
                "tipo" => "string",
                "valor" => config('tenancy.email')
            ],
            [
                "configuracion" => "INVOICE_CONFIG",
                "tipo" => "array",
                "valor" => "{\"HEADER\":true,\"DIRECCION\":\"1001\",\"CIUDAD\":\"1009\",\"INFOGRAL\":[\"4667\",\"4665\",\"4602\",\"4653\",\"4677\",\"4676\",\"4675\",\"4672\"],\"shipper\":\"\",\"consignee\":\"\"}"
            ],
            [
                "configuracion" => "CE_CONFIG",
                "tipo" => "array",
                "valor" => "{\"DIRECCION\":\"1001\",\"CIUDAD\":\"1009\"}"
            ],
            [
                "configuracion" => "DESTINATARIO_REPORTES",
                "tipo" => "string",
                "valor" => config('tenancy.email')
            ],
            [
                "configuracion" => "COT_DECIMAL",
                "tipo" => "string",
                "valor" => "0"
            ],
            [
                "configuracion" => "LANGUAGE",
                "tipo" => "string",
                "valor" => "es"
            ],
            [
                "configuracion" => "CCO_INVOICE_NOTES",
                "tipo" => "string",
                "valor" => "true"
            ]
        ];
    }

    public function Plantillas() {

        return [
            [
                'tipo' => 'QUOT_NOTES_SPA',
                'texto' => '<p>Ingrese aquí las Notas de las cotizaciones en español</p>'
            ],
            [
                'tipo' => 'QUOT_HEADER_ENG',
                'texto' => '<p>Ingrese aquí el Encabezado de las cotizaciones en ingles</p>'
            ],
            [
                'tipo' => 'QUOT_HEADER_SPA',
                'texto' => '<p>Ingrese aquí las Notas de las cotizaciones en español</p>'
            ],
            [
                'tipo' => 'QUOT_NOTES_ENG',
                'texto' => '<p>Ingrese aquí el Encabezado de las cotizaciones en ingles</p>'
            ],
            [
                'tipo' => 'INVOICE_HEADER',
                'texto' => '<p>Ingrese aquí el Encabezado de las facturas</p>'
            ],
            [
                'tipo' => 'INVOICE_FOOTER',
                'texto' => '<p>Ingrese aquí el Pie de pagina de las facturas</p>'
            ],
            [
                'tipo' => 'CIERRE_DO',
                'texto' => '<p>Ingrese aquí el texto para el cierre de DO</p>'
            ],
            [
                'tipo' => 'ANTICIPO_NOTES',
                'texto' => '<p>Ingrese aquí las notas para la solicitud de anticipos</p>'
            ],
        ];
    }

    //tm_roles_pantalla
    public function Trayectos() {
        return [
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "AACHEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "ANSBACH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "AUGSBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BAMBERG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BAYREUTH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BERLIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BIELEFELD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BONN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BRAUNSCHWEIG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BREMEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "BREMENHAVEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "CALLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "CHEMNITZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "COLOGNE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "CUXHAVEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "DARMSTADT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "DORTMUND",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "DRESDEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "DUISBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "DUSSELDORF",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "EMDEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "ERFURT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "ESSEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "FLENSBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "FRANKFURT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "FREIBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "FULDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "GARMISCH-PATENKIRCHE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "GERA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "GIESSEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "GOETTINGEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HAGEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HALLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HAMBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HANNOVER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HEIDELBERG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HEILBRONN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HELMSTEDT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HILDESHEIM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "HOF",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KAISERSLAUTERN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KARLSRUHE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KASSEL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KIEL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KOBLENZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "KONSTANZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "LANDSHUT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "LEIPZIG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "LINDAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "LUDWIGSHAFEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "LUEBECK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "MAGDEBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "MAINZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "MANNHEIM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "MUENSTER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "MUNICH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "NUREMBERG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "OBERHAUSEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "OFFENBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "OLDENBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "OSNABRUECK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "PASSAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "POTSDAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "PUTTGARDEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "REGENSBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "ROSTOK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "SAARBRUECKEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "SCHWERIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "SIEGBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "SIEGEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "SINGEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "STUTTGART",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "ULM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "WIESBADEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "WILHEMSHAVEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "WUERZBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALEMANIA",
                "CIUDAD" => "WUPPERTAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ALGERIA",
                "CIUDAD" => "ALGERIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AMERICAN SAMOA",
                "CIUDAD" => "PAGO PAGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ANTIGUA Y BARBUDA",
                "CIUDAD" => "ANTIGUA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ANTIGUA Y BARBUDA",
                "CIUDAD" => "SAINT JOHN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ANTILLAS HOLANDESAS",
                "CIUDAD" => "SABA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARABIA SAUDI",
                "CIUDAD" => "DAMMAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARABIA SAUDI",
                "CIUDAD" => "JEDDAH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARABIA SAUDI",
                "CIUDAD" => "RIYADH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "ALGER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "ANNABA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "BUJIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "MOSTAGANEM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "ORAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGELIA",
                "CIUDAD" => "SKIKDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGENTINA",
                "CIUDAD" => "BUENOS AIRES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARGENTINA",
                "CIUDAD" => "ZARATE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ARUBA",
                "CIUDAD" => "ORANJESTAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRALIA",
                "CIUDAD" => "ADELAIDE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRALIA",
                "CIUDAD" => "BRISBANE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRALIA",
                "CIUDAD" => "FREMANTLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRALIA",
                "CIUDAD" => "MELBOURNE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRALIA",
                "CIUDAD" => "SYDNEY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "BREGENZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "GRAZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "INNSBRUCK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "LINZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "MINSK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "VIENNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "AUSTRIA",
                "CIUDAD" => "ZALZBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BAHAMAS",
                "CIUDAD" => "NASAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BAHRAIN",
                "CIUDAD" => "BAHRAIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BANGLADESH",
                "CIUDAD" => "CHITTAGONG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BARBADOS",
                "CIUDAD" => "BRIDGETOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BELGICA",
                "CIUDAD" => "AMBERES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BELGICA",
                "CIUDAD" => "BRUSELAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BELGICA",
                "CIUDAD" => "ZEEBRUGGE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BELICE",
                "CIUDAD" => "BELICE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BENIN",
                "CIUDAD" => "COTONU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BIRMANIA",
                "CIUDAD" => "RANGUN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BOLIVIA",
                "CIUDAD" => "COCHABAMBA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BOLIVIA",
                "CIUDAD" => "LA PAZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BOLIVIA",
                "CIUDAD" => "SANTA CRUZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BOTSUANA",
                "CIUDAD" => "GABARONE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "BELO HORIZONTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "BETIM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "CANOAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "CURITIBA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "FORTALEZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "ITAJAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "ITAPOA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "MANAOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "NOVO HAMBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "PARANAGUA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "PORTO ALEGRE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "RIO DE JAINEIRO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "RIO DE JANEIRO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "RIO GRANDE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "SALVADOR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "SALVADOR BAHÍA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "SANTOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRASIL",
                "CIUDAD" => "SUAPE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BRUNEI",
                "CIUDAD" => "MUARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BULGARIA",
                "CIUDAD" => "BURGAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BULGARIA",
                "CIUDAD" => "SOFIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BULGARIA",
                "CIUDAD" => "VARNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BURKINA FASO",
                "CIUDAD" => "BOBO-DIOULASSO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BURKINA FASO",
                "CIUDAD" => "OUAGADOUGOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "BURUNDI",
                "CIUDAD" => "BUJUMBURA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CABO VERDE",
                "CIUDAD" => "PRAIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CAMBODIA",
                "CIUDAD" => "PHNOM PHENH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CAMBODIA",
                "CIUDAD" => "SIHANOUKVILLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CAMERUN",
                "CIUDAD" => "DOUALA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "CALGARY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "CHARLOTTETOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "EDMONTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "HALIFAX",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "MONCTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "MONTREAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "REGINA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "SASKATOON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "ST. JOHNS NF",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "TORONTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "VANCOUVER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CANADA",
                "CIUDAD" => "WINNIPEG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CATAR",
                "CIUDAD" => "DOHA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHARLOTTE AMALIE",
                "CIUDAD" => "ST. THOMAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "ANTOFAGASTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "ARICA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "CONCEPCION",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "CORONEL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "IQUIQUE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "PUNTA ARENAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "SAN ANTONIO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "TALCAHUANO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHILE",
                "CIUDAD" => "VALPARAISO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "BAO JI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "BAODING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "BEIJIAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "BEIJING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHANGCHUN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHANGSHU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHANGZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHENGDE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHENGDU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHIWAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CHONGQING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "CIXI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "DACHAN BAY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "DALIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "DAQING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "DONGGUANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "FOSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "FUJIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "FUSHUN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "FUZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "GAOMING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "GUANGZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HAIKOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HAINING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HAIYAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HANDAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HANGZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HARBIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HEFEI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HONG KONG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HUANGPU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HUANGSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HUAYIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "HUZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JIAMUSI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JIANGMEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JIANGYIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JIAXING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JINAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JINZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "JIUJIANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "KAIFENG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "KAOHSIUNG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "LANZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "LIANHUASHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "LIANYUNGANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "LUOYANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "MAANSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "MACAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "MUDANJIANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "NANHAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "NANJING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "NANSHA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "NANTONG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "NINGBO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "PANYU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "QINGDAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "QINHUANGDAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "QIQIHAR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "RONGQI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHANGHAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHANTOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHAOXING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHEKOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHENYANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHENZHEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHIJIAZHUANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SHUNDE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "SUZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAICANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAICHUNG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAIPEI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAIYUAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TAIZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TANGSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TIANJIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "TONGLU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WEIFANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WENZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WU LU MU QI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WUHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WUHU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WUXI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "WUZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XIAMEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XIAOLAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XIAOSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINCHANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINFENG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINGANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINGTAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINHUI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "XINING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YANCHENG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YANGZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YANTAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YANTIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YINCHUAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YIWU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "YIXING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHANJIANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHENGJIANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHENGZHOU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHENJIANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHONGSHAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHUHAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHINA",
                "CIUDAD" => "ZHUMADIAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHIPRE",
                "CIUDAD" => "LARNACA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHIPRE",
                "CIUDAD" => "LIMASOL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CHIPRE",
                "CIUDAD" => "NICOSIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "BARRANQUILLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "BOGOTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "BQA, CTG, SMM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "BUENAVENTURA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "CARTAGENA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "IPIALES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "MEDELLIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "RIONEGRO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COLOMBIA",
                "CIUDAD" => "SEGOVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COSTA DE MARFIL",
                "CIUDAD" => "ABIYAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COSTA RICA",
                "CIUDAD" => "PUERTO CALDERA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COSTA RICA",
                "CIUDAD" => "PUERTO LIMON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COSTA RICA",
                "CIUDAD" => "PUERTO MOIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "COSTA RICA",
                "CIUDAD" => "SAN JOSE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CROACIA",
                "CIUDAD" => "PLOCE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CROACIA",
                "CIUDAD" => "RIJEKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CROACIA",
                "CIUDAD" => "ZAGREB",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CUBA",
                "CIUDAD" => "LA HABANA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CURAZAO",
                "CIUDAD" => "CURACAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "CURAZAO",
                "CIUDAD" => "WILLEMSTAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "DINAMARCA",
                "CIUDAD" => "AARHUS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "DINAMARCA",
                "CIUDAD" => "COPENHAGUE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "DOMINICA",
                "CIUDAD" => "ROSEAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ECUADOR",
                "CIUDAD" => "GUAYAQUIL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EGIPTO",
                "CIUDAD" => "6TH OF OCTOBER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EGIPTO",
                "CIUDAD" => "ALEJANDRIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EGIPTO",
                "CIUDAD" => "CAIRO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EGIPTO",
                "CIUDAD" => "DAMIETA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EGIPTO",
                "CIUDAD" => "PORT SAID",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EL SALVADOR",
                "CIUDAD" => "ACAJUTLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EL SALVADOR",
                "CIUDAD" => "SAN SALVADOR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "ABU DABI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "AL AIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "DUBAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "FUYAIRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "JEBEL ALI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "KHOR FAKKAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "RAS AL JAIMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "SHARJAH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "EMIRATOS ARABES",
                "CIUDAD" => "UMM AL QAYWAYN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESCOCIA",
                "CIUDAD" => "GLASGOW",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESLOVAQUIA",
                "CIUDAD" => "BRATISLAVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESLOVAQUIA",
                "CIUDAD" => "KOSICE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESLOVENIA",
                "CIUDAD" => "KOPER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESLOVENIA",
                "CIUDAD" => "LIUBLIANA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESLOVENIA",
                "CIUDAD" => "MARIBOR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "ALGECIRAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "ALICANTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "ALMERIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "BARCELONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "BILBAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "BURGOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "CADIZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "CEUTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "CORDOBA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "CORUÑA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "FUERTEVENTURA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "GIJON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "IBIZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "LANZAROTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "LOGROÑO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "LUGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "MADRID",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "MALAGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "MELILLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "MURCIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "ORENSE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "OVIEDO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "PALMA DE MALLORCA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "PAMPLONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "SANTA CRUZ DE TENERIFE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "SEVILLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "TENERIFE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "TOLEDO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "VALENCIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "VALLADOLID",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "VIGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESPAÑA",
                "CIUDAD" => "ZARAGOZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "ATLANTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "BALTIMORE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "BOSTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "CHARLESTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "CHARLOTTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "CHICAGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "CLEVELAND",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "DALLAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "DENVER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "HOUSTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "LOS ANGELES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "MIAMI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "NEW JERSEY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "NEW ORLEANS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "NEW YORK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "NORFOLK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "OAKLAND",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "PENSILVANIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "PHILADELPHIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "PHOENIX",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "PORT EVERGLADES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "PORTLAND",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "SAN DIEGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "SAN FRANCISCO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "SEATTLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "TACOMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTADOS UNIDOS",
                "CIUDAD" => "WASHINGTON DC",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTONIA",
                "CIUDAD" => "PARNU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTONIA",
                "CIUDAD" => "TALLINN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ESTONIA",
                "CIUDAD" => "TARTU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FILIPINAS",
                "CIUDAD" => "CEBU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FILIPINAS",
                "CIUDAD" => "DAVAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FILIPINAS",
                "CIUDAD" => "MANILA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FILIPINAS",
                "CIUDAD" => "MANILA NORTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FILIPINAS",
                "CIUDAD" => "MANILA SUR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FINLANDIA",
                "CIUDAD" => "HELSINKI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FINLANDIA",
                "CIUDAD" => "KOTKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FINLANDIA",
                "CIUDAD" => "TURKU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FIYI",
                "CIUDAD" => "LAUTOKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FIYI",
                "CIUDAD" => "SUVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "BURDEOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "ESTRASBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "FOS SUR MER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "FUTUNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "GENNEVILLIERS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "LE HAVRE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "LYON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "MARSEILLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "MARSELLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "FRANCIA",
                "CIUDAD" => "PARIS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GABON",
                "CIUDAD" => "LIBREVILLE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GALES",
                "CIUDAD" => "CARDIFF",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GAMBIA",
                "CIUDAD" => "BANJUL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GEORGIA",
                "CIUDAD" => "SAVANNAH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GHANA",
                "CIUDAD" => "ACRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GHANA",
                "CIUDAD" => "TEMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRAN CANARIA",
                "CIUDAD" => "LAS PALMAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRAN CANARIA",
                "CIUDAD" => "SANTA CRUZ DE LA PALMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRAN CANARIA",
                "CIUDAD" => "TENERIFE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRANADA",
                "CIUDAD" => "GRANADA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRECIA",
                "CIUDAD" => "ATENAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRECIA",
                "CIUDAD" => "PIREO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRECIA",
                "CIUDAD" => "SALONICA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GRECIA",
                "CIUDAD" => "TESALONICA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUADALUPE",
                "CIUDAD" => "POINTE A PITRE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUATEMALA",
                "CIUDAD" => "GUATEMALA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUATEMALA",
                "CIUDAD" => "PUERTO QUETZAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUATEMALA",
                "CIUDAD" => "SANTO TOMAS DE CASTILLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUINEA",
                "CIUDAD" => "CONAKRI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUYANA",
                "CIUDAD" => "GEORGETOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "GUYANA FRANCESA",
                "CIUDAD" => "DEGRAD DES CANNES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HAITI",
                "CIUDAD" => "PUERTO PRINCIPE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HOLANDA",
                "CIUDAD" => "AMSTERDAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HOLANDA",
                "CIUDAD" => "ROTTERDAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HONDURAS",
                "CIUDAD" => "PUERTO CORTES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HONDURAS",
                "CIUDAD" => "SAN PEDRO  SULA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HONDURAS",
                "CIUDAD" => "TEGUCIGALPA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "HUNGRIA",
                "CIUDAD" => "BUDAPEST",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "AHMEDABAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "BANGALORE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "CALCUTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "CHENNAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "COCHIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "HYDERABAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "MUMBAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "MUMBAY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "MUNDRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "NHAVA SHEVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "NUEVA  DELHI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "TUTICORIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDIA",
                "CIUDAD" => "VADODARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "BALIKPAPAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "BATAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "BELAWAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "JAKARTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "SEMARANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INDONESIA",
                "CIUDAD" => "SURABAYA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "BIRMINGHAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "BRISTOL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "DERBY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "FELIXSTOWE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "LEEDS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "LIVERPOOL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "LONDON GATEWAY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "LONDRES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "MANCHESTER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "SOUTHAMPTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "THAMESPORT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "INGLATERRA",
                "CIUDAD" => "TILBURY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "IRAN",
                "CIUDAD" => "BANDAR ABBAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "IRAQ",
                "CIUDAD" => "UM KASAR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "IRLANDA",
                "CIUDAD" => "BELFAST",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "IRLANDA",
                "CIUDAD" => "CORK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "IRLANDA",
                "CIUDAD" => "DUBLIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLA WALLIS",
                "CIUDAD" => "WALLIS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLANDA",
                "CIUDAD" => "REIKIAVIK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLANDIA",
                "CIUDAD" => "AKUREYRI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLANDIA",
                "CIUDAD" => "REIKIAVIK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CAIMAN",
                "CIUDAD" => "GEORGE TOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CAIMAN",
                "CIUDAD" => "GRAN CAIMAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CANARIAS",
                "CIUDAD" => "EL HIERRO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CANARIAS",
                "CIUDAD" => "FUERTEVENTURA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CANARIAS",
                "CIUDAD" => "LA GOMERA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS CANARIAS",
                "CIUDAD" => "LANZAROTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS SALOMON",
                "CIUDAD" => "HONIARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS VIRGENES",
                "CIUDAD" => "SAINT CROIX",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS VIRGENES",
                "CIUDAD" => "TORTOLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISLAS VIRGENES BRITANICAS",
                "CIUDAD" => "VIRGEN GORDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISRAEL",
                "CIUDAD" => "ASHOOD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ISRAEL",
                "CIUDAD" => "HAIFA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ALESSANDRIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ANCONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "AOSTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "AREZZO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ASCOLI PICENO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ASTI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "AVELLINO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BARI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BELLUNO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BENEVENTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BERGAMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BIELLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BOLONIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BOLZANO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BRESCIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "BRINDIGI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CAGLIARI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CALTANISETTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CAMPOBASSO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CASERTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CATANIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CATANZARO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CESENA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CHIETI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CIVITAVECCHIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "COMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "COSENZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CREMONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "CUNEO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ELBA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ENNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "FERRARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "FLORENCIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "FOGGIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "FORLI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "FROSINONE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "GENOVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "GIOIA TAURO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "GORIZIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "GROSSETO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "IMPERIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ISERNIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LA SPEZIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LAQUILA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LATINA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LECCE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LECCO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LIVORNO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "LUCCA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MACERATA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MANTOVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MASSA CARRARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MATERA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MESSINA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MILAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "MODENA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "NAPOLES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "NOVARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "NUORO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PADUA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PALERMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PARMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PAVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PERUGIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PESARO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PESCARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PIACENZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PISA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PORDENONE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "PRATO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "RAVENA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "REGGIO EMILIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "REGIO DE CALABRIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "RIMINI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ROMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "ROVIGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "SALERNO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "SAVONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "SIRACUSA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "SONDRIO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TARANTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TERAMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TERNI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TRENTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TREVISO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TRIESTE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "TURIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "UDINE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VADO LIGURE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VARESE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VENICE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VERCELLI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VERONA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VICENZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ITALIA",
                "CIUDAD" => "VITERBO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAMAICA",
                "CIUDAD" => "KINGSTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "AKITA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "CHIBA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "FUKUYAMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "HAKATA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "HIROSHIMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "IMABARI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "KANAZAWA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "KOBE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "KUMAMOTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "MATSUYAMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "MIZUSHIMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "MOJI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "NAGASAKI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "NAGOYA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "NAHA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "NIIGATA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "OITA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "OSAKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "SAKAIMINATO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "SAKATA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "SHIMIZU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "SHIMONOSEKI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TAKAMATSU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TOKUSHIMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TOKYO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TOMAKOMAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TOYAMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TOYOHASHI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "TSURUGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "YOKKAICHI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JAPON",
                "CIUDAD" => "YOKOHAMA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JORDAN",
                "CIUDAD" => "AMAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "JORDAN",
                "CIUDAD" => "AQABA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "AKTAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "AKTOBE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "ALMATY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "ASTANA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "ATYRAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "KARAGANDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "KOKSHETAU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "KOSTANAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "ORAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "OSKEMEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "PAVLODAR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "PETROPAVLOSVSK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "SHYMKENT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "TALDYKORGAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "TARAZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KAZAJISTAN",
                "CIUDAD" => "ZHEZKAZGAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KENIA",
                "CIUDAD" => "MOMBASA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KENIA",
                "CIUDAD" => "NAIROBI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KIRGUISTAN",
                "CIUDAD" => "BISKEK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KIRIBATI",
                "CIUDAD" => "TARAWA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KOREA",
                "CIUDAD" => "BUSAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KOREA",
                "CIUDAD" => "INCHEON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KOREA",
                "CIUDAD" => "INCHON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KOREA",
                "CIUDAD" => "SEUL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "KUWAIT",
                "CIUDAD" => "KUWAIT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LETONIA",
                "CIUDAD" => "DAUGAVPILS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LETONIA",
                "CIUDAD" => "LIEPAJA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LETONIA",
                "CIUDAD" => "RIGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LETONIA",
                "CIUDAD" => "VENSPILS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBANO",
                "CIUDAD" => "BEIRUT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBERIA",
                "CIUDAD" => "MONROVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBIA",
                "CIUDAD" => "AL KHUMS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBIA",
                "CIUDAD" => "BENGASI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBIA",
                "CIUDAD" => "MISURATA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LIBIA",
                "CIUDAD" => "TRIPOLI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LITUANIA",
                "CIUDAD" => "KAUNAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LITUANIA",
                "CIUDAD" => "KLAIPEDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LITUANIA",
                "CIUDAD" => "PANEVEZYS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LITUANIA",
                "CIUDAD" => "SIAULIAI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LITUANIA",
                "CIUDAD" => "VILNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "LUXEMBURGO",
                "CIUDAD" => "LUXEMBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MADAGASCAR",
                "CIUDAD" => "TOAMASINA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "BINTULU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "KOTA KINABALU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "KUCHING",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "KWANGYANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "LABUAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "MIRI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "PASIR GUDANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "PENANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "PORT KLANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "SANDAKEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "SIBU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALASIA",
                "CIUDAD" => "TANJUNG PELEPAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALAUI",
                "CIUDAD" => "BLANTYRE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALAUI",
                "CIUDAD" => "LILONGUE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALDIVAS",
                "CIUDAD" => "MALE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALTA",
                "CIUDAD" => "LA VALETA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MALTA",
                "CIUDAD" => "MALTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MARRUECOS",
                "CIUDAD" => "AGADIR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MARRUECOS",
                "CIUDAD" => "CASABLANCA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MARRUECOS",
                "CIUDAD" => "TANGER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MARTINICA",
                "CIUDAD" => "FORT DE FRANCE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MAURICIO",
                "CIUDAD" => "PORT LOUIS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MAURITANIA",
                "CIUDAD" => "NUAKCHOT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "AGUAS CALIENTES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "ALTAMIRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "CANCUN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "GUADALAJARA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "LEON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "MANZANILLO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "MERIDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "MEXICO DF",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "MONTERREY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "PROGRESO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "PUEBLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "QUERETARO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "TAMPICO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MEXICO",
                "CIUDAD" => "VERACRUZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MONTSERRAT",
                "CIUDAD" => "MONTSERRAT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MOZAMBIQUE",
                "CIUDAD" => "BEIRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "MOZAMBIQUE",
                "CIUDAD" => "MAPUTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NEVIS",
                "CIUDAD" => "NEVIS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NICARAGUA",
                "CIUDAD" => "CORINTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NICARAGUA",
                "CIUDAD" => "MANAGUA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NICARAGUA",
                "CIUDAD" => "PUERTO LIMON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NIGERIA",
                "CIUDAD" => "LAGOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NIUE",
                "CIUDAD" => "NIUE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "BERGEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "OSLO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "SANDEFJORD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "STAVANGER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "TANANGER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NORUEGA",
                "CIUDAD" => "TRONDHEIM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA CALEDONIA",
                "CIUDAD" => "NUMEA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "AUCKLAND",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "CHRISTCHURCH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "LYTTELTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "LYTTLETON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "NAPIER",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "NELSON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "RAROTONGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "TAURANGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "NUEVA ZELANDA",
                "CIUDAD" => "WELLINGTON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "OMAN",
                "CIUDAD" => "MASCATE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "OMAN",
                "CIUDAD" => "SULTAN QABOOS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAISES BAJOS",
                "CIUDAD" => "SAN EUSTAQUIO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "FAISALABAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "ISLAMABAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "KARACHI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "LAHORE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "PORT QASIM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "RAWALPINDI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAKISTAN",
                "CIUDAD" => "SIALKOT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "BALBOA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "CIUDAD DE PANAMÁ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "COLON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "CRISTOBAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "MANZANILLO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PANAMA",
                "CIUDAD" => "RODMAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAPUA NUEVA GUINEA",
                "CIUDAD" => "LAE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PAPUA NUEVA GUINEA",
                "CIUDAD" => "PUERTO MORESBY",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PARAGUAY",
                "CIUDAD" => "ASUNCION",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PERU",
                "CIUDAD" => "CALLAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PERU",
                "CIUDAD" => "PAITA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "BRESLAVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "GDANSK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "GDYNIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "KATOWICE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "POZNAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "VARSOVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "POLONIA",
                "CIUDAD" => "WARZAWA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "FUNCHAL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "LEIXOES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "LISBOA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "OPORTO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "PONTA DELGADA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PORTUGAL",
                "CIUDAD" => "VILA DO CONDE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "PUERTO RICO",
                "CIUDAD" => "SAN JUAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REINO UNIDO",
                "CIUDAD" => "GIBRALTAR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA  CHECA",
                "CIUDAD" => "BRNO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA  CHECA",
                "CIUDAD" => "OSTRAVA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA  CHECA",
                "CIUDAD" => "PRAGA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA DEL CONGO",
                "CIUDAD" => "KINSASA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA DEL CONGO",
                "CIUDAD" => "POINTE NOIRE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA DOMINICANA",
                "CIUDAD" => "CAUCEDO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REPUBLICA DOMINICANA",
                "CIUDAD" => "RIO HAINA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "REUNION",
                "CIUDAD" => "POINTE DES GALETS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "RUANDA",
                "CIUDAD" => "KIGALI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "RUMANIA",
                "CIUDAD" => "BUCAREST",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "RUMANIA",
                "CIUDAD" => "CONSTANZA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "RUSIA",
                "CIUDAD" => "MOSCU",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "RUSIA",
                "CIUDAD" => "SAN PETERSBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SALVADOR",
                "CIUDAD" => "SAN SALVADOR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SAMOA",
                "CIUDAD" => "APIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SAN BARTOLOME",
                "CIUDAD" => "GUSTAVIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SAN BARTOLOME",
                "CIUDAD" => "SAN BARTOLOME",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SAN CRISTOBAL",
                "CIUDAD" => "SAINT KITTS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SANTA LUCIA",
                "CIUDAD" => "CASTRIES",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SENEGAL",
                "CIUDAD" => "DAKAR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SERBIA",
                "CIUDAD" => "BELGRADO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SERBIA",
                "CIUDAD" => "NOVI SAD",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SEYCHELLES",
                "CIUDAD" => "MAHE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SIERRA LEONA",
                "CIUDAD" => "FREETOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SINGAPUR",
                "CIUDAD" => "SINGAPUR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SINT MAARTEN",
                "CIUDAD" => "PHILIPSBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SIRIA",
                "CIUDAD" => "LATAKIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SIRIA",
                "CIUDAD" => "TARTUS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SRI LANKA",
                "CIUDAD" => "COLOMBO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ST. THOMAS",
                "CIUDAD" => "CHARLOTTE AMALIA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ST. VINCENT",
                "CIUDAD" => "KINGSTOWN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "CABO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "DURBAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "EAST LONDON",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "JOHANNESBURG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "JOHANNESBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAFRICA",
                "CIUDAD" => "PORT ELIZABETH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUDAN",
                "CIUDAD" => "PUERTO SUDAN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUECIA",
                "CIUDAD" => "ESTOCOLMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUECIA",
                "CIUDAD" => "GOTEMBURGO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUECIA",
                "CIUDAD" => "HELSINGBORG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUECIA",
                "CIUDAD" => "MALMO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUIZA",
                "CIUDAD" => "BASILEA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUIZA",
                "CIUDAD" => "BERNA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUIZA",
                "CIUDAD" => "GINEBRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUIZA",
                "CIUDAD" => "LAUSANA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SUIZA",
                "CIUDAD" => "ZURICH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "SURINAME",
                "CIUDAD" => "PARAMARIBO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAHITI",
                "CIUDAD" => "PAPEETE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAILANDIA",
                "CIUDAD" => "BANGKOK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAILANDIA",
                "CIUDAD" => "LAEM CHABANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAILANDIA",
                "CIUDAD" => "LAT KRABANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAIWAN",
                "CIUDAD" => "KAOHSIUNG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TAIWAN",
                "CIUDAD" => "KEELUNG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TANZANIA",
                "CIUDAD" => "DAR ES SALAAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TANZANIA",
                "CIUDAD" => "DAR ES SALAM",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TOGO",
                "CIUDAD" => "LOME",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TONGA",
                "CIUDAD" => "NUKUALOFA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TRINIDAD Y TOBAGO",
                "CIUDAD" => "POINT LISAS",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TRINIDAD Y TOBAGO",
                "CIUDAD" => "PUERTO ESPAÑA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TUNEZ",
                "CIUDAD" => "SFAX",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TUNEZ",
                "CIUDAD" => "TUNEZ",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TURQUIA",
                "CIUDAD" => "ESTAMBUL",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TURQUIA",
                "CIUDAD" => "GEBZE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TURQUIA",
                "CIUDAD" => "GEMLIK",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TURQUIA",
                "CIUDAD" => "IZMIR",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TURQUIA",
                "CIUDAD" => "MERSIN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "TUVALU",
                "CIUDAD" => "FUNAFUTI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "UGANDA",
                "CIUDAD" => "ENTEBBE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "UGANDA",
                "CIUDAD" => "KAMPALA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "URUGUAY",
                "CIUDAD" => "MONTEVIDEO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "UZBEKISTAN",
                "CIUDAD" => "TASKENT",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VANUATU",
                "CIUDAD" => "PORT VILA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "EL GUAMACHE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "GUANTA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "GUARANAO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "LA GUAIRA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "MARACAIBO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VENEZUELA",
                "CIUDAD" => "PUERTO CABELLO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VIETNAM",
                "CIUDAD" => "DA NANG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VIETNAM",
                "CIUDAD" => "HAI PHONG",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VIETNAM",
                "CIUDAD" => "HANOI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "VIETNAM",
                "CIUDAD" => "HO CHI MINH",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "YEMEN",
                "CIUDAD" => "ADEN",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "YEMEN",
                "CIUDAD" => "AL HUDAYDA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "YIBUTI",
                "CIUDAD" => "YIBUTI",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZAMBIA",
                "CIUDAD" => "KITWE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZAMBIA",
                "CIUDAD" => "LUSAKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZAMBIA",
                "CIUDAD" => "LUZAKA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZAMBIA",
                "CIUDAD" => "NDOLA",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZIMBABUE",
                "CIUDAD" => "BULAWAYO",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ],
            [
                "PAIS" => "ZIMBABUE",
                "CIUDAD" => "HARARE",
                "ESTADO" => "ACTIVA",
                "MAQUINA" => "::1",
                "USUARIO" => "SOPORTE",
                "FECHA_MOD" => now()
            ]
        ];
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateOptionTable() {
//        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
//        \App\Models\Customer\Plantilla::truncate();
//        \App\Models\Customer\TmTrayecto::truncate();
//        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
