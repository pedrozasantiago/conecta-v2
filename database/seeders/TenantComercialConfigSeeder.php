<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Str as Str;

class TenantComercialConfigSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //        $this->command->info('Truncating System table');
        ////        $this->truncateOptionTable();
        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->Actividades() as $key => $value) {
            \App\Models\Customer\SgcActividad::insertOrIgnore($value);
        }

        // insertar atributos
        $this->command->info('Insertar datos de Atributos');

        foreach ($this->Atributos() as $key => $value) {
            \App\Models\Customer\SgcAtributo::create($value);
        }

        // insertar impuestos
        $this->command->info('Insertar datos de Impuestos');

        foreach ($this->Impuestos() as $key => $value) {
            \App\Models\Customer\SgcImpuesto::create($value);
        }

        // insertar tipos de servicios
        $this->command->info('Insertar datos de tipos de servicios');

        foreach ($this->TipoServicios() as $key => $value) {
            \App\Models\Customer\SgcTipoServicio::create($value);
        }

        // insertar servicios
        $this->command->info('Insertar datos de servicios');

        foreach ($this->Servicios() as $key => $value) {
            \App\Models\Customer\SgcServicio::create($value);
        }

        foreach ($this->ServiciosVariaciones() as $key => $value) {
            \App\Models\Customer\SgcServiciosVariaciones::create($value);
        }


        // insertar grupos
        foreach ($this->GrupoServicios() as $key => $value) {
            \App\Models\Customer\SgcServicioGrupo::create($value);
        }


        // insertar TRM
        $this->command->info('Insertar datos de trm');

        foreach ($this->Trm() as $key => $value) {
            \App\Models\Customer\SgcTrm::create($value);
        }

        // insertar TRM
        $this->command->info('Insertar datos de Consecutivos');

        foreach ($this->Consecutivos() as $key => $value) {
            \App\Models\Customer\SggConsecutivo::create($value);
        }


        // insertar Tipos de cargos
        $this->command->info('Insertar datos de tipo de cargo');

        foreach ($this->TiposCargo() as $key => $value) {
            \App\Models\Customer\SgcTipoCargo::create($value);
        }

        // insertar cargos
        $this->command->info('Insertar datos de cargos');

        foreach ($this->Cargos() as $key => $value) {
            \App\Models\Customer\SgcCargo::create($value);
        }

        // insertar tarifarios
        $this->command->info('Insertar datos de tarifarios');

        foreach ($this->tipo_tarifario() as $key => $value) {
            \App\Models\Customer\SgcTipoTarifario::create($value);
        }

        foreach ($this->tipo_tarifario_adicionales() as $key => $value) {
            \App\Models\Customer\SgcTipoTarifarioAdicionales::create($value);
        }

        foreach ($this->tipo_tarifario_compras() as $key => $value) {
            \App\Models\Customer\SgcTipoTarifarioCompras::create($value);
        }

        foreach ($this->tipo_tarifario_servicios() as $key => $value) {
            \App\Models\Customer\SgcTipoTarifarioServicios::create($value);
        }
    }

    // arreglo de sgc_actividades
    public function Actividades()
    {
        return $jayParsedAry = [
            [
                "ACTIVIDAD_ID" => "LLAMACONT",
                "DESCRIPCION" => "Llamada de primer contacto",
                "SECUENCIA" => 1,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "OFRESER",
                "DESCRIPCION" => "Ofrecer servicio y/o Producto",
                "SECUENCIA" => 2,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "EMAIL",
                "DESCRIPCION" => "Envío de correo",
                "SECUENCIA" => 3,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "LLAMSEGPRES",
                "DESCRIPCION" => "Lllamada de seguimiento presentacion",
                "SECUENCIA" => 4,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "SMS",
                "DESCRIPCION" => "Envío de mensaje SMS",
                "SECUENCIA" => 5,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "LLAMACITA",
                "DESCRIPCION" => "Acordar visita",
                "SECUENCIA" => 6,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FCCITA",
                "DESCRIPCION" => "Fecha de Visita Acordada  ",
                "SECUENCIA" => 7,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FRCITA",
                "DESCRIPCION" => "Fecha de Visita realizada",
                "SECUENCIA" => 8,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FENTREGA",
                "DESCRIPCION" => "Fecha entrega pricing",
                "SECUENCIA" => 1,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "REQTARIFA",
                "DESCRIPCION" => "Fecha solicitud tarifas",
                "SECUENCIA" => 0,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FCENVCOT",
                "DESCRIPCION" => "Fecha de Compromiso de envio de cotización",
                "SECUENCIA" => 1,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FRENVCOT",
                "DESCRIPCION" => "Fecha real de envio Cotización",
                "SECUENCIA" => 2,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "LLAMACOT",
                "DESCRIPCION" => "Llamada seguimiento a cotizacion",
                "SECUENCIA" => 3,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "CIERRENE",
                "DESCRIPCION" => "Cierre negocio",
                "SECUENCIA" => 4,
                "TIPO" => "CAMPCCIAL",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "SEGCOT",
                "DESCRIPCION" => "Seguimiento a la cotizacion",
                "SECUENCIA" => 0,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FAPER",
                "DESCRIPCION" => "Fecha apertura1",
                "SECUENCIA" => 0,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FPRINT",
                "DESCRIPCION" => "Fecha impresion cotizacion",
                "SECUENCIA" => 3,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FSHIP",
                "DESCRIPCION" => "Fecha impresion shipping",
                "SECUENCIA" => 4,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FAPRO",
                "DESCRIPCION" => "Fecha aprobado",
                "SECUENCIA" => 5,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "FCIERRE",
                "DESCRIPCION" => "Fecha cierre",
                "SECUENCIA" => 6,
                "TIPO" => "COTIZA",
                "ACTIVO" => "S"
            ],


            [
                "ACTIVIDAD_ID" => "REQ-APER",
                "DESCRIPCION" => "Apertura requerimiento",
                "SECUENCIA" => 0,
                "TIPO" => "REQUERIMIENTO",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "REQ-DETAILS",
                "DESCRIPCION" => "Detalles adicionales",
                "SECUENCIA" => 1,
                "TIPO" => "REQUERIMIENTO",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "REQ-REJECT",
                "DESCRIPCION" => "Requerimiento rechazado",
                "SECUENCIA" => 3,
                "TIPO" => "REQUERIMIENTO",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "REQ-SOPORTES",
                "DESCRIPCION" => "Adjunto documentos",
                "SECUENCIA" => 2,
                "TIPO" => "REQUERIMIENTO",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "REQ-TOMADO",
                "DESCRIPCION" => "Requerimiento tomado",
                "SECUENCIA" => 4,
                "TIPO" => "REQUERIMIENTO",
                "ACTIVO" => "S"
            ],
            [
                "ACTIVIDAD_ID" => "PROVCONTACT",
                "DESCRIPCION" => "Primer contacto",
                "SECUENCIA" => 0,
                "TIPO" => "PROV",
                "ACTIVO" => "S"
            ],

        ];
    }

    // arreglo de sgc_atributos
    public function Atributos()
    {

        return [
            [
                "atributo" => "1101",
                "descrip" => "Circular 0170",
                "descrip_ingles" => null,
                "orden" => 1,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "S",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1102",
                "descrip" => "Visita",
                "descrip_ingles" => null,
                "orden" => 2,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "N",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1103",
                "descrip" => "Acuerdo comercial",
                "descrip_ingles" => null,
                "orden" => 2,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1104",
                "descrip" => "Camara de Comercio",
                "descrip_ingles" => null,
                "orden" => 4,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "N",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1105",
                "descrip" => "RUT",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "",
                "tipo" => "",
                "sistema" => null
            ],
            [
                "atributo" => "1106",
                "descrip" => "Ref comerciales",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1107",
                "descrip" => "CC",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1108",
                "descrip" => "Ref Bancarias",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1109",
                "descrip" => "Estados Financieros",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1110",
                "descrip" => "Certificacion",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1111",
                "descrip" => "Verificacion docs legales",
                "descrip_ingles" => null,
                "orden" => 6,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "N",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1112",
                "descrip" => "Lista Clinton",
                "descrip_ingles" => null,
                "orden" => 7,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "S",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1113",
                "descrip" => "Policia",
                "descrip_ingles" => null,
                "orden" => 7,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => "S",
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "1114",
                "descrip" => "Procuraduria",
                "descrip_ingles" => null,
                "orden" => 7,
                "grupo" => "DOCLEGALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => "FECHA",
                "sistema" => null
            ],
            [
                "atributo" => "8001",
                "descrip" => "Tipo de carga",
                "descrip_ingles" => "Tipo de carga",
                "orden" => 1,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "8002",
                "descrip" => "INCOTERM",
                "descrip_ingles" => "INCOTERM",
                "orden" => 3,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "8003",
                "descrip" => "Partida arancelaria",
                "descrip_ingles" => "Partida arancelaria",
                "orden" => 4,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "8004",
                "descrip" => "disponible",
                "descrip_ingles" => "disponible",
                "orden" => 5,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "INA",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "8005",
                "descrip" => "disponible",
                "descrip_ingles" => "disponible",
                "orden" => 6,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "INA",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "8006",
                "descrip" => "disponible",
                "descrip_ingles" => "disponible",
                "orden" => 7,
                "grupo" => "INFOCOT",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "INA",
                "tipo_campo" => null,
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "1001",
                "descrip" => "Direccion",
                "descrip_ingles" => "Dir",
                "orden" => 1,
                "grupo" => "INFOGRALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "1002",
                "descrip" => "Telefono",
                "descrip_ingles" => "Ph",
                "orden" => 2,
                "grupo" => "INFOGRALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "1003",
                "descrip" => "Ciudad",
                "descrip_ingles" => "City",
                "orden" => 9,
                "grupo" => "INFOGRALC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "1012",
                "descrip" => "SHIPPER",
                "descrip_ingles" => null,
                "orden" => 12,
                "grupo" => "INFOGRALC",
                "lista" => "SINO",
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "select",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "1013",
                "descrip" => "CONSIGNEE",
                "descrip_ingles" => null,
                "orden" => 13,
                "grupo" => "INFOGRALC",
                "lista" => "SINO",
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "select",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "1014",
                "descrip" => "NOTIFY",
                "descrip_ingles" => null,
                "orden" => 14,
                "grupo" => "INFOGRALC",
                "lista" => "SINO",
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "select",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => "SI"
            ],
            [
                "atributo" => "9001",
                "descrip" => "Orden / Factura",
                "descrip_ingles" => null,
                "orden" => 1,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9002",
                "descrip" => "INCOTERM",
                "descrip_ingles" => null,
                "orden" => 2,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => "MONEDA_LST",
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9003",
                "descrip" => "O/F Payable",
                "descrip_ingles" => null,
                "orden" => 5,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9004",
                "descrip" => "BL emisión original",
                "descrip_ingles" => null,
                "orden" => 6,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9005",
                "descrip" => "Información de la compañia",
                "descrip_ingles" => null,
                "orden" => 7,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9006",
                "descrip" => "Información de la compañia de transporte",
                "descrip_ingles" => null,
                "orden" => 8,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9007",
                "descrip" => "Entrega contenedor vació y fecha",
                "descrip_ingles" => null,
                "orden" => 9,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9008",
                "descrip" => "Calendario y buque",
                "descrip_ingles" => null,
                "orden" => 10,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo_campo" => "input",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9009",
                "descrip" => "Descripcion de la mercancía",
                "descrip_ingles" => null,
                "orden" => 11,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => "textarea",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ],
            [
                "atributo" => "9010",
                "descrip" => "Instrucciones especiales",
                "descrip_ingles" => null,
                "orden" => 12,
                "grupo" => "SHIPPING_INSTRUCTION",
                "lista" => null,
                "orden_grp" => "",
                "estado" => "ACT",
                "tipo_campo" => "textarea",
                "obligatorio" => null,
                "tipo" => null,
                "sistema" => null
            ]
        ];
    }

    // arreglo de sgc_impuestos
    public function Impuestos()
    {

        return [
            [
                'impuesto' => 0,
                'activo' => 'SI',
            ],
            [
                'impuesto' => 19,
                'activo' => 'SI',
            ],
        ];
    }

    public function TipoServicios()
    {
        return
            [
                [
                    "id" => 1,
                    "descripcion" => "Agenciamiento aduanero",
                    "descripcion_ingles" => "Custom",
                    "activo" => "SI",
                    "orden" => 1
                ],
                [
                    "id" => 2,
                    "descripcion" => "Gastos en destino",
                    "descripcion_ingles" => "Destiny Charges",
                    "activo" => "SI",
                    "orden" => 2
                ],
                [
                    "id" => 3,
                    "descripcion" => "Flete internacional",
                    "descripcion_ingles" => "International Freight",
                    "activo" => "SI",
                    "orden" => 3
                ],
                [
                    "id" => 4,
                    "descripcion" => "Gastos en origen",
                    "descripcion_ingles" => "Origin Charges",
                    "activo" => "SI",
                    "orden" => 4
                ],
                [
                    "id" => 5,
                    "descripcion" => "Seguro",
                    "descripcion_ingles" => "Insurance",
                    "activo" => "SI",
                    "orden" => 5
                ],
                [
                    "id" => 6,
                    "descripcion" => "Transporte Terrestre",
                    "descripcion_ingles" => "Ground Transport",
                    "activo" => "SI",
                    "orden" => 6
                ],
                [
                    "id" => 7,
                    "descripcion" => "Otros servicios",
                    "descripcion_ingles" => "Other services",
                    "activo" => "SI",
                    "orden" => 7
                ],
                [
                    "id" => 8,
                    "descripcion" => "Servicios adicionales",
                    "descripcion_ingles" => "Aditional Services",
                    "activo" => "SI",
                    "orden" => 8
                ],
                [
                    "id" => 9,
                    "descripcion" => "Asesoría Aduanera",
                    "descripcion_ingles" => "Customer advisory",
                    "activo" => "SI",
                    "orden" => 9
                ]
            ];
    }

    public function Servicios()
    {
        return [
            [
                "descripcion" => "Flete Internacional Aereo ",
                "descripcion_ingles" => "International Air Freight ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 3,
                "compra" => "1",
                "moneda_compra" => "USD",
                "notas" => "Tarifa Aplica para K/Vol Aereo ",
                "notas_internas" => "Tarifa Aplica para K/Vol Aereo . MUL Multiplicar peso por  la venta ",
                "creado_por" => "SOPORTE",
                "orden" => 1
            ],
            [
                "descripcion" => "Recargos Naviera",
                "descripcion_ingles" => "Shipping Line Charge",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 3,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "Tarifa aplica por Cotización",
                "notas_internas" => "Se suman todos los recargos Adicionales ",
                "creado_por" => "SOPORTE",
                "orden" => 2
            ],
            [
                "descripcion" => "Transporte terrestre",
                "descripcion_ingles" => "Transporte terrestre",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "Vehiculo Tipo patineta ",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 31
            ],
            [
                "descripcion" => "ITR (Desconsolidacion)",
                "descripcion_ingles" => "ITR (Desconsolidacion)",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 32
            ],
            [
                "descripcion" => "Agenciamiento aduanero",
                "descripcion_ingles" => "Custom Agent ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "0,28% Sobre el Valor CIF ",
                "creado_por" => "SOPORTE",
                "orden" => 19
            ],
            [
                "descripcion" => "Elaboración DIM ",
                "descripcion_ingles" => "Elaboración DIM ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 20
            ],
            [
                "descripcion" => "Devolucion de Contenedor ",
                "descripcion_ingles" => "Devolucion de Contenedor ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 33
            ],
            [
                "descripcion" => "Elaboración DAV ",
                "descripcion_ingles" => "Elaboración DAV",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 21
            ],
            [
                "descripcion" => "Elaboración Registro Importación ",
                "descripcion_ingles" => "Elaboración Registro Importación ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "Por Cada Declaración ",
                "creado_por" => "SOPORTE",
                "orden" => 22
            ],
            [
                "descripcion" => "Gastos Agrupados ",
                "descripcion_ingles" => "Gastos Agrupados ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 23
            ],
            [
                "descripcion" => "Reconocimiento de Mercancías ",
                "descripcion_ingles" => "Reconocimiento de Mercancías",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 24
            ],
            [
                "descripcion" => "Descargue Directo ",
                "descripcion_ingles" => "Descargue Directo ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 25
            ],
            [
                "descripcion" => "Almacenaje Mercancías ",
                "descripcion_ingles" => "Almacenaje Mercancías ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 26
            ],
            [
                "descripcion" => "Pick Up ",
                "descripcion_ingles" => "Pick Up ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 4
            ],
            [
                "descripcion" => "Aduana en Origen ",
                "descripcion_ingles" => "Custom ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 5
            ],
            [
                "descripcion" => "Documentación (BL)",
                "descripcion_ingles" => "Document Fee ",
                "activo" => "SI",
                "venta" => "80",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "50",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 6
            ],
            [
                "descripcion" => "Transfer Aerolinea",
                "descripcion_ingles" => "Air Transfer",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 7
            ],
            [
                "descripcion" => "Gastos FOB ",
                "descripcion_ingles" => "FOB Charges ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 8
            ],
            [
                "descripcion" => "Traslado a Deposito y/o Descargue Directo ",
                "descripcion_ingles" => "Traslado a Deposito y/o Descargue Directo ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 11
            ],
            [
                "descripcion" => "Desconsolidacion ",
                "descripcion_ingles" => "Desconsolidacion",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 12
            ],
            [
                "descripcion" => "Collect Fee ",
                "descripcion_ingles" => "Collect Fee ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 13
            ],
            [
                "descripcion" => "Radicacion ",
                "descripcion_ingles" => "Radicacion ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 14
            ],
            [
                "descripcion" => "Liberación ",
                "descripcion_ingles" => "Liberación",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 15
            ],
            [
                "descripcion" => "Consolidacion Maritima",
                "descripcion_ingles" => "Consolidation Fee",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 9
            ],
            [
                "descripcion" => "Recargo IMO ",
                "descripcion_ingles" => "IMO Charges ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 10
            ],
            [
                "descripcion" => "Gastos Naviera",
                "descripcion_ingles" => "Gastos Naviera",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 16
            ],
            [
                "descripcion" => "Uso de Instalaciones Portuarias ",
                "descripcion_ingles" => "Uso de Instalaciones Portuarias",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 17
            ],
            [
                "descripcion" => "Emisión Bl   y/o Corrección",
                "descripcion_ingles" => "Corrección y/o Emisión de Bl",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "Si Aplica ",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 18
            ],
            [
                "descripcion" => "Seguro",
                "descripcion_ingles" => "Insurance",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 5,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 37
            ],
            [
                "descripcion" => "Cargue y/o Descargue ",
                "descripcion_ingles" => "Cargue y/o Descargue ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 34
            ],
            [
                "descripcion" => "Escolta y/o Acompañamiento",
                "descripcion_ingles" => "Escolta y/o Acompañamiento",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 35
            ],
            [
                "descripcion" => "Stand By ",
                "descripcion_ingles" => "Stand By ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 6,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 36
            ],
            [
                "descripcion" => "Bodegaje Tercero ",
                "descripcion_ingles" => "Bodegaje Tercero ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 8,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 37
            ],
            [
                "descripcion" => "Tributos aduaneros",
                "descripcion_ingles" => "TAX ID",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "",
                "tipo_servicio" => 1,
                "compra" => "1",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Due Agent",
                "descripcion_ingles" => "Due Agent",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Due Carrier",
                "descripcion_ingles" => "Due Carrier",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Embalaje",
                "descripcion_ingles" => "Packaging",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Collect Fee",
                "descripcion_ingles" => "Collect Fee",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Manejo",
                "descripcion_ingles" => "Handling",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "19",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Impuestos",
                "descripcion_ingles" => "TAXES",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Aduana Destino",
                "descripcion_ingles" => "Custom",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Aforo",
                "descripcion_ingles" => "Customer advisory",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Posicionamiento Unidad ",
                "descripcion_ingles" => "Posicionamiento Unidad ",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Elaboracion DEX",
                "descripcion_ingles" => "Elaboracion DEX",
                "activo" => "SI",
                "venta" => "1",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "1",
                "moneda_compra" => "",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Minima Impo / Exp",
                "descripcion_ingles" => "Minima Impo / Exp",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Inspección Visto Bueno",
                "descripcion_ingles" => "Inspección Visto Bueno",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Liberación ",
                "descripcion_ingles" => "Liberación ",
                "activo" => "SI",
                "venta" => "56000",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "1",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Certificación Seguro",
                "descripcion_ingles" => "Certificación Seguro",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "1",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Flete Internacional Marítimo",
                "descripcion_ingles" => "International Sea Freight",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 3,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Low Sulphur Surcharge",
                "descripcion_ingles" => "Low Sulphur Surcharge",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Due Carrier Collect",
                "descripcion_ingles" => "Due Carrier Collect",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "Verificar con la Aerolinea",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Due Agent Collect",
                "descripcion_ingles" => "Due Agent Collect",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "Verificar con la Aerolinea",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Collect Fee",
                "descripcion_ingles" => "Collect Fee",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Manejo (Registro Aduana)",
                "descripcion_ingles" => "Handling (Custom Check)",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "LIberacion HAWB",
                "descripcion_ingles" => "Release HAWB",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Corte AWB",
                "descripcion_ingles" => "Cut AWB",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "AIT",
                "descripcion_ingles" => "AIT",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Bodegaje",
                "descripcion_ingles" => "Warehouse",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "",
                "tipo_servicio" => 2,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Cargue / Descargue",
                "descripcion_ingles" => "Cargue / Descargue",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Arriendo Reefer",
                "descripcion_ingles" => "Arriendo Reefer",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Conexion Electrica Reefer",
                "descripcion_ingles" => "Conexion Electrica Reefer",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Llenado Contenedor",
                "descripcion_ingles" => "LLenado Contenedor",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Movilización no Instructiva",
                "descripcion_ingles" => "Movilización no Instructiva",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Personal Llenado",
                "descripcion_ingles" => "Personal Llenado",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Formularios",
                "descripcion_ingles" => "Formularios",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "0",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Coordinación Logistica Portuaria",
                "descripcion_ingles" => "Coordinación Logistica Portuaria",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "COP",
                "impuesto" => "19",
                "tipo_servicio" => 1,
                "compra" => "0",
                "moneda_compra" => "COP",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Nautical Accessibility CH",
                "descripcion_ingles" => "Nautical Accessibility CH",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ],
            [
                "descripcion" => "Vessel Fuel Surcharge",
                "descripcion_ingles" => "Vessel Fuel Surcharge",
                "activo" => "SI",
                "venta" => "0",
                "moneda_venta" => "USD",
                "impuesto" => "0",
                "tipo_servicio" => 4,
                "compra" => "0",
                "moneda_compra" => "USD",
                "notas" => "",
                "notas_internas" => "",
                "creado_por" => "SOPORTE",
                "orden" => 0
            ]
        ];
    }

    public function ServiciosVariaciones()
    {
        return  [
            [
                "id" => 1,
                "id_servicio" => 1,
                "nombre" => "Min",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:01:23",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:01:23",
                "orden" => 1
            ],
            [
                "id" => 2,
                "id_servicio" => 1,
                "nombre" => "Menos 50kg",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:02:01",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:02:01",
                "orden" => 2
            ],
            [
                "id" => 3,
                "id_servicio" => 1,
                "nombre" => "Mas 50Kg",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:02:09",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:02:09",
                "orden" => 3
            ],
            [
                "id" => 4,
                "id_servicio" => 1,
                "nombre" => "Mas 100Kg",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:02:18",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:02:18",
                "orden" => 4
            ],
            [
                "id" => 5,
                "id_servicio" => 1,
                "nombre" => "Mas 150Kg",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:02:40",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:02:40",
                "orden" => 5
            ],
            [
                "id" => 6,
                "id_servicio" => 49,
                "nombre" => "20 STD",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:13:03",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:13:03",
                "orden" => 1
            ],
            [
                "id" => 7,
                "id_servicio" => 49,
                "nombre" => "40 STD",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:13:16",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:13:16",
                "orden" => 2
            ],
            [
                "id" => 8,
                "id_servicio" => 49,
                "nombre" => "40 HC",
                "modificado_por" => null,
                "fecha_mod" => "2024-09-20 07:13:33",
                "creado_por" => null,
                "fecha_creacion" => "2024-09-20 07:13:33",
                "orden" => 3
            ]
        ];
    }

    public function GrupoServicios()
    {

        return  [
            [
                "id_grupo" => 1,
                "id_servicio" => 5
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 6
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 8
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 9
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 10
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 11
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 12
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 13
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 34
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 42
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 44
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 45
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 46
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 47
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 48
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 59
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 60
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 61
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 62
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 63
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 64
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 65
            ],
            [
                "id_grupo" => 1,
                "id_servicio" => 66
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 19
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 20
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 21
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 22
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 23
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 26
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 27
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 28
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 40
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 41
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 51
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 52
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 53
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 54
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 55
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 57
            ],
            [
                "id_grupo" => 2,
                "id_servicio" => 58
            ],
            [
                "id_grupo" => 3,
                "id_servicio" => 1
            ],
            [
                "id_grupo" => 3,
                "id_servicio" => 2
            ],
            [
                "id_grupo" => 3,
                "id_servicio" => 49
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 14
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 15
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 16
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 17
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 18
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 24
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 25
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 35
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 36
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 37
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 38
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 39
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 43
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 50
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 56
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 67
            ],
            [
                "id_grupo" => 4,
                "id_servicio" => 68
            ],
            [
                "id_grupo" => 5,
                "id_servicio" => 29
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 3
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 4
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 7
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 30
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 31
            ],
            [
                "id_grupo" => 6,
                "id_servicio" => 32
            ],
            [
                "id_grupo" => 8,
                "id_servicio" => 33
            ]
        ];
    }

    // arreglo de sgc_tipos_cargos
    public function TiposCargo()
    {

        return [
            [
                'id' => 1,
                'nombre' => 'Cargos adicionales',
                'tipo' => 'DET',
                'estado' => 'ACT',
                'orden' => 1,
                'operacion' => 'SUM',
                'base' => 'COM'
            ]
        ];
    }

    // arreglo de sgc_cargos
    public function Cargos()
    {
        return [
            [
                'id' => 1,
                'id_tipo' => 1,
                'nombre' => 'GMF 4x1000',
                'valor' => '0.4',
                'estado' => 'ACT',
                'orden' => '1'
            ]
        ];
    }

    public function Trm()
    {
        return [
            [
                'moneda' => 'COP',
                'valor' => 1,
                'estado' => 'ACTIVO',
                'usuario' => 'SOPORTE',
            ],
            [
                'moneda' => 'USD',
                'valor' => 4100,
                'estado' => 'ACTIVO',
                'usuario' => 'SOPORTE',
            ],
            [
                'moneda' => 'EUR',
                'valor' => 4500,
                'estado' => 'ACTIVO',
                'usuario' => 'SOPORTE',
            ],
        ];
    }

    public function Consecutivos()
    {
        return [
            [
                'TIPO' => 'CE',
                'SECUENCIA' => 1,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'CLIENTE_ID',
                'SECUENCIA' => 100000001,
                'PRE' => '',
            ],
            [
                'TIPO' => 'COTIZA',
                'SECUENCIA' => 0,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'FV',
                'SECUENCIA' => 1,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'NC',
                'SECUENCIA' => 1,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'ND',
                'SECUENCIA' => 1,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'PROVEEDOR_ID',
                'SECUENCIA' => 300000000,
                'PRE' => '',
            ],
            [
                'TIPO' => 'RC',
                'SECUENCIA' => 1,
                'PRE' => config('tenancy.prefijo'),
            ],
            [
                'TIPO' => 'CONTABILIDAD',
                'SECUENCIA' => 1,
                'PRE' => '',
            ],
            [
                'TIPO' => 'DRAFT_FV',
                'SECUENCIA' => 1,
                'PRE' => 'PREFACTURA',
            ],
        ];
    }


    public function tipo_tarifario()
    {

        return [
            [
                "id" => 1,
                "nombre" => "IMPO MARITIME FCL",
                "subtipo" => "MI",
                "servicio" => "13",
                "estado" => "SI"
            ],
            [
                "id" => 2,
                "nombre" => "IMPO MARITIME LCL",
                "subtipo" => "MI",
                "servicio" => "12",
                "estado" => "SI"
            ],
            [
                "id" => 3,
                "nombre" => "IMPO AIR",
                "subtipo" => "AI",
                "servicio" => "11",
                "estado" => "SI"
            ],
            [
                "id" => 4,
                "nombre" => "INLAND",
                "subtipo" => "TI",
                "servicio" => "17",
                "estado" => "SI"
            ],
            [
                "id" => 5,
                "nombre" => "EXPO AIR",
                "subtipo" => "AE",
                "servicio" => "21",
                "estado" => "SI"
            ],
            [
                "id" => 6,
                "nombre" => "EXPO MARITIME FCL",
                "subtipo" => "ME",
                "servicio" => "23",
                "estado" => "SI"
            ],
            [
                "id" => 7,
                "nombre" => "EXPO MARITIME LCL",
                "subtipo" => "ME",
                "servicio" => "22",
                "estado" => "SI"
            ]
        ];
    }

    public function tipo_tarifario_adicionales()
    {

        return [
            [
                "id" => 1,
                "id_tarifario" => "3",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:05:35",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:05:35"
            ],
            [
                "id" => 2,
                "id_tarifario" => "3",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:05:51",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:05:51"
            ],
            [
                "id" => 3,
                "id_tarifario" => "3",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:06:03",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:06:03"
            ],
            [
                "id" => 4,
                "id_tarifario" => "3",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:06:16",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:06:16"
            ],
            [
                "id" => 5,
                "id_tarifario" => "5",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:08:48",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:08:48"
            ],
            [
                "id" => 6,
                "id_tarifario" => "5",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:09:02",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:09:02"
            ],
            [
                "id" => 7,
                "id_tarifario" => "5",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:09:11",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:09:11"
            ],
            [
                "id" => 8,
                "id_tarifario" => "5",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:09:19",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:09:19"
            ],
            [
                "id" => 9,
                "id_tarifario" => "7",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:14:27",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:14:27"
            ],
            [
                "id" => 10,
                "id_tarifario" => "7",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:14:37",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:14:37"
            ],
            [
                "id" => 11,
                "id_tarifario" => "7",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:15:00",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:15:00"
            ],
            [
                "id" => 12,
                "id_tarifario" => "7",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:15:13",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:15:13"
            ],
            [
                "id" => 13,
                "id_tarifario" => "6",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:17:10",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:17:10"
            ],
            [
                "id" => 14,
                "id_tarifario" => "6",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:17:18",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:17:18"
            ],
            [
                "id" => 15,
                "id_tarifario" => "6",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:17:24",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:17:24"
            ],
            [
                "id" => 16,
                "id_tarifario" => "6",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:17:30",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:17:30"
            ],
            [
                "id" => 17,
                "id_tarifario" => "1",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:18:11",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:18:11"
            ],
            [
                "id" => 18,
                "id_tarifario" => "1",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:18:17",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:18:17"
            ],
            [
                "id" => 19,
                "id_tarifario" => "1",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:18:23",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:18:23"
            ],
            [
                "id" => 20,
                "id_tarifario" => "1",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:18:31",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:18:31"
            ],
            [
                "id" => 21,
                "id_tarifario" => "2",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:11",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:11"
            ],
            [
                "id" => 22,
                "id_tarifario" => "2",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:17",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:17"
            ],
            [
                "id" => 23,
                "id_tarifario" => "2",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:23",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:23"
            ],
            [
                "id" => 24,
                "id_tarifario" => "2",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:30",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:30"
            ],
            [
                "id" => 25,
                "id_tarifario" => "4",
                "nombre" => "Moneda",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:39",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:39"
            ],
            [
                "id" => 26,
                "id_tarifario" => "4",
                "nombre" => "T.T.",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:45",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:45"
            ],
            [
                "id" => 27,
                "id_tarifario" => "4",
                "nombre" => "Frecuencia",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:51",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:51"
            ],
            [
                "id" => 28,
                "id_tarifario" => "4",
                "nombre" => "Observaciones",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:21:57",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:21:57"
            ]
        ];
    }

    public function tipo_tarifario_compras()
    {

        return [
            [
                "id" => 1,
                "id_tarifario" => "5",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:04",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:04"
            ],
            [
                "id" => 2,
                "id_tarifario" => "6",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:17",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:17"
            ],
            [
                "id" => 3,
                "id_tarifario" => "7",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:22",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:22"
            ],
            [
                "id" => 4,
                "id_tarifario" => "3",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:27",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:27"
            ],
            [
                "id" => 5,
                "id_tarifario" => "1",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:31",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:31"
            ],
            [
                "id" => 6,
                "id_tarifario" => "2",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:35",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:35"
            ],
            [
                "id" => 7,
                "id_tarifario" => "4",
                "nombre" => "Contract",
                "estado" => "SI",
                "tipo" => "text",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 06:58:39",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 06:58:39"
            ]
        ];
    }

    public function tipo_tarifario_servicios()
    {
        return [
            [
                "id" => 1,
                "id_tarifario" => "3",
                "id_servicio" => "1",
                "id_variacion" => "1",
                "estado" => "SI",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:01:39",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:01:39"
            ],
            [
                "id" => 2,
                "id_tarifario" => "3",
                "id_servicio" => "1",
                "id_variacion" => "2",
                "estado" => "SI",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:03:10",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:03:10"
            ],
            [
                "id" => 3,
                "id_tarifario" => "3",
                "id_servicio" => "1",
                "id_variacion" => "3",
                "estado" => "SI",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:03:26",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:03:26"
            ],
            [
                "id" => 4,
                "id_tarifario" => "3",
                "id_servicio" => "1",
                "id_variacion" => "4",
                "estado" => "SI",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:03:37",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:03:37"
            ],
            [
                "id" => 5,
                "id_tarifario" => "3",
                "id_servicio" => "1",
                "id_variacion" => "5",
                "estado" => "SI",
                "orden" => "5",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:03:48",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:03:48"
            ],
            [
                "id" => 6,
                "id_tarifario" => "5",
                "id_servicio" => "1",
                "id_variacion" => "1",
                "estado" => "SI",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:07:12",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:07:12"
            ],
            [
                "id" => 7,
                "id_tarifario" => "5",
                "id_servicio" => "1",
                "id_variacion" => "2",
                "estado" => "SI",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:07:19",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:07:19"
            ],
            [
                "id" => 8,
                "id_tarifario" => "5",
                "id_servicio" => "1",
                "id_variacion" => "3",
                "estado" => "SI",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:07:28",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:07:28"
            ],
            [
                "id" => 9,
                "id_tarifario" => "5",
                "id_servicio" => "1",
                "id_variacion" => "4",
                "estado" => "SI",
                "orden" => "4",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:07:48",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:07:48"
            ],
            [
                "id" => 10,
                "id_tarifario" => "5",
                "id_servicio" => "1",
                "id_variacion" => "5",
                "estado" => "SI",
                "orden" => "5",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:07:59",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:07:59"
            ],
            [
                "id" => 11,
                "id_tarifario" => "1",
                "id_servicio" => "49",
                "id_variacion" => "6",
                "estado" => "SI",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:13:49",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:13:49"
            ],
            [
                "id" => 12,
                "id_tarifario" => "1",
                "id_servicio" => "49",
                "id_variacion" => "7",
                "estado" => "SI",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:13:57",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:13:57"
            ],
            [
                "id" => 13,
                "id_tarifario" => "1",
                "id_servicio" => "49",
                "id_variacion" => "8",
                "estado" => "SI",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:14:06",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:14:06"
            ],
            [
                "id" => 14,
                "id_tarifario" => "6",
                "id_servicio" => "49",
                "id_variacion" => "6",
                "estado" => "SI",
                "orden" => "1",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:15:57",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:15:57"
            ],
            [
                "id" => 15,
                "id_tarifario" => "6",
                "id_servicio" => "49",
                "id_variacion" => "7",
                "estado" => "SI",
                "orden" => "2",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:16:04",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:16:04"
            ],
            [
                "id" => 16,
                "id_tarifario" => "6",
                "id_servicio" => "49",
                "id_variacion" => "8",
                "estado" => "SI",
                "orden" => "3",
                "modificado_por" => "",
                "fecha_mod" => "2024-09-20 07:16:15",
                "creado_por" => "",
                "fecha_creacion" => "2024-09-20 07:16:15"
            ]
        ];
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateOptionTable()
    {
        //        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //        \App\Models\Customer\SgcActividad::truncate();
        //        \App\Models\Customer\SgcAtributo::truncate();
        //        \App\Models\Customer\SgcImpuesto::truncate();
        //        \App\Models\Customer\SgcTipoServicio::truncate();
        //        \App\Models\Customer\SgcServicio::truncate();
        //        \App\Models\Customer\SgcTrm::truncate();
        //        \App\Models\Customer\SggConsecutivo::truncate();
        //        \App\Models\Customer\SgcTipoCargo::truncate();
        //        \App\Models\Customer\SgcCargo::truncate();
        //        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
