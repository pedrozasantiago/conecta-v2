<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TenantDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(TenantPermissionsSeeder::class);
        $this->call(TenantLaratrustSeeder::class);
        $this->call(TenantAclSeeder::class);
        $this->call(TenantBaseConfigSeeder::class);
        $this->call(TenantComercialConfigSeeder::class);
        $this->call(TenantOperativoConfigSeeder::class);
        
    }
}
