<?php

namespace Database\Seeders;

use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TenantLaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Role and Permission tables');
        $this->truncateLaratrustTables();
        
        $database = config('tenancy.uuid');
        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));        

        // configs
        config(['laratrust.tables.permission_role' => $database.'.permission_role']);
        config(['laratrust.tables.permission_user' => $database.'.permission_user']);
        config(['laratrust.tables.role_user' => $database.'.role_user']);

        // crear permisos secundario
        foreach ($config as $key => $modules) {
            
            // Create a new role
            $role = \App\Models\Customer\Role::create([
                'name' => $key,
                'display_name' => ucwords(str_replace("_", " ", $key)),
                'description' => ucwords(str_replace("_", " ", $key))
            ]);

            $this->command->info('Creating Role '. strtoupper($key));

            // Reading role permission modules
            $permissions = [];
            foreach ($modules as $module => $value) {

                foreach (explode(',', $value) as $p => $perm) {

                    $permissionValue = $mapPermission->get($perm);

                    $modulename = str_replace('-', ' ', $module);
                    $display_name = ucfirst($permissionValue) . ' ' . ucwords($modulename);

                    $add_permission = \App\Models\System\Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => $display_name,
                        'description' => $display_name,
                    ])->id;

                    array_push($permissions, $add_permission);                    

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                }                     
            }            
            
            $permissions = array_unique($permissions);

            // Attach all permissions to the role
            $role->permissions()->sync($permissions);

            // Reading role permission modules
            /*foreach ($modules as $module => $value) {
                
                $permissions = explode(',', $value);

                // verificar si el permiso principal existe
                $checkperm = $permission = \App\Models\System\Permission::where('name', $module)->first();

                foreach ($permissions as $p => $perm) {
                    
                    $permissionValue = $mapPermission->get($perm);
                    $perm_name = $permissionValue . '-' . $module;

                    $permission = \App\Models\System\Permission::where('name', $perm_name)->first();

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);

                    //$role->syncPermissions($permission);
                    
                    if (!$role->hasPermission($permission->name)) {
                        $role->attachPermission($permission);
                    } else {
                        $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
                    }
                }
            }*/

    
        }

    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\Models\Customer\Role::truncate();
        Schema::enableForeignKeyConstraints();
        //DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
