<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Str as Str;

class TenantOperativoConfigSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->command->info('Truncating System table');
        $this->truncateOptionTable();


        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->Listas() as $key => $value) {
            \App\Models\Customer\TmLista::create($value);
        }

        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->Consecutivo() as $key => $value) {
            \App\Models\Customer\TmConsecutivo::create($value);
        }

        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->AtributosAduana() as $key => $value) {
            \App\Models\Customer\TmAtributosDocaduana::create($value);
        }

        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->Atributos() as $key => $value) {
            \App\Models\Customer\TmAtributo::create($value);
        }

        foreach ($this->AsociarAtributos() as $key => $value) {
            \App\Models\Customer\TmAsociarCodigo::create($value);
        }


        // insertar actividades
        $this->command->info('Insertar datos de Actividades');

        foreach ($this->Actividades() as $key => $value) {
            \App\Models\Customer\TmActividadesOper::create($value);
        }

        // insertar TRM
        $this->command->info('Insertar datos de proveedores demo');

            \App\Models\Customer\SgcClientes::create(
                [
                    'TIPO_ID' => 'OTRO',
                    'CEDULA' => '100000000',
                    'id_externo' => config('tenancy.nit'),
                    'NOMBRE' => config('tenancy.name_company'),
                    'TIPO_CLIENTE' => 'RATIFICADO',
                    'asesor_id' => '123456789',
                    'ESTADO' => 'ACTIVO',
                    'ESTADO_INFO' => 'PENDIENTE',
                    'CATEGORIA' => 'A',
                    'ORIGEN' => '',
                    'FECHA_VERIFICACION' => 'PENDIENTE',
                    'OBSERVACIONES' => 'Creado automaticamente.',
                    'USUARIO' => 'SOPORTE',
                    'MAQUINA' => '::1',
                    'FECHA_MOD' => now(),
                    'fecha_creacion' => now(),
                ]
            );

        foreach ($this->Proveedores() as $key => $value) {
            \App\Models\Customer\TmProveedores::create($value);
        }
    }

    public function AsociarAtributos()
    {
        return [
            [
                "tipo" => "INFORDEN",
                "codigo" => "4677",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4678",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4677",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4678",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4602",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4604",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4605",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4611",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4631",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4632",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4633",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4642",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4644",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4653",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4664",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4673",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4674",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "WE",
                "grupo" => "WE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "WI",
                "grupo" => "WI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "ME",
                "grupo" => "ME",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "MI",
                "grupo" => "MI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "CUSE",
                "grupo" => "CUSE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "CUSI",
                "grupo" => "CUSI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "TE",
                "grupo" => "TE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4675",
                "codasoc" => "TI",
                "grupo" => "TI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5000",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5000",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5001",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5001",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5002",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5002",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5003",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5003",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5004",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5004",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5005",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5005",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5006",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5006",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5007",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5007",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5008",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5008",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5009",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5009",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5010",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5010",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5011",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5011",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5012",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5012",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5013",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5013",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5014",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5014",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5015",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5015",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5016",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5016",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5017",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5017",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 0,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5018",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 1,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5018",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 1,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5019",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 2,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5019",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 2,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5020",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 3,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5020",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 3,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5021",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 4,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5021",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 4,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5022",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 5,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5022",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 5,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5023",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 6,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5023",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 6,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5024",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 7,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5024",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 7,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5025",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 8,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5025",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 8,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5026",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "5026",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 9,
                "estado" => "ACT"
            ],


            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "AI",
                "grupo" => "AI",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4665",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4667",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                "tipo" => "INFORDEN",
                "codigo" => "4672",
                "codasoc" => "AE",
                "grupo" => "AE",
                "orden" => 9,
                "estado" => "ACT"
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6001',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6001',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6002',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6002',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6003',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6003',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6004',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6004',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6005',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6005',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6006',
                'codasoc' => 'CR',
                'grupo' => 'CR',
                'orden' => 0,
                'estado' => 'ACT',
            ],
            [
                'tipo' => 'INFORDEN',
                'codigo' => '6006',
                'codasoc' => 'WR',
                'grupo' => 'WR',
                'orden' => 0,
                'estado' => 'ACT',
            ],

            [
                'tipo' => 'INFORDEN',
                'codigo' => '4679',
                'codasoc' => 'MI',
                'grupo' => 'MI',
                'orden' => 0,
                'estado' => 'ACT',
            ],

            [
                'tipo' => 'INFORDEN',
                'codigo' => '4679',
                'codasoc' => 'ME',
                'grupo' => 'ME',
                'orden' => 3,
                'estado' => 'ACT',
            ],
            
        ];
    }

    public function Listas()
    {

        return [
            [
                "tipo" => "APLICA_POR",
                "codigo" => "VALOR_FOB",
                "descrip" => "Valor FOB",
                "descrip_ingles" => "FOB value",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "VALOR_CIF",
                "descrip" => "Valor CIF",
                "descrip_ingles" => "CIF value",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "CONTENEDOR",
                "descrip" => "Contenedor",
                "descrip_ingles" => "Container",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "PIEZAS",
                "descrip" => "Piezas",
                "descrip_ingles" => "Pieces",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "PESO_BRUTO",
                "descrip" => "Peso bruto",
                "descrip_ingles" => "Gross weight",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "VOLUMEN",
                "descrip" => "Volumen",
                "descrip_ingles" => "Volume",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "APLICA_POR",
                "codigo" => "PESO_COBRABLE",
                "descrip" => "Peso cobrable",
                "descrip_ingles" => "Chargeable weight",
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],



            [
                "tipo" => "CAMP_CATEG",
                "codigo" => "A",
                "descrip" => "A",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "",
                "cod_parent" => ""
            ],
            [
                "tipo" => "CAMP_CATEG",
                "codigo" => "B",
                "descrip" => "B",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_CATEG",
                "codigo" => "C",
                "descrip" => "C",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_ORIG",
                "codigo" => "B",
                "descrip" => "Base de datos",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "",
                "cod_parent" => ""
            ],
            [
                "tipo" => "CAMP_ORIG",
                "codigo" => "F",
                "descrip" => "Referido",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_ORIG",
                "codigo" => "R",
                "descrip" => "Routing",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_TIPO",
                "codigo" => "CCIAL",
                "descrip" => "Comercial",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_TIPO",
                "codigo" => "EMAIL",
                "descrip" => "Email",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_TIPO",
                "codigo" => "FIDEL",
                "descrip" => "Fidelizacion",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "CAMP_TIPO",
                "codigo" => "SMS",
                "descrip" => "SMS",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADO",
                "codigo" => "ABIERTA",
                "descrip" => "ABIERTA",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADO",
                "codigo" => "CERRADA",
                "descrip" => "CERRADA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCLIEN",
                "codigo" => "ACTIVO",
                "descrip" => "ACTIVO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCLIEN",
                "codigo" => "INACTIVO",
                "descrip" => "INACTIVO",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCLIEN",
                "codigo" => "DESCARTADO",
                "descrip" => "DESCARTADO",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "APROBADA",
                "descrip" => "APROBADA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "DO_ASIGNADO",
                "descrip" => "DO_ ASIGNADO",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "DO_ASIGNADO_M",
                "descrip" => "DO  ASIGNADO MANUALMENTE",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "ENPROCESO",
                "descrip" => "ENPROCESO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "REQUERIMIENTO",
                "descrip" => "REQUERIMIENTO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOCOTIZA",
                "codigo" => "RECHAZADA",
                "descrip" => "RECHAZADA",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO",
                "codigo" => "DOCVENCIDA",
                "descrip" => "DOCVENCIDA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO",
                "codigo" => "PENDIENTE",
                "descrip" => "PENDIENTE",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO",
                "codigo" => "VERIFICADO",
                "descrip" => "VERIFICADO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "FACTURADO",
                "descrip" => "FACTURADO",
                "descrip_ingles" => null,
                "orden" => 10,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "ANULADO",
                "descrip" => "ANULADO",
                "descrip_ingles" => null,
                "orden" => 10,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "COMPLETA",
                "descrip" => "COMPLETA",
                "descrip_ingles" => null,
                "orden" => 9,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "EN ADUANA",
                "descrip" => "EN ADUANA",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "EN OTM",
                "descrip" => "EN OTM",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "NO",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "ENPROCESO",
                "descrip" => "ENPROCESO",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "INCOMPLETA",
                "descrip" => "INCOMPLETA",
                "descrip_ingles" => null,
                "orden" => 9,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "INGRESADO",
                "descrip" => "INGRESADO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTE ARRIBO",
                "descrip" => "PTE ARRIBO",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTE CIERRE",
                "descrip" => "PTE CIERRE",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "NO",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTE EMBARQUE",
                "descrip" => "PTE EMBARQUE",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTE ZARPE",
                "descrip" => "PTE ZARPE",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTEAPROB",
                "descrip" => "PTEAPROB",
                "descrip_ingles" => null,
                "orden" => 8,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTECIERRE",
                "descrip" => "PTECIERRE",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "NO",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTECOSTOS",
                "descrip" => "PTECOSTOS",
                "descrip_ingles" => null,
                "orden" => 8,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTECTL",
                "descrip" => "PTECTL",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PTEDIGITA",
                "descrip" => "PTEDIGITA",
                "descrip_ingles" => null,
                "orden" => 7,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOINFO_ORD",
                "codigo" => "PVERIFICAR",
                "descrip" => "PVERIFICAR",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOORDEN",
                "codigo" => "ABIERTA",
                "descrip" => "ABIERTA",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOORDEN",
                "codigo" => "ANULADA",
                "descrip" => "ANULADA",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "ESTADOORDEN",
                "codigo" => "CERRADA",
                "descrip" => "CERRADA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "CFR",
                "descrip" => "CFR",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "CIF",
                "descrip" => "CIF",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "CIP",
                "descrip" => "CIP",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "CPT",
                "descrip" => "CPT",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "DAP",
                "descrip" => "DAP",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "DAT",
                "descrip" => "DAT",
                "descrip_ingles" => null,
                "orden" => 7,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "DDP",
                "descrip" => "DDP",
                "descrip_ingles" => null,
                "orden" => 8,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "EXW",
                "descrip" => "EXW",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "FAS",
                "descrip" => "FAS",
                "descrip_ingles" => null,
                "orden" => 10,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "FCA",
                "descrip" => "FCA",
                "descrip_ingles" => null,
                "orden" => 11,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INCOTERM",
                "codigo" => "FOB",
                "descrip" => "FOB",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INVOICE_TIPO_DOC",
                "codigo" => "FV",
                "descrip" => "FACTURA DE VENTA",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INVOICE_TIPO_DOC",
                "codigo" => "NC",
                "descrip" => "NOTA CREDITO",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "INVOICE_TIPO_DOC",
                "codigo" => "ND",
                "descrip" => "NOTA DEBITO",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "CHEQUE",
                "descrip" => "Cheque",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "CON",
                "descrip" => "Consignación",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "EFE",
                "descrip" => "Efectivo",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "TC",
                "descrip" => "Tarjeta de crédito",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "TD",
                "descrip" => "Tarjeta de débito",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "METODO_PAGO",
                "codigo" => "TRANS",
                "descrip" => "Transferencia",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MONEDA",
                "codigo" => "COP",
                "descrip" => "COP",
                "descrip_ingles" => "COP",
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MONEDA",
                "codigo" => "EUR",
                "descrip" => "EUR",
                "descrip_ingles" => "EUR",
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MONEDA",
                "codigo" => "USD",
                "descrip" => "USD",
                "descrip_ingles" => "USD",
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "ASIGNA",
                "descrip" => "Carga Asignada a la empresa",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "ASIGOT",
                "descrip" => "Asignado a otro embarcador",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "COTPRY",
                "descrip" => "Cotizacion para Proyecto",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "NOAPROB",
                "descrip" => "Cotización no aprobada.",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "NOLISTA",
                "descrip" => "Mercancia aun no se encuentra lista",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "MOTCOTIZA",
                "codigo" => "REVIS",
                "descrip" => "En proceso de Revision por parte del cliente",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "OFICINA",
                "codigo" => "BG",
                "descrip" => "Bogota",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "OFICINA",
                "codigo" => "MD",
                "descrip" => "Medellin",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "PESO_UNIDADES",
                "codigo" => "KG",
                "descrip" => "KGS",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "PESO_UNIDADES",
                "codigo" => "LB",
                "descrip" => "LBS",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "11",
                "descrip" => "Importacion aerea",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "AI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "12",
                "descrip" => "Carga suelta",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "MI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "13",
                "descrip" => "En contenedor",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "MI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "14",
                "descrip" => "Internacional",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "TI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "15",
                "descrip" => "Almacenaje",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "WI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "16",
                "descrip" => "Seguro",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => ""
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "17",
                "descrip" => "Nacional",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "TI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "18",
                "descrip" => "Aduana",
                "descrip_ingles" => null,
                "orden" => 305,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "CUSI"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "21",
                "descrip" => "Exportacion Aerea",
                "descrip_ingles" => null,
                "orden" => 203,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "AE"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "22",
                "descrip" => "Carga suelta",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "ME"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "23",
                "descrip" => "En contenedor",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "ME"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "24",
                "descrip" => "Internacional",
                "descrip_ingles" => null,
                "orden" => 202,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "TE"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "25",
                "descrip" => "Almacenaje",
                "descrip_ingles" => null,
                "orden" => 205,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "WE"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "26",
                "descrip" => "Seguro",
                "descrip_ingles" => null,
                "orden" => 204,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => null
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "27",
                "descrip" => "Nacional",
                "descrip_ingles" => null,
                "orden" => 202,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "TE"
            ],
            [
                "tipo" => "SERVICIO",
                "codigo" => "28",
                "descrip" => "Aduana",
                "descrip_ingles" => null,
                "orden" => 306,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "SUBTIPO_OPER",
                "cod_parent" => "CUSE"
            ],
            [
                "tipo" => "SINO",
                "codigo" => "NO",
                "descrip" => "NO",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SINO",
                "codigo" => "SI",
                "descrip" => "SI",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "AE",
                "descrip" => "Exportacion aerea",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "EXPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "AI",
                "descrip" => "Importacion aerea",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "IMPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "WE",
                "descrip" => "Almacenaje expo",
                "descrip_ingles" => null,
                "orden" => 8,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "EXPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "WI",
                "descrip" => "Almacenaje impo",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "IMPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "ME",
                "descrip" => "Exportacion maritima",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "EXPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "MI",
                "descrip" => "Importacion maritima",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "IMPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "CUSE",
                "descrip" => "Aduana exportacion",
                "descrip_ingles" => null,
                "orden" => 13,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "EXPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "CUSI",
                "descrip" => "Aduana importacion",
                "descrip_ingles" => null,
                "orden" => 14,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "IMPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "TE",
                "descrip" => "Exportacion terrestre",
                "descrip_ingles" => null,
                "orden" => 7,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "EXPO"
            ],
            [
                "tipo" => "SUBTIPO_OPER",
                "codigo" => "TI",
                "descrip" => "Importacion terrestre",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TIPO_OPERACION",
                "cod_parent" => "IMPO"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "AE",
                "descrip" => "AE",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "AI",
                "descrip" => "AI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "WE",
                "descrip" => "WE",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "WI",
                "descrip" => "WI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "COURR",
                "descrip" => "COURR",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "DTAI",
                "descrip" => "DTAI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "ME",
                "descrip" => "ME",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "MI",
                "descrip" => "MI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "OTM",
                "descrip" => "OTM",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "CUSE",
                "descrip" => "CUSE",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "CUSI",
                "descrip" => "CUSI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "TE",
                "descrip" => "TE",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "SUBTIPO_REPORT",
                "codigo" => "TI",
                "descrip" => "TI",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => "TEMPLATE_REPORT",
                "cod_parent" => "5"
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "ANTICIPO_NOTES",
                "descrip" => "Notas de la solicitud de anticipos",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "CIERRE_DO",
                "descrip" => "Correo de cierre de DO",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "INVOICE_FOOTER",
                "descrip" => "Texto en el pie de la factura",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "INVOICE_HEADER",
                "descrip" => "Texto en el encabezado de la factura",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "QUOT_HEADER_ENG",
                "descrip" => "Encabezado de las cotizaciones en ingles",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "QUOT_HEADER_SPA",
                "descrip" => "Encabezado de las cotizaciones en español",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "QUOT_NOTES_ENG",
                "descrip" => "Notas de las cotizaciones en ingles",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "QUOT_NOTES_SPA",
                "descrip" => "Notas de las cotizaciones en español",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "TEMPLATE_REQUEST",
                "descrip" => "Plantila solicitud tarifas",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "C_FLETES_HEADER",
                "descrip" => "Encabezado certificacion de fletes",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE",
                "codigo" => "C_FLETES_NOTES",
                "descrip" => "Pie de pagina certificacion de fletes",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE_REPORT",
                "codigo" => "0",
                "descrip" => "0",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE_REPORT",
                "codigo" => "1",
                "descrip" => "1",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TEMPLATE_REPORT",
                "codigo" => "2",
                "descrip" => "2",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CLIENTE",
                "codigo" => "PROSPECTO",
                "descrip" => "Prospecto",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CLIENTE",
                "codigo" => "RATIFICADO",
                "descrip" => "Ratificado",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "20 FLAT RACK",
                "descrip" => "20 FLAT RACK",
                "descrip_ingles" => null,
                "orden" => 10,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "20 HQ",
                "descrip" => "20 HQ",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "20 OPEN TOP",
                "descrip" => "20 OPEN TOP",
                "descrip_ingles" => null,
                "orden" => 8,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "20 REEFER",
                "descrip" => "20 REEFER",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "20 STD",
                "descrip" => "20 STD",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 FLAT RACK",
                "descrip" => "40 FLAT RACK",
                "descrip_ingles" => null,
                "orden" => 11,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 HQ",
                "descrip" => "40 HQ",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 OPEN TOP",
                "descrip" => "40 OPEN TOP",
                "descrip_ingles" => null,
                "orden" => 9,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 REEFER",
                "descrip" => "40 REEFER",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 REEFER HQ",
                "descrip" => "40 REEFER HQ",
                "descrip_ingles" => null,
                "orden" => 7,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "40 STD",
                "descrip" => "40 STD",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTENEDOR",
                "codigo" => "N/A",
                "descrip" => "N/A",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTRATO",
                "codigo" => "CONTRATISTA",
                "descrip" => "CONTRATISTA",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTRATO",
                "codigo" => "FIJO",
                "descrip" => "FIJO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_CONTRATO",
                "codigo" => "INDEFINIDO",
                "descrip" => "INDEFINIDO",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "BOLSA",
                "descrip" => "BOLSAS",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "BULTO",
                "descrip" => "BULTOS",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "CAJA",
                "descrip" => "CAJAS",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "CARTON",
                "descrip" => "CARTONS",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "PALLET",
                "descrip" => "PALLETS",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_EMPAQUE",
                "codigo" => "PAQUETE",
                "descrip" => "PAQUETES",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_ID",
                "codigo" => "CC",
                "descrip" => "CEDULA",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_ID",
                "codigo" => "ID",
                "descrip" => "ID",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_ID",
                "codigo" => "NIT",
                "descrip" => "NIT",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_ID",
                "codigo" => "OTRO",
                "descrip" => "OTRO",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_INGRESO",
                "codigo" => "PROPIO",
                "descrip" => "PROPIOS",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_INGRESO",
                "codigo" => "TERCERO",
                "descrip" => "TERCEROS",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_OPERACION",
                "codigo" => "EXPO",
                "descrip" => "EXPO",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_OPERACION",
                "codigo" => "IMPO",
                "descrip" => "IMPO",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "ADMIN",
                "descrip" => "ADMIN",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "ADUANA",
                "descrip" => "ADUANA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "AEROLINEA",
                "descrip" => "AEROLINEA",
                "descrip_ingles" => null,
                "orden" => 3,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "AGENTE",
                "descrip" => "AGENTE",
                "descrip_ingles" => null,
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "ASESOR",
                "descrip" => "ASESOR",
                "descrip_ingles" => null,
                "orden" => 0,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "BODEGA",
                "descrip" => "BODEGA",
                "descrip_ingles" => null,
                "orden" => 5,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "CLD",
                "descrip" => "CLD",
                "descrip_ingles" => null,
                "orden" => 6,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "NAVIERA",
                "descrip" => "NAVIERA",
                "descrip_ingles" => null,
                "orden" => 9,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "OTM",
                "descrip" => "OTM",
                "descrip_ingles" => null,
                "orden" => 10,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "OTROS",
                "descrip" => "OTROS",
                "descrip_ingles" => null,
                "orden" => 11,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "SEGURO",
                "descrip" => "SEGURO",
                "descrip_ingles" => null,
                "orden" => 14,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "TIPO_PROVEEDOR",
                "codigo" => "TERRESTRE",
                "descrip" => "TERRESTRE",
                "descrip_ingles" => null,
                "orden" => 18,
                "activo" => "SI",
                "sistema" => null,
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "VOLUMEN_UNIDADES",
                "codigo" => "CBM",
                "descrip" => "CBM",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "VOLUMEN_UNIDADES",
                "codigo" => "CFT",
                "descrip" => "CFT",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "SEGURO",
                "descrip" => "Seguro",
                "descrip_ingles" => null,
                "orden" => 1,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CONF_INICIO",
                "descrip" => "Inicio operacion",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CONF_LIBERACION",
                "descrip" => "Liberacion",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CONF_RESERVA",
                "descrip" => "Confirmacion Zarpe",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "REV_DRAFT",
                "descrip" => "Revision DRAFT",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "HBL_FINAL",
                "descrip" => "HBL final",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "PREALERTA",
                "descrip" => "Prealerta",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CONF_ZARPE",
                "descrip" => "Confirmacion de Zarpe",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CAMB_ETA",
                "descrip" => "Cambio de ETA",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "RADICACION",
                "descrip" => "Radicacion",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "NOTI_ARRIBO",
                "descrip" => "Notificacion de arribo",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "STATUS_TEMPLATE",
                "codigo" => "CONF_ARRIBO",
                "descrip" => "Confirmacion de arribo",
                "descrip_ingles" => null,
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SHIPPING_TITLE",
                "codigo" => "1",
                "descrip" => "Shipping instruction",
                "descrip_ingles" => "Shipping instruction",
                "orden" => 2,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SHIPPING_TITLE",
                "codigo" => "2",
                "descrip" => "Carta porte",
                "descrip_ingles" => "Carta porte",
                "orden" => 3,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            [
                "tipo" => "SHIPPING_TITLE",
                "codigo" => "3",
                "descrip" => "Orden de recoleccion",
                "descrip_ingles" => "Orden de recoleccion",
                "orden" => 4,
                "activo" => "SI",
                "sistema" => "SI",
                "tipo_parent" => null,
                "cod_parent" => null
            ],
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'LIST_TYPE',
                'descrip' => 'Tipo de lista',
                'descrip_ingles' => NULL,
                'orden' => 0,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'APLICA_POR',
                'descrip' => 'Aplica por',
                'descrip_ingles' => NULL,
                'orden' => 0,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'CAMP_CATEG',
                'descrip' => 'CategorÃ­a del cliente',
                'descrip_ingles' => NULL,
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'CAMP_ORIG',
                'descrip' => 'Origen del cliente',
                'descrip_ingles' => NULL,
                'orden' => 2,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'CAMP_TIPO',
                'descrip' => 'Tipo de campana',
                'descrip_ingles' => NULL,
                'orden' => 3,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'CAMP_CLIE',
                'descrip' => 'Clasificacion de clientes',
                'descrip_ingles' => NULL,
                'orden' => 3,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_INGRESO',
                'descrip' => 'Tipo de ingreso',
                'descrip_ingles' => NULL,
                'orden' => 3,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'MONEDA',
                'descrip' => 'Moneda',
                'descrip_ingles' => NULL,
                'orden' => 3,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADO',
                'descrip' => 'Estados del grid',
                'descrip_ingles' => NULL,
                'orden' => 4,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADOCLIEN',
                'descrip' => 'Estado del cliente',
                'descrip_ingles' => NULL,
                'orden' => 5,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADOCOTIZA',
                'descrip' => 'Estado de la cotizacion',
                'descrip_ingles' => NULL,
                'orden' => 6,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADOINFO',
                'descrip' => 'Estado documentacion',
                'descrip_ingles' => NULL,
                'orden' => 7,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADOINFO_ORD',
                'descrip' => 'Estado proceso del DO',
                'descrip_ingles' => NULL,
                'orden' => 8,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'ESTADOORDEN',
                'descrip' => 'Estado del DO',
                'descrip_ingles' => NULL,
                'orden' => 9,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'INCOTERM',
                'descrip' => 'Incoterm',
                'descrip_ingles' => NULL,
                'orden' => 10,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'INVOICE_TIPO_DOC',
                'descrip' => 'Tipos de documentos contables',
                'descrip_ingles' => NULL,
                'orden' => 11,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'LST_CERTIF',
                'descrip' => 'Tipos de certificaciones',
                'descrip_ingles' => NULL,
                'orden' => 12,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'LST_CERTIF_DIR',
                'descrip' => 'Dirigido a en certificaciones',
                'descrip_ingles' => NULL,
                'orden' => 13,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'METODO_PAGO',
                'descrip' => 'Metodos de pago',
                'descrip_ingles' => NULL,
                'orden' => 14,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'MOTCOTIZA',
                'descrip' => 'Motivo cierre cotizacion',
                'descrip_ingles' => NULL,
                'orden' => 15,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'OFICINA',
                'descrip' => 'Oficina',
                'descrip_ingles' => NULL,
                'orden' => 16,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'PESO_UNIDADES',
                'descrip' => 'Unidades de peso',
                'descrip_ingles' => NULL,
                'orden' => 17,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'SERVICIO',
                'descrip' => 'Servicio',
                'descrip_ingles' => NULL,
                'orden' => 18,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'SINO',
                'descrip' => 'Lista SI / NO',
                'descrip_ingles' => NULL,
                'orden' => 19,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'STATUS_TEMPLATE',
                'descrip' => 'Plantillas de status de operacion',
                'descrip_ingles' => NULL,
                'orden' => 20,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'SUBTIPO_OPER',
                'descrip' => 'Subtipo',
                'descrip_ingles' => NULL,
                'orden' => 21,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'SUBTIPO_REPORT',
                'descrip' => 'Reporte subtipo',
                'descrip_ingles' => NULL,
                'orden' => 22,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TAGS',
                'descrip' => 'Tags',
                'descrip_ingles' => NULL,
                'orden' => 23,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TEMPLATE',
                'descrip' => 'Plantillas',
                'descrip_ingles' => NULL,
                'orden' => 24,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TEMPLATE_REPORT',
                'descrip' => 'Plantillas de reporte',
                'descrip_ingles' => NULL,
                'orden' => 25,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_CLIENTE',
                'descrip' => 'Tipo de cliente',
                'descrip_ingles' => NULL,
                'orden' => 26,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_CONTENEDOR',
                'descrip' => 'Tipo de contenedor',
                'descrip_ingles' => NULL,
                'orden' => 27,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_CONTRATO',
                'descrip' => 'Tipo de contrato',
                'descrip_ingles' => NULL,
                'orden' => 28,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_EMPAQUE',
                'descrip' => 'Tipo de empaque',
                'descrip_ingles' => NULL,
                'orden' => 29,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_ID',
                'descrip' => 'Tipo de identificacion',
                'descrip_ingles' => NULL,
                'orden' => 30,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_OPERACION',
                'descrip' => 'Tipos de operaciones',
                'descrip_ingles' => NULL,
                'orden' => 31,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'TIPO_PROVEEDOR',
                'descrip' => 'Tipo de proveedor',
                'descrip_ingles' => NULL,
                'orden' => 32,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'VOLUMEN_UNIDADES',
                'descrip' => 'Unidades de volumen',
                'descrip_ingles' => NULL,
                'orden' => 33,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'LST_ORIGEN',
                'descrip' => 'Listado trayectos',
                'descrip_ingles' => 'Listado trayectos',
                'orden' => 34,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'LIST_TYPE',
                'codigo' => 'CUSTOM_TEMPLATE',
                'descrip' => 'Generador de documentos',
                'descrip_ingles' => 'Generador de documentos',
                'orden' => 35,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'CUSTOM_TEMPLATE',
                'codigo' => '1',
                'descrip' => 'Plantilla de ejemplo',
                'descrip_ingles' => 'Plantilla de ejemplo',
                'orden' => 36,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            array(
                'tipo' => 'IDIOMA',
                'codigo' => 'SPA',
                'descrip' => 'Español',
                'descrip_ingles' => 'Español',
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'IDIOMA',
                'codigo' => 'ENG',
                'descrip' => 'Ingles',
                'descrip_ingles' => 'English',
                'orden' => 2,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),

            array(
                'tipo' => 'IDIOMA',
                'codigo' => 'PR',
                'descrip' => 'Portugues',
                'descrip_ingles' => 'Portugues',
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => NULL,
                'cod_parent' => NULL,
            ),
            [
                'tipo' => 'TIPO_OPERACION',
                'codigo' => 'WR',
                'descrip' => 'WAREHOUSE',
                'descrip_ingles' => null,
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => 'SI',
                'tipo_parent' => null,
                'cod_parent' => null,
            ],
            [
                'tipo' => 'SUBTIPO_OPER',
                'codigo' => 'WR',
                'descrip' => 'Warehouse',
                'descrip_ingles' => 'Warehouse receipt',
                'orden' => 2,
                'activo' => 'SI',
                'sistema' => null,
                'tipo_parent' => 'TIPO_OPERACION',
                'cod_parent' => 'WR',
            ],
            [
                'tipo' => 'SERVICIO',
                'codigo' => '50',
                'descrip' => 'Recibo de bodega',
                'descrip_ingles' => 'Warehouse receipt',
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => null,
                'tipo_parent' => 'SUBTIPO_OPER',
                'cod_parent' => 'WR',
            ],
            [
                'tipo' => 'SERVICIO',
                'codigo' => '51',
                'descrip' => 'Salida mercancia de bodega',
                'descrip_ingles' => 'Cargo release',
                'orden' => 1,
                'activo' => 'SI',
                'sistema' => null,
                'tipo_parent' => 'SUBTIPO_OPER',
                'cod_parent' => 'WR',
            ],
            [
                'tipo' => 'PIECES_STATUS',
                'codigo' => 'DELIVERED',
                'descrip' => 'Entregado',
                'descrip_ingles' => 'Delivered',
                'orden' => 0,
                'activo' => 'SI',
                'sistema' => null,
                'tipo_parent' => 'SERVICIO',
                'cod_parent' => '51',
            ],
            [
                'tipo' => 'PIECES_STATUS',
                'codigo' => 'ON_HAND',
                'descrip' => 'En bodega',
                'descrip_ingles' => 'On hand',
                'orden' => 0,
                'activo' => 'SI',
                'sistema' => null,
                'tipo_parent' => 'SERVICIO',
                'cod_parent' => '50',
            ],
        ];
    }

    // arreglo de tm_consecutivo
    public function Consecutivo()
    {

        return [
            [
                'SUBTIPO_OPER' => "",
                'SERVICIO' => null,
                'CODIGO_SERV' => null,
                'ANO' => null,
                'PRE' => null,
                'SECUENCIA' => 0,
            ]
        ];
    }

    public function AtributosAduana()
    {
        return [
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 1",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 2",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 3",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 4",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 5",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 6",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 7",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 8",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ],
            [
                "proveedor_id" => "123456789",
                "ciudad" => "BUN",
                "descripcion" => "Doc 9",
                "estado" => "ACT",
                "creado_por" => "SOPORTE",
            ]
        ];
    }

    public function Atributos()
    {
        return [
            [
                "atributo" => "1201",
                "descrip" => "Hoja de vida",
                "orden" => 1,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1202",
                "descrip" => "Fotocopia cedula",
                "orden" => 2,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1203",
                "descrip" => "Registro civil nacimiento",
                "orden" => 3,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1204",
                "descrip" => "Fotocopia libreta militrar",
                "orden" => 4,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1205",
                "descrip" => "Fotocopia certificados de Estudio",
                "orden" => 5,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1206",
                "descrip" => "Fotocopia certificados de Formacion",
                "orden" => 6,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1207",
                "descrip" => "Fotocopia certificados de Laborales",
                "orden" => 7,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1208",
                "descrip" => "Cartas de recomencación",
                "orden" => 8,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1209",
                "descrip" => "Fotocopia Tarjeta Profesional",
                "orden" => 9,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1210",
                "descrip" => "Fotocopia Cedula Ciudadanía Conyugue",
                "orden" => 10,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1211",
                "descrip" => "Fotocopia Tarjeta Identidad de los hijos",
                "orden" => 11,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1212",
                "descrip" => "Registro civil Matrimonio",
                "orden" => 12,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1213",
                "descrip" => "Licencia de conducción",
                "orden" => 13,
                "grupo" => "DOCLEGALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1101",
                "descrip" => "Circular 0170",
                "orden" => 1,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1102",
                "descrip" => "Visita",
                "orden" => 2,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "N",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1103",
                "descrip" => "Registro Proveedores",
                "orden" => 3,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "INA",
                "tipo" => "FECHA",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1105",
                "descrip" => "Estudio Proveedores",
                "orden" => 4,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1109",
                "descrip" => "Acuerdo de Seguridad",
                "orden" => 5,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1106",
                "descrip" => "Camara de Comercio",
                "orden" => 6,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "N",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1113",
                "descrip" => "Policia",
                "orden" => 7,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1110",
                "descrip" => "Lista Clinton",
                "orden" => 8,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1114",
                "descrip" => "Fecha Verificacion",
                "orden" => 9,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => "FECHA",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1115",
                "descrip" => "RUT",
                "orden" => 10,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => "S",
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1116",
                "descrip" => "Certificaciones",
                "orden" => 11,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1117",
                "descrip" => "Poliza",
                "orden" => 12,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1118",
                "descrip" => "Procuraduria",
                "orden" => 13,
                "grupo" => "DOCLEGALP",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1201",
                "descrip" => "Direccion Residencia",
                "orden" => 1,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1202",
                "descrip" => "Telefono Residencia",
                "orden" => 2,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1203",
                "descrip" => "Celular",
                "orden" => 3,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1204",
                "descrip" => "Estado civil",
                "orden" => 4,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1205",
                "descrip" => "Nombre Pareja",
                "orden" => 5,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1206",
                "descrip" => "Ocupacion pareja",
                "orden" => 6,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1207",
                "descrip" => "Tipo de vivienda",
                "orden" => 7,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1208",
                "descrip" => "Personas a Cargo",
                "orden" => 8,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1209",
                "descrip" => "Activos adquiridos en los últimos 12 meses",
                "orden" => 9,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1210",
                "descrip" => "Productos financieros",
                "orden" => 10,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1211",
                "descrip" => "Contacto de emergencia",
                "orden" => 11,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1212",
                "descrip" => "Direccion Contacto de emergencia",
                "orden" => 12,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1213",
                "descrip" => "Telefono Contacto de emergencia",
                "orden" => 13,
                "grupo" => "INFOGRALE",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "1003",
                "descrip" => "Pais",
                "orden" => 0,
                "grupo" => "INFOGRALP",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "input",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "1002",
                "descrip" => "Telefono",
                "orden" => 2,
                "grupo" => "INFOGRALP",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "input",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "1001",
                "descrip" => "Direccion",
                "orden" => 9,
                "grupo" => "INFOGRALP",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "input",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "1009",
                "descrip" => "Ciudad",
                "orden" => 14,
                "grupo" => "INFOGRALP",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "input",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "4667",
                "descrip" => "Origen",
                "orden" => 2,
                "grupo" => "INFORDEN",
                "lista" => "LST_ORIGEN",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "4672",
                "descrip" => "Master",
                "orden" => 2,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 3,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4665",
                "descrip" => "Destino",
                "orden" => 3,
                "grupo" => "INFORDEN",
                "lista" => "LST_DESTINO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => "SI",
                "reporte" => null
            ],
            [
                "atributo" => "4673",
                "descrip" => "Emision Master",
                "orden" => 3,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 3,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4653",
                "descrip" => "Motonave",
                "orden" => 4,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 7,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4675",
                "descrip" => "House",
                "orden" => 4,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 3,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4602",
                "descrip" => "Pedido",
                "orden" => 5,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4674",
                "descrip" => "Emision House",
                "orden" => 5,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 3,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4664",
                "descrip" => "Viaje",
                "orden" => 6,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 7,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4604",
                "descrip" => "Producto",
                "orden" => 7,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4605",
                "descrip" => "Deposito",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4676",
                "descrip" => "Booking",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4677",
                "descrip" => "Place of receipt",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],

            [
                "atributo" => "4678",
                "descrip" => "Place of delivery",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],

            [
                "atributo" => "4679",
                "descrip" => "Naviera",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],

            [
                "atributo" => "4611",
                "descrip" => "Type of move",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4631",
                "descrip" => "Domestic Routing / export instructions",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],

            [
                "atributo" => "4632",
                "descrip" => "Tipo BL",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4240",
                "descrip" => "Procesos",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4241",
                "descrip" => "Certificado origen",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4242",
                "descrip" => "Facturas",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4243",
                "descrip" => "Lista de empaque",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4244",
                "descrip" => "BL master",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4245",
                "descrip" => "AWB",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4246",
                "descrip" => "DIM",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4247",
                "descrip" => "DEX",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4248",
                "descrip" => "Notificacion arribo",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4249",
                "descrip" => "Radicado",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4250",
                "descrip" => "Shiping instruction",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "4251",
                "descrip" => "Prealerta",
                "orden" => 0,
                "grupo" => "ORDENDOC",
                "lista" => null,
                "orden_grp" => null,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5000",
                "descrip" => "To",
                "orden" => 1,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5001",
                "descrip" => "By first carrier",
                "orden" => 2,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5002",
                "descrip" => "To",
                "orden" => 3,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5003",
                "descrip" => "By",
                "orden" => 4,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5004",
                "descrip" => "To",
                "orden" => 5,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5005",
                "descrip" => "By",
                "orden" => 6,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5006",
                "descrip" => "Currency",
                "orden" => 7,
                "grupo" => "INFORDEN",
                "lista" => "MONEDA",
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5007",
                "descrip" => "TRM",
                "orden" => 8,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5008",
                "descrip" => "CHGS Code",
                "orden" => 9,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5009",
                "descrip" => "WT/VAL",
                "orden" => 10,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5010",
                "descrip" => "Other",
                "orden" => 11,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5011",
                "descrip" => "Declared value for customs",
                "orden" => 12,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5012",
                "descrip" => "Declared value for carriage",
                "orden" => 13,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5013",
                "descrip" => "Amount of insurance",
                "orden" => 14,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5014",
                "descrip" => "Handling information",
                "orden" => 15,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "textarea",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5015",
                "descrip" => "Nature and quantity of goods",
                "orden" => 16,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "textarea",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5016",
                "descrip" => "Other charges",
                "orden" => 17,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 1,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "textarea",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5017",
                "descrip" => "Weight charge",
                "orden" => 18,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5018",
                "descrip" => "Term weight",
                "orden" => 19,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5019",
                "descrip" => "Valuation charge",
                "orden" => 20,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5020",
                "descrip" => "Term valuation",
                "orden" => 21,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5021",
                "descrip" => "Tax",
                "orden" => 22,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5022",
                "descrip" => "Term tax",
                "orden" => 23,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5023",
                "descrip" => "Total other charges Due Agent",
                "orden" => 24,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5024",
                "descrip" => "Term Due Agent",
                "orden" => 25,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5025",
                "descrip" => "Total other charges Due Carrier",
                "orden" => 26,
                "grupo" => "INFORDEN",
                "lista" => null,
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => "number",
                "obligatorio" => null,
                "tipo_campo" => null,
                "sistema" => null,
                "reporte" => null
            ],
            [
                "atributo" => "5026",
                "descrip" => "Term Due Carrier",
                "orden" => 27,
                "grupo" => "INFORDEN",
                "lista" => "LST_COBRO",
                "orden_grp" => 2,
                "estado" => "ACT",
                "tipo" => null,
                "obligatorio" => null,
                "tipo_campo" => "select",
                "sistema" => null,
                "reporte" => null
            ],
            [
                'atributo' => '6001',
                'descrip' => 'Carrier name',
                'descrip_ingles' => 'Carrier name',
                'orden' => 1,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
            [
                'atributo' => '6002',
                'descrip' => 'P.O. Number',
                'descrip_ingles' => 'P.O. Number',
                'orden' => 0,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
            [
                'atributo' => '6003',
                'descrip' => 'PRO number',
                'descrip_ingles' => 'PRO number',
                'orden' => 3,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
            [
                'atributo' => '6004',
                'descrip' => 'Supplier name',
                'descrip_ingles' => 'Supplier name',
                'orden' => 4,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
            [
                'atributo' => '6005',
                'descrip' => 'Tracking number',
                'descrip_ingles' => 'Tracking number',
                'orden' => 5,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
            [
                'atributo' => '6006',
                'descrip' => 'Invoice number',
                'descrip_ingles' => 'Invoice number',
                'orden' => 7,
                'grupo' => 'INFORDEN',
                'lista' => '',
                'orden_grp' => 0,
                'estado' => 'ACT',
                'tipo_campo' => 'input',
                'placeholder' => '',
                'default' => '',
                'sistema' => null,
                'reporte' => 'ACT',
            ],
        ];
    }

    public function Actividades()
    {
        return [
            [
                "descripcion" => "Instruccion embargue",
                "secuencia" => 0,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Cargue planta",
                "secuencia" => 5,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Carta responsabilidad",
                "secuencia" => 7,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Inicio recorrido",
                "secuencia" => 8,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Doc aduanera",
                "secuencia" => 9,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Visto bueno awb",
                "secuencia" => 15,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Reserva aerolinea",
                "secuencia" => 18,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "CONF_RESERVA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmación vuelo",
                "secuencia" => 19,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "ETD",
                "activo" => "S"
            ],
            [
                "descripcion" => "Prealerta",
                "secuencia" => 21,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "PREALERTA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Llegada destino",
                "secuencia" => 25,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "ETA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Agente",
                "secuencia" => 27,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 28,
                "tipo" => "EXPO",
                "subtipo_oper" => "AE",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Instruccion embargue",
                "secuencia" => 0,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Booking",
                "secuencia" => 1,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "CONF_RESERVA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Pago comodato",
                "secuencia" => 2,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Carta retiro contenedor",
                "secuencia" => 3,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Retiro cont patio",
                "secuencia" => 4,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Cargue planta",
                "secuencia" => 5,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Seguro",
                "secuencia" => 6,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Carta responsabilidad",
                "secuencia" => 7,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Inicio recorrido",
                "secuencia" => 8,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Doc aduanera",
                "secuencia" => 9,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Llegada cont puerto",
                "secuencia" => 10,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Agenciamiento aduanero",
                "secuencia" => 11,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Visto bueno mbl",
                "secuencia" => 13,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "N"
            ],
            [
                "descripcion" => "Visto bueno hbl",
                "secuencia" => 14,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Entrega doctal",
                "secuencia" => 16,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Entrega fisica",
                "secuencia" => 17,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Proveedor",
                "secuencia" => 20,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "N"
            ],
            [
                "descripcion" => "Prealerta",
                "secuencia" => 21,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "PREALERTA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Zarpe",
                "secuencia" => 22,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "ETD",
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmacion_prealerta",
                "secuencia" => 23,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "N"
            ],
            [
                "descripcion" => "Llegada destino",
                "secuencia" => 25,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "ETA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Envio dex cliente",
                "secuencia" => 26,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Agente",
                "secuencia" => 27,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 28,
                "tipo" => "EXPO",
                "subtipo_oper" => "ME",
                "servicio" => null,
                "incoterm" => "",
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Arribo",
                "secuencia" => 0,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => 'ETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Instrucción",
                "secuencia" => 1,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Finalizacion",
                "secuencia" => 1,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Envio documentos ",
                "secuencia" => 2,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Levante",
                "secuencia" => 2,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Anticipo Aduana",
                "secuencia" => 3,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Preinspeccion",
                "secuencia" => 3,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Anticipo Cliente",
                "secuencia" => 4,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pagos de impuestos",
                "secuencia" => 4,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Planilla de ingreso",
                "secuencia" => 5,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Levante Fisico",
                "secuencia" => 5,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Dex",
                "secuencia" => 6,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Inspeccion",
                "secuencia" => 6,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pagos a puerto",
                "secuencia" => 7,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Aduana",
                "secuencia" => 8,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Entrega plantillas Transportador",
                "secuencia" => 8,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Cliente",
                "secuencia" => 9,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Cliente",
                "secuencia" => 9,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Aduana",
                "secuencia" => 10,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 11,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 11,
                "tipo" => "EXPO",
                "subtipo_oper" => "CUSE",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Instruccion agente",
                "secuencia" => 0,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pick up",
                "secuencia" => 1,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Seguro",
                "secuencia" => 2,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Reserva aerolinea",
                "secuencia" => 3,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "CONF_RESERVA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Retiro mercancía en planta",
                "secuencia" => 4,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmación vuelo",
                "secuencia" => 5,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Vuelo",
                "secuencia" => 6,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "ETD",
                "activo" => "S"
            ],
            [
                "descripcion" => "Vistos buenos master",
                "secuencia" => 8,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Visto bueno awb",
                "secuencia" => 10,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Certificacion flete",
                "secuencia" => 12,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Arribo",
                "secuencia" => 13,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'ETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmacion arribo",
                "secuencia" => 14,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Elaborar factura",
                "secuencia" => 15,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Finalizacion linea",
                "secuencia" => 16,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Liberacion",
                "secuencia" => 18,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Liberacion confirmada",
                "secuencia" => 19,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Llegada destino final",
                "secuencia" => 21,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 26,
                "tipo" => "IMPO",
                "subtipo_oper" => "AI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Instruccion agente",
                "secuencia" => 0,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pick up",
                "secuencia" => 1,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Seguro",
                "secuencia" => 2,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Retiro mercancía en planta",
                "secuencia" => 4,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Zarpe",
                "secuencia" => 6,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'ETD',
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmacion zarpe",
                "secuencia" => 7,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Vistos buenos master",
                "secuencia" => 8,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Vistos buenos house",
                "secuencia" => 9,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Radicacion",
                "secuencia" => 11,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Certificacion flete",
                "secuencia" => 12,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Arribo",
                "secuencia" => 13,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'ETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Confirmacion arribo",
                "secuencia" => 14,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Elaborar factura",
                "secuencia" => 15,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Finalizacion linea",
                "secuencia" => 16,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Vence Modalidad",
                "secuencia" => 17,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Liberacion",
                "secuencia" => 18,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Liberacion confirmada",
                "secuencia" => 19,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Salida puerto",
                "secuencia" => 20,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Llegada destino final",
                "secuencia" => 21,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Drop off",
                "secuencia" => 22,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Drop off fecha",
                "secuencia" => 23,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Devol vacio ciudad",
                "secuencia" => 24,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "Devol vacio puerto",
                "secuencia" => 25,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "",
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 26,
                "tipo" => "IMPO",
                "subtipo_oper" => "MI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Arribo",
                "secuencia" => 0,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => 'ETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Finalizacion",
                "secuencia" => 1,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Levante",
                "secuencia" => 2,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Preinspeccion",
                "secuencia" => 3,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pagos de impuestos",
                "secuencia" => 4,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Levante Fisico",
                "secuencia" => 5,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Inspeccion",
                "secuencia" => 6,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Pagos a puerto",
                "secuencia" => 7,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Entrega plantillas Transportador",
                "secuencia" => 8,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Cliente",
                "secuencia" => 9,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura Aduana",
                "secuencia" => 10,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 11,
                "tipo" => "IMPO",
                "subtipo_oper" => "CUSI",
                "servicio" => "",
                "incoterm" => "",
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ],
            [
                "descripcion" => "Orden de cargue transp",
                "secuencia" => 1,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Entrega plantillas puerto",
                "secuencia" => 2,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Salida de puerto",
                "secuencia" => 3,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "ETD",
                "activo" => "S"
            ],
            [
                "descripcion" => "Llegada destino",
                "secuencia" => 4,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => "ETA",
                "activo" => "S"
            ],
            [
                "descripcion" => "Devolución vacio",
                "secuencia" => 5,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura proveedor",
                "secuencia" => 6,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "Factura cliente",
                "secuencia" => 7,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => null,
                "activo" => "S"
            ],
            [
                "descripcion" => "INFORMAR CIERRE OPERACIÓN",
                "secuencia" => 8,
                "tipo" => "IMPO",
                "subtipo_oper" => "TI",
                "servicio" => null,
                "incoterm" => null,
                "estado_oper" => 'COMPLETA',
                "activo" => "S"
            ]
        ];
    }

    public function Proveedores()
    {
        return [
            [
                'tipo_id' => 'OTRO',
                'cedula' => '123456789',
                'correo' => '',
                'nombre' => 'N/A',
                'tipo' => 'ASESOR',
                'observaciones' => 'Creado automaticamente',
                'estado' => 'ACTIVO',
                'estado_info' => 'PENDIENTE',
                'fecha_verificado' => 'PENDIENTE',
                'usuario' => 'SOPORTE',
                'maquina' => '::1',
                'fecha_mod' => now()
            ],
            [
                'tipo_id' => 'OTRO',
                'cedula' => '987654321',
                'correo' => '',
                'nombre' => 'N/A',
                'tipo' => 'AGENTE',
                'observaciones' => 'Creado automaticamente',
                'estado' => 'ACTIVO',
                'estado_info' => 'PENDIENTE',
                'fecha_verificado' => 'PENDIENTE',
                'usuario' => 'SOPORTE',
                'maquina' => '::1',
                'fecha_mod' => now()
            ],
        ];
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateOptionTable()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\Customer\TmLista::truncate();
        \App\Models\Customer\TmConsecutivo::truncate();
        \App\Models\Customer\TmAtributosDocaduana::truncate();
        \App\Models\Customer\TmAtributo::truncate();
        \App\Models\Customer\TmActividadesOper::truncate();
        \App\Models\Customer\TmProveedores::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
