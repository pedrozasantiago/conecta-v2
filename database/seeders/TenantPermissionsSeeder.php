<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TenantPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Permission tables');
        $this->truncateLaratrustTables();
        
        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        // Create permission primary
        foreach ($config['admin'] as $key => $perm) {
            $permission = \App\Models\System\Permission::firstOrCreate([
                'name' => $key,
                'display_name' => ucwords(str_replace('-', ' ', $key)),
                'description' => ucwords(str_replace('-', ' ', $key)),
            ]);
        }

        // crear permisos secundario
        foreach ($config as $key => $modules) {

            // Reading role permission modules
            foreach ($modules as $module => $value) {
                
                $permissions = explode(',', $value);

                // verificar si el permiso principal existe
                $checkperm = $permission = \App\Models\System\Permission::where('name', $module)->first();

                foreach ($permissions as $p => $perm) {
                    
                    $permissionValue = $mapPermission->get($perm);

                    $permission = \App\Models\System\Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => ucfirst($permissionValue) . ' ' . ucwords(str_replace('-', ' ', $module)),
                        'description' => ucfirst($permissionValue) . ' ' . ucwords(str_replace('-', ' ', $module)),
                        'parent_id' => $checkperm->id,
                    ]);
                }
                
            }

        }

    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\System\Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
