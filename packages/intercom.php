<?php

$uuid = $_SESSION["user_act"] . "_" . id_descripcion($conn, "tm_users", "susuario", "cedula", $_SESSION["user_act"]);

$hash_intercom = hash_hmac(
  'sha256', // hash function
   $uuid, // user's id
  'ipfpU7A8cx5QG56jTfEnP9-0NudZNX_yN34A5EeZ' // secret key (keep safe!)
);

?>
<script>
  window.intercomSettings = {
    app_id: "gopb1hjx",
    user_id: "<?php echo $uuid ?>", // User ID
    user_hash: "<?php echo $hash_intercom ?>", // HMAC using SHA-256,
    name: "<?php echo id_descripcion($conn, "tm_users", "susuario", "nombre", $_SESSION["user_act"]) ?>", // Full name
    email: "<?php echo id_descripcion($conn, "tm_users", "susuario", "correo", $_SESSION["user_act"]) ?>", // Email address
    company: {
        company_id: "<?php echo NIT_EMPRESA ?>",
        name: "<?php echo RAZON_SOCIAL ?>", 
        size: <?php echo count(id_array_all($conn, "tm_users", "perfil", "s"))-1 ?>,
        website: "<?php echo $_SERVER['HTTP_HOST'] ?>",
  },
    "subdomain": "<?php echo $_SERVER['HTTP_HOST'] ?>", 
    created_at: "<?php echo strtotime(id_descripcion($conn, "tm_users", "susuario", "created_at", $_SESSION["user_act"])) ?>" // Signup date as a Unix timestamp
  };
</script>



<script>
// We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/gopb1hjx'
(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/gopb1hjx';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(document.readyState==='complete'){l();}else if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>