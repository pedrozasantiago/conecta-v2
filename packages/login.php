<?php 
ob_start();
if (!isset($_SESSION)) {
    session_start();
};
 
$session_id = session_id();

header('Content-Type: text/html; charset=UTF-8');

$msj_error = "";
$msj_success = "";
$C = "";
require_once $_SERVER['DOCUMENT_ROOT'] . '/operativo/conexion.php';
$conn = conexion_db();
    

if (isset($_POST['lstUsuario']) && isset($_POST["pass"])) {
    

    try {
        
        $lstUsuario = isset($_POST['lstUsuario']) ?  mysqli_real_escape_string($conn, $_POST['lstUsuario']) : "";
        $pass = isset($_POST['pass']) ?  mysqli_real_escape_string($conn, $_POST['pass']) : "";
        $C = "";

        $ConsUc = "SELECT * FROM tm_users where  susuario ='%s' AND slogin ='%s' and perfil = 'S' ";

        $sql1 = sprintf($ConsUc, $lstUsuario, $pass);

        $stmt1 = mysqli_query($conn, $sql1);
        if (!$stmt1)
            throw new Exception("Ha ocurrido un error. Comuniquese con el area de soporte.".mysqli_error($conn));

            while ($row1 = mysqli_fetch_assoc($stmt1)) {
                $C = $row1['slogin'];
            }

            if ($C == "")
                throw new Exception("Clave errada.");

            if ($pass == $C) {

                $_SESSION["login"] = 10;
                $_SESSION["user_act"] = strtoupper($lstUsuario);
                setcookie("user_act", $_SESSION["user_act"], time() + (86400 * 10), "/"); // 86400 = 1 day

                $_SESSION["maq_act"] = mysqli_real_escape_string($conn, $_SERVER['REMOTE_ADDR']);
                setcookie("maq_act", $_SESSION["maq_act"], time() + (86400 * 10), "/"); // 86400 = 1 day
            }else{
                throw new Exception("Contraseña errada");
            }
        
            
        if (!isset($_SESSION["login"]) && $_SESSION["login"] != 10)
                throw new Exception("No ha iniciado sesion en el sistema.");

            setcookie("user_act", $_SESSION["user_act"], time() + (86400 * 10), "/"); // 86400 = 1 day
            setcookie("maq_act", $_SESSION["maq_act"], time() + (86400 * 10), "/"); // 86400 = 1 day

            
            $old_session_id = id_descripcion($conn, "tm_users", "susuario", "remember_token", $_SESSION['user_act']);
            
            
            mysqli_set_charset($conn, "utf8");
            $fecha = date('d/m/Y');
            $hora = date('h:i');
            $sql = "INSERT INTO `log` (`pantalla`, `detalle_nuevo`, `detalle_viejo`, `usuario`, `maquina`, `fecha`, `accion`) 
                    VALUES ('INICIO_SESION',  'RESPUESTA : S', '', '" . $_SESSION['user_act'] . "', '$session_id', now(), 'A');";
            if(!$result = mysqli_query($conn, $sql))
                throw new Exception("Error insertando el log".mysqli_error($conn));

            $sql = "UPDATE tm_users set remember_token = '$session_id'  where susuario = '" . $_SESSION['user_act'] . "' ; ";
            if(!$result = mysqli_query($conn, $sql))
                throw new Exception("Error actualizando la sesion");

            if (defined("MULTISESSION"))
                if ($session_id != $old_session_id) {
                    session_destroy();
                    //session_commit();

                    session_id($old_session_id);
                    
                    session_start();
                    session_destroy();
                    
                    session_id($session_id);
                    session_start();
                    
                    
                    throw new Exception("Se ha cerrado su sesion anterior. Vuelva a ingresar.");
                     
                };

                
            header("Location: $modulo");

    } catch (\Exception $e) {
        $msj_error = $e->getMessage();
        //mysqli_rollback($conn);
    }
}else{
    session_regenerate_id();
}


?>