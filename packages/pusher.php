  <script>

            // Enable pusher logging - don't include this in production
            //Pusher.logToConsole = true;


            var pusher = new Pusher('588fcb0c53328e97f26e', {
              cluster: 'us2'
            });

            var socketId = null;
          pusher.connection.bind("connected", () => {
          socketId = pusher.connection.socket_id;
          });
          
            var channel = pusher.subscribe('<?php  echo $_SERVER['HTTP_HOST'] ?>');
            channel.bind('wp_notification', function(data) {
                notificaciones()
                try {
                  console.log(data)
                  //message = JSON.stringify(data);
                  message = data;
                  var audio = new Audio('assets/sounds//notificacion.wav');
                  audio.play()
                  $("title").prepend("(1) - ")
                  toastr.success(message['message'], '');
 
                              // Comprobamos si el navegador soporta las notificaciones
                              if (!("Notification" in window)) {
                                  alert("Este navegador no soporta notificaciones de CONECTA CARGA.\n\
                          Utiliza google Chrome preferiblemente para recibir notificaciones en el escritorio.");
                              }    // Comprobamos si ya nos habían dado permiso
                              else if (Notification.permission === "granted") {
                                  // Si esta correcto lanzamos la notificación
                                  var notification = spawnNotification(message['message'], "Notificacion CONECTA CARGA")
                                  

                              }

                              // Si no, tendremos que pedir permiso al usuario
                              else if (Notification.permission !== 'denied') {
                                  Notification.requestPermission(function (permission) {
                                      // Si el usuario acepta, lanzamos la notificación
                                      if (permission === "granted") {
                                          var notification = spawnNotification(message['message'], "Notificacion CONECTA CARGA")
                                      }
                                  });
                              }

                              // Finalmente, si el usuario te ha denegado el permiso y 
                              // quieres ser respetuoso no hay necesidad molestar más.



                 } catch { 
                  alert(JSON.stringify(data));
                }
                
                
            });  
  </script>