 <script>
            document.tidioIdentify = {
                distinct_id: "<?php echo $_SESSION["user_act"] . "_" . id_descripcion($conn, "tm_users", "susuario", "cedula", $_SESSION["user_act"]) ?>", // Unique user ID in your system
                email: "<?php echo id_descripcion($conn, "tm_users", "susuario", "correo", $_SESSION["user_act"]) ?>", // User email
                name: "<?php echo id_descripcion($conn, "tm_users", "susuario", "nombre", $_SESSION["user_act"]) ?>", // Visitor name
                //city: "Denver", // City
                //country: "US" // Country
                phone: "<?php echo "+57 " . id_descripcion($conn, "tm_users", "susuario", "telefono", $_SESSION["user_act"]) ?>" // Country
            };
        </script>
        <!-- Start of conectacarga TIDIO Widget script -->
        <script src="//code.tidio.co/larq7pkxgfwyyiwg4gndsshpqn6ve65p.js" async></script>
        <!-- End of conectacarga TIDIO Widget script -->
        <script>
            (function () {
                function onTidioChatApiReady() {
                    tidioChatApi.setVisitorData({
                        distinct_id: "<?php echo $_SESSION["user_act"] . "_" . id_descripcion($conn, "tm_users", "susuario", "cedula", $_SESSION["user_act"]) ?>", // Unique user ID in your system
                        email: "<?php echo id_descripcion($conn, "tm_users", "susuario", "correo", $_SESSION["user_act"]) ?>", // User email
                        name: "<?php echo id_descripcion($conn, "tm_users", "susuario", "nombre", $_SESSION["user_act"]) ?>", // Visitor name
                        //city: "Denver", // City
                        //country: "US" // Country
                        phone: "<?php echo "+57 " . id_descripcion($conn, "tm_users", "susuario", "telefono", $_SESSION["user_act"]) ?>" // Country
                    });

                    tidioChatApi.setContactProperties({
                        pantalla: "<?php echo $enl_id ?>",
                        host: "<?php echo $_SERVER['HTTP_HOST'] ?>",
                        empresa_2: "<?php echo RAZON_SOCIAL ?>",
                        empresa: "<?php echo RAZON_SOCIAL ?>",
                        nombre: "<?php echo utf8_encode(substr(id_descripcion($conn, "tm_users", "susuario", "nombre", $_SESSION["user_act"]), 0, strpos(id_descripcion($conn, "tm_users", "susuario", "nombre", $_SESSION["user_act"])," ")));  ?>",
                        usuario: "<?php echo $_SESSION["user_act"] ?>",
                        ultima_actividad: "<?php echo date("Ymd H:i") ?>"
                    }); 
                }
 
                if (window.tidioChatApi) {
                    window.tidioChatApi.on("ready", onTidioChatApiReady);
                } else {
                    document.addEventListener("tidioChat-ready", onTidioChatApiReady);
                } 
            })(); 
        </script>