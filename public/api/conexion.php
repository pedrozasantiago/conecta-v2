<?php

$conn = mysqli_connect($_SERVER['DB_ENDPOINT'], $_SERVER['DB_ADMIN_USER'], $_SERVER['DB_ADMIN_PASS'], $_SERVER['DB_ADMIN']);

if (!$conn)
    die('Connexion fallida ' . mysqli_connect_errno());


$token = isset($_POST['csyp_token']) ? mysqli_real_escape_string($conn, $_POST['csyp_token']) : '';

$sql = "select id from " . $_SERVER['DB_ADMIN'] . ".customers where uuid = '" . $token . "' limit 1";
if (!$result = mysqli_query($conn, $sql))
    die('Error en la consulta del uuid' . mysqli_connect_error());
$arreglo = mysqli_fetch_all($result, MYSQLI_ASSOC);
if(count($arreglo) > 0){
define('ID_CUSTOMER', $arreglo[0]['id']);    
}else{
    die("No hay websites relacionados");    
}


if (!$result = mysqli_query($conn, "select hostname_id as id from " . $_SERVER['DB_ADMIN'] . ".customers_hostnames where customer_id = '" . ID_CUSTOMER . "' limit 1"))
    die('Error en la consulta del hostname1' . mysqli_connect_errno());
$arreglo = mysqli_fetch_all($result, MYSQLI_ASSOC);
define('ID_HOSTNAME', $arreglo[0]['id']);

if (!$result = mysqli_query($conn, "select website_id as id,fqdn  from " . $_SERVER['DB_ADMIN'] . ".hostnames where id = '" . ID_HOSTNAME . "' limit 1"))
    die('Error en la consulta del hostname2' . mysqli_connect_errno());
$arreglo = mysqli_fetch_all($result, MYSQLI_ASSOC);
define('ID_WEBSITE', $arreglo[0]['id']);
define('HOSTNAME', $arreglo[0]['fqdn']);

if (!$result = mysqli_query($conn, "select * from " . $_SERVER['DB_ADMIN'] . ".websites where id = '" . ID_WEBSITE . "' limit 1"))
        die('Error en la consulta del hostname3' . mysqli_error($conn));
$arreglo = mysqli_fetch_all($result, MYSQLI_ASSOC);
define('DB_TENANT_DB', $arreglo[0]['uuid']);

$hash = md5(sprintf(
                '%d.%s.%s.%s', $arreglo[0]['id'], $arreglo[0]['uuid'], $arreglo[0]['created_at'], $_SERVER['APP_KEY']
        ));


if ($arreglo[0]['managed_by_database_connection'] === NULL) {
    define('DB_ENDPOINT', $_SERVER['DB_ENDPOINT']);
} else {
    define('DB_ENDPOINT', $arreglo[0]['managed_by_database_connection']);
}

define('DB_TENANT_USER', $arreglo[0]['uuid']);
define('DB_TENANT_PASS', $hash);

?>