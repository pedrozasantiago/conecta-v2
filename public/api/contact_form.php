<?php 
header('Access-Control-Allow-Origin: *');

?>
<!-- Latest compiled and minified CSS -->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->

<style>
 .theme-config {
  position: absolute;
  top: 90px;
  right: 0;
  overflow: hidden;
}
.theme-config-box {
  margin-right: -500px;
  position: relative;
  z-index: 2000;
  transition-duration: 0.8s;
}
.theme-config-box.show {
  margin-right: 0;
}
    .spin-icon {
  background: #1ab394;
  position: absolute;
  padding: 7px 10px 7px 13px;
  border-radius: 20px 0 0 20px;
  font-size: 16px;
  top: 0;
  left: 0;
  width: 40px;
  color: #fff;
  cursor: pointer;
}
.skin-settings {
  width: 500px;
  margin-left: 40px;
  background: #f3f3f4;
}
.skin-settings .title {
  background: #efefef;
  text-align: center;
  text-transform: uppercase;
  font-weight: 600;
  display: block;
  padding: 10px 15px;
  font-size: 12px;
}
.setings-item {
  padding: 10px 30px;
}
.setings-item.skin {
  text-align: center;
}
.setings-item .switch {
  float: right;
}
.skin-name a {
  text-transform: uppercase;
}
.setings-item a {
  color: #fff;
}
 
    
</style>



<div class="theme-config">
    <div class="theme-config-box">
        <div class="spin-icon">
            <span>
                <i class="fas fa-comments"></i>
            </span>
            
        </div>
        <div class="skin-settings">
            <div class="title">Formulario de contacto <br/>
            <small style="text-transform: none;font-weight: 400">
                Dejanos tus datos y nos pondremos en contacto
            </small></div>
            <form name="csyp_form_contact" id="csyp_form_contact" action="//localhostlaravel/conecta/public/contact.php" method="POST" target="_blank" >
                
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group"> 
                            <label for="csyp_nombre"></label> 
                            <input placeholder="Ingresa tú nombre" class="form-control" id="csyp_nombre" name="csyp_nombre">
                        </div>
                        <div class="form-group"> 
                            <label for="csyp_empresa"></label> 
                            <input placeholder="Ingresa tú empresa" class="form-control" id="csyp_empresa" name="csyp_empresa">
                        </div>
                        <div class="form-group"> 
                            <label for="csyp_pais"></label> 
                            <input placeholder="Pais donde estas ubicado" class="form-control" id="csyp_pais" name="csyp_pais">
                        </div>
                        <div class="form-group"> 
                            <label for="csyp_numero"></label> 
                            <input placeholder="Ingresa un número de celular" class="form-control" id="csyp_numero" name="csyp_numero">
                        </div>
                         <div class="form-group"> 
                            <label for="csyp_correo"></label> 
                            <input placeholder="Ingresa tú correo" class="form-control" id="csyp_correo" name="csyp_correo">
                        </div>
                        <div class="form-group"> 
                            <label for="csyp_notas"></label> 
                            <input placeholder="Ingresa aqui tu necesidad" class="form-control" id="csyp_notas" name="csyp_notas">
                        </div> <div class="form-group" style="text-align: center"> 
                            
                            <input type="submit" class="btn btn-primary" value="Enviar" >
                        </div>
                    </div>
                </div>
            </div>
                <input type="hidden" id="csyp_token" name="csyp_token">
            </form>
            
               
       <!--     
            <div class="setings-item">
                    <span>
                        Collapse menu
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="collapsemenu">
                        <label class="onoffswitch-label" for="collapsemenu">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="setings-item">
                    <span>
                        Fixed sidebar
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="fixedsidebar" class="onoffswitch-checkbox" id="fixedsidebar">
                        <label class="onoffswitch-label" for="fixedsidebar">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="setings-item">
                    <span>
                        Top navbar
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="fixednavbar" class="onoffswitch-checkbox" id="fixednavbar">
                        <label class="onoffswitch-label" for="fixednavbar">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="setings-item">
                    <span>
                        Top navbar v.2
                        <br/>
                        <small>*Primary layout</small>
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="fixednavbar2" class="onoffswitch-checkbox" id="fixednavbar2">
                        <label class="onoffswitch-label" for="fixednavbar2">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="setings-item">
                    <span>
                        Boxed layout
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="boxedlayout" class="onoffswitch-checkbox" id="boxedlayout">
                        <label class="onoffswitch-label" for="boxedlayout">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="setings-item">
                    <span>
                        Fixed footer
                    </span>

                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" name="fixedfooter" class="onoffswitch-checkbox" id="fixedfooter">
                        <label class="onoffswitch-label" for="fixedfooter">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
-->
           <!--- <div class="title">Skins</div>
            <div class="setings-item default-skin">
                    <span class="skin-name ">
                         <a href="#" class="s-skin-0">
                             Default
                         </a>
                    </span>
            </div>
            <div class="setings-item blue-skin">
                    <span class="skin-name ">
                        <a href="#" class="s-skin-1">
                            Blue light
                        </a>
                    </span>
            </div>
            <div class="setings-item yellow-skin">
                    <span class="skin-name ">
                        <a href="#" class="s-skin-3">
                            Yellow/Purple
                        </a>
                    </span>
            </div>
            <div class="setings-item ultra-skin">
                    <span class="skin-name ">
                        <a href="md-skin.html" class="md-skin">
                            Material Design
                        </a>
                    </span>
            </div>  -->
        </div>
    </div>
</div>
<script>
    
    
    $("#csyp_form_contact").submit(function(){
            
            var frm = $(this).serialize();
            $(this).find("input").prop("disabled",true)            
            $.ajax({
            dataType: 'json',
            data: frm,
            method: "POST",
            url: '//operativo1.csyp.co/api/api_contact.php',
            success: function (msj) {
                toastr.success("Mensaje enviado correctamente")
            },
            complete: function () { 
                  
            },
            error: function (msj) {
                toastr.error('Error en el servidor');
            }
        })
        $(this).find("input").prop("disabled",false)       
        return false;
            
        })
    

    // Config box

    // SKIN Select
    $('.spin-icon').click(function (){
        $(".theme-config-box").toggleClass("show");
    });
 
</script>