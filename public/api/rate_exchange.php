<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/autoload.php";

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;



function get_rate($from, $to){
    
    try {




        $client = new Client([
            'headers' => ['Accept' => 'application/json'],
            'timeout' => 5, // Response timeout
            'connect_timeout' => 5, // Connection timeout
        ]);


        $headers = [
//            'Authorization' => 'Bearer ' . $_COOKIE["WP_TOKEN"],
            'Accept' => 'application/json',
        ];
        
        $exchange = $from."_".$to;
 
        $response = $client->request('GET', 'https://free.currconv.com/api/v7/convert?q='.$exchange .'&compact=ultra&apiKey=1f394451e145ff3d9816');



        if ($response->getBody()) {

            $resp = json_decode($response->getBody());

//            die(print_r($resp));
        return ($resp->$exchange) ?? false;
//            return true;
        }
    } catch (ClientException $e) {
        return false;
        return $e->getCode();
//        echo $e->getMessage() . "\n";
//        echo $e->getRequest()->getMethod();
        die();
    } catch (RequestException $e) {
        return false;
        return $e->getCode();
        echo $e->getMessage() . "\n";
        echo $e->getRequest()->getMethod();
        die();
    }



    
    
}



function get_rate_currencylayer($from,$to){
      try {


        $client = new Client([
            'headers' => ['Accept' => 'application/json'],
            'timeout' => 5, // Response timeout
            'connect_timeout' => 5, // Connection timeout
        ]);


        $headers = [
//            'Authorization' => 'Bearer ' . $_COOKIE["WP_TOKEN"],
            'Accept' => 'application/json',
        ];
        
        $exchange = $from."_".$to;
        
        
        $response = $client->request('GET', 'http://api.currencylayer.com/live?access_key=2567e23da524d13f04202cd373e555e5');


        if ($response->getBody()) {

            $resp = json_decode($response->getBody());

            die(print_r($resp));
        return $resp->$exchange;
//            return true;
        }
    } catch (ClientException $e) {
        echo $e->getMessage() . "\n";
//    echo $e->getCode() . "\n";
        echo $e->getMessage() . "\n";
        echo $e->getRequest()->getMethod();
        die();
    } catch (RequestException $e) {

    echo $e->getCode() . "\n";
        echo $e->getMessage() . "\n";
        echo $e->getRequest()->getMethod();
        die();
    }

    
    
    
    
}