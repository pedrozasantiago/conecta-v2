$("document").ready(function () {

    

    reiniciar_frm()

    $('#Nuevo').click(function () {

        $('#Clie_div').find('input, select, textarea').attr('readonly', false)
        $(this).attr('disabled', true)
        $("#Modificar").attr('disabled', true)
        $("#Borrar").attr('disabled', true)
        $("#Grabar").attr('disabled', false)
        $("#Consecutivo_id").attr('disabled', false)


        $('#Clie_div select, #Clie_div input, #Clie_div textarea').each(function () {
            $(this).val("")
        });

        $("#txtIdExterno").attr('readonly', true)
        $('#txtAccion_frm').val("A")
        $('#ClienCamp_frm').find("input[name=_method]").val("POST")
        $('#ClienCamp_frm').attr("action","../admin/clients")
    })

    $('#Modificar').click(function () {
        $('#Clie_div').find('input, select, textarea').attr('readonly', false)
//        $('#txtNit').attr('readonly', true)
//        $('#txtNombre').attr('readonly', true)

        $('#txtAccion_frm').val("M")
        $('#ClienCamp_frm').find("input[name=_method]").val("PUT")
        $('#ClienCamp_frm').attr("action","../admin/clients/"+$('#txtId').val())

        $(this).attr('disabled', true)
        $("#Nuevo").attr('disabled', true)
        $("#Borrar").attr('disabled', true)
        $("#Grabar").attr('disabled', false)
    })

    $('#Borrar').click(function () {
        $('#txtAccion_frm').val("B")
        $('#ClienCamp_frm').find("input[name=_method]").val("DELETE")
        $('#ClienCamp_frm').attr("action","../admin/clients/"+$('#txtId').val())

        $(this).attr('disabled', true)
        $("#Nuevo").attr('disabled', true)
        $("#Modificar").attr('disabled', true)
        $("#Grabar").attr('disabled', false)
    })

     

    $('#ClienCamp_frm').submit(function () {

        grabar_cliente()

        return false;
    });


})


function grabar_cliente(){
    
            $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").addClass("sk-loading")
        var frm = $('#ClienCamp_frm').serialize();
    
    $.ajax({
            dataType: 'json',
            data: frm,
            method: $('#ClienCamp_frm').attr('method'),
            url: $('#ClienCamp_frm').attr('action'),
            success: function (msj) {

                if (msj['error'].length > 0) {
                    $.each(msj['error'], function (key, value) {
                        toastr.error(value)
                    });
                }
                
 
                if (msj['OK'].length > 0) {

                    $.each(msj['OK'], function (key, value) {
                        toastr.success(value)
                    });
                    tabla_grid()

                }

            },
            complete: function () {
                $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").removeClass("sk-loading")
            },
            error: function (msj) {
                console.log(msj)
                toastr.error('Error en el servidor.')
            }
        })
    
    
}

function reiniciar_frm() {
    $('#Clie_div').find('input, select, textarea').attr('readonly', true)
    $('#Clie_div').find('select option').attr('readonly', true)
    $("#Nuevo").attr('disabled', false)
    $("#Modificar").attr('disabled', false)
    $("#Borrar").attr('disabled', false)
    $("#Grabar").attr('disabled', true)
    $("#Consecutivo_id").attr('disabled', true)
    $('#txtAccion_frm').val("")
    $('#txtActualiza_ID').val("")
    $('#ClienCamp_frm').find("input[name=_method]").val("")
}


function clientes_frm() {
    $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").addClass("sk-loading")
    cliente_activo = $('.active > td:nth-child(1)').html();
    // token = $("input[name='_token']").val()
    // var frm = {cliente_activo: cliente_activo, _token: token
    // };

 var frm = {cliente_activo: cliente_activo};
   
//    console.log(frm)
    $.ajax({
        dataType: 'json',
        method: 'POST',
        data: frm,
        url: '../admin/clients/clientes_frm',
        success: function (msj) {
            
            console.log(typeof msj['arreglo'] !== 'undefined')
            if(typeof msj['arreglo'] !== 'undefined') { 
                
                $('#txtId').val(msj["arreglo"]["id"])
                $('#txtNit').val(msj["arreglo"]["nit"])
                $('#txtNombre').val(msj["arreglo"]["name_company"])
                $('#txtSubdomain').val(msj["arreglo"]["subdomain"])
                $('#txtPrefix').val(msj["arreglo"]["prefix"])
                $('#txtCorreo').val(msj["arreglo"]["email"])
                
                $('#lstEstado').val(msj["arreglo"]["status"])
                $('#txtFechaCobro').val(msj["arreglo"]["next_billing_at"])
                $('#txtFechaFin').val(msj["arreglo"]["ends_at"])
                $('#txtLicencias').val(msj["arreglo"]["licencias"])
                $('#txtLicenciasGratis').val(msj["arreglo"]["licencias_gratis"])
            }else{
                $('#Clie_div').find('input,select,textarea').each(function () {
                    $(this).val("")
                });
            }


        },
        complete: function () {
            $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").removeClass("sk-loading")
            $('#tabs-0').find('div.alert.alert-info > p').html('<b>REGISTRO ACTIVO: </b>' + cliente_activo + '-' + $('.active > td:nth-child(3)').html());

        },
        error: function (msj) {
            console.log(msj)
            toastr.error('Error en el servidor.')
        }
    })
}


function lista_nombres(arreglo, id) {

    var contenido = "";
    for (i = 0; i < arreglo.length; i++) {
        contenido = contenido + "<option value='" + arreglo[i]['cedula'] + "' >" + arreglo[i]['nombre'] + "</option>"
    }

    $(id).append(contenido)

}

function listas(arreglo, tipo, id) {

    var contenido = "";
    for (i = 0; i < arreglo.length; i++) {
        if (arreglo[i]['tipo'] == tipo) {
            contenido = contenido + "<option value='" + arreglo[i]['codigo'] + "' >" + arreglo[i]['descrip'] + "</option>"
        }
    }
    $(id).append(contenido)

}