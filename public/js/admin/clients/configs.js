$(document).ready(function () {

    
    config_list();
    
  
 

})

function reiniciar_frm() {
    $('#ClienContactos_div').find('input, select, textarea').attr('readonly', true)
    $('#ClienContactos_div').find('select option').attr('readonly', true)
    $("#btnContacto_nuevo").attr('disabled', false)
    $("#btnContacto_modificar").attr('disabled', false)
    $("#btnContacto_grabar").attr('disabled', true)
    $('#txtContacto_accion').val("")
    $('#txtContacto_id').val("")
    // $("#fldFirma").val("")
}
 
 

function config_list() {
    $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").addClass("sk-loading")
    cliente_activo = $('.active').data("id")
 
 var frm = {cliente_activo: cliente_activo};
   
//    console.log(frm)
    $.ajax({
        dataType: 'json',
        method: 'GET',
        // data: frm,
        url: '../admin/clients/getConfigs/'+cliente_activo,
        success: function (msj) {
            msj['arreglo'] = msj['configs'];
            var contenido = "<table class='table table-responsive hovertable'>";

            for (i = 0; i < msj['arreglo'].length; i++) {
                contenido +=  '<div class="config-container">'
                contenido += '<div class="config-item">'
                contenido += '<div class="config-label">'+msj['arreglo'][i]['name']+'</div>'
                contenido += '<div class="config-description">'
                contenido += msj['arreglo'][i]['value']
                contenido += '</div>'
                contenido += '<div class="config-type">Tipo: <span>'+msj['arreglo'][i]['type']+'</span></div>'
                contenido += '</div>'
                contenido += '<div class="config-inputs">'
                contenido += '<b>Cliente</b>'
                if(msj['arreglo'][i]['type'] == 'array') {
                    contenido += '<textarea class="form-control text_config" id="txtConfig_CLIE_CONFIG">'
                    contenido += msj['arreglo'][i]['valor']
                    contenido += '</textarea>'
                }else{
                    contenido += '<input type="text" class="form-control" id="txtConfig_CLIE_CONFIG" value="'+msj['arreglo'][i]['valor']+'">'
                }
                contenido += '<b>Default</b>'
                if(msj['arreglo'][i]['type'] == 'array') {
                    contenido += '<textarea class="form-control text_config" id="txtConfig_CLIE_CONFIG">'
                    contenido += msj['arreglo'][i]['default']
                    contenido += '</textarea>'
                }else{
                    contenido += '<input type="text" class="form-control" id="txtConfig_CLIE_CONFIG" value="'+msj['arreglo'][i]['default']+'">'
                }
                contenido += '</div>'
                contenido += '</div>'

            }



            contenido = contenido + "</table>"
            $('#tabla_contactos').html(contenido)
            prettyPrint()
            reiniciar_frm()


        },
        complete: function () {
            $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").removeClass("sk-loading")
        },
        error: function (msj) {
          
        }
    })
}

function prettyPrint() {
try{
    $( ".text_config" ).each(function( index, element ) {

        var ugly = $( this ).val();
        var obj = JSON.parse(ugly);
        var pretty = JSON.stringify(obj, undefined, 4);
        $( this ).text(pretty)

      });

}catch(e){

}
}