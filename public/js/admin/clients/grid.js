$("document").ready(function () {

    (function () {
        $("#pagina_activa").html("<h2><b>Pantalla Clientes</b></h2>")
    })();

    (function () {
        tabla_grid();
    })();

    $("#btn_buscar").click(function () {
        tabla_grid()
        return false;
    });
    
     $("#Clientes_grid_ppal").find('select').change(function () {

        $(this).val() != '' ? $(this).addClass('S') : $(this).removeClass('S')

    })
    
    $("#Clientes_grid_ppal").find('select').each(function (key, value) {
        $(this).val() != '' ? $(this).addClass('S') : $(this).removeClass('S')
    })


})




function tabla_grid(activo) {
//    $('#grid_tabla').html('<div><img height="50" src="assets/img/loading.gif"/></div>');
    $('#grid_tabla').html('<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>');
    var frm = $("#Clientes_grid_ppal").serialize();
    $.ajax({
        dataType: 'json',
        method: 'POST',
        data: frm,
        url: '../admin/clients/grid_ppal',
        success: function (msj) { 
            var contenido = "<a class='btn btn-primary' href='javascript:void(0);' onclick=\"javascript:ExportToExcel('table_report','reporte_do')\" >EXCEL</a>"
            contenido += "<table  id='table_report' class='table table-responsive hovertable'>";
            contenido = contenido + "<thead style='position: sticky; top: 0; background-color: white;  z-index: 10;'><tr>\n\
            <th>ID</th>\n\
            <th>NIT</th>\n\
            <th>NOMBRE EMPRESA</th>\n\
            <th>SUBDOMINIO</th>\n\
            <th>HOSTNAME</th>\n\
            <th>ESTADO</th>\n\
            <th>FACTURAR</th>\n\
            <th>FINALIZA</th>\n\
            <th>LICENCIAS</th>\n\
            <th>EN USO</th>\n\
            <th>REQ</th>\n\
            <th>COT</th>\n\
            <th>DO</th>\n\
            <th>FACT</th>\n\
            <th>COMPRAS</th>\n\
            <th>CONTENEDORES</th>\n\
            <th>LOGS</th>\n\
            </tr></thead>";
            for (i = 0; i < msj['arreglo'].length; i++) {

                let website = msj['arreglo'][i]['website']['id'] ?? ""

                contenido = contenido + "<tr  data-website='" + website + "' data-id='" + msj['arreglo'][i]['id'] + "'   >\n\
                <td>" + msj['arreglo'][i]['id'] + "</td>\n\
                <td>" + msj['arreglo'][i]['nit'] + "</td>\n\
                <td>" + msj['arreglo'][i]['name_company'] + "</td>\n\
                <td>" + msj['arreglo'][i]['subdomain'] + "</td>\n\
                <td>" + msj['arreglo'][i]['fqdn'] + "</td>\n\
                <td>" + msj['arreglo'][i]['status'] + "</td>\n\
                <td>" + msj['arreglo'][i]['next_billing_at'] + "</td>\n\
                <td>" + msj['arreglo'][i]['ends_at'] + "</td>\n\
                <td>" + msj['arreglo'][i]['licencias'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['users'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['requests'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['quotes'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['shipments'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['invoices'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['purchases'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['containers'] + "</td>\n\
                <td>" + msj['arreglo'][i]['data']['logs'] + "</td>\n\
                </tr>";
            }
            contenido = contenido + "</table>"
            $('#grid_tabla').html(contenido)
        },
        complete: function () {
            if (activo == undefined) {
                $('#grid_tabla > table > tbody > tr:nth-child(1)').addClass("active")
            } else {
                $('#grid_tabla > table > tbody > tr[data-cliente_id="' + activo + '"]').addClass("active")
            }

            $('#grid_tabla').find('.table tr').off("click")
            $('#grid_tabla').find('.table tr').click(function () {
                $('#grid_tabla').find('.table .active').removeClass('active')
                $(this).addClass("active")
               
                funciones_frms()
                
            })

             funciones_frms()
                

        },
        error: function (msj) {
            console.log(msj)
            toastr.error('Error en el servidor.')
        }
    })
}

function funciones_frms(){
    
     try {clientes_frm()} catch (err) {}
                try {reiniciar_frm()} catch (err) {}
                try {infogral()} catch (err) {}
                try {doclegal()} catch (err) {}
                try {docaduana()} catch (err) {}
                try {tabla_contactos()} catch (err) {}
                try {config_list()} catch (err) {}
                
    
}
