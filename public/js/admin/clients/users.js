$("document").ready(function () {

    
    clientes_frm();
    
  
 

})

function reiniciar_frm() {
    $('#ClienContactos_div').find('input, select, textarea').attr('readonly', true)
    $('#ClienContactos_div').find('select option').attr('readonly', true)
    $("#btnContacto_nuevo").attr('disabled', false)
    $("#btnContacto_modificar").attr('disabled', false)
    $("#btnContacto_grabar").attr('disabled', true)
    $('#txtContacto_accion').val("")
    $('#txtContacto_id').val("")
    // $("#fldFirma").val("")
}
 
 

function clientes_frm() {
    $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").addClass("sk-loading")
    cliente_activo = $('.active').data("id")
    // token = $("input[name='_token']").val()
    // var frm = {cliente_activo: cliente_activo, _token: token
    // };

 var frm = {cliente_activo: cliente_activo};
   
//    console.log(frm)
    $.ajax({
        dataType: 'json',
        method: 'GET',
        // data: frm,
        url: '../admin/clients/getUsers/'+cliente_activo,
        success: function (msj) {
            msj['arreglo'] = msj['users'];
            var contenido = "<table class='table table-responsive hovertable'>";
            contenido = contenido + "<thead><tr>\n\
            <th>CEDULA</th>\n\
            <th>NOMBRE</th>\n\
            <th>USUARIO</th>\n\
            <th>CORREO</th>\n\
            <th>CARGO</th>\n\
            <th>ESTADO</th>\n\
            <th>CELULAR</th>\n\
            <th>ROL</th>\n\
            <th>COORDINADOR</th>\n\
            <th>COMERCIAL</th>\n\
            <th>PRICING</th>\n\
            <th>ADTIVO</th>\n\
            <th>WHATSAPP</th>\n\
            </tr></thead>";
            for (i = 0; i < msj['arreglo'].length; i++) {
                contenido = contenido + "<tr>\n\
                <td>" + msj['arreglo'][i]['cedula'] + "</td>\n\
                <td>" + msj['arreglo'][i]['nombre'] + "</td>\n\
                <td>" + msj['arreglo'][i]['susuario'] + "</td>\n\
                <td>" + msj['arreglo'][i]['correo'] + "</td>\n\
                <td>" + msj['arreglo'][i]['cargo'] + "</td>\n\
                <td>" + msj['arreglo'][i]['perfil'] + "</td>\n\
                <td>" + msj['arreglo'][i]['telefono'] + "</td>\n\
                <td>" + msj['arreglo'][i]['rol'] + "</td>\n\
                <td>" + msj['arreglo'][i]['coordinador'] + "</td>\n\
                <td>" + msj['arreglo'][i]['comercial'] + "</td>\n\
                <td>" + msj['arreglo'][i]['pricing'] + "</td>\n\
                <td>" + msj['arreglo'][i]['adtivo'] + "</td>\n\
                <td>" + msj['arreglo'][i]['whatsapp'] + "</td>\n\
                </tr>";
            }
            contenido = contenido + "</table>"
            $('#tabla_contactos').html(contenido)

            reiniciar_frm()


        },
        complete: function () {
            $("#" + $('ul[role="tablist"] > li[aria-selected="true"] > a').attr("aria-controls")).find(".ibox-content").removeClass("sk-loading")
        },
        error: function (msj) {
          
        }
    })
}

 