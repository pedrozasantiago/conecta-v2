$('document').ready(function () {

    $('#lstPregunta').select2();


     (function () {
         cambio_modulo();

    })()
    
    iniciar_tiny()
    
    
    $('#software').change(function () {
       cambio_modulo();
    })
    
    
    $('#lstPregunta').change(function () {
    $("#frmFaqs").find(".ibox-content").addClass("sk-loading")
    tinyMCE.get('respuesta').setContent("");
        frm = $('#lstPregunta').serialize()
        $.ajax({
            data: frm,
            method: 'POST',
            dataType: 'json',
            url: '../admin/faqs/response',
            success: function (msj) {
                if(msj['respuesta'] != null){
                    tinyMCE.get('respuesta').setContent(msj['respuesta']['respuesta']);    
                }
            },
            complete: function(){
                
              $("#frmFaqs").find(".ibox-content").removeClass("sk-loading")  
            },
            error: function (msj) {
                console.log(msj)
            },
        })
        $('#frmFaqs').find("input[name=_method]").val("PUT")
        $('#frmFaqs').attr("action", "../admin/faqs/" + $('#lstPregunta').val())
    })



    $('#frmFaqs').submit(function () {

        $("#frmFaqs").find(".ibox-content").addClass("sk-loading")
        tinymce.triggerSave();
        var frm = $('form#frmFaqs').serialize();

        $.ajax({
            dataType: 'json',
            data: frm,
            method: $('#frmFaqs').attr('method'),
            url: $('#frmFaqs').attr('action'),
            success: function (msj) {

                if (msj['error'].length > 0) {

                    $.each(msj['error'], function (key, value) {
                        toastr.error(value)
                    });

                }

                if (msj['OK'].length > 0) {

                    $.each(msj['OK'], function (key, value) {
                        toastr.success(value)
                    });


//                    contactos_frm()
//                    reiniciar_frm_contacto()


                }

            },
            complete: function () {
//                tabla_grid()
                $("#frmFaqs").find(".ibox-content").removeClass("sk-loading")
            },
            error: function (msj) {
                console.log(msj)
                toastr.error('Error en el servidor.');
            }
        })


        return false;
    });

})

function iniciar_tiny() {
$("#frmFaqs").find(".ibox-content").addClass("sk-loading")

    tinymce.init({
        paste_data_images: true,
        automatic_uploads: true,
        images_upload_url: '../admin/faqs/cargar_img',
        // basic tinyMCE stuff
        selector: "textarea",
        language: 'es_MX',
        theme: 'modern',
        readonly: false,
        menubar: false,
        forced_root_block: false,
        toolbar1: 'fontselect  | code restoredraft undo redo removeformat  styleselect  bold italic  alignleft aligncenter alignright alignjustify bullist numlist ',
        toolbar2: 'variables outdent indent print preview forecolor backcolor ',
        statusbar: true,
        plugins: "variable paste save code lists preview print",
        content_css: '../css/style.css',
        variable_valid: ["hostname"],
        variable_prefix: "{%",
        variable_suffix: "%}",
       variable_mapper: {
                    hostname: 'Hostname'
                },  
        setup: function (ed)
        {  
            ed.addButton('variables', {
                type: 'menubutton',
                text: 'Agregar variable',
                menu: [
                    {
                      text: "Hostname",  
                      content: "{%hostname%}",
                      onclick: function () { 
                          ed.insertContent(this.settings.content);
                      }
                    }, 
              ]
            });
            
        },  
    });
$("#frmFaqs").find(".ibox-content").removeClass("sk-loading")

}

function cambio_modulo() {
    $('#lstPregunta').select2("val", "");
    $('#lstPregunta').prop('disabled', true);
    
    $("#frmFaqs").find(".ibox-content").addClass("sk-loading")
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {modulo:$("#software").val()},
        url: '../admin/faqs/grid',
        success: function (msj) {

            contenido = '';
            $.each(msj['preguntas'], function (key, value) {
                contenido += '<optgroup label="' + key + '">';
                $.each(value, function (key1, value2) {
                    contenido += "<option value='" + value2['id'] + "' >" + "ID " + value2['id'] + ' - ' + value2['pregunta'] + "</option>"
                })
                contenido += '</optgroup>'
            });
            $('#lstPregunta').html(contenido)
        },
        complete: function () {
            $('#lstPregunta').prop('disabled', false);
$("#frmFaqs").find(".ibox-content").removeClass("sk-loading")
        },
        error: function (r) {
            console.log(r)
            toastr.error('Error cargando la lista de campañas')
        }
    })
}