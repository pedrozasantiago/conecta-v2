$("document").ready(function () {

    (function () {
        $("#pagina_activa").html("<h2><b>Pantalla Clientes</b></h2>")
        $("title").html("OPERACIONES - CLIENTES")
    })();

    (function () {
        $.ajax({
            dataType: 'json',
            url: '../operative/clients/grid',
            success: function (msj) {

                if (msj['msj'][0] == 'error') {
                    toastr.error('Error cargando las listas del men�.')
                    console.log(msj)
                } else {
//                    console.log(msj)

                    for (i = 0; i < msj['arreglo'].length; i++) {

                        if (msj['arreglo'][i]['tipo'] == "TIPO_CLIENTE") {
                            var contenido = "";
                            contenido = "<option value='" + msj['arreglo'][i]['codigo'] + "' >" + msj['arreglo'][i]['descrip'] + "</option>"
                            $('#lstTipoCBC').append(contenido)
                        }

                        if (msj['arreglo'][i]['tipo'] == "ESTADOCLIEN") {
                            var contenido = "";
                            contenido = "<option value='" + msj['arreglo'][i]['codigo'] + "' >" + msj['arreglo'][i]['descrip'] + "</option>"
                            $('#lstEstadoCBC').append(contenido)
                        }

                        if (msj['arreglo'][i]['tipo'] == "ESTADOINFO") {
                            var contenido = "";
                            contenido = "<option value='" + msj['arreglo'][i]['codigo'] + "' >" + msj['arreglo'][i]['descrip'] + "</option>"
                            $('#lstEstadoInfoBC').append(contenido)
                        }
                    }
                     $('#lstTipoCBC').val("RATIFICADO").addClass('S')
                }

            },
            complete: function () {
//                $('#lstEstadoBO').find('option[value="ABIERTA"]').attr('selected',true)
//                tabla_grid();
            },
            error: function (msj) {
                console.log(msj)
                toastr.error('Error en el servidor.')
            }
        })
    })();

    $("#btn_buscar").click(function () {
//        tabla_grid()
        return false;
    });
    
     $("#Clientes_grid_ppal").find('select').change(function () {

        $(this).val() != '' ? $(this).addClass('S') : $(this).removeClass('S')

    })
    
    $("#Clientes_grid_ppal").find('select').each(function (key, value) {
        $(this).val() != '' ? $(this).addClass('S') : $(this).removeClass('S')
    })


})

function tabla_grid(activo) {
//    $('#grid_tabla').html('<div><img height="50" src="assets/img/loading.gif"/></div>');
    $('#grid_tabla').html('<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>');
    var frm = $("#Clientes_grid_ppal").serialize();
    $.ajax({
        dataType: 'json',
        method: 'POST',
        data: frm,
        url: '../operative/clients/grid_ppal',
        success: function (msj) {
            console.log(msj)
            if (msj['msj'][0] == 'error') {
                toastr.error('Error cargando la lista de clientes.')
                console.log(msj['msj'])
            }  
            
            if (msj['error'].length > 0) {
                $.each(msj['error'], function (key, value) {
                    toastr.error(value)
                });
            }
            
            
                var contenido = "<table class='table table-responsive hovertable'>";
                contenido = contenido + "<thead><tr>\n\
                <th>ID</th>\n\
                <th>ID publico</th>\n\
                <th>CLIENTE</th>\n\
                <th>ASESOR</th>\n\
                <th>TIPO</th>\n\
                <th>ESTADO</th>\n\
                <th>ESTADOINFO</th>\n\
                <th>OBSERVACIONES</th>\n\
                </tr></thead>";
                for (i = 0; i < msj['arreglo'].length; i++) {
                    contenido = contenido + "<tr data-cliente_id='" + msj['arreglo'][i]['cedula'] + "' >\n\
                    <td>" + msj['arreglo'][i]['cedula'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['id_externo'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['nombre'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['nom_asesor'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['tipo_cliente'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['estado'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['estado_info'] + "</td>\n\
                    <td>" + msj['arreglo'][i]['observaciones'] + "</td>\n\
                    </tr>";
                }
                contenido = contenido + "</table>"
                $('#grid_tabla').html(contenido)
            
        },
        complete: function () {
            if (activo == undefined) {
                $('#grid_tabla > table > tbody > tr:nth-child(1)').addClass("active")
            } else {
                $('#grid_tabla > table > tbody > tr[data-cliente_id="' + activo + '"]').addClass("active")
            }

            $('#grid_tabla').find('.table tr').off("click")
            $('#grid_tabla').find('.table tr').click(function () {
                $('#grid_tabla').find('.table .active').removeClass('active')
                $(this).addClass("active")
               
                funciones_frms()
                
            })

             funciones_frms()
                

        },
        error: function (msj) {
            console.log(msj)
            toastr.error('Error en el servidor.')
        }
    })
}

function funciones_frms(){
    
     try {clientes_frm()} catch (err) {}
                try {reiniciar_frm()} catch (err) {}
                try {infogral()} catch (err) {}
                try {doclegal()} catch (err) {}
                try {docaduana()} catch (err) {}
                try {tabla_contactos()} catch (err) {}
    
}