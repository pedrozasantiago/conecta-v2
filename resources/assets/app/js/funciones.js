$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).idleTimer(15000);

    $(function () {

        enlace = getParameterByName("enl_id")

//        $("a[href*='"+enlace+"']").parent("li").addClass("active")        
//        $("a[href*='"+enlace+"']").parent().removeClass("active").parent().parent("li").addClass("active")     
        $('.footable').footable();
    })

    $(".tabs").steps({
        headerTag: "h3",
        bodyTag: "section",
        contentContainerTag: "div",
        transitionEffect: "slideLeft",
//        autoFocus: true,
        enableKeyNavigation: false,
        enableAllSteps: true,
        onInit: function (event, currentIndex) {
            $("li[role='tab']").addClass("done")
            $("li[role='tab'].current").removeClass("done")
        },
//        suppressPaginationOnFocus: false,
        saveState: true,
        enablePagination: false,
        labels: {
            cancel: "Cancelar",
            current: "Actual paso:",
            pagination: "Paginacion",
            finish: "Finalizar",
            next: "Siguiente",
            previous: "Anterior",
            loading: '<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>'
        }
//        cssClass: "tabcontrol"
    });



    $(function () {
        $(".txtfecha").datepicker({
            language: "es",
            altFormat: "dd/mm/yyyy",
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            todayHighlight: true,
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
    });
 




//   $(".lstselect2").change(function () {
//            console.log($(this).val())
//            $(this).trigger('change')
//
//        })

//        $(".lstselect2").on("change", function (e) { 
//            console.log("change"); 
//        });
//
//
//    $(function () {
//
//     
//
//        $(".lstselect2").select2();
//
//
//
//    });

    $(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

//        $('.timepicker').timepicker({
//            timeFormat: 'HH:mm',
//            dynamic: false,
//            dropdown: true,
//            scrollbar: true
//        });
    });


});

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function actualizar_trm() {


    $("body").find("#trm_modal").remove()
    $("body").append("<div id='trm_modal' class='modal fade' ></div>")

    $("#trm_modal").load("../comercial/trm/index.php", function () {

        $("#trm_modal").modal()

    })

}

function abrir_calendario() {
    cadena = '';
    $("body").find("#calendar_modal").remove()

    $("body").append("<div id='calendar_modal' class='modal fade' ></div>")
    cadena = '<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header">'
    cadena = cadena + '   <button type="button" class="close" data-dismiss="modal">&times;</button>'
    cadena = cadena + '<h4 class="modal-title">CALENDARIO</h4></div>'
    cadena = cadena + '<div class="modal-body"></div><div class="modal-footer">'
    cadena = cadena + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div>';

    $("#calendar_modal").html(cadena)

    $("#calendar_modal").find('.modal-body').load("calendarioX/index.php", function () {
        $("#calendar_modal").modal()
        $("#calendar_modal").on('hide.bs.modal', function () {
            $(".popover").popover('hide');
        });
    })
}

function formulario_citas() {

    cadena = '';
    $("body").find("#citas_modal").remove()

    $("body").append("<div id='citas_modal' class='modal fade' ></div>")
    cadena = '<form name="cita_formulario" id="cita_formulario">\n\
                <div class="modal-dialog modal-lg">\n\
                <div class="modal-content ibox-content">\n\
                <div class="sk-spinner sk-spinner-chasing-dots">\n\
                    <div class="sk-dot1"></div><div class="sk-dot2"></div></div>\n\
                    <div class="modal-header">'
    cadena = cadena + '   <button type="button" class="close" data-dismiss="modal">&times;</button>'
    cadena = cadena + '<h4 class="modal-title"><b>CREAR CITA</b></h4></div>'
    cadena = cadena + '<div class="modal-body"></div><div class="modal-footer">'
    cadena = cadena + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                        <button type="submit" class="btn btn-primary">Crear</button>\n\
                </div></div></div></form>';

    $("#citas_modal").html(cadena)

    $("#citas_modal").find('.modal-body').load("citas/formulario.html", function () {

        $("#citas_modal").modal()
        $(".txtfecha").datepicker({
            language: "es",
            altFormat: "dd/mm/yyyy",
            format: "dd/mm/yyyy",
            todayBtn: "linked",
            todayHighlight: true,
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
        $('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            startTime: '08:00',
            defaultTime: new Date(0, 0, 0, 9, 0, 0),
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            zindex: 2210
        });

        var d = new Date();
        var dia = ("0" + d.getDate()).slice(-2)
        var mes = ("0" + (d.getMonth() + 1)).slice(-2)
        var ano = d.getFullYear()
//
//
//        var hora = (d.getHours() < 10 ? '0' : '') + d.getHours()
//        var minutos = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes()
//
        $("#txtFechaIni_task").val(dia + "/" + mes + "/" + ano)
        $("#txtFechaFin_task").val(dia + "/" + mes + "/" + ano)
//        $("#txtHoraIni_task").val(hora + ":" + minutos)
//        $("#txtHoraFin_task").val(++hora + ":" + minutos)

    })


}

function descargarGrid(id_tabla, nombre) {
    var f = new Date();
    f = "" + f.getFullYear() + (f.getMonth() + 1) + f.getDate() + f.getHours() + f.getMinutes()
    //Creamos un Elemento Temporal en forma de enlace
    var tmpElemento = document.createElement('a');
    // obtenemos la información desde el div que lo contiene en el html
    // Obtenemos la información de la tabla
    var data_type = 'data:application/vnd.ms-excel';
    var tabla_div = document.getElementById(id_tabla);
    var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
    tmpElemento.href = data_type + ', ' + tabla_html;
    //Asignamos el nombre a nuestro EXCEL
    tmpElemento.download = nombre + f + '.xls';
    // Simulamos el click al elemento creado para descargarlo
    tmpElemento.click();
}

/*$(document).on("idle.idleTimer", function (event, elem, obj) {
 toastr.options = {
 "positionClass": "toast-top-right",
 "timeOut": 8000
 }
 
 toastr.warning('You can call any function after idle timeout.', 'Idle time');
 $('.custom-alert').fadeIn();
 $('.custom-alert-active').fadeOut();
 
 });*/

$(document).on("active.idleTimer", function (event, elem, obj, triggerevent) {
    toastr.options = {
        "positionClass": "toast-top-right",
        "timeOut": 5000
    }

    // function you want to fire when the user becomes active again
    toastr.clear();
    $('.custom-alert').fadeOut();
    user = $("#side-menu > li.nav-header > div.dropdown.profile-element > a > span > span.block.m-t-xs > strong").html();
    toastr.success('Usted está en  CONECTA COMERCIAL', '');
    toastr.success(user, 'Bienvenido nuevamente');
//    notificaciones()



});

function notificaciones() {

    var alertas = []

    $.ajax({
        url: 'calendarioX/server/notificaciones.php',
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        type: 'GET',
        success: (msj) => {
            console.log(msj)

            if (msj['error'].length > 0) {
                $.each(msj['error'], function (key, value) {
                    toastr.error(value)
                });
            }
            alertas = msj['arreglo'];
            console.log(alertas)
            var contenido = "";
            if (msj['arreglo'].length > 0) {
                $.each(msj['arreglo'], function (key, value) {
                    contenido += "<li> \n\
                    <div class='dropdown-messages-box'> \n\
                    <div class='media-body'> \n\
                    <small class='pull-right'>" + value['diferencia'] + " días</small> \n\
                    <strong>" + value['nom_cliente'] + "</strong> " + value['cita'] + " <br>  " + value['descripcion'] + " <br> \n\
                    <small class='text-muted'>Inicio " + value['fecha_ini'] + " - \n\
                    Fin " + value['fecha_fin'] + "</small> \n\
                    </div></div></li> <li class='divider'></li>";
                });
            }


            contenido += "<li><div class='text-center link-block'> \n\
                            <a href='javascript:abrir_calendario()'><i class='fa fa-calendar'></i>\n\
                            <strong>Ver calendario</strong></a></div></li>"


            $("#inicio_count_notif").html("<i class='fa fa-bell'></i>  <span class='label label-warning'>" + msj['arreglo'].length + "</span>")



            $("#inicio_notificaciones").html(contenido)

        }, complete: function (data) {


            // Comprobamos si el navegador soporta las notificaciones
            if (!("Notification" in window)) {
                alert("Este navegador no soporta notificaciones de CONECTA CARGA.\n\
         Utiliza google Chrome preferiblemente para recibir notificaciones en el escritorio.");
            }    // Comprobamos si ya nos habían dado permiso
            else if (Notification.permission === "granted") {
                // Si esta correcto lanzamos la notificación
                var notification = spawnNotification("Tienes un total de " + alertas.length + " citas programadas.", "Notificacion CONECTA CARGA")

            }

            // Si no, tendremos que pedir permiso al usuario
            else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    // Si el usuario acepta, lanzamos la notificación
                    if (permission === "granted") {
                        var notification = spawnNotification("Tienes un total de " + alertas.length + " citas programadas.", "Notificacion CONECTA CARGA")
                    }
                });
            }

            // Finalmente, si el usuario te ha denegado el permiso y 
            // quieres ser respetuoso no hay necesidad molestar más.



        },
        error: function (data) {
            console.log(data)
            alert("error en la comunicación con el servidor");
        }
    })

}

function spawnNotification(theBody, theTitle) {
    var options = {
        body: theBody,
        icon: $("#side-menu > li.nav-header > div.dropdown.profile-element > span > img").attr("src")
    }
    var n = new Notification(theTitle, options);
    setTimeout(n.close.bind(n), 5000);
}