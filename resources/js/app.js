require('./bootstrap')
import Vue from 'vue'

// indexeddb
import {SchemaSyncHandler} from './schemasync'
SchemaSyncHandler.sync()

//vuex
import Vuex from 'vuex'
Vue.use(Vuex)
//.vuex


//Bootstrap Vue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
//.Bootstrap Vue


//VueCurrencyFilter
import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter,
{
  symbol : '',
  thousandsSeparator: '.',
  fractionCount: 0,
  fractionSeparator: ',',
  symbolPosition: 'front',
	symbolSpacing: true
})
//.VueCurrencyFilter

//Popper
//import Popper from 'vue-popperjs';
//Vue.use(Popper);
import 'vue-popperjs/dist/css/vue-popper.css'
//.Popper

//Autocomplete
import 'vue-instant/dist/vue-instant.css'
import VueInstant from 'vue-instant'
Vue.use(VueInstant)
//.Autocomplete

//SweetAlert
import VueSweetalert2 from 'vue-sweetalert2'
const options = {
  confirmButtonColor: '#41AF4B',
  cancelButtonColor: '#cf423b'
}
Vue.use(VueSweetalert2, options)
//.SweetAlert

// VueToast
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/index.css'
Vue.use(VueToast, {
  position: 'top'
})
//.VueToast

// PerfectScrollbar
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
Vue.use(PerfectScrollbar)
//.PerfectScrollbar

// Mask Money
import money from 'v-money'
Vue.use(money, {
  decimal: ',',
  thousands: '.',
  precision: 0
})
//.Mask Money

//VueBarcodeScanner
import VueBarcodeScanner from 'vue-barcode-scanner'
Vue.use(VueBarcodeScanner)
//.VueBarcodeScanner


//Print Directive
import Print from './directives/print'
Vue.use(Print)
//.Print Directive


//Print Ticket
import PrintTicket from './plugins/print-ticket'
Vue.use(PrintTicket)
//.Print Ticket


//Modulos
import { state, actions, getters, mutations } from './store/store'
import modSell from './store/modules/sell'
import modCashClosures from './store/modules/cash_closures'
import modClients from './store/modules/clients'
import modStockEntries from './store/modules/stock_entries'

// Modules Ecommerce
import modEcommerceMenus from './store/modules/ecommerce/menus'

//almacén global de datos con vuex
export const store = new Vuex.Store({
	state,
	actions,
  getters,
	mutations,
	modules: {
    mod_sell: modSell,
    mod_clients: modClients,
    mod_cash_closures: modCashClosures,
    mod_stock_entries: modStockEntries,
    mod_ecommerce_menus: modEcommerceMenus
  }
});
//.almacén global de datos con vuex

// componentes
Vue.component('vue-instant', VueInstant.VueInstant)
Vue.component('sell', require('./components/SellApp.vue'))
Vue.component('cash-closure-open', require('./components/cash-closures/Open.vue'))
Vue.component('cash-closure-close', require('./components/cash-closures/Close.vue'))

// inventory
Vue.component('header-stock-entries', require('./components/stock/HeaderEntries.vue'))
Vue.component('stock-entries', require('./components/stock/Entries.vue'))


// Ecommerce
Vue.component('ecommerce-menus', require('./components/ecommerce/menus/MenusApp.vue'))


// renderizar la app
const app = new Vue({
    el: '#app',
    store,
})
