<?php

return [
    'token_failed' => 'El Token de seguridad no es valido.',
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos fallidos en muy poco tiempo. Por favor intente de nuevo en :seconds segundos.',

    
    'login-title' => 'Iniciar Sesión',
    'login' => 'Iniciar Sesión',
    'username' => 'Usuario',
    'email' => 'Email',
    'password' => 'Contraseña',
    'password-confirmation' => 'Confirmar Contraseña',
    'forgot-password' => 'Recuperar contraseña',
    'forgot-password-title' => 'Recuperar contraseña',
    'send-forgot-password' => 'Enviar email',
    'forgot-password-desc' => 'Ingresa tu correo electrónico y haz clic en el botón enviar email. recibirás un correo con los pasos para recuperar tu contraseña.',    

    'reset-password-title' => 'Recuperar contraseña',
    'reset-password-desc' => 'Ingresa tu email y asigna una nueva contraseña para inciar sesión',
    'send-reset-password' => 'Confirmar',

    'app_account_error' => 'Esta cuenta no existe',
    'app_error_permission' => 'No tienes acceso a la aplicación movil',
    'api_login_error' => 'Error al ingresar al sistema. Vuelve a intentarlo.',
    




];
