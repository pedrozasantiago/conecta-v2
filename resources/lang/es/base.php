<?php

return [
    'home'                      => 'Inicio',
    'estimate'                  => 'Cotización',
    'sale'                      => 'Factura',
    'invoice'                   => 'Factura',
    'order'                     => 'Orden De Compra',
    'purchase'                  => 'Compra',
    'date_from'                 => 'Fecha desde',
    'date_to'                   => 'Fecha hasta',
    'select_store_success'      => 'Cambio de sucursal realizado correctamente',
    'error_no_store'            => 'Debe seleccionar un punto de venta',
    'error_no_register'         => 'Debe seleccionar una caja registradora',
    'loading_text'				=> 'Cargando, por favor espere...',
    'error_load_datatable'		=> 'Hubo un error al cargar la tabla. actualize la pagina para ver los cambios',
    'ajax_processing'			=> 'Procesando su solicitud',
    'ajax_failure'				=> 'Hubo un error al procesar la solicitud. por favor vuelve a intentarlo',
    'submit_processing_text'    => '<i class="fa fa-spinner spinner"></i> Procesando',
    'need_help'                 => '¿Necesitas Ayuda?',
    'without-desc'              => 'Sin descripción',
    'yes'                       => 'Si',
    'no'                        => 'No',
    'actions'                   => 'Acciones',
    'back'                      => 'Regresar',
    'print'                     => 'Imprimir',
    'send'                      => 'Enviar',
    'close'                     => 'Cerrar',
    'cancel'               		=> 'Cancelar',
    'save'               		=> 'Guardar',
    'pay'                       => 'Pagar',
    'confirm'                   => 'Confirmar',
    'change'                    => 'Cambiar',
    'create_report'             => 'Generar Reporte',
    'export_report'             => 'Exportar',
    'errors'                    => 'Errores',
    'title_alert'      			=> '¿Estás Seguro?',
    'error_deleted'				=> 'Error al eliminar',
    'error_restored'            => 'Error al restaurar',
    'delete_error'              => 'No se pudo eliminar. por favor intentelo nuevamente.',
    'restore_error'             => 'No se pudo restaurar. por favor intentelo nuevamente.',
    'cancelled'                 => 'Cancelado',
    'deleted'					=> 'Eliminado',
    'restored'                  => 'Restaurado',
    
    
    /* notificaciones */     
    'notifications'             => 'Notificaciones',
    'notifications_news'        => 'Nuevas',
    'read_all_notifications'    => 'Ver todas las notificaciones',
    'message_send_success'      => 'Muchas gracias por contactarnos. Hemos recibido su mensaje y en el transcurso del día lo estaremos contactando.',
    

    'select_state'              => 'Seleccionar Departamento',
    'select_city'               => 'Seleccionar Ciudad',
    'select_commune'            => 'Seleccionar Comuna',
    'select_neighborhood'       => 'Seleccionar Barrio',

    /* Errores */

    'go_home'                   => 'Regresar al inicio',
    '404_error'                 => 'Página no encontrada.',
    '404_error_desc'            => 'Esta página no existe en nuestros servidores.',

    '403_error'                 => 'Acceso Restringido.',
    '403_error_desc'            => 'Usted no tiene accedo para ingresar a esta sección.',

    '500_error'                 => 'Error de servidor.',
    '500_error_desc'            => 'Tuvimos un problema con nuestro servidor. Por favor intentalo nuevamente',


    'status'                    => 'Estado',
    'status-active'             => 'Activos',
    'status-trashed'            => 'Eliminados',
    'published'                 => 'Publicado',
    'unpublished'               => 'No publicado',

];
