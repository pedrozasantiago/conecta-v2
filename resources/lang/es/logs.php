<?php

return [

	// cotizaciones
    'create_estimate'           => 'Cotizacion Creada Correctamente',
    'attachment_estimate'       => 'Archivo Adjunto',

    // facturas de venta
    'create_invoice'    	    => 'Factura de Venta Creada Correctamente',
    'attachment_invoice'        => 'Archivo Adjunto',
    'payment_sales'			    => 'Pago Agregado',

    // ordenes de compras
    'create_order'           	=> 'Orden de Compra Creada Correctamente',
    'attachment_order'	        => 'Archivo Adjunto',

    // facturas de compra
    'create_purchase'    	    => 'Factura de Compra Creada Correctamente',
    'attachment_purchase'       => 'Archivo Adjunto',
    'payment_purchase'			=> 'Pago de Factura',
];