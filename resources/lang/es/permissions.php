<?php

return [
    'permissions'               => 'Permisos',
    'create'					=> 'Nuevo Permiso',
    'edit'						=> 'Editar Permiso',
    'name'               		=> 'Slug',
    'display_name'              => 'Nombre',
    'desc'			            => 'Descripción',
    'parent'			        => 'Principal',
    'select_parent'		        => 'Permiso Principal',
    'child'			        	=> 'Secundario',
    'icon'                      => 'Icono',
    'create_success'            => 'Permiso Creado Correctamente',
    'edit_success'              => 'Permiso Editado Correctamente',
    'title_alert'      			=> '¿Estás Seguro?',
    'text_alert'      			=> '¿Deseas eliminar este Permiso?',
    'confirm_delete'			=> 'Si, Eliminar',
    'delete_success'      		=> 'Permiso Eliminado Correctamente',


    /* listado de permisos*/
    'users' => 'Usuarios',
    'create-users' => 'Crear Usuarios',
    'create-users-desc' => 'Puede crear usuarios',
    'read-users' => 'Listado de Usuarios',
    'read-users-desc' => 'Puede ver la lista de usuarios',
    'update-users' => 'Editar Usuarios',
    'update-users-desc' => 'Puede editar usuarios',
    'delete-users'  => 'Eliminar Usuarios',
    'delete-users-desc' => 'Puede eliminar usuarios',
    'sellers' => 'Vendedores',
    'create-sellers' => 'Crear Vendedores',
    'create-sellers-desc' => 'Puede crear nuevos vendedores',
    'read-sellers' => 'Listado de Vendedores',
    'read-sellers-desc' => 'Puede ver la lista de vendedores',
    'update-sellers' => 'Editar Vendedores',
    'update-sellers-desc' => 'Puede editar vendedores',
    'delete-sellers' => 'Eliminar Vendedores',
    'delete-sellers-desc' => 'Puede eliminar vendedores',
    'profile' => 'Perfil',
    'read-profile' => 'Ver Perfil',
    'read-profile-desc' => 'Permite ver el perfil del usuario',
    'update-profile' => 'Editar Perfil',
    'update-profile-desc' => 'Permite editar el perfil del usuario',
    'roles' => 'Roles',
    'create-roles' => 'Crear Roles',
    'create-roles-desc' => 'Permite crear un nuevo rol',
    'read-roles' => 'Listado de Roles',
    'read-roles-desc' => 'Permite ver la lista de roles',
    'update-roles' => 'Editar Roles',
    'update-roles-desc' => 'Permite editar un rol',
    'delete-roles' => 'Eliminar Roles',
    'delete-roles-desc' => 'Permite eliminar un rol',
    'clients' => 'Clientes',
    'create-clients' => 'Crear Clientes',
    'create-clients-desc' => 'Permite crear un nuevo cliente',
    'read-clients' => 'Listado de Clientes',
    'read-clients-desc' => 'Permite ver la lista de clientes',
    'import-clients' => 'Importar Clientes',
    'import-clients-desc' => 'Permite importar clientes',
    'export-clients' => 'Exportar Clientes',
    'export-clients-desc' => 'Permite exportar clientes',
    'update-clients' => 'Editar Clientes',
    'update-clients-desc' => 'Permite editar clientes',
    'delete-clients' => 'Eliminar Clientes',
    'delete-clients-desc' => 'Permite eliminar clientes',
    'clients-groups' => 'Grupo de Clientes',
    'create-clients-groups' => 'Crear Grupos de Clientes',
    'create-clients-groups-desc' => 'Permite crear grupos de clientes',
    'read-clients-groups' => 'Listado de Grupos de Clientes',
    'read-clients-groups-desc' => 'Permite ver la lista de grupos de clientes',
    'update-clients-groups' => 'Editar Grupos de Clientes',
    'update-clients-groups-desc' => 'Permite editar grupos de clientes',
    'delete-clients-groups' => 'Eliminar Grupos de Clientes',
    'delete-clients-groups-desc' => 'Permite eliminar grupos de clientes',
    'suppliers' => 'Proveedores',
    'create-suppliers' => 'Crear Proveedores',
    'create-suppliers-desc' => 'Permite crear proveedores',
    'read-suppliers' => 'Listado de Proveedores',
    'read-suppliers-desc' => 'Permite ver la lista de proveedores',
    'import-suppliers' => 'Importar Proveedores',
    'import-suppliers-desc' => 'Permite importar proveedores',
    'export-suppliers' => 'Exportar Proveedores',
    'export-suppliers-desc' => 'Permite exportar proveedores',
    'update-suppliers' => 'Editar Proveedores',
    'update-suppliers-desc' => 'Permite editar proveedores',
    'delete-suppliers' => 'Eliminar Proveedores',
    'delete-suppliers-desc' => 'Permite eliminar proveedores',
    'sales' => 'Ventas',
    'create-sales' => 'Realizar Ventas',
    'create-sales-desc' => 'Permite realizar ventas',
    'read-sales' => 'Listado de Ventas',
    'read-sales-desc' => 'Permite ver la lista de ventas',
    'update-sales' => 'Editar Ventas',
    'update-sales-desc' => 'Permite editar ventas',
    'delete-sales' => 'Eliminar Ventas',
    'delete-sales-desc' => 'Permite eliminar ventas',
    'cash-register' => 'Cajas Registradoras',
    'create-cash-register' => 'Crear Cajas Registradoras',
    'create-cash-register-desc' => 'Permite crear cajas registradoras',
    'read-cash-register' => 'Listado de Cajas Registradoras',
    'read-cash-register-desc' => 'Permite ver la lista de cajas registradoras',
    'update-cash-register' => 'Editar Cajas Registradoras',
    'update-cash-register-desc' => 'Permite editar cajas registradoras',
    'delete-cash-register' => 'Eliminar Cajas Registradoras',
    'delete-cash-register-desc' => 'Permite eliminar cajas registradoras',
    'cash-management' => 'Gestión de Efectivo',
    'create-cash-management' => 'Realizar Ingresos y Salidas de Efectivo',
    'create-cash-management-desc' => 'Permite realizar ingresos y salidas de efectivo en las cajas',
    'read-cash-management' => 'Listado de Ingresos/Egresos',
    'read-cash-management-desc' => 'Permite ver la lista de ingresos y egresos de las cajas',
    'update-cash-management' => 'Editar Ingresos y Salidas de Efectivo',
    'update-cash-management-desc' => 'Permite modificar ingresos y salidas de efectivo en las cajas',
    'delete-cash-management' => 'Eliminar Ingresos/Egresos',
    'delete-cash-management-desc' => 'Permite eliminar ingresos y salidas de efectivo en las cajas',
    'expenses' => 'Gastos',
    'create-expenses' => 'Crear Gastos',
    'create-expenses-desc' => 'Permite crear gastos',
    'read-expenses' => 'Listado de Gastos',
    'read-expenses-desc' => 'Permite ver la lista de gastos',
    'update-expenses' => 'Editar Gastos',
    'update-expenses-desc' => 'Permite editar gastos',
    'delete-expenses' => 'Eliminar Gastos',
    'delete-expenses-desc' => 'Permite eliminar gastos',
    'expenses-categories' => 'Categorías de Gastos',
    'create-expenses-categories' => 'Crear Categorías de Gastos',
    'create-expenses-categories-desc' => 'Permite crear categorías de gastos',
    'read-expenses-categories' => 'Listado de Categorías de Gastos',
    'read-expenses-categories-desc' => 'Permite ver la lista de gastos',
    'update-expenses-categories' => 'Editar Categorías de Gastos',
    'update-expenses-categories-desc' => 'Permite editar categorías de gastos',
    'delete-expenses-categories' => 'Eliminar Categorías de Gastos',
    'delete-expenses-categories-desc' => 'Permite eliminar categorías de gastos',
    'purchases' => 'Compras',
    'create-purchases' => 'Realizar Compras',
    'create-purchases-desc' => 'Permite realizar compras',
    'read-purchases' => 'Listado de Compras',
    'read-purchases-desc' => 'Permite ver la lista de compras',
    'update-purchases' => 'Editar Compras',
    'update-purchases-desc' => 'Permite editar compras',
    'delete-purchases' => 'Eliminar Compras',
    'delete-purchases-desc' => 'Permite eliminar compras',
    'products' => 'Productos',
    'create-products' => 'Crear Productos',
    'create-products-desc' => 'Permite crear productos',
    'read-products' => 'Listado de Productos',
    'read-products-desc' => 'Permite ver la lista de productos',
    'update-products' => 'Editar Productos',
    'update-products-desc' => 'Permite editar productos',
    'delete-products' => 'Eliminar Productos',
    'delete-products-desc' => 'Permite eliminar productos',
    'moves' => 'Traslados',
    'create-moves' => 'Hacer Traslado de productos',
    'create-moves-desc' => 'Permite realizar traslados de productos entre puntos de venta',
    'read-moves' => 'Listado de Traslados',
    'read-moves-desc' => 'Permite ver la lista de traslados',
    'update-moves' => 'Editar Traslados',
    'update-moves-desc' => 'Permite editar traslados de productos',
    'delete-moves' => 'Eliminar Traslados',
    'delete-moves-desc' => 'Permite eliminar traslados de productos',
    'stock-entry' => 'Ingreso de Existencias',
    'create-stock-entry' => 'Realizar Ingreso de Existencias',
    'create-stock-entry-desc' => 'Permite realizar ingresos de existencias',
    'read-stock-entry' => 'Listado de Ingreso de Existencias',
    'read-stock-entry-desc' => 'Permite ver la lista de ingresos de existencias',
    'update-stock-entry' => 'Editar Ingreso de Existencias',
    'update-stock-entry-desc' => 'Permite editar ingresos de existencias',
    'delete-stock-entry' => 'Eliminar Ingreso de Existencias',
    'delete-stock-entry-desc' => 'Permite eliminar ingresos de existencias',
    'stock-exit' => 'Salida de Existencias',
    'create-stock-exit' => 'Realizar Salida de Existencias',
    'create-stock-exit-desc' => 'Permite realizar salidas de existencias',
    'read-stock-exit' => 'Listado de Salida de Existencias',
    'read-stock-exit-desc' => 'Permite ver la lista de salidas de existencias',
    'update-stock-exit' => 'Editar Salida de Existencias',
    'update-stock-exit-desc' => 'Permite editar salidas de existencias',
    'delete-stock-exit' => 'Eliminar Salida de Existencias',
    'delete-stock-exit-desc' => 'Permite eliminar salidas de existencias',
    'categories' => 'Categorías de Productos',
    'create-categories' => 'Crear Categorías de Productos',
    'create-categories-desc' => 'Permite crear categorías de productos',
    'read-categories' => 'Listado de Categorías de Productos',
    'read-categories-desc' => 'Permite ver la lista de categorías de productos',
    'update-categories' => 'Editar Categorías de Productos',
    'update-categories-desc' => 'Permite editar categorías de productos',
    'delete-categories' => 'Eliminar Categorías de Productos',
    'delete-categories-desc' => 'Permite eliminar categorías de productos',
    'brands' => 'Marcas',
    'create-brands' => 'Crear Marcas',
    'create-brands-desc' => 'Permite crear marcas',
    'read-brands' => 'Listado de Marcas',
    'read-brands-desc' => 'Permite ver la lista de marcas',
    'update-brands' => 'Editar Marcas',
    'update-brands-desc' => 'Permite editar marcas',
    'delete-brands' => 'Eliminar Marcas',
    'delete-brands-desc' => 'Permite eliminar marcas',
    'attributes' => 'Atributos de Productos',
    'create-attributes' => 'Crear Atributos de Productos',
    'create-attributes-desc' => '',
    'read-attributes' => 'Listado de Atributos de Productos',
    'read-attributes-desc' => '',
    'update-attributes' => 'Editar Atributos de Productos',
    'update-attributes-desc' => '',
    'delete-attributes' => 'Eliminar Atributos de Productos',
    'delete-attributes-desc' => '',
    'reports' => 'Reportes',
    'read-reports' => 'Listado de Reportes',
    'read-reports-desc' => 'Permite ver el listado de reportes disponibles',
    'config' => 'Configuración',
    'read-config' => 'Configuraciónes',
    'read-config-desc' => 'Permite acceder a las configuraciónes del sistema',
    'update-config' => 'Cambiar Configuraciónes',
    'update-config-desc' => 'Permite cambiar las configuraciónes del sistema',
    'taxes' => 'Impuestos',
    'create-taxes' => 'Crear Impuestos',
    'create-taxes-desc' => 'Permite crear impuestos',
    'read-taxes' => 'Listado de Impuestos',
    'read-taxes-desc' => 'Permite ver la lista de impuestos',
    'update-taxes' => 'Editar Impuestos',
    'update-taxes-desc' => 'Permite editar impuestos',
    'delete-taxes' => 'Eliminar Impuestos',
    'delete-taxes-desc' => 'Permite eliminar impuestos',
    'units' => 'Unidades de medida',
    'create-units' => 'Crear Unidades de Medida',
    'create-units-desc' => 'Permite crear unidades de medida',
    'read-units' => 'Listado de Unidades de Medida',
    'read-units-desc' => 'Permite ver la lista de unidades de medida',
    'update-units' => 'Editar Unidades de Medida',
    'update-units-desc' => 'Permite editar unidades de medida',
    'delete-units' => 'Eliminar Unidades de Medida',
    'delete-units-desc' => 'Permite eliminar unidades de medida',
    'payment-methods' => 'Metodos de Pago',
    'create-payment-methods' => 'Crear Metodos de Pago',
    'create-payment-methods-desc' => 'Permite crear metodos de pago',
    'read-payment-methods' => 'Listado de Metodos de Pago',
    'read-payment-methods-desc' => 'Permite ver la lista de metodos de pago',
    'update-payment-methods' => 'Editar Metodos de Pago',
    'update-payment-methods-desc' => 'Permite editar metodos de pago',
    'delete-payment-methods' => 'Eliminar Metodos de Pago',
    'delete-payment-methods-desc' => 'Permite eliminar metodos de pago',
    'stores' => 'Puntos de venta',
    'create-stores' => 'Agregar punto de venta',
    'create-stores-desc' => 'Permite agregar un nuevo punto de venta',
    'read-stores' => 'Lista de Puntos de Venta',
    'read-stores-desc' => 'Permite ver la lista de puntos de venta',
    'update-stores' => 'Editar Punto de Venta',
    'update-stores-desc' => 'Permite modificar un punto de venta',
    'delete-stores' => 'Eliminar Punto de Venta',
    'delete-stores-desc' => 'Permite eliminar un punto de venta',

];