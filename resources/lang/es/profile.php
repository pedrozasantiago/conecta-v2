<?php

return [
    'profile'                   => 'Perfil',
    'update-description'		=> 'Aquí puedes cambiar tu información personal.',
    'image'						=> 'Imagen',
    'remove-image'              => 'Eliminar Imagen',
    'image_removed_success'     => 'Imagen eliminada correctamente',
    'update_success'            => 'Perfil Actualizado Correctamente',
];