<?php

return [
    'roles'                     => 'Roles',
    'create'					=> 'Nuevo Rol',
    'edit'						=> 'Editar Rol',
    'description-header'        => 'Listado de roles',
    'help-desc'                 => '<p>Agregar información de marca a sus productos es útil cuando ejecuta informes o administra su inventario. También hace que configurar su tienda de comercio electrónico Winkel sea muy fácil.</p>

            <p>Una marca es el fabricante de un producto, mientras que un proveedor es la compañía a la que le compra, que podría ser un distribuidor.</p>',
    'role-search'               => 'Buscar Rol',
    'role-search-desc'          => 'Ingresa el nombre del rol que deseas buscar',
    'name'               		=> 'Slug',
    'perms-description-header'  => 'Asigne los permisos del rol',
    'display_name'              => 'Nombre',
    'desc'			            => 'Descripción',
    'permissions'	            => 'Permisos:',
    'permision_success'			=> 'Permiso modificado',
    'create_success'            => 'Rol Creado Correctamente',
    'edit_success'              => 'Rol Editado Correctamente',
    'title_alert'      			=> '¿Estás Seguro?',
    'text_alert'      			=> '¿Deseas eliminar este Rol?',
    'confirm_delete'			=> 'Si, Eliminar',
    'delete_success'      		=> 'Rol Eliminado Correctamente',

    /* roles del sistema */
    'admin'                     => 'Administrador',
    'admin-desc'                => 'Acceso a todo el sistema',
    'seller'                    => 'Vendedor',
    'seller-desc'               => 'Acceso a ventas',
];