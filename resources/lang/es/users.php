<?php

return [
    'users'                     => 'Usuarios',

    'description-header'        => 'Lista de todos los usuarios.',

    'help-desc'                 => '<p>Agregar información de marca a sus productos es útil cuando ejecuta informes o administra su inventario. También hace que configurar su tienda de comercio electrónico Winkel sea muy fácil.</p>

            <p>Una marca es el fabricante de un producto, mientras que un proveedor es la compañía a la que le compra, que podría ser un distribuidor.</p>',

    'import'                    => 'Importar',
    'full-name-search'          => 'Buscar Usuario',
    'full-name-search-desc'     => 'Ingresa el nombre del usuario que deseas buscar',

    'create'					=> 'Nuevo Usuario',
    'edit'						=> 'Editar Usuario',
    'name'               		=> 'Nombre',
    'first_name'                => 'Nombres',
    'last_name'                 => 'Apellidos',
    'username'			    	=> 'Nombre de Usuario',
    'email'						=> 'E-mail',
    'role'			    	    => 'Rol',
    'status'                    => 'Estado',
    'password'                  => 'Contaseña',
    'password_confirmation'     => 'Confirmar Contaseña',

    'update-login-data'         => 'Cambiar usuario y contraseña',
    'edit_login_data_success'   => 'Datos actualizados correctamente',
    
    'stores'			    	=> 'Tiendas',
    'select_role'   			=> 'Seleccionar Rol',
    'create_success'            => 'Usuario Creado Correctamente',
    'edit_success'              => 'Usuario Editado Correctamente',
    'title_alert'      			=> '¿Estás Seguro?',
    'text_alert'      			=> '¿Deseas eliminar este Usuario?',
    'confirm_delete'			=> 'Si, Eliminar',
    'delete_success'      		=> 'Usuario Eliminado Correctamente',
];