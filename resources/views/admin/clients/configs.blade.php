<head>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="{{ asset('js/admin/clients/configs.js') }}"></script>
    <style>

        #tabla_contactos table tr {

            cursor:  pointer;

        }


        .config-container {
            display: flex;
            flex-direction: column;
            gap: 1.5rem;
            border: 1px solid #ddd;
            border-radius: 8px;
            padding: 1rem;
            background-color: #f9f9f9;
            }

            .config-item {
            display: flex;
            flex-direction: column;
            gap: 0.5rem;
            }

            .config-label {
            font-weight: bold;
            font-size: 1.2rem;
            color: #333;
            }

            .config-description {
            font-size: 0.9rem;
            color: #666;
            }

            .config-type span {
            font-weight: bold;
            color: #007bff;
            }

            .config-inputs {
            display: flex;
            gap: 1rem;
            flex-wrap: wrap;
            }

            textarea {
            flex: 1;
            min-width: 200px;
            height: 100px;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 0.5rem;
            font-size: 0.9rem;
            }


    </style>

</head>
<div class="ibox">
    <div class="ibox-title"> 
        <div class="alert alert-info" >
            <p><strong>REGISTRO ACTIVO:</strong></p>
        </div>
    </div>
    <div class="ibox-content">
        <div class="sk-spinner sk-spinner-chasing-dots">
            <div class="sk-dot1"></div>
            <div class="sk-dot2"></div>
        </div>
         
        <div class="container-fluid">
            
            <form  id="Form_configs" >
                <div class="row">
                    <div class="col-sm-12 table-responsive" id="tabla_contactos">
                        <table  class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Configuracion</th>
                                    <th>Descripcion</th>
                                    <th>Valor</th>
                                    <th>Tipo</th>
                                </tr>
                            </thead>
                            <tbody>
    
                            </tbody>
                        </table>
                    </div> 
                </div>
              

            </form>


            
        </div>
    </div>
</div> 
 