@extends("master.layouts.master")

@section('title', 'Clientes')

@section('load_script')

<script src="{{ asset('js/admin/clients/grid.js') }}"></script>
<script src="{{ asset('js/admin/clients/clientes_frm.js') }}"></script>

@endsection

@section("content")

<div class="col-sm-12">

    <div class="ibox">
        <div class="ibox-title">
            <form  action="clientesX/server/reporte.php" method="POST" target="_blank" id="Clientes_grid_ppal"  class="form-inline"  >
                @csrf
                <div class="row">
                    <div class="col-sm-12"> 
                        <div class="form-group">
                            <input id="btn_buscar" class="btn btn-primary" type="submit" value="Buscar" >

                            <label class="sr-only" for="txtNitBC"></label>  
                            <Input placeholder="ID"   name="txtNitBC"  class="form-control btn-xs"    id="txtNitBC"  > 

                             
                            <label class="sr-only" for="txtNombreBC"></label> 
                            <Input placeholder="Nombre Cliente"  name ="txtNombreBC"   class="form-control btn-xs"  id ="txtNombreBC"  >  

                                <label class="sr-only" for="txtSubdomainBC"></label>  
                                <Input placeholder="Subdominio"   name="txtSubdomainBC"  class="form-control btn-xs"    id="txtSubdomainBC"  > 
                                    
                                    <label class="sr-only" for="txtPrefixBC"></label>  
                                    <Input placeholder="Prefijo"   name="txtPrefixBC"  class="form-control btn-xs"    id="txtPrefixBC"  > 
        
     
 

                            <label class="sr-only" for="lstEstadoCBC"></label>  
                            <select class="form-control btn-xs" name="lstEstadoCBC" id="lstEstadoCBC">
                                <option value="" >Todos</option>    
                                <option value="1" selected>Activo</option>
                                <option value="0" >Inactivo</option>
                                <option value="2" >Borrados</option>
                            </select>
 
                        </div>
                    </div>
                </div> 
            </form>  
        </div>
        <div class="ibox-content">
            <div id="grid_tabla" class="Alto_Grid table-responsive"></div>    
        </div>        
    </div> 
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 tabs" >
            <!--   <ul>
                   <li><a href="#tabs-0">Cliente</a></li>
                   <li><a href="clientesX/Clientes_infogral_frm.html">Info General</a></li> 
                   <li><a href="clientesX/Clientes_doclegal_frm.html">Doc Legal</a></li> 
                   <li><a href="clientesX/Clientes_doccred_frm.html">Doc Credito</a></li> 
               </ul> -->
            <h3>Cliente</h3>
            <section> 
                <div id="tabs-0" class="ibox">
                    <div class="ibox-title"> 
                        <div class="alert alert-info" >
                            <p><strong>REGISTRO ACTIVO:</strong></p>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-chasing-dots">
                            <div class="sk-dot1"></div>
                            <div class="sk-dot2"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <form method="POST" id="ClienCamp_frm" >
                                    @method("")
                                    @csrf
                                    <div id="Clie_btn">
                                        <button type="button" class="btn btn-primary " id="Nuevo"     value="Nuevo"  >Nuevo</button>
                                        <button type="button" class="btn btn-primary " id="Modificar" value="Modificar" >Modificar</button>
                                        <button  type="button" class="btn btn-primary " id="Borrar" value="Borrar" >Borrar</button>
                                        <button  type="submit" class="btn btn-primary " id="Grabar"  disabled >Grabar</button> 
                                        
                                    </div> 

                                    <div id="Clie_div" class="container-fluid">
                                        <div class="form-group row">
                                             
                                            <div class="col-sm-3">
                                                <label for="txtNit">(*)ID:</label>
                                                <Input class="form-control input-sm"    name="txtNit" id="txtNit"  required    >
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="txtNombre">Nombre empresa</label>
                                                <Input class="form-control input-sm" name="txtNombre" id="txtNombre"    >
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="txtSubdomain">(*)Subdominio</label>
                                                <Input  class="form-control input-sm"    name ="txtSubdomain"   id ="txtSubdomain"  required  >
                                            </div> 

                                            <div class="col-sm-3">
                                                <label for="txtPrefix">(*)Prefijo</label>
                                                <Input  class="form-control input-sm"    name ="txtPrefix"   id ="txtPrefix"  required  >
                                            </div>  
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="txtCorreo">(*)Correo</label>
                                                <Input  class="form-control input-sm"    name ="txtCorreo"   id ="txtCorreo"  required  >
                                            </div>  
                                            <div class="col-sm-3">
                                                <label for="lstEstado">Estado:</label>
                                                <select required class="form-control input-sm" name="lstEstado" id="lstEstado"  >
                                                    <option value="1" selected>Activo</option>
                                                    <option value="0" >Inactivo</option>
                                                </select> 
                                            </div>  
                                            
                                            <div class="col-sm-3">
                                                <label for="txtFechaCobro">Fecha Facturacion:</label> 
                                                <input required class="form-control txtfecha" name="txtFechaCobro" id="txtFechaCobro" >
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="txtFechaFin">Fecha Fin:</label> 
                                                <input required class="form-control txtfecha" name="txtFechaFin" id="txtFechaFin" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="txtLicencias">Licencias</label>
                                                <Input  class="form-control input-sm" type="number"   name ="txtLicencias"   id ="txtLicencias"  required  >
                                            </div>  
                                             
                                            <div class="col-sm-3">
                                                <label for="txtLicenciasGratis">Licencias Gratis</label>
                                                <Input  class="form-control input-sm"  type="number"  name ="txtLicenciasGratis"   id ="txtLicenciasGratis"  required  >
                                            </div>
                                             
                                        </div>
                                        <Input type= "hidden"  name ="txtId"    id ="txtId"  >
                                    </div>
                                    <!-- <Input type= "hidden"  name ="txtAccion_frm"    id ="txtAccion_frm"  > -->
                                    

                                </form> 
                            </div>
                        </div>

                    </div> 
                </div>
            </section>
            <h3>Configuraciones</h3>
            <section data-mode="async" data-url="clients/configs"></section> 
            <h3>Usuarios</h3>
            <section data-mode="async" data-url="clients/users"></section> 
            <!-- <h3>Carga masiva de clientes</h3>
            <section data-mode="async" data-url="clientesX/carga_masiva.html"></section> 
            LA CARGA MASIVA DE CLIENTES DEBE ESTAR UBICADA EN EL MODULO COMERCIAL --> 
            <!-- <h3>DocumentaciÃ³n crÃ©dito</h3>
            <section data-mode="async" data-url="clientesX/Clientes_doccred_frm.html"></section> -->
        </div>
    </div>
</div>
@endsection