<head>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="{{ asset('js/admin/clients/users.js') }}"></script>
    <style>

        #tabla_contactos table tr {

            cursor:  pointer;

        }

    </style>

</head>
<div class="ibox">
    <div class="ibox-title"> 
        <div class="alert alert-info" >
            <p><strong>REGISTRO ACTIVO:</strong></p>
        </div>
    </div>
    <div class="ibox-content">
        <div class="sk-spinner sk-spinner-chasing-dots">
            <div class="sk-dot1"></div>
            <div class="sk-dot2"></div>
        </div>
         
        <div class="container-fluid">
            
            <form  id="Form_contactos" >
                <div id="btn_contacto">
                    <button class="btn btn-primary" type="button" id="btnContacto_nuevo" >Nuevo</button>
                    <button class="btn btn-primary" type="button" id="btnContacto_modificar" >Modificar</button>
                    <button class="btn btn-primary" type="button" id="btnContacto_borrar" >Borrar</button>
                    <button class="btn btn-primary" disabled type="submit" id="btnContacto_grabar" >Grabar</button>                            
                </div>
                <div id="ClienContactos_div"  class="form-group row" >

                    <Input   type = "hidden" name="txtContacto_id" id="txtContacto_id"     >
                    <Input type= "hidden"  name ="txtContacto_accion"    id ="txtContacto_accion"  >
                    <div class="col-sm-2">
                        <label for="txtContacto_nombre">Nombre del Contacto</label>
                        <Input class="form-control input-sm" type = "text" name="txtContacto_nombre" id="txtContacto_nombre"  required    >
                    </div>
                    <div class="col-sm-2">
                        <label for="txtContacto_cargo">Cargo</label>
                        <Input  class="form-control input-sm"    name ="txtContacto_cargo"   id ="txtContacto_cargo"    >
                    </div>
                    <div class="col-sm-2">
                        <label for="txtContacto_correo">Correo</label>
                        <Input type="email" class="form-control input-sm"  multiple  name ="txtContacto_correo"   id ="txtContacto_correo"    >
                    </div>
                    <div class="col-sm-2">
                        <label for="txtContacto_celular">Celular</label>
                        <Input type="number"  class="form-control input-sm"    name ="txtContacto_celular"   id ="txtContacto_celular"    >
                    </div> 
                    <div class="col-sm-2">
                        <label for="txtContacto_telefono">Telefono</label>
                        <Input  class="form-control input-sm"    name ="txtContacto_telefono"   id ="txtContacto_telefono"    >
                    </div> 
                    <div class="col-sm-2">
                        <label for="txtContacto_skype">Skype</label>
                        <Input  class="form-control input-sm"    name ="txtContacto_skype"   id ="txtContacto_skype"    >
                    </div>
                    <div class="col-sm-2">
                        <label for="lstContacto_comercial">Comercial</label>
                        <select class="form-control input-sm" name ="lstContacto_comercial"  required id ="lstContacto_comercial">
                            <option selected value="NO">NO</option>
                            <option value="SI">SI</option>
                        </select> 
                    </div>
                    <div class="col-sm-2">
                        <label for="lstContacto_operaciones">Operaciones</label>
                       <select class="form-control input-sm" name ="lstContacto_operaciones" required  id ="lstContacto_operaciones">
                            <option selected value="NO">NO</option>
                            <option value="SI">SI</option>
                        </select>
                    </div>
                    
                    <div class="col-xs-6">
                        <label for="txtContacto_notas">Notas </label>
                        <textarea  class="form-control input-sm"   name="txtContacto_notas"  id="txtContacto_notas"  rows="2"     ></textarea>
                    </div> 
                </div> 

            </form>


            <div class="row">
                <div class="col-sm-12 table-responsive" id="tabla_contactos">
                    <table  class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nombre del Contacto</th>
                                <th>Cargo</th>
                                <th>Correo</th>
                                <th>Celular</th>
                                <th>Telefono</th>
                                <th>Skype</th>
                                <th>Notas</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
    </div>
</div> 
<div style='display:none' class="ibox">
    <div class="ibox-title"> 
        <div class="alert alert-info" >
            <h4><strong>INFORMACIÓN DE LOS CONTACTOS</strong></h4>
        </div>
    </div>
    <div class="ibox-content">
        <div class="sk-spinner sk-spinner-chasing-dots">
            <div class="sk-dot1"></div>
            <div class="sk-dot2"></div>
        </div>

    </div>    
</div>
 