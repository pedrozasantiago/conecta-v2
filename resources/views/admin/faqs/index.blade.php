@extends("master.layouts.master")

@section('title', 'FAQS')

@section('load_script')

{!! Html::script( asset('js/admin/faqs/index.js') ) !!}

@endsection

@section("content")
<style>
     .variable {
    cursor: default;
    background-color: #65b9dd;
    color: #FFF;
    padding: 2px 8px;
    border-radius: 3px;
    font-weight: bold;
    font-style: normal;
    font-size: 10px;
    display: inline-block;
    line-height: 12px;
} 
    </style>

<div class="col-sm-12"> 
    <div class="container-fluid"> 
        <form action="../admin/clients/" name="frmFaqs" id="frmFaqs" method="POST">  
            @method("")
            @csrf
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            <label for="software">Seleccione modulo</label>
                            <select id="software" name="software" class="form-control" >
                                <option value="comercial">Comercial</option>
                                <option value="operativo">Operativo</option>
                            </select>
                        </div> 
                        <div class="col-sm-offset-3 col-sm-6">
                            <label for="lstPregunta">Seleccione la pregunta</label>
                            <select id="lstPregunta" name="lstPregunta" class="form-control" >
                                <option value="">...</option>
                            </select>
                        </div> 
                    </div> 

                </div> 
                <div class="ibox-content"> 
                     <div class="sk-spinner sk-spinner-chasing-dots">
                            <div class="sk-dot1"></div>
                            <div class="sk-dot2"></div>
                        </div>
                    <div class="row">
                        <div id="DatosCorreo" class="col-sm-12">  
                            <div class="form-group">
                                <label for="respuesta">Escriba la respuesta</label>
                                <textarea  rows="20" class="form-control"  id="respuesta" name="respuesta"></textarea>
                            </div> 
                            <div style="text-align: center; margin: auto">
                                <button  class="btn btn-primary" type="submit" >Grabar</button>     
                            </div>  

                        </div>
                    </div> 
                </div> 
            </div> 
        </form>
    </div> 
</div>

@endsection