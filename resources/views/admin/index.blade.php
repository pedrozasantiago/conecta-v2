<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.7, maximum-scale=0.7, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>CONECTA-ADMIN @yield('title')</title>

<link href="{{ asset('favicon.ico') }}" type="image/png" rel="icon">

<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        
        
        
<link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">




<!-- Mainly scripts -->


<!--                <link rel="stylesheet" href="calendarioX/css/fullcalendar.min.css">
                <link rel="stylesheet" href="calendarioX/css/fullcalendar.print.css"> -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<!--<script src="//momentjs.com/downloads/moment.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
 <!--<script src="calendarioX/js/fullcalendar.min.js"></script>--> 
<!--     <link href="assets/fine-uploader/fine-uploader-new.css" rel="stylesheet"> 
    <script src="assets/fine-uploader/fine-uploader.js"></script> -->
    
<!-- Idle Timer plugin -->
{{-- <script src="{{ asset('assets/js/plugins/idle-timer/idle-timer.min.js') }}"></script> --}}



{{-- <script src="{{ asset('assets/funciones.js') }}"></script> --}}

<!--Leer documentacion de esta libreria-->
<!--<script src="../comercial/assets/js/plugins/validate/jquery.validate.min.js"></script>-->
        
@yield('load_script')

<script type="text/javascript">	

    @yield('inline_script')
    
</script>  