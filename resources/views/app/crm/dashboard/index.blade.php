@extends("layouts.crm")

@section("content")

<div class="col-sm-12">

    <div class="row  border-bottom white-bg dashboard-header">

        <div class="col-md-5">
            <h2>Bienvenido <?php echo  $_COOKIE['user_act'] ?></h2>
             

            <ul class="list-group clear-list m-t">
                 
            </ul>
        </div>
        <div class="col-md-7">
            <div class="">
                <!--<div class="flot-chart-content" id="flot-dashboard-chart"></div>-->
                <canvas id="flot-dashboard-chart" height="70"></canvas>
            </div>
            <div style="display: none" class="row text-left">
                <div class="col-xs-4">
                    <div class=" m-l-md">
                        <span class="h4 font-bold m-t block">$ 406,100</span>
                        <small class="text-muted m-b block">Sales marketing report</small>
                    </div>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">$ 150,401</span>
                    <small class="text-muted m-b block">Annual sales revenue</small>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">$ 16,822</span>
                    <small class="text-muted m-b block">Half-year revenue margin</small>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-success pull-right">MES</span>
                            <h5># COTIZACIONES</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                 
                            </h1>
                            <div class="stat-percent font-bold text-success">100%</div>
                            <small>Total cotizaciones este mes</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-primary pull-right">MES</span>
                            <h5>Ventas cotizadas</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                
                            </h1>
                            <div class="stat-percent font-bold text-navy">100%<i class="fa fa-level-up"></i></div>
                            <small>Total ventas cotizadas</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">MES</span>
                            <h5>Profit estimado</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                
                            </h1>
                            <div class="stat-percent font-bold text-danger">
                                 
                            </div>
                            <small>Total profit cotizado</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-success pull-right">MES</span>
                            <h5># COTIZACIONES de  <?php echo $_COOKIE['user_act'] ?></h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                 
                            </h1>
                            <div class="stat-percent font-bold text-success">
                                 
                            </div>
                            <small>Total cotizaciones de <?php echo $_COOKIE['user_act'] ?> este mes</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-primary pull-right">MES</span>
                            <h5>Ventas cotizadas de <?php echo $_COOKIE['user_act'] ?> </h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                
                            </h1>
                            <div class="stat-percent font-bold text-navy">
                                
                                <i class="fa fa-level-up"></i>
                            </div>
                            <small>Ventas cotizadas por <?php echo $_COOKIE['user_act'] ?> este mes</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">MES</span>
                            <h5>Profit estimado de <?php echo $_COOKIE['user_act'] ?></h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">

                            </h1>
                            <div class="stat-percent font-bold text-danger">
                                 
                            </div>
                            <small>Profit cotizado por <?php echo $_COOKIE['user_act'] ?> este mes</small>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h4>
                                Estados de las cotizaciones
                            </h4>
                        </div>
                        <div class="ibox-content"> 

                            <div class="row text-center">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <canvas id="doughnutChart" width="100%" height="100%" style="margin: 18px auto 0"></canvas>
                                    <h5 >Cotizaciones</h5>
                                </div>

                            </div>
                            <div class="m-t">
                                <small>Estado de las cotizaciones del mes actual</small>
                            </div> 
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>






</div>


@endsection