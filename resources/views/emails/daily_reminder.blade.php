<!DOCTYPE html>
<html>
<head>
    <title>Recordatorio Diario de {{ $type_process ?? "" }}</title>
</head>
<body>
    <p>Hola {{ $userName ?? "" }}</p>
    <p>Queremos notificarte que tienes un total de <b>{{ count($process) ?? ""  }} {{ $type_process ?? "" }}  ABIERTOS a cargo</b></p>
    @php $module = $type_process == "Embarques" ? "operativo/login.php" : "comercial/login.php" @endphp
    <a href="{{ 'https://' . $link . '/' . $module }}">Ver detalles</a>

    <p>¡Que tengas un buen día!</p>
</body>
</html>