@component('mail::message')
# Pedido Autorizado

Se ha autorizado un pedido y está pendiente por capturar. Por favor ingrese al sistema o haga clic en el siguiente botón.

@component('mail::button', ['url' => url('orders/'.$order->id)])
Ver Pedido
@endcomponent

Muchas Gracias.<br>
@endcomponent