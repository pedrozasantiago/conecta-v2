@component('mail::message')
# Pedido Capturado

Se ha capturado un pedido y está pendiente por embarcar. Por favor ingrese al sistema o haga clic en el siguiente botón.

@component('mail::button', ['url' => url('orders/'.$order->id)])
Ver Pedido
@endcomponent

Muchas Gracias.<br>
@endcomponent