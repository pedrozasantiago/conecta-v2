@component('mail::message')
# Nuevo pedido generado

Se ha generado un nuevo pedido y está pendiente de aprobación. Por favor ingrese al sistema o haga clic en el siguiente botón.

@component('mail::button', ['url' => url('orders/'.$order->id)])
Ver Pedido
@endcomponent

Muchas Gracias.<br>
@endcomponent