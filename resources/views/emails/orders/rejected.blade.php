@component('mail::message')
# Pedido Rechazado

Se ha rechazado un pedido. Por favor ingrese al sistema o haga clic en el siguiente botón.

@component('mail::button', ['url' => url('orders/'.$order->id)])
Ver Pedido
@endcomponent

Muchas Gracias.<br>
@endcomponent