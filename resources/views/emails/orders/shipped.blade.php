@component('mail::message')
# Pedido Embarcado

Se ha enbarcado un pedido. Por favor ingrese al sistema o haga clic en el siguiente botón.

@component('mail::button', ['url' => url('orders/'.$order->id)])
Ver Pedido
@endcomponent

Muchas Gracias.<br>
@endcomponent