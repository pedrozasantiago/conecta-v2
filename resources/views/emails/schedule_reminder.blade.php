<!DOCTYPE html>
<html>
<head>
    <title>Recordatorio de Horario</title>
</head>
<body>
    <p>Hola, {{ $userName }}:</p>
    <p>Este es un recordatorio de tu horario laboral para el día de hoy ({{ $dia }}):</p>
    <p>Hora de Entrada: {{ $horaEntrada }}</p>
    <p>Hora de Salida: {{ $horaSalida }}</p>
    <p>¡Que tengas un buen día!</p>
</body>
</html>