@extends('errors.blank')

@section('title')
    500 - {{  trans('base.500_error') }}
@endsection

@section('content')
<section class="flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-md-6 p-0">
            <div class="card-header bg-transparent border-0">

                <div class="row match-height">
                    <div class="col-sm-4 p-0">
                        <div class="display-table">
                            <div class="display-table-cell align-middle">
                                <img src="{{ asset('images/errors/500.svg') }}" class="img-fluid" />
                            </div>    
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h2 class="error-code text-center mb-2">500</h2>
                        <h3 class="text-uppercase text-center">{{  trans('base.500_error') }}</h3>
                        <p class="text-center">{{  trans('base.500_error_desc') }}</p>

                        <a href="{{ url('app') }}" class="btn btn-primary btn-block mt-3">
                            <i class="ft-home"></i> {{ trans('base.go_home') }}
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop