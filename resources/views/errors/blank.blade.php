<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>@yield('site_title')</title>
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('favicon.ico')}}">
	@include('errors.partials.styles')
</head>
<body class="vertical-layout vertical-compact-menu 1-column menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-compact-menu" data-col="1-column">

	<div class="app-content content">
		
		<div class="content-wrapper">
		
			<div class="content-header row"></div>
		
		  	<div class="content-body">
				@yield('content')
			</div>

		</div>

	</div>

	@include('errors.partials.scripts')

</body>
</html>