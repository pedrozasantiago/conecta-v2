<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.7, maximum-scale=0.7, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CONECTA-COMERCIAL</title>
       
        @include('partials.styles')

       @include('partials.scripts')

    </head>
    <body>
        <div id="wrapper">
            @include('layouts.sidebar')
            

            <div id="page-wrapper" class="gray-bg dashbard-1">

                @include('layouts.nav')
                
                <div class="wrapper wrapper-content animated fadeInRight"> 
                    <div class="row">

                        @yield("content")


                    </div> 
                </div>
                <div class="footer">
                    <div class="pull-right">
                        <a target="_blank" href="https://conectacarga.co">www.conectacarga.co</a>
                    </div>
                    <div>
                        <strong>Copyright</strong> CONECTA CARGA - 2020
                    </div>

                </div>
            </div> 
        </div>
    </body>
    