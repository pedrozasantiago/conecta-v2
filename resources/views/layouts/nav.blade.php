<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>                            
        </div>
        <ul class="nav navbar-top-links navbar-left">
            <div id="pagina_activa">

            </div> 
        </ul>

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Bienvenido a Conecta carga</span>
            </li>
            <li>
                <a href="javascript:actualizar_trm()"><span class="fas fa-dollar-sign"></span> TRM del día</a> 
            </li>
            <li>
                <a href="javascript:abrir_calendario()"><span class="glyphicon glyphicon-calendar"></span> Calendario</a> 
            </li>
            <li><a href="javascript:formulario_citas()"><span class="glyphicon glyphicon-tasks"></span> Crear recordatorio</a></li>


            <li class="dropdown">
                <a id="inicio_count_notif" class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">

                </a>
                <ul id="inicio_notificaciones" class="dropdown-menu dropdown-messages">

                </ul>
            </li>
            <li>
                <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> 
                    Salir
                </a>
                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </li>

        </ul>

    </nav>
</div> 