<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element" style="text-align: center"> <span>
                        <img alt="image" class="img-circle" style='max-width:150px; max-height: 150px' src="{{ asset('images/logo_conecta.jpeg') }}" />
                    </span>
                    <a data-toggle="" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs">
                                <strong class="font-bold">

                                </strong>
                            </span> <span class="text-muted text-xs block">
                                <!--{{  Auth::guard('web_admin')->user()->first_name }}-->
                                <b class="caret"></b></span> </span>
                    </a>

                </div>
                <div class="logo-element">
                    CONECTA SOFTWARE Y PROCESOS SAS
                </div>
            </li>

            @foreach ($menu as $link)
                @if (count($link['hijo']) > 0)
                <li>
                        <a href="{{ $link['ref'] }}"><i class="{{ $link['icon'] }}"></i> <span class="nav-label">{{ $link['titulo'] }}</span><span class="fa arrow"></span></a>
                </li>  
                <ul class="nav nav-second-level collapse in"> 
                    @foreach ($link['hijo'] as $hijo) 
                        <li class="">
                            <a href="{{ $hijo['ref'] }}"><i class="fas fa-users-cog"></i>{{ $hijo['titulo'] }}</a>
                        </li>
                    @endforeach
                </ul> 
                @else
                    <li>
                        <a href="{{ $link['ref'] }}"><i class="{{ $link['icon'] }}"></i> <span class="nav-label">{{ $link['titulo'] }}</span></a>
                    </li>   
                @endif    
            @endforeach

 

        </ul>
    </div>
</nav>