@extends('master.layouts.blank')

@section('site_title')
{{ trans('auth.login-title') }}
@endsection

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">

    <div style="padding-bottom: 20px">
        <img alt="image"  style='max-width:100%; max-height: 150px' src="https://www.conectacarga.co/wp-content/uploads/2019/11/conecta-contacto.png" />
    </div>

    <div style="padding-bottom: 20px">
        <h2>Bienvenido al</h2>
        <h3>Módulo Administrador</h3>
    </div>

    <div>
        
        @include('messages')
        
        <form class="m-t" method="POST" action="{{ route('admin.login') }}" role="form" >
            
            {{ csrf_field() }}
            
            <div class="form-group">
                <input name="email" type="text" class="form-control" id="email" placeholder="{{ trans('auth.email') }}" value="{{ old('email') }}" required autofocus>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" name="password" 
               placeholder="{{ trans('auth.password') }}" required>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('auth.login') }}</button>


        </form>
        
        <div class="item-forgot">
            <p class="float-sm-left text-xs-center m-0">
                <a href="{{ route('admin.password.request') }}" class="card-link">{{ trans('auth.forgot-password') }}</a>
            </p>
        </div>
    </div>

</div>
@endsection