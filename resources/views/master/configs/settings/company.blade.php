<h2 class="vd-header vd-header-settings">
    <i class="fa fa-file-alt"></i> {{ trans('settings.title_company') }}
</h2>

<div class="card">
    <div class="card-body">
        <div class="card-block">

            <div role="tabpanel" class="tab-pane fade show active" id="settings-company">

                <div class="form-group">
                    {!! htmlspecialchars_decode(Form::label('company_name', trans('settings.company_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                    {{ Form::text('company_name', PublikBase::get_option('company_name'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {!! htmlspecialchars_decode(Form::label('company_address', trans('settings.company_address').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                    {{ Form::text('company_address', PublikBase::get_option('company_address'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {!! htmlspecialchars_decode(Form::label('company_city', trans('settings.company_city').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                    {{ Form::text('company_city', PublikBase::get_option('company_city'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('company_country', trans('settings.company_country'), ['class' => 'vd-label']) }}
                    {{ Form::text('company_country', PublikBase::get_option('company_country'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {!! htmlspecialchars_decode(Form::label('company_phone', trans('settings.company_phone').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                    {{ Form::number('company_phone', PublikBase::get_option('company_phone'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('company_cellphone', trans('settings.company_cellphone'), ['class' => 'vd-label']) }}
                    {{ Form::number('company_cellphone', PublikBase::get_option('company_cellphone'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {!! htmlspecialchars_decode(Form::label('company_email', trans('settings.company_email').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                    {{ Form::email('company_email', PublikBase::get_option('company_email'), array('class' => 'form-control vd-input')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('company_domain', trans('settings.company_domain'), ['class' => 'vd-label']) }}
                    {{ Form::text('company_domain', PublikBase::get_option('company_domain'), array('class' => 'form-control vd-input')) }}
                </div>


            </div>

        </div>
    </div>
</div>