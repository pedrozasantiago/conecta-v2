@extends('master.layouts.crud')

@section('inline_script')
    {!! $configs !!}
    {!! get_submit_vars('POST', 'Simple', 1) !!}
@endsection

@section('title')
    {{ trans('sidebar.settings') }}
@endsection

@section('description')
    {{ trans('settings.update-description') }} <a href="#">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">

    <a class="vd-button btn btn-secondary" href="{{ url('admin/customers/'.$customer->id) }}">{{ trans('base.cancel') }}</a>

    <button id="send_form" class="vd-button btn btn-success"><i class="fa fa-check-square-o"></i> {{ trans('base.save') }}</button>

</div>
@endsection

@section('content')


    {{ Form::open(array('url' => 'admin/customers/config/settings/'.$customer->id, 'id' => 'settings_submit')) }}

    <div class="row">

        <div class="col-xs-12 col-md-3">
            <ul class="nav nav-pills nav-stack flex-column" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#settings-company" role="tab" data-toggle="tab"><i class="ft-user"></i> {{ trans('settings.title_company') }}</a>
                </li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-9">
            <div class="tab-content">
                @include('master.configs.settings.company')
            </div>
        </div>

    </div>
    {{ Form::close() }}
@stop