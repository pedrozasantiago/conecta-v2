@extends('master.layouts.crud')

@section('inline_script')
    {!! $configs !!}
    {!! get_submit_vars('POST', 'Simple', 1) !!}
@endsection

@section('title')
    {{ trans('customers.edit') }}
@endsection

@section('description')
    {{ trans('customers.admin-edit-description') }} <a href="#">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">

    <a class="vd-button btn btn-secondary" href="{{ url('admin/customers') }}">{{ trans('base.cancel') }}</a>

    <button id="send_form" class="vd-button btn btn-success"><i class="fa fa-check-square-o"></i> {{ trans('base.save') }}</button>

</div>
@endsection

@section('content')
<div class="vd-mtl vd-section">
    <div class="vd-section-wrap">

        {!! Form::model($customer, [
            'method' => 'PUT',
            'id' => 'customers_submit',
            'route' => ['admin.customers.update', $customer]
        ]) !!}

        <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('first_name', trans('customers.first_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::text('first_name', Request::old('first_name'), array('class' => 'form-control vd-input')) }}
                    </div>

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('last_name', trans('customers.last_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::text('last_name', Request::old('last_name'), array('class' => 'form-control vd-input')) }}
                    </div>

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('username', trans('customers.username').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::text('username', Request::old('username'), array('class' => 'form-control vd-input')) }}
                    </div>

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('email', trans('customers.email').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::email('email', Request::old('email'), array('class' => 'form-control vd-input')) }}
                    </div>

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('phone', trans('customers.phone').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::text('phone', Request::old('phone'), array('class' => 'form-control vd-input')) }}
                    </div>

                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('company_name', trans('customers.company_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                        {{ Form::text('company_name', Request::old('company_name'), array('class' => 'form-control vd-input')) }}
                    </div>

                </div>
            </div>
        </div>

        {{ Form::close() }}

    </div>
</div>
@stop