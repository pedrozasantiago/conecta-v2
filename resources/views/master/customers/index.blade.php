@extends('master.layouts.master')

@section('inline_script')
	{!! $configs !!}
@endsection

@section('title')
	{{ trans('sidebar.customers') }}
@endsection

@section('description')
	{{ trans('customers.admin-description-header') }} <a data-toggle="collapse" href="#collapseHelp" role="button" aria-expanded="false" aria-controls="collapseHelp">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">

	<a class="vd-button btn btn-success" href="{{ url('admin/customers/create') }}"><i class="fa fa-check-square-o"></i> {{ trans('customers.create') }}</a>

</div>
@endsection

@section('filters')
	@include('master.customers.filters')
@endsection

@section('content')
<table id="customers" class="table vd-table dt-responsive table-full-width">
	<thead>
		<tr>
			<th>{{ trans('customers.name') }}</th>
			<th>{{ trans('customers.email') }}</th>
			<th>{{ trans('customers.hostname') }}</th>
			<th>{{ trans('base.actions') }}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop