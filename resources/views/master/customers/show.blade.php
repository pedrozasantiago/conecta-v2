@extends('master.layouts.crud')

@section('title')
  {{ $customer->full_name }}
@endsection

@section('description')
    {{ trans('customers.admin-details-description') }} <a href="#">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">    

    <a class="vd-button btn btn-secondary" href="{{ url('admin/customers') }}">{{ trans('base.back') }}</a>

    <a class="vd-button btn btn-success" href="{{ url('admin/customers/'.$customer->id.'/edit') }}">{{ trans('customers.edit') }}</a>

</div>
@endsection

@section('content')
<div class="vd-mtl vd-section mb-3">
    <div class="vd-section-wrap">
      
      @include('master.customers.show.details')

      @include('master.customers.show.configs')

    </div>
</div>
@stop