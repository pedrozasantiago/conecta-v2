<h2 class="vd-header vd-header-settings">
	<i class="fa fa-th-list"></i> Configuración
</h2>

<div class="card">
	<div class="card-content">
		<div class="card-body">

			<div class="config-list">

				<div class="config-item">
					<a href="{{ url('admin/customers/config/settings/'.$customer->id) }}">
						<i class="fa fa-cog"></i>
						Ajustes
					</a>
				</div>

				<div class="config-item">
					<a href="{{ url('admin/customers/config/users/'.$customer->id) }}">
						<i class="fa fa-users"></i>
						Usuarios
					</a>
				</div>

				<div class="config-item">
					<a href="{{ url('admin/customers/config/stores/'.$customer->id) }}">
						<i class="fa fa-store"></i>
						Puntos de venta
					</a>
				</div>

			</div>

		</div>
	</div>
</div>