<h2 class="vd-header vd-header-settings">
    <i class="fa fa-file-alt"></i> Información General
</h2>

<div class="card">
    <div class="card-content">
        <div class="card-body">

            <!-- detalles -->
            <div class="row">

                <div class="col-xs-12 col-md-5">

                    <table class="table table-details">
                        <tbody>
                            <tr>
                                <td><strong>{{ trans('customers.name') }}:</strong></td>
                                <td>{{ $customer->full_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ trans('customers.username') }}:</strong></td>
                                <td>{{ $customer->username }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ trans('customers.email') }}:</strong></td>
                                <td>{{ $customer->email }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ trans('customers.phone') }}:</strong></td>
                                <td>{{ $customer->phone }}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div class="col-xs-12 col-md-5">

                    <table class="table table-details">
                        <tbody>
                            <tr>
                                <td><strong>{{ trans('customers.company_name') }}:</strong></td>
                                <td>{{ $customer->company_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ trans('customers.fqdn') }}:</strong></td>
                                <td>{{ $customer->hostname->fqdn }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ trans('customers.database') }}:</strong></td>
                                <td>{{ $customer->website->uuid }}</td>
                            </tr>                            
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- / detalles -->

        </div>
    </div>
</div>