<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>@yield('title')</title>
	<link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
	@include('partials.styles')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

	@include('layouts.nav')

	<div class="app-content content container-fluid">
		<div class="content-wrapper">

			<div class="content-header row">
				<div class="content-header-left col-md-6 col-xs-12 mb-2">
					<h3 class="content-header-title mb-0">@yield('title')</h3>

					<div class="row breadcrumbs-top mt-1">
              			<div class="breadcrumb-wrapper col-xs-12">
              				{!! PublikBase::breadcrumb() !!}
              			</div>
            		</div>

				</div>
				<div class="content-header-right col-md-6 col-xs-12">
				@yield('buttons')
				</div>
			</div>

			<div class="content-body">
				@yield('content')
			</div>
		</div>
	</div>

	@include('partials.scripts')

	@include('partials.modal_crud')

</body>
</html>