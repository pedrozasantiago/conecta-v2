<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('site_title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
    @include('master.partials.styles')
</head>
<body class="gray-bg">
    @yield('content')
    @include('master.partials.scripts')
</body>
</html>