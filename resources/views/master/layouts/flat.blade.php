<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>@yield('title')</title>
	<link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
	@include('master.partials.styles')
</head>

<body class="vertical-layout vertical-compact-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">

	@include('master.layouts.nav')

	@include('master.layouts.sidebar')

	<div class="app-content content">		

    	<div class="content-wrapper">
			<div class="content-body">
				@include('messages')
				@yield('content')
			</div>
		</div>
	</div>

	@include('master.partials.scripts')

	@include('master.partials.modal_crud')

</body>
</html>