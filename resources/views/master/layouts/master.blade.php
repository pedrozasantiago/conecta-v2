<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>@yield('title')</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
        @include('master.partials.styles')
    </head>
    <body>
        <div id="wrapper">
            @include('master.layouts.sidebar')
            

            <div id="page-wrapper" class="gray-bg dashbard-1">

                @include('master.layouts.nav')
                
                <div class="wrapper wrapper-content animated fadeInRight"> 
                    <div class="row">

                        @yield("content")


                    </div> 
                </div>
                <div class="footer">
                    <div class="pull-right">
                        <a target="_blank" href="https://conectacarga.co">www.conectacarga.co</a>
                    </div>
                    <div>
                        <strong>Copyright</strong> CONECTA CARGA - 2024
                    </div>

                </div>
            </div> 
        </div>
        @include('master.partials.scripts')

        @include('master.partials.modal_crud')

    </body>
</html>