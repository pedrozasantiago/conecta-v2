<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element" style="text-align: center"> <span>
                        <img alt="image" class="img-circle" style='max-width:150px; max-height: 150px' src="{{ asset('images/logo_conecta.jpeg') }}" />
                    </span>
                    <a data-toggle="" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs">
                                <strong class="font-bold">

                                </strong>
                            </span> <span class="text-muted text-xs block">
                                {{ Auth::guard('web_admin')->user()->first_name }}
                                <b class="caret"></b></span> </span>
                    </a>

                </div>
                <div class="logo-element">
                    CONECTA SOFTWARE Y PROCESOS SAS
                </div>
            </li>
            <li>
                <a href="clients"><i class="fa fa-tachometer-alt"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li>
                <a href="clients"><i class="fa fa-users"></i> <span class="nav-label">Clientes</span></a>
            </li>
            <li>
                <a href="clients/users"><i class="fa fa-users"></i> <span class="nav-label">Cliente Usuarios</span></a>
            </li>
            <li>
                <a href="faqs"><i class="fa fa-question"></i> <span class="nav-label">FAQS</span></a>
            </li>

        </ul>
    </div>
</nav>