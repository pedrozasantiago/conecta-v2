<!-- Modal crud -->
<div class="modal fade text-xs-left" id="modal_crud" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-success white">
				<button type="button" class="close" data-dismiss="modal" aria-label="{{ trans('base.close') }}">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">{{ trans('base.loading_text') }}</h4>
			</div>
			<div class="modal-body">
				<div class="m-t">
					<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">{{ trans('base.close') }}</button>
				<button type="button" class="btn btn-outline-success" id="send_form">{{ trans('base.save') }}</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal crud -->