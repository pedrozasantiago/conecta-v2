<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/funciones.js') }}"></script>
<script src="{{ asset('assets/xlsx.js') }}"></script>
<script src="{{ asset('assets/js/plugins/idle-timer/idle-timer.min.js') }}"></script>

<script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>


<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Flot -->
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Peity -->
<script src="{{ asset('assets/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('assets/js/inspinia.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('assets/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ asset('assets/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('assets/js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<script src="{{ asset('assets/js/plugins/chartJs/Chart.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('assets/js/plugins/toastr/toastr.min.js') }}"></script>

<!-- Steps js -->
<script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>

<!-- Ladda -->
<script src="{{ asset('assets/js/plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/ladda/ladda.jquery.min.js') }}"></script>

<!-- FooTable -->
<script src="{{ asset('assets/js/plugins/footable/footable.all.min.js') }}"></script>

<!-- Tinymce -->
<script src="{{ asset('assets/tinymce/tinymce.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script src="{{ asset('assets/js.cookie.js') }}"></script>

<!-- Full Calendar -->
<script src="{{ asset('assets/js/plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script> 



@yield('load_script')

<script type="text/javascript">	

	{!! get_global_json() !!}
	
	@yield('inline_script')
</script>