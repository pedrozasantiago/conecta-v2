<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/estilos.css') }}" rel="stylesheet">

<!-- Full Calendar -->
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.print.css') }}" rel='stylesheet' media='print'> 


<!-- Ladda style -->
 <link href="{{ asset('assets/css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

<!-- Jquery steps -->
<link href="{{ asset('assets/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

<!-- Toastr style -->
<link href="{{ asset('assets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


<!-- FooTable -->
<link href="{{ asset('assets/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

<!-- Gritter -->
<link href="{{ asset('assets/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/estilos.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

@yield('load_style')