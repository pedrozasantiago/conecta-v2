<li class="dropdown dropdown-notification nav-item">
	<a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-bell"></i>
		@if(count(Auth::guard('web_admin')->user()->unreadNotifications) > 0)
		<span class="tag tag-pill tag-default tag-danger tag-default tag-up">{{ count(Auth::guard('web_admin')->user()->unreadNotifications) }}</span>
		@endif
	</a>
	<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
		<li class="dropdown-menu-header">
			<h6 class="dropdown-header m-0">
				<span class="grey darken-2">{{ trans('base.notifications') }}</span>
				<span class="notification-tag tag tag-default tag-danger float-xs-right m-0">{{ count(Auth::guard('web_admin')->user()->unreadNotifications) }} {{ trans('base.notifications_news') }}</span>
			</h6>
		</li>
		<li class="list-group scrollable-container">
			@foreach(Auth::guard('web_admin')->user()->unreadNotifications as $notification)
				<a href="{{ url($notification->data['url']) }}" class="list-group-item">
				<div class="media">
					@if($notification->data['icon'])
					<div class="media-left valign-middle">
						<i class="{{ $notification->data['icon'] }} icon-bg-circle {{ $notification->data['bg'] }}"></i>
					</div>
					@endif
					<div class="media-body">
						<h6 class="media-heading {{ $notification->data['color'] }}">{{ trans( $notification->data['title']) }}</h6>
						<small>
							<p class="alert-desc">{{ $notification->data['desc'] }}</p>
							<span class="media-meta text-muted">{{ $notification->created_at->diffForHumans() }}</span>
						</small>
					</div>
				</div>
				</a>
			@endforeach
		</li>
		<li class="dropdown-menu-footer"><a href="{{ url('admin/notifications') }}" class="dropdown-item text-muted text-xs-center">{{ trans('base.read_all_notifications') }}</a></li>
	</ul>
</li>