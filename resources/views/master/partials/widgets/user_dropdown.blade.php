@if(Auth::guard('web_admin')->check())
<li class="dropdown dropdown-user nav-item">
	<a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
		<span class="avatar avatar-online">
			@php
				if(Auth::guard('web_admin')->user()->image){
					$profile = asset('images/profile/'.Auth::guard('web_admin')->user()->image);
				}else{
					$profile = asset('images/user-placeholder.jpg');
				}
			@endphp
			<img src="{{ $profile }}" alt="avatar"><i></i></span>
			<span class="user-name">{{ Auth::guard('web_admin')->user()->first_name }}</span>
		</a>
		<div class="dropdown-menu dropdown-menu-right">
			<a href="{{ url('admin/profile') }}" class="dropdown-item"><i class="ft-user"></i> Editar Perfil</a>
			<div class="dropdown-divider"></div>
			<a href="{{ url('admin/logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ft-power"></i> Salir</a>
			<form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
		</div>
	</li>
@endif