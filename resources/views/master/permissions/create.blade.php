@extends('Backend.layouts.modals')

@section('inline_modal_js')
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans('permissions.create') }}
@endsection

@section('modal_content')
  
{{ Form::open(array('url' => 'admin/permissions', 'id' => 'permissions_submit')) }}

    <div class="form-group">
        {{ Form::label('display_name', trans('permissions.display_name')) }}
        {{ Form::text('display_name', Request::old('display_name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', trans('permissions.name')) }}
        {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', trans('permissions.desc')) }}
        {{ Form::text('description', Request::old('description'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('icon', trans('permissions.icon')) }}
        <div class="input-group">
            {{ Form::text('icon', 'fa-navicon', array('class' => 'form-control icp icp-auto', 'data-placement' => 'bottomRight')) }}
            <span class="input-group-addon"></span>
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('parent_id', trans('permissions.parent')) }}
        {{ Form::select('parent_id', array('0' => trans('permissions.select_parent')) + $permissions, Request::old('parent_id'), array('class' => 'form-control')) }}
    </div>

{{ Form::close() }}

@stop