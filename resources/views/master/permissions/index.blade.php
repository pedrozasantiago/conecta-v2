@extends('Backend.layouts.master')

@section('inline_script')
	{!! $configs !!}
@endsection

@section('title')
	{{ trans('sidebar.permissions') }}
@endsection


@section('buttons')
<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
	@if(Auth::guard('web_admin')->user()->can('create-permissions'))
		{!! PublikButtons::create_modal('permissions', 'admin/permissions', 'create', '', 'md') !!}
	@endif
</div>
@endsection

@section('content')
<section id="configuration">
	<div class="row">
		<div class="col-xs-12">

			<table id="permissions" class="table table-condensed table-stack table-striped table-wp">
				<thead>
					<tr>
						<th>{{ trans('permissions.display_name') }}</th>
						<th>{{ trans('permissions.name') }}</th>
						<th>{{ trans('permissions.desc') }}</th>
						<th>{{ trans('permissions.parent') }}</th>
						<th>{{ trans('base.actions') }}</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>


		</div>
	</div>
</section>
@stop