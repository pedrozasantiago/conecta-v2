@extends('master.layouts.crud')

@section('inline_script')
    {!! $configs !!}
    {!! get_submit_vars('POST', 'Simple', 1) !!}
@endsection

@section('title')
    {{ $user->full_name }}
@endsection

@section('description')
    {{ trans('profile.update-description') }}
@endsection

@section('buttons')
<div class="float-md-right">

    <a class="vd-button btn btn-secondary" href="{{ url('admin/dashboard') }}">{{ trans('base.cancel') }}</a>

    <button id="send_form" class="vd-button btn btn-success"><i class="fa fa-check-square-o"></i> {{ trans('base.save') }}</button>

</div>
@endsection

@section('content')
<div class="vd-mtl vd-section">
    <div class="vd-section-wrap">

        {!! Form::model($user, [
            'method' => 'PUT',
            'id' => 'profile_submit',
            'files' => true,
            'route' => ['admin.profile.update', $user]
        ]) !!}

            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('first_name', trans('users.first_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::text('first_name', Request::old('first_name'), array('class' => 'form-control vd-input')) }}
                                </div>

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('last_name', trans('users.last_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::text('last_name', Request::old('last_name'), array('class' => 'form-control vd-input')) }}
                                </div>

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('email', trans('users.email').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::email('email', Request::old('email'), array('class' => 'form-control vd-input')) }}
                                </div>

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('password', trans('users.password').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::password('password', array('class' => 'form-control vd-input')) }}
                                </div>

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('password_confirmation', trans('users.password_confirmation').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::password('password_confirmation', array('class' => 'form-control vd-input')) }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">


                                @php
                                if(Auth::guard('web_admin')->user()->image){
                                    $profile = asset('images/profile/'.Auth::guard('web_admin')->user()->image);
                                }else{
                                    $profile = asset('images/user-placeholder.jpg');
                                }
                                @endphp

                                <img src="{{ $profile }}" class="img-fluid">

                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('image', trans('profile.image').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
                                    {{ Form::file('image', ['class' => 'form-control', 'id' => 'image']) }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        {{ Form::close() }}

    </div>
</div>
@stop