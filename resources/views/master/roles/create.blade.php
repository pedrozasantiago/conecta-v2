@extends('Backend.layouts.modals')

@section('inline_modal_js')
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans('roles.create') }}
@endsection

@section('modal_content')
  
{{ Form::open(array('url' => 'admin/roles', 'id' => 'roles_submit')) }}

    <div class="form-group">
        {{ Form::label('display_name', trans('roles.display_name')) }}
        {{ Form::text('display_name', Request::old('display_name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', trans('roles.name')) }}
        {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', trans('roles.desc')) }}
        {{ Form::text('description', Request::old('description'), array('class' => 'form-control')) }}
    </div>

{{ Form::close() }}

@stop