@extends('Backend.layouts.master')

@section('inline_script')
	{!! $configs !!}
@endsection

@section('title')
	{{ trans('sidebar.roles') }}
@endsection

@section('buttons')
<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
	@if(Auth::guard('web_admin')->user()->can('create-roles'))
		{!! PublikButtons::create_modal('roles', 'admin/roles', 'create', '', 'md') !!}
	@endif
</div>
@endsection

@section('content')
<section id="configuration">
	<div class="row">
		<div class="col-xs-12">

			<table id="roles" class="table table-condensed table-stack table-striped table-wp">
				<thead>
					<tr>
						<th>{{ trans('roles.display_name') }}</th>
						<th>{{ trans('roles.name') }}</th>
						<th>{{ trans('roles.desc') }}</th>
						<th>{{ trans('base.actions') }}</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>

		</div>
	</div>
</section>
@stop