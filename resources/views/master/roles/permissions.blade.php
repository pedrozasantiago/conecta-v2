@extends('Backend.layouts.master')

@section('title')
  {{ trans('roles.permissions') }} {{ $role->display_name }}
@endsection

@section('content')
<div class="content-role-perms">

  {{ Form::hidden('role_id', $role->id, array('id' => 'role_id')) }}

  @foreach($permissions as $key => $permission)

  <div class="content-perm">
    <div class="row">

      <div class="col-md-3 col-xs-12">
          <div class="perms-header">
          @if($permission->icon)
          <i class="fa {{ $permission->icon }}"></i>
          @endif
          {{ $permission->display_name }}
          </div>
      </div>

      <div class="col-xs-12 col-md-9">
  
      @foreach($permission->childs as $key => $child)

        <div class="form-group">

          @php
            if($role->hasPermissionTo($child->name) == 1){
              $check = true;
            }else{
              $check = false;
            }
          @endphp

          <label class="custom-control custom-checkbox">
          {!! Form::checkbox('perm[]', $child->id, $check, array('id' => 'perm', 'class' => 'custom-control-input update_perm')) !!}
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">{{ $child->display_name }}</span>
          </label>
          <div class="perms-description">{{ $child->description }}</div>
      </div>

      @endforeach

      </div>


    </div>
  </div>
  @endforeach

</div>
@stop