@extends('Backend.layouts.modals')

@section('inline_modal_js')
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans('users.create') }}
@endsection

@section('modal_content')
  
{{ Form::open(array('url' => 'admin/users', 'id' => 'users_submit')) }}

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('first_name', trans('users.first_name').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
        {{ Form::text('first_name', Request::old('first_name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('last_name', trans('users.last_name').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
        {{ Form::text('last_name', Request::old('last_name'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('username', trans('users.username').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
        {{ Form::text('username', Request::old('username'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('email', trans('users.email').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
        {{ Form::email('email', Request::old('email'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('password', trans('users.password').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
        {{ Form::password('password', array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
		{!! htmlspecialchars_decode(Form::label('role_id', trans('users.role').'<span class="required">*</span>', array('class' => 'control-label'))) !!}
		{{ Form::select('role_id', array('' => trans('users.select_role')) + $roles, Request::old('role_id'), array('class' => 'form-control')) }}
	</div>

{{ Form::close() }}

@stop