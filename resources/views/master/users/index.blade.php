@extends('Backend.layouts.master')

@section('inline_script')
	{!! $configs !!}
@endsection

@section('title')
	{{ trans('sidebar.users') }}
@endsection

@section('buttons')
<div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
	@if(Auth::guard('web_admin')->user()->can('create-users'))
		{!! PublikButtons::create_modal('users', 'admin/users', 'create', '', 'md') !!}
	@endif
</div>
@endsection

@section('content')
<section id="configuration">
	<div class="row">
		<div class="col-xs-12">

			<table id="users" class="table table-condensed table-stack table-striped table-wp">
				<thead>
					<tr>
						<th>{{ trans('users.name') }}</th>
						<th>{{ trans('users.username') }}</th>
						<th>{{ trans('users.email') }}</th>
						<th>{{ trans('users.role') }}</th>
						<th>{{ trans('base.actions') }}</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>

		</div>
	</div>
</section>
@stop