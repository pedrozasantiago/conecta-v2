<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.7, maximum-scale=0.7, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>OPERATIVE MODULE @yield('title')</title>

<link href="{{ asset('favicon.ico') }}" type="image/png" rel="icon">

<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        
        
        
<link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- Full Calendar -->
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.print.css') }}" rel='stylesheet' media='print'> 


<!-- Ladda style -->
 <link href="{{ asset('assets/css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

<!-- Jquery steps -->
<link href="{{ asset('assets/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

<!-- Toastr style -->
<link href="{{ asset('assets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


<!-- FooTable -->
<link href="{{ asset('assets/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

<!-- Gritter -->
<link href="{{ asset('assets/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/estilos.css') }}" rel="stylesheet">

<link href="{{ asset('assets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">


<!-- Mainly scripts -->
<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>


<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Flot -->
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Peity -->
<script src="{{ asset('assets/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('assets/js/inspinia.js') }}"></script>
<script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('assets/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ asset('assets/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('assets/js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<script src="{{ asset('assets/js/plugins/chartJs/Chart.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('assets/js/plugins/toastr/toastr.min.js') }}"></script>

<!-- Steps js -->
<script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>

<!-- Ladda -->
<script src="{{ asset('assets/js/plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/ladda/ladda.jquery.min.js') }}"></script>

<!-- FooTable -->
<script src="{{ asset('assets/js/plugins/footable/footable.all.min.js') }}"></script>

<!-- Tinymce -->
<script src="{{ asset('assets/tinymce/tinymce.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script src="{{ asset('assets/js.cookie.js') }}"></script>

<!-- Full Calendar -->
<script src="{{ asset('assets/js/plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script> 

<!--                <link rel="stylesheet" href="calendarioX/css/fullcalendar.min.css">
                <link rel="stylesheet" href="calendarioX/css/fullcalendar.print.css"> -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<!--<script src="//momentjs.com/downloads/moment.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
 <!--<script src="calendarioX/js/fullcalendar.min.js"></script>--> 
<!--     <link href="assets/fine-uploader/fine-uploader-new.css" rel="stylesheet"> 
    <script src="assets/fine-uploader/fine-uploader.js"></script> -->
    
<!-- Idle Timer plugin -->
{{-- <script src="{{ asset('assets/js/plugins/idle-timer/idle-timer.min.js') }}"></script> --}}
{!! Html::script( asset('assets/js/plugins/idle-timer/idle-timer.min.js') ) !!}

{!! Html::script( asset('assets/funciones.js') ) !!}
{{-- <script src="{{ asset('assets/funciones.js') }}"></script> --}}

<!--Leer documentacion de esta libreria-->
<!--<script src="../comercial/assets/js/plugins/validate/jquery.validate.min.js"></script>-->
        
@yield('load_script')

<script type="text/javascript">	

    @yield('inline_script')
    
</script>  
 
    </head>
    <body >
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element" style="text-align: center"> <span>
                                    <img alt="image" class="img-circle" style='max-width:150px; max-height: 150px' src="{{ asset('images/logo_conecta.jpeg') }}" />
                                </span>
                                <a data-toggle="" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs">
                                            <strong class="font-bold">

                                            </strong>
                                        </span> <span class="text-muted text-xs block">
                                            CONECTA CARGA
                                            <b class="caret"></b></span> </span>
                                </a>

                            </div>
                            <div class="logo-element">
                                CONECTA SOFTWARE Y PROCESOS SAS
                            </div>
                        </li>
                        <li>
                            <a href="dashboard"><i class="fa fa-tachometer-alt"></i> <span class="nav-label">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="clients"><i class="fa fa-users"></i> <span class="nav-label">Clients</span></a>
                        </li>
                        <li>
                            <a href="suppliers"><i class="fa fa-question"></i> <span class="nav-label">Suppliers</span></a>
                        </li>

                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>                            
                        </div>
                        <ul class="nav navbar-top-links navbar-left">
                            <div id="pagina_activa">

                            </div> 
                        </ul>

                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome to CONECTA-ADMIN.</span>
                            </li>


                            <li class="dropdown">
                                <a id="inicio_count_notif" class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">

                                </a>
                                <ul id="inicio_notificaciones" class="dropdown-menu dropdown-messages">

                                </ul>
                            </li>
                            <li>
                                <a href="sesion.php">
                                    <i class="fas fa-sign-out-alt"></i> Log out
                                </a>
                            </li>

                        </ul>

                    </nav>
                </div> 
                <div class="wrapper wrapper-content animated fadeInRight"> 
                    <div class="row">
                        
                        @yield("content")
                        
                        
                    </div> 
                </div>
                <div class="footer">
                    <div class="pull-right">
                        <a target="_blank" href="https://conectacarga.co">www.conectacarga.co</a>
                    </div>
                    <div>
                        <strong>Copyright</strong> CONECTA CARGA - 2020
                    </div>

                </div>
            </div> 
        </div> 

       
    </body>    