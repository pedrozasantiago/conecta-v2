 <!-- Mainly scripts -->
        <script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>


        <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

        <!-- Flot -->
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>

        <!-- Peity -->
        <script src="{{ asset('assets/js/plugins/peity/jquery.peity.min.js') }}"></script>
        <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{ asset('assets/js/inspinia.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

        <!-- jQuery UI -->
        <script src="{{ asset('assets/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

        <!-- GITTER -->
        <script src="{{ asset('assets/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

        <!-- Sparkline -->
        <script src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

        <!-- Sparkline demo data  -->
        <script src="{{ asset('assets/js/demo/sparkline-demo.js') }}"></script>

        <!-- ChartJS-->
        <script src="{{ asset('assets/js/plugins/chartJs/Chart.min.js') }}"></script>

        <!-- Toastr -->
        <script src="{{ asset('assets/js/plugins/toastr/toastr.min.js') }}"></script>

        <!-- Steps js -->
        <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>

        <!-- Ladda -->
        <script src="{{ asset('assets/js/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/ladda/ladda.jquery.min.js') }}"></script>

        <!-- FooTable -->
        <script src="{{ asset('assets/js/plugins/footable/footable.all.min.js') }}"></script>

        <!-- Tinymce -->
        <script src="{{ asset('assets/tinymce/tinymce.min.js') }}"></script>

        <!-- Data picker -->
        <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

        <script src="{{ asset('assets/js.cookie.js') }}"></script>

        <!-- Full Calendar -->
        <script src="{{ asset('assets/js/plugins/fullcalendar/moment.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script> 

        
        <!--<script src="//momentjs.com/downloads/moment.min.js"></script>-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
         <!--<script src="calendarioX/js/fullcalendar.min.js"></script>--> 
        
            <script src="assets/fine-uploader/fine-uploader.js"></script> -->
        <script src="{{ asset('assets/app/js/funciones.js') }}"></script>

        <!--Leer documentacion de esta libreria-->
        <!--<script src="../comercial/assets/js/plugins/validate/jquery.validate.min.js"></script>-->

        <!-- Idle Timer plugin -->
        <script src="{{ asset('assets/js/plugins/idle-timer/idle-timer.min.js') }}"></script>