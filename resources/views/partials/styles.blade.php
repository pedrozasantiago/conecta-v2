
 <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <!-- Full Calendar -->
        <link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/plugins/fullcalendar/fullcalendar.css') }}" rel='stylesheet' media='print'> 


        <!-- Ladda style -->
        <link href="{{ asset('assets/css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

        <!-- Jquery steps -->
        <link href="{{ asset('assets/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

        <!-- Toastr style -->
        <link href="{{ asset('assets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


        <!-- FooTable -->
        <link href="{{ asset('assets/css/plugins/footable/footable.core.css') }}" rel="stylesheet">

        <!-- Gritter -->
        <link href="{{ asset('assets/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/estilos.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
        
        <!--                <link rel="stylesheet" href="calendarioX/css/fullcalendar.min.css">
                        <link rel="stylesheet" href="calendarioX/css/fullcalendar.print.css"> -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        
        <!--     <link href="assets/fine-uploader/fine-uploader-new.css" rel="stylesheet"> 