@extends('pos.layouts.blank')

@section('site_title')
  {{ trans('auth.login-title') }}
@endsection

@section('content')
<section class="flexbox-container">

  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-sm-12 col-md-4 box-shadow-2 p-0">
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">

        <div class="card-header border-0">
          <div class="card-title text-center">
              <img src="{{ (PublikBase::get_option('logo_login')) ? PublikBase::get_s3_image(PublikBase::get_option('logo_login'), false, 'images/logos') : asset('images/logo-login.png') }}" class="img-fluid">
          </div>
        </div>        

        <div class="card-body">
          <div class="card-block">

            @include('messages')

            <form class="form-horizontal" method="POST" action="{{ route('login') }}">

              {{ csrf_field() }}

              <div class="form-group mb-0">
                <input name="username" type="text" class="form-control vd-input" id="username" placeholder="{{ trans('auth.username') }}" value="{{ old('username') }}" required autofocus>
              </div>              

              <div class="form-group">
                <input type="password" class="form-control vd-input" id="password" name="password" 
                placeholder="{{ trans('auth.password') }}" required>
              </div>

              <button type="submit" class="vd-button btn btn-success btn-lg btn-block"><i class="ft-unlock"></i> {{ trans('auth.login') }}</button>

            </form>
          </div>
        </div>

        <div class="card-footer">
          <div class="item-forgot">
            <p class="float-sm-left text-xs-center m-0"><a href="{{ route('password.request') }}" class="card-link">{{ trans('auth.forgot-password') }}</a></p>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
@endsection