@extends('pos.layouts.blank')

@section('site_title')
  {{ trans('auth.forgot-password-title') }}
@endsection

@section('content')
<section class="flexbox-container">

  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-md-4 col-10 box-shadow-2 p-0">
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">

        <div class="card-header border-0">
          <div class="card-title text-center">
              <img src="{{ (PublikBase::get_option('logo_login')) ? PublikBase::get_s3_image(PublikBase::get_option('logo_login'), false, 'images/logos') : asset('images/logo-login.png') }}">
          </div>
        </div>        

        <div class="card-body">
          <div class="card-block">

            @include('messages')

            <p>{{ trans('auth.forgot-password-desc') }}</p>

            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">

              {{ csrf_field() }}

              <div class="form-group">
                <input name="email" type="text" class="form-control vd-input" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
              </div>

              <button type="submit" class="vd-button btn btn-success btn-lg btn-block m-0">{{ trans('auth.send-forgot-password') }}</button>

            </form>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
@endsection