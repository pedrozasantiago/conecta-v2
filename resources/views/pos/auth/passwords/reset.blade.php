@extends('pos.layouts.blank')

@section('site_title')
  {{ trans('auth.reset-password-title') }}
@endsection

@section('content')
<section class="flexbox-container">

  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-md-4 col-10 box-shadow-2 p-0">
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">

        <div class="card-header border-0">
          <div class="card-title text-center">
              <img src="{{ (PublikBase::get_option('logo_login')) ? PublikBase::get_s3_image(PublikBase::get_option('logo_login'), false, 'images/logos') : asset('images/logo-login.png') }}">
          </div>
        </div>        

        <div class="card-body">
          <div class="card-block">

            @include('messages')

            <p>{{ trans('auth.reset-password-desc') }}</p>

            <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">

              {{ csrf_field() }}

              <input type="hidden" name="token" value="{{ $token }}">

              <div class="form-group">
                <input name="email" type="text" class="form-control vd-input" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
              </div>

              <div class="form-group">
                <input type="password" class="form-control vd-input" id="password" name="password" 
                placeholder="{{ trans('auth.password') }}" required>
              </div>

              <div class="form-group">
                <input type="password" class="form-control vd-input" id="password-confirm" name="password_confirmation" 
                placeholder="{{ trans('auth.password-confirmation') }}" required>
              </div>

              <button type="submit" class="vd-button btn btn-success btn-lg btn-block m-0">{{ trans('auth.send-reset-password') }}</button>

            </form>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
@endsection