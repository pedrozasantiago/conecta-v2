<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>@yield('site_title')</title>
	<link rel="manifest" href="{{{ URL::asset('manifest.json') }}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico') }}}">
	@include('pos.partials.styles')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column {{ get_login_bg() }} blank-page blank-page">

	<div class="app-content content container-fluid">
		<div class="content-wrapper">

			<div class="content-header row"></div>

			<div class="content-body">
				@yield('content')
			</div>
		</div>
	</div>

	@include('pos.partials.scripts')

</body>
</html>