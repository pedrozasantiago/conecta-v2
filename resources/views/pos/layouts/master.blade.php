<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>@yield('title')</title>
	<link rel="manifest" href="{{{ URL::asset('manifest.json') }}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
	@include('pos.partials.styles')
</head>

<body class="vertical-layout vertical-compact-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">

	@include('pos.layouts.nav')

	@include('pos.layouts.sidebar')

	<div class="app-content content">

		<div class="vd-section content-header-title">
			<h1>@yield('title')</h1>
		</div>

		<div class="vd-section vd-section-secondary action-bar">
			<div class="content-header row">
				<div class="content-header-left col-md-6 col-xs-12 align-middle">				
					<span class="head-desc-text">@yield('description')</span>
				</div>
				<div class="content-header-right col-md-6 col-xs-12">
					@yield('buttons')
				</div>
			</div>
		</div>

		@if(View::hasSection('filters'))
		<div class="vd-section vd-section-tertiary filters-content">
			@include('messages')
			@yield('filters')
		</div>
		@else
			@include('messages')
		@endif


    	<div class="content-wrapper">
			<div class="content-body">
				@yield('content')
			</div>
		</div>
	</div>

	@include('pos.partials.scripts')

	@include('pos.partials.modal_crud')

</body>
</html>