<!DOCTYPE html>
<html lang="es">
<head>
	<title>@yield('modal_title')</title>
	@yield('modal_style')
	@yield('modal_script')
	<script type="text/javascript">
		@yield('inline_modal_js')
	</script>
</head>
<body>
	@yield('modal_content')
</body>
</html>