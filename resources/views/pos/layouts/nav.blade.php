<!-- fixed-top -->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark navbar-brand-left">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-brand" href="{{ url('app/dashboard') }}">
            <img class="brand-logo" alt="Winkel POS" src="{{ asset('images/logo.png') }}">
          </a>
        </li>
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item d-md-none">
          <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
        </li>
      </ul>
    </div>
    <div class="navbar-container content">
      <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left"></ul>
          <ul class="nav navbar-nav float-right">
            @include('pos.widgets.stores_list')
            @include('pos.widgets.registers_list')
            @include('pos.widgets.notifications')
            @include('pos.widgets.user_dropdown')
          </ul>
        </div>
      </div>
  </div>
</nav>
<!--/ fixed-top-->