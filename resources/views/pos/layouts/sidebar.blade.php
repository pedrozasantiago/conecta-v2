<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
	<div class="main-menu-content">
    	<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
			@foreach($sidebar_tenant as $item)

				@php
          			$url = $item->url;
					$perm = ($item->permission) ? Auth::user()->can($item->permission) : 1;
					$options = json_decode($item->params);
				@endphp

				@if($perm)
				<li class=" nav-item {{{ (Request::is($url) ? 'active' : '') }}}">

					<a href="{{ url($url) }}">
						@if(isset($options->icon))
						<i class="{{$options->icon}}" style="color: #{{ ($options->color) ?? '000' }}"></i>
						@endif
						<span class="menu-title">{{ trans('sidebar.' . $item->name ) }}</span>
					</a>
			
					@if(count($item->childs)>0)
					<ul class="menu-content">
              			@foreach($item->childs as $submenu)

                			@php
                  				$url = $submenu->url;
								$perm = ($submenu->permission) ? Auth::user()->can($submenu->permission) : 1;
                			@endphp

                			@if($perm)
							<li class="{{{ (Request::is($url) ? 'active' : '') }}}">
								<a href="{{ url($url) }}" class="menu-item">{{ trans('sidebar.' . $submenu->name ) }}</a>
							</li>
							@endif

              			@endforeach
          			</ul>
					@endif
				</li>
				@endif
			@endforeach
		</ul>
	</div>
</div>