<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="api-base-url" content="{{ url('/') }}" />
	<title>@yield('title')</title>
	<link rel="manifest" href="{{{ URL::asset('manifest.json') }}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{{ URL::asset('favicon.ico')}}}">
	@include('pos.partials.vue_styles')
</head>

<body class="vertical-layout vertical-compact-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">

	@include('pos.layouts.nav')

	@include('pos.layouts.sidebar')

	<div  id="app" class="app-content content">

		@yield('header')

    	<div class="content-wrapper">
			<div class="content-body">
				@yield('content')
			</div>
		</div>
	</div>

	@include('pos.partials.vue_scripts')

</body>
</html>