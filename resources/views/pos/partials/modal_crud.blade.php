<!-- Modal crud -->
<div class="modal fade text-left" id="modal_crud" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h2 class="modal-title">{{ trans('base.loading_text') }}</h2>
				<button type="button" class="close" data-dismiss="modal" aria-label="{{ trans('base.close') }}">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="m-t">
					<i class="fa fa-circle-notch fa-spin fa-fw"></i>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn vd-button btn-secondary" data-dismiss="modal">{{ trans('base.close') }}</button>
				<button type="button" class="btn vd-button btn-success" id="send_form">{{ trans('base.save') }}</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal crud -->