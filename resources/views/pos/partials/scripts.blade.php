{!! html::script( mix('js/web-app.js') ) !!}

@yield('load_script')

<script type="text/javascript">
	{!! get_global_json() !!}
	@yield('inline_script')	
</script>