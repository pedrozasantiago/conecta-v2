<script>    

    window.App = {
        _root_: '{{ url('/') }}',
        assets_url: '{{ asset('/') }}'
    };

    @yield('global_script')
</script>

{!! html::script( mix('js/web-app.js') ) !!}
{!! html::script( mix('js/app.js') ) !!}

@yield('load_script')

<script type="text/javascript">
    {!! get_global_json() !!}
    
    @yield('inline_script')

	// Service Worker
	if ('serviceWorker' in navigator && 'PushManager' in window) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('/service-worker.js').then(function(registration) {                
                console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
                console.log('ServiceWorker registration failed: ', err);
            });
        });
    }
    
</script>