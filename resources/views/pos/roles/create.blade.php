@extends('pos.layouts.modals')

@section('inline_modal_js')
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans('roles.create') }}
@endsection

@section('modal_content')
  
{{ Form::open(array('url' => 'app/roles', 'id' => 'roles_submit')) }}

    <div class="form-group">
        {{ Form::label('display_name', trans('roles.display_name'), ['class' => 'vd-label']) }}
        {{ Form::text('display_name', Request::old('display_name'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', trans('roles.desc'), ['class' => 'vd-label']) }}
        {{ Form::text('description', Request::old('description'), array('class' => 'form-control vd-input')) }}
    </div>

{{ Form::close() }}

@stop