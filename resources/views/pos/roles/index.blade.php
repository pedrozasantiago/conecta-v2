@extends('pos.layouts.master')

@section('inline_script')
	{!! $config_pos_roles !!}
@endsection

@section('title')
	{{ trans('sidebar.roles') }}
@endsection

@section('description')
	{{ trans('roles.description-header') }} <a data-toggle="collapse" href="#collapseHelp" role="button" aria-expanded="false" aria-controls="collapseHelp">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">
	{!! PublikButtons::create_modal('roles', 'app/roles', 'create', '', 'md') !!}
</div>
@endsection

@section('filters')
	@include('pos.roles.filters')
@endsection

@section('content')
<table id="roles" class="table vd-table">
	<thead>
		<tr>
			<th>{{ trans('roles.display_name') }}</th>
			<th>{{ trans('roles.desc') }}</th>
			<th>{{ trans('base.actions') }}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop