@extends('pos.layouts.crud')

@section('title')
  {{ trans('roles.permissions') }} {{ $role->display_name }}
@endsection

@section('description')
  {{ trans('roles.perms-description-header') }} <a href="#">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">
  <a class="vd-button btn btn-secondary" href="{{ url('app/roles') }}">{{ trans('base.back') }}</a>
</div>
@endsection


@section('content')
<div class="content-role-perms">

  {{ Form::hidden('role_id', $role->uuid, ['id' => 'role_id']) }}

  @foreach($permissions as $key => $permission)

  <div class="content-perm">
    <div class="row">

      <div class="col-md-3 col-xs-12">
          <div class="perms-header">
            @if($permission->icon)
              <i class="fa {{ $permission->icon }}"></i>
            @endif
            {{ trans('permissions.'.$permission->name) }}
          </div>
      </div>

      <div class="col-xs-12 col-md-9">
  
      @foreach($permission->childs as $key => $child)

        <div class="form-group">

          @php
            if($role->hasPermission($child->name) == 1){
              $check = true;
            }else{
              $check = false;
            }
          @endphp

          <div class="custom-control custom-checkbox">
              {!! Form::checkbox('perm[]', $child->uuid, $check, ['id' => 'perm_val_'.$child->uuid, 'class' => 'custom-control-input update_perm']) !!}
              <label class="custom-control-label" for="perm_val_{{ $child->uuid }}">{{ trans('permissions.'.$child->name) }}</label>
          </div>
          <div class="perms-description">{{ trans('permissions.'.$child->name.'-desc') }}</div>

      </div>

      @endforeach

      </div>


    </div>
  </div>
  @endforeach

</div>
@stop