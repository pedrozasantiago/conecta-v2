@extends('pos.layouts.modals')

@section('inline_modal_js')
    $('.select2').select2();
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans($title.'.edit') }}
@endsection

@section('modal_content')

{!! Form::model($user, [
    'method' => 'PUT',
    'id' => $title.'_submit',
    'route' => [$title.'.update', $user]
]) !!}


    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('first_name', trans($title.'.first_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::text('first_name', Request::old('first_name'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('last_name', trans($title.'.last_name').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::text('last_name', Request::old('last_name'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('username', trans($title.'.username').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::text('username', Request::old('username'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('email', trans($title.'.email').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::email('email', Request::old('email'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('role_id', trans($title.'.role').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::select('role_id', array('' => trans($title.'.select_role')) + $roles, $user->roles->first()->id, array('class' => 'form-control vd-select')) }}
    </div>

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('store_id', trans($title.'.stores').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::select('store_id[]', $stores, $store_selected, array('class' => 'form-control vd-select select2', 'multiple', 'id' => 'store_id')) }}
    </div>

{{ Form::close() }}

@stop