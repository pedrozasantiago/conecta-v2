<div class="collapse" id="collapseHelp">

	<div class="row">
		<div class="col-6">
			{!! trans($title.'.help-desc') !!}
		</div>

		<div class="col-6">
			<img src="{{ asset('assets/images/product-brand.png') }}">
		</div>

	</div>

</div>

<div class="filters-inputs">
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="form-group">
        		{{ Form::label('full_name', trans($title.'.full-name-search'), ['class' => 'vd-label']) }}
        		{{ Form::text('full_name', Request::old('full_name'), array('class' => 'form-control vd-input search-input-text', 'data-column' => 0, 'placeholder' => trans($title.'.full-name-search-desc'))) }}
    		</div>
		</div>

	</div>
</div>