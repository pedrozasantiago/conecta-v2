@extends('pos.layouts.master')

@section('inline_script')
	@if($title === 'users')
		{!! $config_pos_users !!}
	@else
		{!! $config_pos_sellers !!}
	@endif
@endsection

@section('title')
	{{ trans('sidebar.'.$title) }}
@endsection

@section('description')
	{{ trans($title.'.description-header') }} <a data-toggle="collapse" href="#collapseHelp" role="button" aria-expanded="false" aria-controls="collapseHelp">{{ trans('base.need_help') }}</a>
@endsection

@section('buttons')
<div class="float-md-right">

	<a class="vd-button btn btn-secondary" href="{{ url('app/'.$title.'/import') }}"><i class="fa fa-check-square-o"></i> {{ trans($title.'.import') }}</a>

	{!! PublikButtons::create_modal($title, 'app/'.$title, 'create', '', 'md') !!}

</div>
@endsection

@section('filters')
	@include('pos.users.filters')
@endsection

@section('content')  
<table id="{{ $title }}" class="table vd-table dt-responsive table-full-width">
	<thead>
		<tr>
			<th>{{ trans($title.'.name') }}</th>
			<th>{{ trans($title.'.username') }}</th>
			<th>{{ trans($title.'.email') }}</th>
			<th>{{ trans($title.'.role') }}</th>
			<th>{{ trans('base.actions') }}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop