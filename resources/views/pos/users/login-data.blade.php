@extends('pos.layouts.modals')

@section('inline_modal_js')
    {!! get_submit_vars('POST', 'Simple') !!}
@endsection

@section('modal_title')
    {{ trans('users.change-login-data') }}
@endsection


@section('modal_content')

{!! Form::model($user, [
    'method' => 'PUT',
    'id' => $title.'_submit',
    'route' => ['users.update_login_data', $user->uuid]
]) !!}

    <div class="form-group">
        {!! htmlspecialchars_decode(Form::label('username', trans('users.username').'<span class="required">*</span>', array('class' => 'control-label vd-label'))) !!}
        {{ Form::text('username', Request::old('username'), array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', trans('users.password'), array('class' => 'vd-label')) }}
        {{ Form::password('password', array('class' => 'form-control vd-input')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', trans('users.password_confirmation'), array('class' => 'vd-label')) }}
        {{ Form::password('password_confirmation', array('class' => 'form-control vd-input')) }}
    </div>

{{ Form::close() }}

@stop