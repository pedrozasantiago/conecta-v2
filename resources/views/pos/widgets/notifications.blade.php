<li class="dropdown dropdown-notification nav-item">
	<a href="#" class="nav-link nav-link-label" data-toggle="dropdown">
		<i class="ficon ft-bell"></i>
		@if(count(Auth::user()->unreadNotifications) > 0)
		<span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">{{ count(Auth::user()->unreadNotifications) }}</span>
		@endif
	</a>
	<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
		<li class="dropdown-menu-header">
			<h6 class="dropdown-header m-0">
				<span class="grey darken-2">{{ trans('base.notifications') }}</span>
			</h6>
			<span class="notification-tag badge badge-default badge-danger float-right m-0">{{ count(Auth::user()->unreadNotifications) }} {{ trans('base.notifications_news') }}</span>
		</li>
		<li class="scrollable-container media-list w-100">
			@foreach(Auth::user()->unreadNotifications as $notification)
			<a href="{{ url($notification->data['url']) }}">
				<div class="media">
					@if($notification->data['icon'])
					<div class="media-left align-self-center">
						<i class="{{ $notification->data['icon'] }} icon-bg-circle {{ $notification->data['bg'] }}"></i>
					</div>
					@endif
					<div class="media-body">
						<h6 class="media-heading {{ $notification->data['color'] }}">{{ trans($notification->data['title']) }}</h6>

						<p class="notification-text font-small-3 text-muted">{{ trans($notification->data['desc']) }}</p>
						<small>
                        	<time class="media-meta text-muted">{{ $notification->created_at->diffForHumans() }}</time>
						</small>
					</div>
				</div>
			</a>
			@endforeach
		</li>
		<li class="dropdown-menu-footer">
			<a class="dropdown-item text-muted text-center" href="{{ url('admin/notifications') }}">{{ trans('base.read_all_notifications') }}</a>
		</li>
	</ul>
</li>