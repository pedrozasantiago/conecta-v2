@if(Auth::check())
@php
	$current = PublikBase::current_register();
	$registers = PublikBase::users_registers('Change');
@endphp
@if($current)
<li class="dropdown dropdown-register nav-item">
	<a href="#" data-toggle="dropdown" class="nav-link dropdown-register-link">
		<span class="register-avatar"><i class="fa fa-money-check-alt"></i></span>
		<span class="register-name">{{ $current->register->name }}</span>
	</a>
	@if($registers)
	<div class="dropdown-menu dropdown-menu-right">
		@foreach($registers as $key => $register)

			<a href="{{ route('set_register') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('set-register-{{ $key }}').submit();"><i class="fa fa-money-check-alt"></i> {{ $register }}</a>

			<form id="set-register-{{ $key }}" action="{{ route('set_register') }}" method="POST" style="display: none;">
				<input type="hidden" name="redirect" value="{{ Request::url() }}">
				<input type="hidden" name="register_id" value="{{ $key }}">
				{{ csrf_field() }}
			</form>

			@if($register != end($registers))
			<div class="dropdown-divider"></div>
			@endif
			
		@endforeach
	</div>
	@endif
</li>
@endif
@endif