@if(Auth::check())
@php
	$user = Auth::user();
	$current = $user->current_store();
	//$stores = PublikBase::users_stores();
	$stores = $user->stores_change()->get();
@endphp
@if($current)
<li class="dropdown dropdown-store nav-item">
	<a href="#" data-toggle="dropdown" class="nav-link dropdown-store-link">
		<span class="store-avatar"><i class="fa fa-store"></i></span>
		<span class="store-name">{{ $current->store->name }}</span>
	</a>
	@if($stores->count() > 0)
	<div class="dropdown-menu dropdown-menu-right">
		@foreach($stores as $key => $store)			

			<a href="{{ route('set_store') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('set-store-{{ $store->store_id }}').submit();"><i class="fa fa-store"></i> {{ $store->store->name }}</a>

			<form id="set-store-{{ $store->store_id }}" action="{{ route('set_store') }}" method="POST" style="display: none;">
				<input type="hidden" name="redirect" value="{{ Request::url() }}">
				<input type="hidden" name="store_id" value="{{ $store->store_id }}">
				{{ csrf_field() }}
			</form>

			@if($store != end($stores))
			<div class="dropdown-divider"></div>
			@endif
			
		@endforeach
	</div>
	@endif
</li>
@endif
@endif