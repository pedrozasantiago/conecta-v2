@if(Auth::check())
<li class="dropdown dropdown-user nav-item">
	<a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
		<span class="avatar avatar-online">
			@php
				if(Auth::user()->image){
					$profile = PublikBase::get_s3_image(Auth::user()->image, false, 'images/profile');
				}else{
					$profile = asset('images/user-placeholder.jpg');
				}
			@endphp
			<img src="{{ $profile }}" alt="{{ Auth::user()->first_name }}"><i></i></span>
			<span class="user-name">{{ Auth::user()->first_name }}</span>
		</a>
		<div class="dropdown-menu dropdown-menu-right">
			<a href="{{ url('app/profile') }}" class="dropdown-item"><i class="ft-user"></i> Editar Perfil</a>
			<div class="dropdown-divider"></div>
			<a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ft-power"></i> Salir</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
		</div>
	</li>
@endif