<?php
 use App\Http\Controllers\Admin\ClientsController;
 use App\Models\System\Customer;
 use Illuminate\Support\Facades\Route;
/* * ******************************* */
/* * ********* CLIENTES *********** */
/* * ****************************** */

// Route::get('clients/grid', 'ClientsController@grid')->name('admin.clients.grid');



Route::resource('faqs', 'FaqsController', ['as' => 'admin']);
Route::post('faqs/grid', 'FaqsController@grid_ppal');
Route::post('faqs/response', 'FaqsController@response');
Route::post('faqs/cargar_img', 'FaqsController@uploadImages');


Route::get('clients/users', function () {
    return view("admin.clients.users");
});

Route::get('clients/configs', function () {
    return view("admin.clients.configs");
});

Route::resource('clients', 'ClientsController');

Route::get('clients/getUsers/{client}', [ClientsController::class, 'getUsers']);

Route::get('clients/getConfigs/{client}', [ClientsController::class, 'getConfigs']);


Route::post('clients/grid_ppal', 'ClientsController@grid_ppal');
Route::post('clients/clientes_frm', 'ClientsController@clientes_frm');
//	Route::post('clients/grabar_cliente', 'ClientsController@grabar_cliente');



	// Resources Clientes
