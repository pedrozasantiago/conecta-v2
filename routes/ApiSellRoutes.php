<?php

Route::group(['middleware' => ['json.response', 'tenancy.enforce'], 'namespace' => 'Api', 'prefix' => 'api'], function () {

	// Lista de tiendas
	Route::get('/stores', 'StoresController@index');

	// Lista de clientes
	Route::get('/clients', 'ClientsController@index');

	// Lista de categorias
	Route::get('/categories-list/{category?}', 'CategoriesController@index');

	// Lista de productos
	Route::get('/products-list', 'ProductsController@index');

	// Lista de metodos de pago
	Route::get('/payment-methods', 'PaymentMethodsController@index');

});