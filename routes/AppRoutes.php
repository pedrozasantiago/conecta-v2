<?php


Route::get('test', function () {
    return 'hola';
})->middleware('tenancy.enforce');

// ruta del dashboard
Route::get('/{file}', 'HomeController@index')->name('app.index');

Route::get('/login', 'LoginController@index')->name('app.login');



//Route::group(['middleware' => ['tenancy.enforce']], function(){
//
//	// Dashboard
//	Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('app.dashboard');
//
//	// vista de cambiar usuario y contraseña
//	Route::get('/users/login-data/{type}/{user}', 'UsersController@login_data')->name('users.login_data');
//	
//	// cambiar usuario y contraseña
//	Route::put('/users/login-data/{user}', 'UsersController@update_login_data')->name('users.update_login_data');
//
//	// Resources Usuarios
//	Route::resource('users', 'UsersController');
//
//
//
//	/**********************************/
//	/************ ROLES ************/
//	/*********************************/
//
//	// lista de los permisos de un rol
//	Route::get('/roles/{role}/permissions', 'RolesController@permissions')->name('roles.permissions');
//
//	// Modificar permisos del rol
//	Route::post('/roles/perm', 'RolesController@perm')->name('roles.edit_perm');
//
//	// Resources Roles
//	Route::resource('roles', 'RolesController');
//
//});