<?php

//Logged in admin cannot access or send requests these pages
Route::group(['namespace' => 'Admin\Auth', 'middleware' => ['admin_guest', 'admin_domain']], function() {

	// registrar usuario
	Route::get('admin/register', 'RegisterController@showRegistrationForm')
	->name('admin.register');

	Route::post('admin/register', 'RegisterController@register');

	// login de administradores
	Route::get('admin/login', 'LoginController@showLoginForm')
	->name('admin.login');

	Route::post('admin/login', 'LoginController@login')
	->name('admin.login.submit');

	//Password reset routes
	Route::get('admin/password/reset', 'ForgotPasswordController@showLinkRequestForm')
	->name('admin.password.request');

	Route::post('admin/password/email', 'ForgotPasswordController@sendResetLinkEmail')
	->name('admin.password.email');

	Route::get('admin/password/reset/{token}', 'ResetPasswordController@showResetForm')
	->name('admin.password.reset');

	Route::post('admin/password/reset', 'ResetPasswordController@reset');
});

// rutas protegidas con login
Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth', 'middleware' => ['admin_auth']], function() {

	// cerrar session del admin
	Route::post('logout', 'LoginController@logout')
	->name('admin.logout');

	// eliminar imagen del perfil
	Route::get('profile/remove-image', 'ProfileController@remove_image')->name('admin.profile.remove_image');

	// perfil del usuario
	Route::resource('profile', 'ProfileController', [
		'as' => 'admin'
	]);
	
});



