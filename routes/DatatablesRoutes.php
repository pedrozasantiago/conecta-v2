<?php


Route::group(['middleware' => ['auth', 'tenancy.enforce', 'store']], function(){


	/**********************************/
	/******* DATATABLES ROUTE ********/
	/*********************************/


	/* Datatables Clientes */
	Route::post('/getClients', 'ClientsController@getClients')
	->name('clients.datatables');

	/* Datatables Grupos de Clientes */
	Route::post('/getClientsGroups', 'ClientsGroupsController@getClientsGroups')
	->name('clients-groups.datatables');

	/* Datatables Proveedores */
	Route::post('/getSuppliers', 'SuppliersController@getSuppliers')
	->name('suppliers.datatables');

	/* Datatables Categorias */
	Route::post('/getProductsCategories', 'ProductsCategoriesController@getProductsCategories')
	->name('products-categories.datatables');

	/* Datatables Categorias de gastos */
	Route::post('/getExpensesCategories', 'ExpensesCategoriesController@getExpensesCategories')
	->name('expenses-categories.datatables');

	/* Datatables metodos de pagos */
	Route::post('/getPaymentMethods', 'PaymentMethodsController@getPaymentMethods')
	->name('payment-methods.datatables');

	/* Datatables Impuestos */
	Route::post('/getTaxes', 'TaxesController@getTaxes')
	->name('taxes.datatables');

	/* Datatables Unidades de medida */
	Route::post('/getUnits', 'UnitsController@getUnits')
	->name('units.datatables');

	/* Datatables Marcas */
	Route::post('/getBrands', 'BrandsController@getBrands')
	->name('brands.datatables');

	/* Datatables Tiendas */
	Route::post('/getStores', 'StoresController@getStores')
	->name('stores.datatables');

	/* Datatables Resoluciones de facturas */
	Route::post('/getResolutions', 'ResolutionsController@getResolutions')
	->name('resolutions.datatables');
	
	/* Datatables Gastos */
	Route::post('/getExpenses', 'ExpensesController@getExpenses')
	->name('expenses.datatables');

	/* Datatables Cotizaciones */
	//Route::post('/getEstimates', 'EstimatesController@getEstimates')
	//->name('estimates.datatables');

	/* Datatables Ventas */
	Route::post('/getSales', 'SalesController@getSales')
	->name('sales.datatables');

	/* Datatables Compras */
	Route::post('/getPurchases', 'PurchasesController@getPurchases')
	->name('purchases.datatables');

	/* Datatables Ordenes de Compra */
	//Route::post('/getOrders', 'OrdersController@getOrders')
	//->name('orders.datatables');

	/* Datatables Pagos */
	Route::post('/getPayments', 'PaymentsController@getPayments')
	->name('payments.datatables');

	/* Datatables Compras */
	Route::post('/getPurchases', 'PurchasesController@getPurchases')
	->name('purchases.datatables');

	/* Datatables Productos */
	Route::post('/getProducts', 'ProductsController@getProducts')
	->name('products.datatables');

	/* Datatables Productos */
	//Route::post('/getPriceBooks', 'PriceBooksController@getPriceBooks')
	//->name('price-books.datatables');

	/* Datatables Atributos */
	Route::post('/getAttributes', 'AttributesController@getAttributes')
	->name('attributes.datatables');

	/* Datatables Atributos */
	Route::post('/getAttributesValues', 'AttributesValuesController@getAttributesValues')
	->name('attributes-values.datatables');

	/* Datatables Atributos */
	Route::post('/getMoves', 'MovesController@getMoves')
	->name('moves.datatables');

	/* Datatables Ingreso de Existencias */
	Route::post('/getStockEntries', 'StockEntriesController@getStockEntries')
	->name('stock-entries.datatables');


	/* Datatables Ingreso de Existencias */
	Route::post('/getStockExits', 'StockExitsController@getStockExits')
	->name('stock-exits.datatables');

	/* Datatables Usuarios */
	Route::post('/getUsers', 'UsersController@getUsers')
	->name('users.datatables');

	/* Datatables Vendedores */
	Route::post('/getSellers', 'SellersController@getSellers')
	->name('sellers.datatables');

	/* Datatables Roles */
	Route::post('/getRoles', 'RolesController@getRoles')
	->name('roles.datatables');

	/* Datatables Ingresos/Egresos */
	Route::post('/getCashManagements', 'CashManagementController@getCashManagements')
	->name('cash-management.datatables');

});