<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// login de la aplicacion movil
Route::group(['middleware' => ['json.response', 'tenancy.enforce'], 'namespace' => 'Api\Restful', 'prefix' => 'v2'], function () {
    
    //Route::post('login', 'LoginController@login')->name('api.login');

});

// api de la aplicacion movil
Route::group(['middleware' => ['json.response', 'auth:api', 'tenancy.enforce'], 'namespace' => 'Api\Restful', 'prefix' => 'v2'], function () {

    //Route::get('logout', 'LoginController@logout')->name('api.logout');
    //Route::get('profile', 'LoginController@profile')->name('api.profile');
    
    //Route::apiResource('users', 'UsersController');
    
});