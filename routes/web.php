<?php

//dd('hola mundo');

// Route::get('/admin', "AdminController@index");

// Route::get('/admin/clients', "AdminController@clientes");

// Rutas del Tenant Center

// ruta del dashboard


Route::get('/whatsapp/{number}', 'WhatsappController@index');
Route::get('/whatsapp/waba/{id}', 'WhatsappController@getPhoneNumbers');

Route::get('/', function(){
    
    return redirect('/admin/login');
    
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin_auth']], function () {
    require(__DIR__ . DIRECTORY_SEPARATOR . "AdminRoutes.php");
});


Route::group(['prefix' => 'app/crm', 'namespace' => 'App'], function () {
    require(__DIR__ . DIRECTORY_SEPARATOR . "AppRoutes.php");
});


//Route::group([
//	'prefix' => 'operative',
//	'namespace' => 'Operative'
//], function () {
//	require(__DIR__ . DIRECTORY_SEPARATOR . "OperativeRoutes.php");
//}
//);


// Rutas de Autenticacion
require(__DIR__ . DIRECTORY_SEPARATOR . "AuthRoutes.php");

// Rutas del POS
//Route::group(['prefix' => 'app', 'namespace' => 'App'], function () {
//	require(__DIR__ . DIRECTORY_SEPARATOR . "AppRoutes.php");
//});
