let mix = require('laravel-mix');

// Web App Styles
mix.styles([
   //'resources/assets/app/css/bootstrap.min.css',
   'resources/assets/app/css/bootstrap/bootstrap.min.css',
   'resources/assets/app/css/extensions/pace.css',
   'resources/assets/app/css/bootstrap-extended.min.css',
   'resources/assets/app/css/colors.min.css',
   'resources/assets/app/css/components.min.css',
   'resources/assets/app/css/vertical-compact-menu.min.css',
   'resources/assets/app/css/palette-gradient.min.css',   
   'resources/assets/app/css/login-register.min.css',
   'resources/assets/app/css/error.min.css',

   // fonts   
   'resources/assets/app/fonts/google-fonts/google-fonts.css',
   'resources/assets/app/fonts/font-awesome/font-awesome.css',
   'resources/assets/app/fonts/line-awesome/line-awesome.min.css',   
   'resources/assets/app/fonts/feather/style.min.css',
   'resources/assets/app/fonts/simple-line-icons/style.min.css',

   // plugins
   'resources/assets/app/css/extensions/toastr.css',
   'resources/assets/app/css/extensions/sweetalert.css',
   'resources/assets/app/css/extensions/ui/jquery-ui.min.css',
   'resources/assets/app/css/extensions/imageuploadify/imageuploadify.min.css',
   'resources/assets/app/css/extensions/select/select2.min.css',
   'resources/assets/app/css/extensions/daterange/daterangepicker.css',
   'resources/assets/app/css/extensions/timepicker/timepicker.less',
   'resources/assets/app/css/extensions/calendars/fullcalendar.min.css',
   'resources/assets/app/css/extensions/validation/form-validation.css',
   'resources/assets/app/css/extensions/minicolors/jquery.minicolors.css',

   // datatables css
   'resources/assets/app/css/extensions/datatables/dataTables.bootstrap4.min.css',
   'resources/assets/app/css/extensions/datatables/responsive.bootstrap4.min.css',
   
   // custom
   'resources/assets/app/css/style.css'
   //'resources/assets/app/css/print/print-ticket.css'

], 'public/css/web-app.css');

// Web App Scripts
mix.scripts([
   'resources/assets/app/js/vendors.min.js',
   'resources/assets/app/js/bootstrap/bootstrap.min.js',
   'resources/assets/app/js/extensions/perfect-scrollbar/perfect-scrollbar.min.js',
   'resources/assets/app/js/extensions/unison/unison.min.js',
   'resources/assets/app/js/extensions/jquery.blockUI.js',
   'resources/assets/app/js/extensions/match-height/jquery.matchHeight-min.js',
   'resources/assets/app/js/extensions/slinky/slinky.min.js',
   'resources/assets/app/js/pace.min.js',
   'resources/assets/app/js/extensions/screenfull/screenfull.min.js',
   'resources/assets/app/js/app.min.js',
   'resources/assets/app/js/app-menu.min.js',  
   
   'resources/assets/app/js/extensions/toastr.min.js',
   'resources/assets/app/js/extensions/sweetalert.min.js',
   'resources/assets/app/js/extensions/ui/jquery-ui.min.js',
   'resources/assets/app/js/extensions/imageuploadify/imageuploadify.js',
   'resources/assets/app/js/extensions/select/select2.full.min.js',   
   'resources/assets/app/js/extensions/moment.min.js',
   'resources/assets/app/js/extensions/dateTime/moment-with-locales.min.js',
   'resources/assets/app/js/extensions/daterange/daterangepicker.js',   
   'resources/assets/app/js/extensions/timepicker/bootstrap-timepicker.js',   
   'resources/assets/app/js/extensions/fullcalendar/fullcalendar.min.js',
   'resources/assets/app/js/extensions/fullcalendar/locale-all.js',   
   'resources/assets/app/js/extensions/charts/chart.min.js',
   'resources/assets/app/js/extensions/jquery-number/jquery.number.min.js',
   'resources/assets/app/js/extensions/tinymce/tinymce.js',
   'resources/assets/app/js/extensions/minicolors/jquery.minicolors.min.js',
   
   // datatables js
   'resources/assets/app/js/extensions/datatables/jquery.dataTables.min.js',
   'resources/assets/app/js/extensions/datatables/dataTables.bootstrap4.min.js',
   'resources/assets/app/js/extensions/datatables/dataTables.responsive.min.js',
   'resources/assets/app/js/extensions/datatables/responsive.bootstrap4.min.js',

   
   


   'resources/assets/app/js/forms/select2.js',
   'resources/assets/app/js/forms/datepicker.js',
   'resources/assets/app/js/forms/imageuploadify.js',
   'resources/assets/app/js/forms/timepicker.js',
   'resources/assets/app/js/forms/calendar.js',
   'resources/assets/app/js/core/datatable.js',
   'resources/assets/app/js/forms/geography.js',
   'resources/assets/app/js/forms/role-permissions.js',
   'resources/assets/app/js/forms/admin-role-permissions.js',
   'resources/assets/app/js/forms/editor-tinymce.min.js',
   'resources/assets/app/js/forms/picker-color.js',

   // products js
   'resources/assets/app/js/forms/add-products.js',
   'resources/assets/app/js/forms/combinations-products.js',
   'resources/assets/app/js/forms/composite-products.js',

   // purchases
   'resources/assets/app/js/forms/create-purchase.js',
   'resources/assets/app/js/forms/create-stock-move.js',
   'resources/assets/app/js/forms/create-move.js',
   'resources/assets/app/js/forms/show-invoice.js',

   // custom
   'resources/assets/app/js/core/base.js',
   'resources/assets/app/js/core/submit.js',
 
], 'public/js/web-app.js');


// WebSite Styles
mix.styles([
   'resources/assets/app/css/bootstrap.min.css',   
   'resources/assets/web/css/owl.carousel.min.css',
   'resources/assets/web/css/animate.css',
   'resources/assets/web/css/reset.css',

   // fonts
   'resources/assets/app/fonts/google-fonts/google-fonts.css',
   'resources/assets/app/fonts/font-awesome/font-awesome.css',

   // custom
   'resources/assets/web/css/style.css',
   'resources/assets/web/css/custom-style.css',

], 'public/css/website.css');

// WebSite Scripts
mix.scripts([
   'resources/assets/web/js/jquery-3.2.1.min.js',
   'resources/assets/web/js/popper.min.js',
   'resources/assets/web/js/bootstrap.min.js',
   'resources/assets/web/js/plugins.js',
   'resources/assets/web/js/active.js',

], 'public/js/website.js');


// Vuejs App
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
   mix.version();
}  
